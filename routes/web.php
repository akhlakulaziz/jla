<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BusinessController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::prefix('/')->middleware(['auth'])->group(function () {
    Route::view('dashboard','dashboard')->name('dashboard');
    
    Route::post('/mail', [App\Http\Controllers\ConfigsController::class, 'mail'])->name('settings.mail');
    Route::post('users/{id}/update-password', [App\Http\Controllers\UsersController::class, 'updatePassword'])->name('update.password');
    Route::get('/activity-log', [App\Http\Controllers\ActivityLogController::class, 'index'])->name('activity-log.index');
    Route::resource('/users', App\Http\Controllers\UsersController::class);
    Route::get('users/trash/soft', [App\Http\Controllers\UsersController::class, 'trash'])->name('users.trash');
    Route::get('users/restore/{id}', [App\Http\Controllers\UsersController::class, 'restore'])->name('users.restore');
    Route::get('users/trash/restore', [App\Http\Controllers\UsersController::class, 'restoreAll'])->name('users.restoreAll');
    Route::post('users/force-delete/{id}', [App\Http\Controllers\UsersController::class, 'forceDelete'])->name('users.forceDelete');
    Route::get('download/data-pengguna', [App\Http\Controllers\UsersController::class, 'UserExport'])->name('UserExport');
    Route::resource('/roles', App\Http\Controllers\RolesController::class);
    Route::resource('/settings', App\Http\Controllers\ConfigsController::class);
    
    
    // LOGIN SEBAGAI USER LAIN
    Route::get('users/login-as/{id}', [App\Http\Controllers\UserAccessController::class, 'loginAs'])->name('user.login-as');

    // ============================================JLA===================================================
    Route::get('/flowchart',[App\Http\Controllers\HarianLepasController::class, 'flowchart'])->name('flowchart');
    // master hrd
    Route::resource('/master-departemen', App\Http\Controllers\MasterDepartemenController::class);
    Route::resource('/master-bagian', App\Http\Controllers\MasterBagianController::class);
    Route::resource('/harian-lepas', App\Http\Controllers\HarianLepasController::class);

    // master harian proses
    Route::resource('/master-item', App\Http\Controllers\MasterItemController::class);
    Route::resource('/master-keterangan-bahan', App\Http\Controllers\MasterKeteranganBahanController::class);
    Route::resource('/master-type-bahan', App\Http\Controllers\MasterTypeBahanController::class);
    Route::resource('/master-warna', App\Http\Controllers\MasterWarnaController::class);
    Route::resource('/master-group-kualitas', App\Http\Controllers\MasterGroupKualitasController::class);
    Route::resource('/master-kualitas', App\Http\Controllers\MasterKualitasController::class);
    Route::resource('/master-jenis-kayu', App\Http\Controllers\MasterJenisKayuController::class);
    Route::resource('/master-keterangan-proses', App\Http\Controllers\MasterKeteranganProsesController::class);
    Route::resource('/master-departemen-harian-proses', App\Http\Controllers\MasterDepartemenHarianProsesController::class);
    Route::resource('/master-unit', App\Http\Controllers\MasterUnitController::class);
    Route::resource('/master-sub-unit', App\Http\Controllers\MasterSubUnitController::class);
    Route::resource('/master-supplier', App\Http\Controllers\MasterSupplierController::class);
    Route::resource('/master-product', App\Http\Controllers\MasterProductController::class);
    Route::resource('/master-no-po', App\Http\Controllers\MasterNoPoController::class);
    Route::resource('/master-pemborong', App\Http\Controllers\MasterPemborongController::class);
    Route::resource('/master-jenis-pekerjaan', App\Http\Controllers\MasterJenisPekerjaanController::class);
    Route::resource('/master-keterangan-pekerjaan', App\Http\Controllers\MasterKeteranganPekerjaanController::class);
    Route::resource('/master-lokasi', App\Http\Controllers\MasterLokasiController::class);
    Route::resource('/master-shift', App\Http\Controllers\MasterShiftController::class);
    Route::resource('/master-tenaga-kerja', App\Http\Controllers\MasterTenagaKerjaController::class);
    Route::resource('/master-status-tenaga-kerja', App\Http\Controllers\MasterStatusTenagaKerjaController::class);
    
    //fitur harian proses
    Route::resource('/item', App\Http\Controllers\ItemController::class);
    Route::resource('/input-harian', App\Http\Controllers\InputHarianController::class);
    // Route::get('/json-input-harian',[App\Http\Controllers\InputHarianController::class, 'json_departemen'])->name('input-harian.json-departemen');
    Route::post('/json-input-harian-departemen',[App\Http\Controllers\InputHarianController::class, 'json_departemen'])->name('input-harian.json-departemen');
    Route::post('/json-input-harian-unit',[App\Http\Controllers\InputHarianController::class, 'json_unit'])->name('input-harian.json-unit');
    Route::post('/input/history',[App\Http\Controllers\InputHarianController::class, 'history'])->name('input-harian.history');
    Route::post('/input/history/list-item',[App\Http\Controllers\InputHarianController::class, 'list_item'])->name('input-harian.list');
    Route::post('/input/history/list-item/form',[App\Http\Controllers\InputHarianController::class, 'input_harian'])->name('input-harian.input');
    Route::get('/json-item',[App\Http\Controllers\InputHarianController::class, 'json_item'])->name('input-harian.json-item');
    Route::resource('/input-harian-in', App\Http\Controllers\InputHarianInController::class);
    Route::resource('/input-harian-out', App\Http\Controllers\InputHarianOutController::class);

});