<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterTypeBahan extends Model
{
    use HasFactory;
    protected $table = 'master_type_bahans';
    protected  $guarded = [];
}
