<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterKeteranganProses extends Model
{
    use HasFactory;
    protected $table = 'master_keterangan_proses';
    protected  $guarded = [];
    
}
