<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterDepartemen;

class MasterBagian extends Model
{
    use HasFactory;
    protected $table = 'master_bagians';
    protected  $guarded = [];

    public function departemen()
    {
        return $this->belongsTo(MasterDepartemen::class, 'master_departemens_id');
    }
}
