<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterKeteranganBahan extends Model
{
    use HasFactory;
    protected $table = 'master_keterangan_bahans';
    protected  $guarded = [];
}
