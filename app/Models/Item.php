<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterItem;
use App\Models\MasterKeteranganBahan;
use App\Models\MasterTypeBahan;
use App\Models\MasterWarna;
use App\Models\MasterGroupKualitas;
use App\Models\MasterKualitas;
use App\Models\MasterJenisKayu;
use App\Models\MasterKeteranganProses;

class Item extends Model
{
    use HasFactory;
    protected $table = 'items';
    protected  $guarded = [];

    public function item()
    {
        return $this->belongsTo(MasterItem::class, 'master_items_id');
    }

    public function ket_bahan()
    {
        return $this->belongsTo(MasterKeteranganBahan::class, 'master_keterangan_bahans_id');
    }

    public function type_bahan()
    {
        return $this->belongsTo(MasterTypeBahan::class, 'master_type_bahans_id');
    }

    public function warna()
    {
        return $this->belongsTo(MasterWarna::class, 'master_warnas_id');
    }

    public function group_kualitas()
    {
        return $this->belongsTo(MasterGroupKualitas::class, 'master_group_kualitas_id');
    }

    public function kualitas()
    {
        return $this->belongsTo(MasterKualitas::class, 'master_kualitas_id');
    }

    public function jenis_kayu()
    {
        return $this->belongsTo(MasterJenisKayu::class, 'master_jenis_kayus_id');
    }

    public function ket_proses()
    {
        return $this->belongsTo(MasterKeteranganProses::class, 'master_keterangan_proses_id');
    }
}
