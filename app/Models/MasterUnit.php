<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterDepartemenHarianProses;
use App\Models\MasterLokasi;

class MasterUnit extends Model
{
    use HasFactory;
    protected $table = 'master_units';
    protected  $guarded = [];

    public function departemen()
    {
        return $this->belongsTo(MasterDepartemenHarianProses::class, 'master_departemen_harian_proses_id');
    }
    public function lokasi()
    {
        return $this->belongsTo(MasterLokasi::class, 'master_lokasis_id');
    }
}
