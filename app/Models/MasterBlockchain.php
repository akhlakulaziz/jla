<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBlockchain extends Model
{
    use HasFactory;
    protected $table = 'master_blockchains';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'status'];

    public function kelola_nft()
    {
        return $this->belongsTo(KelolaNFT::class, 'blockchain');
    }
}
