<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterUnit;

class MasterSubUnit extends Model
{
    use HasFactory;
    protected $table = 'master_sub_units';
    protected  $guarded = [];

    public function unit()
    {
        return $this->belongsTo(MasterUnit::class, 'master_units_id');
    }
}
