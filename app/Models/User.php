<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Ramsey\Uuid\Uuid;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'address', 'gender', 'npwp', 'nik', 'place_of_birth', 'date_of_birth', 'JRU', 'password','uuid'
    ];

    public function post()
    {
        return $this->belongsTo(MyBusiness::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->uuid = Uuid::uuid4()->getHex();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    public function isAdmin()
    {
        foreach($this->roles()->get() as $role)
        {
            if ($role->name == 'admin')
            {
                return true;
            }
        }
    }

    public function isKoordinator()
    {
        foreach($this->roles()->get() as $role)
        {
            if ($role->name == 'koordinator')
            {
                return true;
            }
        }
    }

    // public function isAdministrator()
    // {
    //     foreach($this->roles()->get() as $role)
    //     {
    //         if ($role->name == 'administrator')
    //         {
    //             return true;
    //         }
    //     }
    // }

    public function total_pendapatan()
    {
        try {
            $business = BusinessOwner::where('user_id', Auth::user()->id)->where('status', '1')->get();
            $total_pendapatan = 0;
            foreach ($business as $item)
            {
                $total_pendapatan += (int)$item->business->calculate_income($item->business_id);
            }
            return $total_pendapatan;
        } catch (\Throwable $e) {
            return 0;
        }
    }

    public function total_pendapatan_pendamping()
    {
        $business = BusinessCompanion::where('user_id', Auth::user()->id)->where('status', '1')->get();
        $total_pendapatan_pendamping = 0;
        foreach ($business as $item)
        {
            $total_pendapatan_pendamping += (int)$item->salary;
        }
        return $total_pendapatan_pendamping;
    }

    public function total_shu_new(){
        try {
            $business_list = Business::where('user_id', Auth::user()->id)->pluck('id')->toArray();
            $business_owner = BusinessOwner::where('user_id', Auth::user()->id)->pluck('business_id')->toArray();
            $business_companion = BusinessCompanion::where('user_id', Auth::user()->id)->pluck('business_id')->toArray();
            
            $array = array_unique(array_merge($business_owner, $business_list, $business_companion));
            $business_owner = Business::whereIn('id', $array)->get();
            $total_shu = 0;
            foreach($business_owner as $item){
                if (isset($item->percent($item->id)->percent)){
                    $total_shu=$total_shu+$item->shu($item->percent($item->id)->percent, $item->share_shu);
                }
            }
            return $total_shu;
        } catch (\Throwable $e) {
            return 0;
        }
    }

    
    // public function kelola_nft()
    // {
    //     return $this->belongsTo(KelolaNFT::class, 'pic');
    // }
}
