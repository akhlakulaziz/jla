<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterKeteranganPekerjaan extends Model
{
    use HasFactory;
    protected $table = 'master_keterangan_pekerjaans';
    protected  $guarded = [];
}
