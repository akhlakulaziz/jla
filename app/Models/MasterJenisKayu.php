<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterJenisKayu extends Model
{
    use HasFactory;
    protected $table = 'master_jenis_kayus';
    protected  $guarded = [];
}
