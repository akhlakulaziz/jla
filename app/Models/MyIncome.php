<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MyIncome extends Model
{
    use HasFactory;

    public function getTotalPriceAttribute() {
        return $this->quantity * $this->price;
    }

    public function total_biaya($id)
    {
        $karyawan = Karyawan::where('id', $id)->first();
        $pendamping = BusinessCompanion::where('id', $id)->first();
        $owners = BusinessOwner::where('business_id', $id)->first();
        
        if($karyawan){
            $total_salary = $karyawan->basic_salary + $karyawan->meal_allowance + $karyawan->other_allowance;
             return $total_salary;
         }else{
             return '0';
         } 
    }
}
