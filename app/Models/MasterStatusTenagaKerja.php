<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterStatusTenagaKerja extends Model
{
    use HasFactory;
    protected $table = 'master_status_tenaga_kerjas';
    protected  $guarded = [];
}
