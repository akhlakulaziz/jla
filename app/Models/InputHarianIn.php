<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputHarianIn extends Model
{
    use HasFactory;
    protected $table = 'input_harian_ins';
    protected  $guarded = [];

    public function master_shift()
    {
        return $this->belongsTo(MasterShift::class, 'shift');
    }
    public function suppliers()
    {
        return $this->belongsTo(MasterSupplier::class, 'master_suppliers_id');
    }
    public function product()
    {
        return $this->belongsTo(MasterProduct::class, 'master_products_id');
    }
    public function no_product()
    {
        return $this->belongsTo(MasterNoPo::class, 'master_no_pos_id');
    }
    public function pemborong()
    {
        return $this->belongsTo(MasterPemborong::class, 'master_pemborongs_id');
    }
}
