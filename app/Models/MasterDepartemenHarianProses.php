<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterDepartemenHarianProses extends Model
{
    use HasFactory;
    protected $table = 'master_departemen_harian_proses';
    protected  $guarded = [];
    
}
