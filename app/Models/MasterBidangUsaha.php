<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business;
use App\Models\karyawan;
use App\Models\MasterPositionEmploee;

class MasterBidangUsaha extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_bidang_usahas';

    /**
    * The database primary key value.
    *
    * @var string 
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    public function business()
    {
        return $this->hasMany(Business::class, 'bidang_id');

    }

    public function karyawan()
    {
        return $this->hasMany(karyawan::class, 'position_id');

    }

    public function master_position()
    {
        return $this->hasMany(MasterPositionEmploee::class, 'master_bidang_usahas_id');
    }
    
    protected $fillable = ['name', 'status'];
    
}
