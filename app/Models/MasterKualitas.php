<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterKualitas extends Model
{
    use HasFactory;
    protected $table = 'master_kualitas';
    protected  $guarded = [];
}
