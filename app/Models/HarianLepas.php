<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HarianLepas extends Model
{
    use HasFactory;
    protected $table = 'harian_lepas';
    protected  $guarded = [];
}
