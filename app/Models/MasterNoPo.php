<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterProduct;

class MasterNoPo extends Model
{
    use HasFactory;
    protected $table = 'master_no_pos';
    protected  $guarded = [];

    public function product()
    {
        return $this->belongsTo(MasterProduct::class, 'master_products_id');
    }
}
