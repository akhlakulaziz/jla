<?php

namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Spatie\Permission\Models\Role;
use App\Models\User;
use HasRoles;

class UserExport implements FromView
{
    public function view(): View
    {
        return view('pages.users.user', [
            'users' => User::whereHas('roles', function($q)
            {
                $q->where('name', 'koordinator');
            })->get()
        ]);
        
    }
}