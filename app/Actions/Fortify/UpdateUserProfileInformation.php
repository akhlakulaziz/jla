<?php

namespace App\Actions\Fortify;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * Validate and update the given user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    public function update($user, array $input)
    {
        // dd($input['photo']);
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
            // 'photo' => ['required', 'mimes:jpg,jpeg,png', 'max:5120', 'dimensions:max_width=1000,max_height=1000'],
            // 'nik' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:13'],
            'address' => ['required', 'string'],
            'gender' => ['required'],
            'place_of_birth' => ['required', 'string'],
            'date_of_birth' => ['required', 'date'],
            ])->validateWithBag('updateProfileInformation');
           
        if (isset($input['photo'])) {
            $user->updateProfilePhoto($input['photo']);
        }

        if ($input['email'] !== $user->email &&
            $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $input);
        } else {
            $user->forceFill([
                'name' => $input['name'],
                'email' => $input['email'],
                'nik' => $input['nik'],
                'phone' => $input['phone'],
                'address' => $input['address'],
                'gender' => $input['gender'],
                'place_of_birth' => $input['place_of_birth'],
                'date_of_birth' => $input['date_of_birth'],
            ])->save();
        }
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    protected function updateVerifiedUser($user, array $input)
    {
        $user->forceFill([
            'name' => $input['name'],
            'email' => $input['email'],
            'email_verified_at' => null,
            'nik' => $input['nik'],
            'phone' => $input['phone'],
            'address' => $input['address'],
            'gender' => $input['gender'],
            'place_of_birth' => $input['place_of_birth'],
            'date_of_birth' => $input['date_of_birth'],
        ])->save();

        $user->sendEmailVerificationNotification();
    }
}
