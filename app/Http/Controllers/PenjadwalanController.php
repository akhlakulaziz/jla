<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB,Excel,PDF,Auth,Charts;
use Validator;

use App\Models\KelolaNFT;
use App\Models\Configs;
use Illuminate\Http\Request;

class PenjadwalanController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:jadwal-list', ['only' => ['index', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $kelola_nft = KelolaNFT::all();

        return view('pages.penjadwalan.index', compact('kelola_nft'));
    }

    public function store(Request $request)
    {
        $rules =array(
            'jadwal' => 'nullable',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        Configs::where('key','mail.from.schedule')->update(['value' => $request->jadwal]);
        
        return response()->json(["status"=>"success","message"=>'Update was successful!'], 200);
    }
}
