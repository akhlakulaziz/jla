<?php

namespace App\Http\Controllers;

use App\Models\MasterProduct;
use App\Models\MasterNoPo;
use Illuminate\Http\Request;
use Auth;
use Validator;
class MasterNoPoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterNoPo = MasterNoPo::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterNoPo = MasterNoPo::orderBy('name', 'asc')->get();
        }

        return view('pages.master-no-po.index', compact('masterNoPo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = MasterProduct::where('status', 1)->get();
        return view('pages.master-no-po.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required',
            'master_products_id' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterNoPo = MasterNoPo::create($requestData);

        activity('master_no_po')
            ->performedOn($masterNoPo)
            ->causedBy(Auth::user())
            ->log('created');

        return redirect('master-no-po')->with('flash_message', 'Master Nomer PO added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterNoPo  $masterNoPo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterNoPo = MasterNoPo::findOrFail($id);
        $master_product =  MasterProduct::where('id', $masterNoPo->master_products_id)->first();
        return view('pages.master-no-po.show', compact('masterNoPo', 'master_product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterNoPo  $masterNoPo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterNoPo = MasterNoPo::findOrFail($id);
        $product = MasterProduct::where('status', 1)->get();
        return view('pages.master-no-po.edit', compact('masterNoPo', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterNoPo  $masterNoPo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'master_products_id' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterNoPo = MasterNoPo::findOrFail($id);
        $masterNoPo->update($requestData);

        activity('master_no_po')
            ->performedOn($masterNoPo)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-no-po')->with('flash_message', 'Master Nomer PO updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterNoPo  $masterNoPo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterNoPo = MasterNoPo::find($id);
        MasterNoPo::destroy($id);

        
        activity('master_no_po')
            ->performedOn($masterNoPo)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-no-po')->with('flash_message', 'Master Nomer PO deleted!');
    }
}
