<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Session;
use Validator;

class UserAccessController extends Controller
{
    public function loginAs(Request $request, $id)
    {
        $userRoles = Auth::user()->getRoleNames()->toArray();
        if(in_array('admin', $userRoles)) 
        {
            //login the user
            $user = User::find($id);
            if(Auth::user()->id != $id)
            {
                Auth::loginUsingId($id);
                return response()->json(["status"=>"success","message"=>'Login Success, Please Wait!'], 200);
            }
            else
            {
                return response()->json(["status"=>"danger","message"=>'You already logged in!'], 200);
            }
        }
        else
        {
            return response()->json(["status"=>"danger","message"=>'You Are NOT Admin!'], 200);
        }
    }
}
