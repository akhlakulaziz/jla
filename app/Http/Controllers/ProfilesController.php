<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Business;
use App\Models\User;
use App\Models\BusinessOwner;
use App\Models\BusinessCompanion;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profiles.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function show_profile($uuid)
    {
        $pemilik = User::where('uuid', $uuid)->first();
        // dd($pemilik);
        $business_list = Business::where('user_id', $pemilik->id)->pluck('id')->toArray();
        $business_owner = BusinessOwner::where('user_id', $pemilik->id)->pluck('business_id')->toArray();
        $business_companion = BusinessCompanion::where('user_id', $pemilik->id)->pluck('business_id')->toArray();
        
        $array = array_unique(array_merge($business_owner, $business_list, $business_companion));
        // dd($array);
        $business = Business::whereIn('id', $array)->latest()->take(4)->get();
        $descriptions = Business::where('user_id', $pemilik->id)->first();
        return view('pages.user-profile.index', compact('pemilik','business', 'descriptions'));
        // return view('profiles.profile-detail');
    }

    public function show_business()
    {
        return view('profiles.profile-business');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
