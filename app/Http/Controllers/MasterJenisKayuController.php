<?php

namespace App\Http\Controllers;

use App\Models\MasterJenisKayu;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterJenisKayuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterJenisKayu = MasterJenisKayu::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterJenisKayu = MasterJenisKayu::orderBy('name', 'asc')->get();
        }
        return view('pages.master-jenis-kayu.index', compact('masterJenisKayu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-jenis-kayu.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterJenisKayu = MasterJenisKayu::create($requestData);
        activity('master_jenis_kayu')
            ->performedOn($masterJenisKayu)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-jenis-kayu')->with('flash_message', 'Master Jenis Kayu added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterJenisKayu  $masterJenisKayu
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterJenisKayu = MasterJenisKayu::findOrFail($id);
        return view('pages.master-jenis-kayu.show', compact('masterJenisKayu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterJenisKayu  $masterJenisKayu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterJenisKayu = MasterJenisKayu::findOrFail($id);
        return view('pages.master-jenis-kayu.edit', compact('masterJenisKayu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterJenisKayu  $masterJenisKayu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterJenisKayu = MasterJenisKayu::findOrFail($id);
        $masterJenisKayu->update($requestData);

        activity('master_jenis_kayu')
            ->performedOn($masterJenisKayu)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-jenis-kayu')->with('flash_message', 'Master Jenis Kayu updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterJenisKayu  $masterJenisKayu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterJenisKayu = MasterJenisKayu::find($id);
        MasterJenisKayu::destroy($id);
        activity('master_jenis_kayu')
            ->performedOn($masterJenisKayu)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-jenis-kayu')->with('flash_message', 'Master Master Jenis Kayu deleted!');
    }
}
