<?php

namespace App\Http\Controllers;

use App\Models\InputHarianOut;
use Illuminate\Http\Request;

use App\Models\MasterSupplier;
use App\Models\MasterNoPo;
use App\Models\MasterProduct;
use App\Models\MasterPemborong;
use App\Models\MasterShift;

use Auth;

class InputHarianOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $outs = InputHarianOut::where('departemen', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $outs = InputHarianOut::orderBy('departemen', 'asc')->latest()->paginate($perPage);
        }
        $supplier = MasterSupplier::where('status', 1)->get();
        $product = MasterProduct::where('status', 1)->get();
        $no_po = MasterNoPo::where('status', 1)->get();
        $pemborong = MasterPemborong::where('status', 1)->get();
        $shift = MasterShift::where('status', 1)->get();
        return view('pages.harian-proses.outs.index',compact('outs', 'supplier', 'product', 'no_po', 'pemborong', 'shift'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InputHarianOut  $inputHarianOut
     * @return \Illuminate\Http\Response
     */
    public function show(InputHarianOut $inputHarianOut)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InputHarianOut  $inputHarianOut
     * @return \Illuminate\Http\Response
     */
    public function edit(InputHarianOut $inputHarianOut)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InputHarianOut  $inputHarianOut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();        
        $item = InputHarianOut::findOrFail($request->id);
        $item->shift = $request->shift;
        $item->pcs = $request->pcs;
        $item->master_suppliers_id  = $request->supplier;
        $item->no_kiriman = $request->no_kiriman;
        $item->master_products_id  = $request->po;
        $item->master_no_pos_id  = $request->po_no;
        $item->no_pallet = $request->no_pallet;
        $item->nampan = $request->nampan;
        $item->operator = $request->operator;
        $item->harian_tetap = $request->harian_tetap;
        $item->harian_lepas = $request->harian_lepas;
        $item->borong = $request->borong;
        $item->master_pemborongs_id  = $request->pemborong;
        $item->memo = $request->memo;
        $item->ot = $request->ot;
        $item->keterangan = $request->keterangan;
        $item->update();


        activity('item')
            ->performedOn($item)
            ->causedBy(Auth::user())
            ->log('updated');
        return response()->json(["status"=>"success","message"=>'Input Harian Out Updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InputHarianOut  $inputHarianOut
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = InputHarianOut::find($id);
        $item->delete();

        activity('item')
        ->performedOn($item)
        ->causedBy(Auth::user())
        ->log('deleted');

        return response()->json(["status"=>"success","message"=>'Item deleted!'], 200);
    }
}
