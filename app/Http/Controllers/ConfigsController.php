<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Configs;
use Illuminate\Support\Facades\Validator;
use Auth;

class ConfigsController extends Controller
{

    // function __construct()
    // {
    //     $this->middleware('permission:survey-quis-answer-list', ['only' => ['index']]);

    // }


    public function index()
    {
        return view('pages.settings.index');
    }


    public function store(Request $request)
    {
        // dd($request->all());
        $rules =array(
            'name' => 'required',
            'url' => 'required|url',
            'description' => 'required',
            'logo' => 'required',
            'favicon' => 'required',// |dimensions:max_width=32px,max_height=32',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            
			//name file
			$imagename = $image->getClientOriginalName();
			$a = explode(".", $imagename);
			$fileExt = strtolower(end($a));  
            $namaFile = substr(md5(date("YmdHis")),0,10).".".$fileExt;

			//penyimpanan
            $destination_path= public_path().'/image/';
            
			// simpan ke folder
			$request->file('logo')->move($destination_path,$namaFile);
        }else{
            $namaFile = config('mail.from.app_logo');
        }

        if ($request->hasFile('favicon')) {
            $imageFavicon = $request->file('favicon');
            
			//name file
			$imagenameFavicon = $imageFavicon->getClientOriginalName();
			$aFavicon = explode(".", $imagenameFavicon);
			$fileExtFavicon = strtolower(end($aFavicon));  
            $namaFileFavicon = substr(md5(date("YmdHis")),0,10).".".$fileExtFavicon;

			//penyimpanan
            $destination_pathFavicon = public_path().'/image/';
            
			// simpan ke folder
			$request->file('favicon')->move($destination_pathFavicon,$namaFileFavicon);
        }else{
            $namaFileFavicon = config('mail.from.app_favicon');
        }

        
        Configs::where('key','app.name')->update(['value' => $request->name]);
        Configs::where('key','app.url')->update(['value' => $request->url]);
        Configs::where('key','mail.from.app_description')->update(['value' => $request->description]);
        Configs::where('key','mail.from.app_logo')->update(['value' => $namaFile]);
        Configs::where('key','mail.from.app_favicon')->update(['value' => $namaFileFavicon]);
    
        return response()->json(["status"=>"success","message"=>'Update was successful!'], 200);
    }

    public function mail(Request $request){

        $rules =array(
            'mail_name' => 'required',
            'mail_address' => 'required',
            'mail_driver' => 'required',
            'mail_host' => 'required',
            'mail_port' => 'required',
            'mail_username' => 'required',
            'mail_password' => 'required',
            'mail_encryption' => 'required'
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        Configs::where('key','mail.from.name')->update(['value' => $request->mail_name]);
        Configs::where('key','mail.from.address')->update(['value' => $request->mail_address]);
        Configs::where('key','mail.default')->update(['value' => $request->mail_driver]);
        Configs::where('key','mail.mailers.smtp.host')->update(['value' => $request->mail_host]);
        Configs::where('key','mail.mailers.smtp.port')->update(['value' => $request->mail_port]);
        Configs::where('key','mail.mailers.smtp.username')->update(['value' => $request->mail_username]);
        Configs::where('key','mail.mailers.smtp.password')->update(['value' => $request->mail_password]);
        $configs = Configs::where('key','mail.mailers.smtp.encryption')->update(['value' => $request->mail_encryption]);
        
    
        return response()->json(["status"=>"success","message"=>'Update was successful!'], 200);
    }

}
