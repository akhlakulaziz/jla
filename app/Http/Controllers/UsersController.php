<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Exports\UserExport;
use Carbon\Carbon;
use App\Models\BusinessCompanion;
use App\Models\BusinessAsset;
use App\Models\BusinessOwner;
use App\Models\Business;
use App\Models\BusinessOtherType;
use DB,Excel,PDF,Auth,Charts;
use Validator;


class UsersController extends Controller
{

    
    function __construct()
    {
        $this->middleware('permission:users-list', ['only' => ['index','trash']]);
        $this->middleware('permission:users-view', ['only' => ['show']]);
        $this->middleware('permission:users-create', ['only' => ['create','store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy','trash','restore','restoreAll','forceDelete']]);  
    }


    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $users = User::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $users = User::latest()->paginate($perPage);
        }
        $trashed = User::onlyTrashed()->get();

        return view('pages.users.index', compact('users','trashed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $roles = Role::select('id', 'name')->get();
        $roles = $roles->pluck('name', 'id');

        return view('pages.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required',
            'email' => 'required|string|max:255|email|unique:users',
            'phone' => 'required|max:14',
            'address' => 'required',
            'gender' => 'required',
            'npwp' => 'required',
            // 'nik' => 'required',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required',
            'password' => 'required',
            // 'JRU' => 'required',
            'roles' => 'required',
        
        );
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        $data['email_verified_at'] = now();

        $user = User::create($data);

        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }

        activity('user')
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->log('created');

        return response()->json(["status"=>"success","message"=>'User added!'], 200);
    }

    public function UserExport() 
    {
        return Excel::download(new UserExport, 'datapengguna.xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('pages.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $user = User::with('roles')->select('id', 'name', 'email')->findOrFail($id);
        $roles = Role::pluck('name','id')->all();
        $user_roles = $user->roles->pluck('id','id')->all();
    
        
        return view('pages.users.edit', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        
        $rules =array(
            'name' => 'required',
            'phone' => 'required|max:14',
            'address' => 'required',
            'gender' => 'required',
            'npwp' => 'required',
            // 'nik' => 'required',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required',
            // 'JRU' => 'required',
            'roles' => 'required',
            
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        
        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }
        
        $user = User::findOrFail($id);
        $user->update($data);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));

        
        activity('user')
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->log("updated");

        return response()->json(["status"=>"success","message"=>'User Updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        // soft delete user
        $user = User::find($id);
        $bisnis = Business::where('user_id', $user->id)->get();
        foreach($bisnis as $item){
            if($item != null){
                // soft delete menurut bisnis owner menurut bisnis
                $bisnis_owner = BusinessOwner::where('business_id', $item->id)->delete();
                $bisnis_relation = BusinessOwner::where('business_relation', $item->id)->delete();
                $bisnis_assets = BusinessAsset::where('business_id', $item->id)->delete();
                $bisnis_company = BusinessCompanion::where('business_id', $item->id)->delete();
                $bisnis_other = BusinessOtherType::where('business_id', $item->id)->delete();
                $bisnis = Business::where('id', $item->id)->delete();
            }
        }

        // soft delete bisnis owner menurut user
        $bisnis_owner_user = BusinessOwner::where('user_id', $user->id)->delete();

        // soft delete bisnis compani menurut user
        $bisnis_compani_user = BusinessCompanion::where('user_id', $user->id)->delete();

        $user->delete();

        activity('user')
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->log("deleted"); 

        return response()->json(["status"=>"success","message"=>'User Deleted!'], 200);
    }

    public function updatePassword(Request $request)
    {

        $rules =array(
            'password'              => 'required|min:8|same:password_confirmation',
            'password_confirmation' => 'required',
        );
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        
        $user = User::find($request->id);

        $user->password = bcrypt(request('password'));
        $user->save();

        activity('user')
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->log('user Update Password by ' . Auth::user()->name);

        return response()->json(["status"=>"success","message"=>'Password has been updated!'], 200);
    
    }

    public function trash(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $users = User::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
                ->onlyTrashed()->paginate($perPage);
        } else {
            $users = User::onlyTrashed()->paginate($perPage);
        }
        return view('pages.users.trash',compact('users'));
    }
    
    public function restore($id)
    {
        $user = User::onlyTrashed()->where('id',$id)->first();
        $business = Business::onlyTrashed()->where('user_id', $user->id)->get();
        foreach ($business as $item){
            
            $bisnis_restore = Business::onlyTrashed()->where('id',$item->id);
            
            // soft delete menurut bisnis owner menurut bisnis
            $bisnis_owner = BusinessOwner::onlyTrashed()->where('business_id', $item->id)->restore();
            
            // soft delete menurut bisnis owner menurut bisnis relasi
            $bisnis_relation = BusinessOwner::onlyTrashed()->where('business_relation', $item->id)->restore();
            
            // soft delete menurut bisnis compani menurut bisnis
            $bisnis_company = BusinessCompanion::onlyTrashed()->where('business_id', $item->id)->restore();
            
            // soft delete menurut assets compani menurut bisnis
            $bisnis_assets = BusinessAsset::onlyTrashed()->where('business_id', $item->id)->restore();
            
            // soft delete menurut bisnis Other Type menurut bisnis
            $bisnis_Other = BusinessOtherType::onlyTrashed()->where('business_id', $item->id)->restore();
            
            $bisnis_restore->restore();
        }
        // soft delete bisnis owner menurut user
        $bisnis_owner_user = BusinessOwner::onlyTrashed()->where('user_id', $user->id)->restore();

        // soft delete bisnis compani menurut user
        $bisnis_compani_user = BusinessCompanion::onlyTrashed()->where('user_id', $id)->restore();

        $user->restore();

        return redirect()->back();
    }
    
    public function restoreAll()
    {
        $users = User::onlyTrashed()->get();
        foreach ($users as $user){
            // dd($item->id);
            $business = Business::onlyTrashed()->where('user_id', $user->id)->get();

            foreach ($business as $bisnis){
                $bisnis_restore = Business::onlyTrashed()->where('id',$bisnis->id);
                
                // soft delete menurut bisnis owner menurut bisnis
                $bisnis_owner = BusinessOwner::onlyTrashed()->where('business_id', $bisnis->id)->restore();

                // soft delete menurut bisnis owner menurut bisnis relasi
                $bisnis_relation = BusinessOwner::onlyTrashed()->where('business_relation', $bisnis->id)->restore();

                // soft delete menurut bisnis compani menurut bisnis
                $bisnis_company = BusinessCompanion::onlyTrashed()->where('business_id', $bisnis->id)->restore();

                // soft delete menurut assets compani menurut bisnis
                $bisnis_assets = BusinessAsset::onlyTrashed()->where('business_id', $bisnis->id)->restore();

                // soft delete menurut bisnis Other Type menurut bisnis
                $bisnis_Other = BusinessOtherType::onlyTrashed()->where('business_id', $bisnis->id)->restore();

                $bisnis_restore->restore();
            }
            // soft delete bisnis owner menurut user
            $bisnis_owner_user = BusinessOwner::onlyTrashed()->where('user_id', $user->id)->restore();
            
            // soft delete bisnis compani menurut user
            $bisnis_compani_user = BusinessCompanion::onlyTrashed()->where('user_id', $user->id)->restore();
            
            $user = User::onlyTrashed()->where('id',$user->id)->restore();
        }

        $users = User::onlyTrashed()->restore();

        return redirect()->route('users.index');
    }

    public function forceDelete($id)
    {
        $user = User::onlyTrashed()->where('id',$id)->first();
        $business = Business::onlyTrashed()->where('user_id', $user->id)->get();
        foreach($business as $item){
            $bisnis_restore = Business::onlyTrashed()->where('id',$item->id);
            
            // soft delete menurut bisnis owner menurut bisnis
            $bisnis_owner = BusinessOwner::onlyTrashed()->where('business_id', $item->id)->forceDelete();

            // soft delete menurut bisnis owner menurut bisnis relasi
            $bisnis_relation = BusinessOwner::onlyTrashed()->where('business_relation', $item->id)->forceDelete();

            // soft delete menurut bisnis compani menurut bisnis
            $bisnis_company = BusinessCompanion::onlyTrashed()->where('business_id', $item->id)->forceDelete();

            // soft delete menurut assets compani menurut bisnis
            $bisnis_assets = BusinessAsset::onlyTrashed()->where('business_id', $item->id)->forceDelete();

            // soft delete menurut bisnis Other Type menurut bisnis
            $bisnis_Other = BusinessOtherType::onlyTrashed()->where('business_id', $item->id)->forceDelete();

            $bisnis_restore->forceDelete();
        }

        // soft delete bisnis owner menurut user
        $bisnis_owner_user = BusinessOwner::onlyTrashed()->where('user_id', $user->id)->forceDelete();

        // soft delete bisnis company menurut user
        $bisnis_company_user = BusinessCompanion::onlyTrashed()->where('user_id', $user->id)->forceDelete();
        
        $user->forceDelete();

        return response()->json(["status"=>"success","message"=>'User Deleted!'], 200);
    }
}
