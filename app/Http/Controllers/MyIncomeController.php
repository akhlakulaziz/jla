<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\BusinessOwner;
use App\Models\Business;
use App\Models\BusinessCompanion;


class MyIncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business_list = Business::where('user_id', Auth::user()->id)->pluck('id')->toArray();
        $business_owner = BusinessOwner::where('user_id', Auth::user()->id)->pluck('business_id')->toArray();
        $business_companion = BusinessCompanion::where('user_id', Auth::user()->id)->pluck('business_id')->toArray();

        $array = array_unique(array_merge($business_owner, $business_list, $business_companion));
        $business = Business::whereIn('id', $array)->where('status', '1')->orderBy('name', 'asc')->simplepaginate(20);

        $business_companion = BusinessCompanion::where('user_id', Auth::user()->id)->where('status', '1')->simplepaginate(20);

        return view('pages.business.my-income.index', compact('business', 'business_companion'));
    }


}
