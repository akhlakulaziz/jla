<?php

namespace App\Http\Controllers;

use App\Models\HarianLepas;
use App\Models\MasterDepartemen;
use App\Models\MasterBagian;
use Auth;
use Illuminate\Http\Request;

class HarianLepasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if(!empty($keyword)) {
            $hl = HarianLepas::where('name', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $hl = HarianLepas::latest()->paginate($perPage);
        }

        $departemen = MasterDepartemen::all();
        $bagian = MasterBagian::all();

        return view('pages.harian-lepas.data.index', compact('hl', 'departemen', 'bagian'));
    }

    public function flowchart(){
        // dd("halo");
        return view('pages.flowchart.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // $rules =array(
        //     'name' => 'required',
        //     'position_id' => 'required',
        //     'gender' => 'required',
        //     'year_in_work' => 'required',
        //     'status' => 'required',
        // );

        // $validator=Validator::make($request->all(),$rules);
        
        // if($validator->fails())
        // {
        //     $messages=$validator->messages();
        //     $errors=$messages->all();
        //     return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        // }
        
        //pembagian tanggal
        $periode_bulan = $request->periode_bulan;
        $pemisah = explode(" - ", $periode_bulan);
        $star_period_month = strtolower(current($pemisah));
        $end_period_month = strtolower(end($pemisah));
        // dd($periode_bulan);

        $periode_minggu = $request->periode_minggu;
        $pemisah = explode(" - ", $periode_minggu);
        $star_period_week = strtolower(current($pemisah));
        $end_period_week = strtolower(end($pemisah));

        $hl = new HarianLepas();
        $hl->star_period_month = $star_period_month;
        $hl->end_period_month = $end_period_month;
        $hl->star_period_week = $star_period_week;
        $hl->end_period_week = $end_period_week;
        $hl->tanggal = $request->tanggal;
        $hl->name = $request->name;
        $hl->master_departemens_id = $request->master_departemens_id;
        $hl->master_bagians_id = $request->master_bagians_id;
        // $hl->jml_tenaga = $request->date_of_birth;
        $hl->jla = $request->jla;
        $hl->status = $request->status;
        $hl->mandor = $request->mandor;
        $hl->shift = $request->shift;
        $hl->save();

        activity('hl')
            ->performedOn($hl)
            ->causedBy(Auth::user())
            ->log('created');

        return response()->json(["status"=>"success","message"=>'Data HL added!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HarianLepas  $harianLepas
     * @return \Illuminate\Http\Response
     */
    public function show(HarianLepas $harianLepas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HarianLepas  $harianLepas
     * @return \Illuminate\Http\Response
     */
    public function edit(HarianLepas $harianLepas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HarianLepas  $harianLepas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HarianLepas $harianLepas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HarianLepas  $harianLepas
     * @return \Illuminate\Http\Response
     */
    public function destroy(HarianLepas $harianLepas)
    {
        //
    }
}
