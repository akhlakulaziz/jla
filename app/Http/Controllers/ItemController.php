<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\MasterItem;
use App\Models\MasterKeteranganBahan;
use App\Models\MasterTypeBahan;
use App\Models\MasterWarna;
use App\Models\MasterGroupKualitas;
use App\Models\MasterKualitas;
use App\Models\MasterJenisKayu;
use App\Models\MasterKeteranganProses;
use Auth;
use Illuminate\Http\Request;
use Validator;

class ItemController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:item-list', ['only' => ['index']]);
        // $this->middleware('permission:owners-view', ['only' => ['show']]);
        $this->middleware('permission:item-create', ['only' => ['create','store']]);
        $this->middleware('permission:item-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:item-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd("halo");
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $item = Item::where('item', 'LIKE', "%$keyword%")->orWhere('no_part_item', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $items = Item::latest()->get();
        }
        $master_item = MasterItem::where('status', 1)->get();
        $keterangan_bahan = MasterKeteranganBahan::where('status', 1)->get();
        $type_bahan = MasterTypeBahan::where('status', 1)->get();
        $warna = MasterWarna::where('status', 1)->get();
        $group_kualitas = MasterGroupKualitas::where('status', 1)->get();
        $kualitas = MasterKualitas::where('status', 1)->get();
        $jenis_kayu = MasterJenisKayu::where('status', 1)->get();
        $keterangan_proses = MasterKeteranganProses::where('status', 1)->get();

        return view('pages.harian-proses.items.index', compact('items', 'master_item', 'keterangan_bahan', 'type_bahan', 'warna', 'group_kualitas', 'kualitas', 'jenis_kayu', 'keterangan_proses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $rules =array(
            'item' => 'required',
            'keterangan_bahan' => 'required',
            'type_bahan' => 'required',
            'diameter' => 'required',
            'tinggi' => 'required',
            'lebar' => 'required',
            'panjang' => 'required',
            'warna' => 'required',
            'group_kualitas' => 'required',
            'kualitas' => 'required',
            'jenis_kayu' => 'required',
            'keterangan_proses' => 'required',
            'status' => 'required',
            
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        // dd($request->all());
        $item = new Item();
        $item->master_items_id = $request->item;
        $item->master_keterangan_bahans_id = $request->keterangan_bahan;
        $item->master_type_bahans_id = $request->type_bahan;
        $item->diameter = $request->diameter;
        $item->tinggi = $request->tinggi;
        $item->lebar = $request->lebar;
        $item->panjang = $request->panjang;
        $item->master_warnas_id = $request->warna;
        $item->master_group_kualitas_id = $request->group_kualitas;
        $item->master_kualitas_id = $request->kualitas;
        $item->master_jenis_kayus_id = $request->jenis_kayu;
        $item->master_keterangan_proses_id = $request->keterangan_proses;
        $item->status = $request->status;
        $item->save();

        activity('item')
            ->performedOn($item)
            ->causedBy(Auth::user())
            ->log('created');
        
        $item = Item::latest()->first();
        $code = 'Item'.$item->id;
        $item->no_part_item = $code;
        $item->update();
        return response()->json(["status"=>"success","message"=>'Item added!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'item' => 'required',
            'keterangan_bahan' => 'required',
            'type_bahan' => 'required',
            'diameter' => 'required',
            'tinggi' => 'required',
            'lebar' => 'required',
            'panjang' => 'required',
            'warna' => 'required',
            'group_kualitas' => 'required',
            'kualitas' => 'required',
            'jenis_kayu' => 'required',
            'keterangan_proses' => 'required',
            'status' => 'required',
            
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();        
        $item = Item::findOrFail($request->id);
        $item->master_items_id = $request->item;
        $item->master_keterangan_bahans_id = $request->keterangan_bahan;
        $item->master_type_bahans_id = $request->type_bahan;
        $item->diameter = $request->diameter;
        $item->tinggi = $request->tinggi;
        $item->lebar = $request->lebar;
        $item->panjang = $request->panjang;
        $item->master_warnas_id = $request->warna;
        $item->master_group_kualitas_id = $request->group_kualitas;
        $item->master_kualitas_id = $request->kualitas;
        $item->master_jenis_kayus_id = $request->jenis_kayu;
        $item->master_keterangan_proses_id = $request->keterangan_proses;
        $item->status = $request->status;
        $item->update();


        activity('item')
            ->performedOn($item)
            ->causedBy(Auth::user())
            ->log('updated');
        return response()->json(["status"=>"success","message"=>'Item Updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->delete();

        activity('item')
        ->performedOn($item)
        ->causedBy(Auth::user())
        ->log('deleted');

        return response()->json(["status"=>"success","message"=>'Item deleted!'], 200);
    }
}
