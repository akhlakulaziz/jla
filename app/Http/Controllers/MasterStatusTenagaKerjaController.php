<?php

namespace App\Http\Controllers;

use App\Models\MasterStatusTenagaKerja;
use Illuminate\Http\Request;
use Validator;
use Auth;
class MasterStatusTenagaKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $master_status_tenaga_kerja = MasterStatusTenagaKerja::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $master_status_tenaga_kerja = MasterStatusTenagaKerja::orderBy('name', 'asc')->get();
        }
        return view('pages.master-status-tenaga-kerja.index', compact('master_status_tenaga_kerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-status-tenaga-kerja.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $master_status_tenaga_kerja = MasterStatusTenagaKerja::create($requestData);
        activity('master_status_tenaga_kerja')
            ->performedOn($master_status_tenaga_kerja)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-status-tenaga-kerja')->with('flash_message', 'Master Status Tenaga Kerja added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterStatusTenagaKerja  $masterStatusTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $master_status_tenaga_kerja = MasterStatusTenagaKerja::findOrFail($id);
        return view('pages.master-status-tenaga-kerja.show', compact('master_status_tenaga_kerja'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterStatusTenagaKerja  $masterStatusTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $master_status_tenaga_kerja = MasterStatusTenagaKerja::findOrFail($id);
        return view('pages.master-status-tenaga-kerja.edit', compact('master_status_tenaga_kerja'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterStatusTenagaKerja  $masterStatusTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $master_status_tenaga_kerja = MasterStatusTenagaKerja::findOrFail($id);
        $master_status_tenaga_kerja->update($requestData);

        activity('master_status_tenaga_kerja')
            ->performedOn($master_status_tenaga_kerja)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-status-tenaga-kerja')->with('flash_message', 'Master Status Tenaga Kerja updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterStatusTenagaKerja  $masterStatusTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $master_status_tenaga_kerja = MasterStatusTenagaKerja::find($id);
        MasterStatusTenagaKerja::destroy($id);
        activity('master_status_tenaga_kerja')
            ->performedOn($master_status_tenaga_kerja)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-status-tenaga-kerja')->with('flash_message', 'Master Master Status Tenaga Kerja deleted!');
    }
}
