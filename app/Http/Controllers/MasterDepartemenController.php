<?php

namespace App\Http\Controllers;

use App\Models\MasterDepartemen;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterDepartemenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterdepartemen = MasterDepartemen::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterdepartemen = MasterDepartemen::orderBy('name', 'asc')->latest()->paginate($perPage);
        }
        return view('pages.master-departemen.index', compact('masterdepartemen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-departemen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterdepartemen = MasterDepartemen::create($requestData);
        activity('master_departemen')
            ->performedOn($masterdepartemen)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-departemen')->with('flash_message', 'Master Departemen added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterDepartemen  $masterDepartemen
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterDepartemen = MasterDepartemen::findOrFail($id);
        return view('pages.master-departemen.show', compact('masterDepartemen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterDepartemen  $masterDepartemen
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $masterdepartemen = MasterDepartemen::findOrFail($id);
        return view('pages.master-departemen.edit', compact('masterdepartemen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterDepartemen  $masterDepartemen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterdepartemen = MasterDepartemen::findOrFail($id);
        $masterdepartemen->update($requestData);

        activity('master_departemen')
            ->performedOn($masterdepartemen)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-departemen')->with('flash_message', 'Master Departemen updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterDepartemen  $masterDepartemen
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $masterdepartemen = MasterDepartemen::find($id);
        MasterDepartemen::destroy($id);
        activity('master_departemen')
            ->performedOn($masterdepartemen)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-departemen')->with('flash_message', 'Master Master Departemen deleted!');
    }
}
