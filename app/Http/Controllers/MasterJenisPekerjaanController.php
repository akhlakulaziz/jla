<?php

namespace App\Http\Controllers;

use App\Models\MasterJenisPekerjaan;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterJenisPekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterJenisPekerjaan = MasterJenisPekerjaan::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterJenisPekerjaan = MasterJenisPekerjaan::orderBy('name', 'asc')->get();
        }
        return view('pages.master-jenis-pekerjaan.index', compact('masterJenisPekerjaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-jenis-pekerjaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterJenisPekerjaan = MasterJenisPekerjaan::create($requestData);
        activity('master_jenis_pekerjaan')
            ->performedOn($masterJenisPekerjaan)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-jenis-pekerjaan')->with('flash_message', 'Master Jenis Pekerjaan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterJenisPekerjaan  $masterJenisPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterJenisPekerjaan = MasterJenisPekerjaan::findOrFail($id);
        return view('pages.master-jenis-pekerjaan.show', compact('masterJenisPekerjaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterJenisPekerjaan  $masterJenisPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterJenisPekerjaan = MasterJenisPekerjaan::findOrFail($id);
        return view('pages.master-jenis-pekerjaan.edit', compact('masterJenisPekerjaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterJenisPekerjaan  $masterJenisPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterJenisPekerjaan = MasterJenisPekerjaan::findOrFail($id);
        $masterJenisPekerjaan->update($requestData);

        activity('master_jenis_pekerjaan')
            ->performedOn($masterJenisPekerjaan)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-jenis-pekerjaan')->with('flash_message', 'Master Jenis Pekerjaan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterJenisPekerjaan  $masterJenisPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterJenisPekerjaan = MasterJenisPekerjaan::find($id);
        MasterJenisPekerjaan::destroy($id);
        activity('master_jenis_pekerjaanm')
            ->performedOn($masterJenisPekerjaan)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-jenis-pekerjaan')->with('flash_message', 'Master Master Jenis Pekerjaan deleted!');
    }
}
