<?php

namespace App\Http\Controllers;

use App\Models\MasterBagian;
use App\Models\MasterDepartemen;
use Auth;
use Illuminate\Http\Request;
use Validator;

class MasterBagianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterbagian = MasterBagian::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterbagian = MasterBagian::latest()->paginate($perPage);
        }

        return view('pages.master-bagian.index', compact('masterbagian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departemen = MasterDepartemen::where('status', 1)->get();
        return view('pages.master-bagian.create', compact('departemen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterbagian = MasterBagian::create($requestData);

        activity('master_bagian')
            ->performedOn($masterbagian)
            ->causedBy(Auth::user())
            ->log('created');

        return redirect('master-bagian')->with('flash_message', 'Master Bagian added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterBagian  $masterBagian
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterbagian = MasterBagian::findOrFail($id);
        $master_departeman =  MasterDepartemen::where('id', $masterbagian->master_departemens_id)->first();
        return view('pages.master-bagian.show', compact('masterbagian', 'master_departeman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterBagian  $masterBagian
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterbagian = MasterBagian::findOrFail($id);
        $departemen = MasterDepartemen::where('status', 1)->get();
        return view('pages.master-bagian.edit', compact('masterbagian', 'departemen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterBagian  $masterBagian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        
        $masterbagian = MasterBagian::findOrFail($id);
        $masterbagian->update($requestData);

        activity('master_bagian')
            ->performedOn($masterbagian)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-bagian')->with('flash_message', 'Master Bagian updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterBagian  $masterBagian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterbagian = MasterBagian::find($id);
        MasterBagian::destroy($id);

        
        activity('master_bagian')
            ->performedOn($masterbagian)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-bagian')->with('flash_message', 'Master Bagian deleted!');
    }
}
