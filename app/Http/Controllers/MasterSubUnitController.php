<?php

namespace App\Http\Controllers;

use App\Models\MasterSubUnit;
use App\Models\MasterUnit;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterSubUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterSubUnit = MasterSubUnit::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterSubUnit = MasterSubUnit::orderBy('name', 'asc')->get();
        }
        return view('pages.master-sub-unit.index', compact('masterSubUnit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unit = MasterUnit::where('status', 1)->get();
        return view('pages.master-sub-unit.create', compact('unit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterUnit = MasterSubUnit::create($requestData);

        activity('master_sub_unit')
            ->performedOn($masterUnit)
            ->causedBy(Auth::user())
            ->log('created');

        return redirect('master-sub-unit')->with('flash_message', 'Master Unit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterSubUnit  $masterSubUnit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterSubUnit = MasterSubUnit::findOrFail($id);
        $masterUnit =  MasterUnit::where('id', $masterSubUnit->master_units_id)->first();
        return view('pages.master-sub-unit.show', compact('masterSubUnit', 'masterUnit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterSubUnit  $masterSubUnit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterSubUnit = MasterSubUnit::findOrFail($id);
        $unit = MasterUnit::where('status', 1)->get();
        return view('pages.master-sub-unit.edit', compact('masterSubUnit', 'unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterSubUnit  $masterSubUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'master_units_id' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterSubUnit = MasterSubUnit::findOrFail($id);
        $masterSubUnit->update($requestData);

        activity('master_sub_unit')
            ->performedOn($masterSubUnit)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-sub-unit')->with('flash_message', 'Master Sub Unit updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterSubUnit  $masterSubUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterSubUnit = MasterSubUnit::find($id);
        MasterSubUnit::destroy($id);
        
        activity('master_sub_unit')
            ->performedOn($masterSubUnit)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-sub-unit')->with('flash_message', 'Master Sub Unit deleted!');
    }
}
