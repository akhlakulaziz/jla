<?php

namespace App\Http\Controllers;

use App\Models\MasterLokasi;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterLokasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterlokasi = MasterLokasi::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterlokasi = MasterLokasi::orderBy('name', 'asc')->get();
        }
        return view('pages.master-lokasi.index', compact('masterlokasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-lokasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterlokasi = MasterLokasi::create($requestData);
        activity('master_lokasi')
            ->performedOn($masterlokasi)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-lokasi')->with('flash_message', 'Master Lokasi added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterLokasi  $masterLokasi
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterlokasi = MasterLokasi::findOrFail($id);
        return view('pages.master-lokasi.show', compact('masterlokasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterLokasi  $masterLokasi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterlokasi = MasterLokasi::findOrFail($id);
        return view('pages.master-lokasi.edit', compact('masterlokasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterLokasi  $masterLokasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterlokasi = MasterLokasi::findOrFail($id);
        $masterlokasi->update($requestData);

        activity('master_lokasi')
            ->performedOn($masterlokasi)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-lokasi')->with('flash_message', 'Master Lokasi updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterLokasi  $masterLokasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterlokasi = MasterLokasi::find($id);
        MasterLokasi::destroy($id);
        activity('master_lokasi')
            ->performedOn($masterlokasi)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-lokasi')->with('flash_message', 'Master Master Lokasi deleted!');
    }
}
