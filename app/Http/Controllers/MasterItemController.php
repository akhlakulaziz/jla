<?php

namespace App\Http\Controllers;

use App\Models\MasterItem;
use Illuminate\Http\Request;
use Validator;
use Auth;

class MasterItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masteritem = MasterItem::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masteritem = MasterItem::orderBy('name', 'asc')->get();
        }
        return view('pages.master-item.index', compact('masteritem'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masteritem = MasterItem::create($requestData);
        activity('master_item')
            ->performedOn($masteritem)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-item')->with('flash_message', 'Master Item added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterItem  $masterItem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masteritem = MasterItem::findOrFail($id);
        return view('pages.master-item.show', compact('masteritem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterItem  $masterItem
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masteritem = MasterItem::findOrFail($id);
        return view('pages.master-item.edit', compact('masteritem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterItem  $masterItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masteritem = MasterItem::findOrFail($id);
        $masteritem->update($requestData);

        activity('master_item')
            ->performedOn($masteritem)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-item')->with('flash_message', 'Master Item updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterItem  $masterItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masteritem = MasterItem::find($id);
        MasterItem::destroy($id);
        activity('master_item')
            ->performedOn($masteritem)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-item')->with('flash_message', 'Master Master item deleted!');
    }
}
