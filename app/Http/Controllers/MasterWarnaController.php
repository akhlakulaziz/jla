<?php

namespace App\Http\Controllers;

use App\Models\MasterWarna;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterWarnaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterWarna = MasterWarna::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterWarna = MasterWarna::orderBy('name', 'asc')->get();
        }
        return view('pages.master-warna.index', compact('masterWarna'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-warna.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterWarna = MasterWarna::create($requestData);
        activity('master_warna')
            ->performedOn($masterWarna)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-warna')->with('flash_message', 'Master Warna added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterWarna  $masterWarna
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterWarna = MasterWarna::findOrFail($id);
        return view('pages.master-warna.show', compact('masterWarna'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterWarna  $masterWarna
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterWarna = MasterWarna::findOrFail($id);
        return view('pages.master-warna.edit', compact('masterWarna'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterWarna  $masterWarna
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterWarna = MasterWarna::findOrFail($id);
        $masterWarna->update($requestData);

        activity('master_warna')
            ->performedOn($masterWarna)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-warna')->with('flash_message', 'Master Warna updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterWarna  $masterWarna
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterWarna = MasterWarna::find($id);
        MasterWarna::destroy($id);
        activity('master_warna')
            ->performedOn($masterWarna)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-warna')->with('flash_message', 'Master Master Warna deleted!');
    }
}
