<?php

namespace App\Http\Controllers;

use App\Models\MasterProduct;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterProduct = MasterProduct::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterProduct = MasterProduct::orderBy('name', 'asc')->get();
        }
        return view('pages.master-product.index', compact('masterProduct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterProduct = MasterProduct::create($requestData);
        activity('master_product')
            ->performedOn($masterProduct)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-product')->with('flash_message', 'Master Product added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterProduct  $masterProduct
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterProduct = MasterProduct::findOrFail($id);
        return view('pages.master-product.show', compact('masterProduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterProduct  $masterProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterProduct = MasterProduct::findOrFail($id);
        return view('pages.master-product.edit', compact('masterProduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterProduct  $masterProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterProduct = MasterProduct::findOrFail($id);
        $masterProduct->update($requestData);

        activity('master_product')
            ->performedOn($masterProduct)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-product')->with('flash_message', 'Master Product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterProduct  $masterProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterProduct = MasterProduct::find($id);
        MasterProduct::destroy($id);
        activity('master_product')
            ->performedOn($masterProduct)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-product')->with('flash_message', 'Master Master Product deleted!');
    }
}
