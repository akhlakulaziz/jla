<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
class ActivityLogController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $activitys = Activity::where('log_name', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $activitys = Activity::latest()->paginate($perPage);
        }
       
        return view('pages.activity.index', compact('activitys'));
    }
}
