<?php

namespace App\Http\Controllers;

use App\Models\MasterKualitas;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterKualitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterKualitas = MasterKualitas::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterKualitas = MasterKualitas::orderBy('name', 'asc')->get();
        }
        return view('pages.master-kualitas.index', compact('masterKualitas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-kualitas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterKualitas = MasterKualitas::create($requestData);
        activity('master_kualitas')
            ->performedOn($masterKualitas)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-kualitas')->with('flash_message', 'Master Kualitas added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterKualitas  $masterKualitas
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterKualitas = MasterKualitas::findOrFail($id);
        return view('pages.master-kualitas.show', compact('masterKualitas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterKualitas  $masterKualitas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterKualitas = MasterKualitas::findOrFail($id);
        return view('pages.master-kualitas.edit', compact('masterKualitas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterKualitas  $masterKualitas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterKualitas = MasterKualitas::findOrFail($id);
        $masterKualitas->update($requestData);

        activity('master_kualitas')
            ->performedOn($masterKualitas)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-kualitas')->with('flash_message', 'Master Kualitas updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterKualitas  $masterKualitas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterKualitas = MasterKualitas::find($id);
        MasterKualitas::destroy($id);
        activity('master_kualitas')
            ->performedOn($masterKualitas)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-kualitas')->with('flash_message', 'Master Master Kualitas deleted!');
    }
}
