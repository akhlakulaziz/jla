<?php

namespace App\Http\Controllers;

use App\Models\MasterPemborong;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterPemborongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterPemborong = MasterPemborong::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterPemborong = MasterPemborong::orderBy('name', 'asc')->get();
        }
        return view('pages.master-pemborong.index', compact('masterPemborong'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-pemborong.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterPemborong = MasterPemborong::create($requestData);
        activity('master_pemborong')
            ->performedOn($masterPemborong)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-pemborong')->with('flash_message', 'Master pemborong added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterPemborong  $masterPemborong
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterPemborong = MasterPemborong::findOrFail($id);
        return view('pages.master-pemborong.show', compact('masterPemborong'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterPemborong  $masterPemborong
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterPemborong = MasterPemborong::findOrFail($id);
        return view('pages.master-pemborong.edit', compact('masterPemborong'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterPemborong  $masterPemborong
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterPemborong = MasterPemborong::findOrFail($id);
        $masterPemborong->update($requestData);

        activity('master_pemborong')
            ->performedOn($masterPemborong)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-pemborong')->with('flash_message', 'Master Pemborong updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterPemborong  $masterPemborong
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterPemborong = MasterPemborong::find($id);
        MasterPemborong::destroy($id);
        activity('master_pemborong')
            ->performedOn($masterPemborong)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-pemborong')->with('flash_message', 'Master Master Pemborong deleted!');
    }
}
