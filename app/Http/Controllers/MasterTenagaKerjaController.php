<?php

namespace App\Http\Controllers;

use App\Models\MasterTenagaKerja;
use App\Models\MasterStatusTenagaKerja;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterTenagaKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterTenagaKerja = MasterTenagaKerja::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterTenagaKerja = MasterTenagaKerja::orderBy('name', 'asc')->get();
        }
        return view('pages.master-tenaga-kerja.index', compact('masterTenagaKerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $master_status_tenaga_kerjas_id = MasterStatusTenagaKerja::all();
        return view('pages.master-tenaga-kerja.create', compact('master_status_tenaga_kerjas_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterTenagaKerja = MasterTenagaKerja::create($requestData);
        activity('master_tenaga_kerja')
            ->performedOn($masterTenagaKerja)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-tenaga-kerja')->with('flash_message', 'Master Tenaga Kerja added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterTenagaKerja  $masterTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterTenagaKerja = MasterTenagaKerja::findOrFail($id);
        return view('pages.master-tenaga-kerja.show', compact('masterTenagaKerja'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterTenagaKerja  $masterTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterTenagaKerja = MasterTenagaKerja::findOrFail($id);
        $master_status_tenaga_kerjas_id = MasterStatusTenagaKerja::all();
        return view('pages.master-tenaga-kerja.edit', compact('masterTenagaKerja', 'master_status_tenaga_kerjas_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterTenagaKerja  $masterTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterTenagaKerja = MasterTenagaKerja::findOrFail($id);
        $masterTenagaKerja->update($requestData);

        activity('master_tenaga_kerja')
            ->performedOn($masterTenagaKerja)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-tenaga-kerja')->with('flash_message', 'Master Tenaga Kerja updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterTenagaKerja  $masterTenagaKerja
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterTenagaKerja = MasterTenagaKerja::find($id);
        MasterTenagaKerja::destroy($id);
        activity('master_tenaga_kerja')
            ->performedOn($masterTenagaKerja)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-tenaga-kerja')->with('flash_message', 'Master Master Tenaga Kerja deleted!');
    }
}
