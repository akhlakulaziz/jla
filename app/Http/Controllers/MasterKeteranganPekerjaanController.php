<?php

namespace App\Http\Controllers;

use App\Models\MasterKeteranganPekerjaan;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterKeteranganPekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterKeteranganPekerjaan = MasterKeteranganPekerjaan::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterKeteranganPekerjaan = MasterKeteranganPekerjaan::orderBy('name', 'asc')->get();
        }
        return view('pages.master-keterangan-pekerjaan.index', compact('masterKeteranganPekerjaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-keterangan-pekerjaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterKeteranganPekerjaan = MasterKeteranganPekerjaan::create($requestData);
        activity('master_keterangan_pekerjaan')
            ->performedOn($masterKeteranganPekerjaan)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-keterangan-pekerjaan')->with('flash_message', 'Master Keterangan Pekerjaan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterKeteranganPekerjaan  $masterKeteranganPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterKeteranganPekerjaan = MasterKeteranganPekerjaan::findOrFail($id);
        return view('pages.master-keterangan-pekerjaan.show', compact('masterKeteranganPekerjaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterKeteranganPekerjaan  $masterKeteranganPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterKeteranganPekerjaan = MasterKeteranganPekerjaan::findOrFail($id);
        return view('pages.master-keterangan-pekerjaan.edit', compact('masterKeteranganPekerjaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterKeteranganPekerjaan  $masterKeteranganPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterKeteranganPekerjaan = MasterKeteranganPekerjaan::findOrFail($id);
        $masterKeteranganPekerjaan->update($requestData);

        activity('master_keterangan_pekerjaan')
            ->performedOn($masterKeteranganPekerjaan)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-keterangan-pekerjaan')->with('flash_message', 'Master Keterangan Pekerjaan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterKeteranganPekerjaan  $masterKeteranganPekerjaan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterKeteranganPekerjaan = MasterKeteranganPekerjaan::find($id);
        MasterKeteranganPekerjaan::destroy($id);
        activity('master_keterangan-pekerjaan')
            ->performedOn($masterKeteranganPekerjaan)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-keterangan-pekerjaan')->with('flash_message', 'Master Master Keterangan Pekerjaan deleted!');
    }
}
