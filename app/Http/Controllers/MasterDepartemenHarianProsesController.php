<?php

namespace App\Http\Controllers;

use App\Models\MasterDepartemenHarianProses;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterDepartemenHarianProsesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterDepartemenHarianProses = MasterDepartemenHarianProses::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterDepartemenHarianProses = MasterDepartemenHarianProses::orderBy('name', 'asc')->get();
        }
        return view('pages.master-departemen-harian-proses.index', compact('masterDepartemenHarianProses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-departemen-harian-proses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterDepartemenHarianProses = MasterDepartemenHarianProses::create($requestData);
        activity('master_departmen_harian_proses')
            ->performedOn($masterDepartemenHarianProses)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-departemen-harian-proses')->with('flash_message', 'Master Departemen added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterDepartemenHarianProses  $masterDepartemenHarianProses
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterDepartemenHarianProses = MasterDepartemenHarianProses::findOrFail($id);
        return view('pages.master-departemen-harian-proses.show', compact('masterDepartemenHarianProses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterDepartemenHarianProses  $masterDepartemenHarianProses
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterDepartemenHarianProses = MasterDepartemenHarianProses::findOrFail($id);
        return view('pages.master-departemen-harian-proses.edit', compact('masterDepartemenHarianProses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterDepartemenHarianProses  $masterDepartemenHarianProses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterDepartemenHarianProses = MasterDepartemenHarianProses::findOrFail($id);
        $masterDepartemenHarianProses->update($requestData);

        activity('master_departmen_harian_proses')
            ->performedOn($masterDepartemenHarianProses)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-departemen-harian-proses')->with('flash_message', 'Master Departemen updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterDepartemenHarianProses  $masterDepartemenHarianProses
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterDepartemenHarianProses = MasterDepartemenHarianProses::find($id);
        MasterDepartemenHarianProses::destroy($id);
        activity('master_departemen_harian_proses')
            ->performedOn($masterDepartemenHarianProses)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-departemen-harian-proses')->with('flash_message', 'Master Master Departemen deleted!');
    }
}
