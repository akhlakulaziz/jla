<?php

namespace App\Http\Controllers;

use App\Models\MasterKeteranganProses;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterKeteranganProsesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterKeteranganProses = MasterKeteranganProses::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterKeteranganProses = MasterKeteranganProses::orderBy('name', 'asc')->get();
        }
        return view('pages.master-keterangan-proses.index', compact('masterKeteranganProses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-keterangan-proses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterKeteranganProses = MasterKeteranganProses::create($requestData);
        activity('master_keterangan_proses')
            ->performedOn($masterKeteranganProses)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-keterangan-proses')->with('flash_message', 'Master Keterangan Proses added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterKeteranganProses  $masterKeteranganProses
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterKeteranganProses = MasterKeteranganProses::findOrFail($id);
        return view('pages.master-keterangan-proses.show', compact('masterKeteranganProses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterKeteranganProses  $masterKeteranganProses
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterKeteranganProses = MasterKeteranganProses::findOrFail($id);
        return view('pages.master-keterangan-proses.edit', compact('masterKeteranganProses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterKeteranganProses  $masterKeteranganProses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterKeteranganProses = MasterKeteranganProses::findOrFail($id);
        $masterKeteranganProses->update($requestData);

        activity('master_keterangan_proses')
            ->performedOn($masterKeteranganProses)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-keterangan-proses')->with('flash_message', 'Master Keterangan Proses updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterKeteranganProses  $masterKeteranganProses
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterKeteranganProses = MasterKeteranganProses::find($id);
        MasterKeteranganProses::destroy($id);
        activity('master_keterangan_proses')
            ->performedOn($masterKeteranganProses)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-keterangan-proses')->with('flash_message', 'Master Master Keterangan Proses deleted!');
    }
}
