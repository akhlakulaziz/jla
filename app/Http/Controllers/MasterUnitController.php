<?php

namespace App\Http\Controllers;

use App\Models\MasterDepartemenHarianProses;
use App\Models\MasterUnit;
use App\Models\MasterLokasi;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterUnit = MasterUnit::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterUnit = MasterUnit::orderBy('name', 'asc')->get();
        }
        return view('pages.master-unit.index', compact('masterUnit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departemen = MasterDepartemenHarianProses::where('status', 1)->get();
        $lokasi = MasterLokasi::where('status', 1)->get();
        return view('pages.master-unit.create', compact('departemen', 'lokasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'master_departemen_harian_proses_id' => 'required',
            'master_lokasis_id' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterUnit = MasterUnit::create($requestData);

        activity('master_unit')
            ->performedOn($masterUnit)
            ->causedBy(Auth::user())
            ->log('created');

        return redirect('master-unit')->with('flash_message', 'Master Unit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterUnit  $masterUnit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterUnit = MasterUnit::findOrFail($id);
        $master_departeman =  MasterDepartemenHarianProses::where('id', $masterUnit->master_departemen_harian_proses_id)->first();
        $master_lokasi =  MasterLokasi::where('id', $masterUnit->master_departemen_harian_proses_id)->first();
        return view('pages.master-unit.show', compact('masterUnit', 'master_departeman', 'master_lokasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterUnit  $masterUnit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterUnit = MasterUnit::findOrFail($id);
        $departemen = MasterDepartemenHarianProses::where('status', 1)->get();
        $lokasi = MasterLokasi::where('status', 1)->get();
        return view('pages.master-unit.edit', compact('masterUnit', 'departemen', 'lokasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterUnit  $masterUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'master_departemen_harian_proses_id' => 'required',
            'master_lokasis_id' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterUnit = MasterUnit::findOrFail($id);
        $masterUnit->update($requestData);

        activity('master_unit')
            ->performedOn($masterUnit)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-unit')->with('flash_message', 'Master Unit updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterUnit  $masterUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterUnit = MasterUnit::find($id);
        MasterUnit::destroy($id);
        
        activity('master_unit')
            ->performedOn($masterUnit)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-unit')->with('flash_message', 'Master Unit deleted!');
    }
}
