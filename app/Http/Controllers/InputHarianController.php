<?php

namespace App\Http\Controllers;

use App\Models\InputHarian;
use App\Models\Item;
use App\Models\MasterDepartemenHarianProses;
use App\Models\MasterUnit;
use App\Models\MasterSubUnit;
use App\Models\MasterSupplier;
use App\Models\MasterNoPo;
use App\Models\MasterProduct;
use App\Models\MasterPemborong;
use App\Models\MasterShift;
use App\Models\MasterItem;
use App\Models\MasterKeteranganBahan;
use App\Models\MasterTypeBahan;
use App\Models\MasterWarna;
use App\Models\MasterGroupKualitas;
use App\Models\MasterKualitas;
use App\Models\MasterJenisKayu;
use App\Models\MasterKeteranganProses;

use App\Models\InputHarianIn;
use App\Models\InputHarianOut;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Response;
use Validator;

class InputHarianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request->all());
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $items = Item::whereHas('item', function ($query) use ($keyword){
                $query->where('name', 'like', '%'.$keyword.'%');
            })
            ->with(['item' => function($query) use ($keyword){
                $query->where('name', 'like', '%'.$keyword.'%');
            }])->latest()->paginate($perPage);

            // $item = Item::where('no_part_item', 'LIKE', "%$keyword%")
            //     ->latest()->paginate($perPage);
        } else {
            $items = Item::where('status', 1)->latest()->paginate($perPage);
        }
        $departemen = MasterDepartemenHarianProses::where('status', 1)->get();
        $unit = MasterUnit::where('status', 1)->get();
        $subunit = MasterSubUnit::where('status', 1)->get();
        return view('pages.harian-proses.input-harian.index',compact('items', 'departemen', 'unit', 'subunit'));
    }

    public function history(Request $request)
    {
        // dd($request->all());
        $rules =array(
            'tanggal' => 'required',
            'departemen' => 'required',
            'unit' => 'required',
            'subunit' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }

        $tanggal = $request->tanggal;
        $departemen = $request->departemen;
        $unit = $request->unit;
        $subunit = $request->subunit;
        $perPage = 25;
        $ins = InputHarianIn::where('tanggal', $request->tanggal)->where('departemen', $request->departemen)->where('unit', $request->unit)->where('sub_unit', $request->subunit)->latest()->paginate($perPage);
        $outs = InputHarianOut::where('tanggal', $request->tanggal)->where('departemen', $request->departemen)->where('unit', $request->unit)->where('sub_unit', $request->subunit)->latest()->paginate($perPage);
        return view('pages.harian-proses.input-harian.history',compact('ins', 'outs', 'tanggal', 'departemen', 'unit', 'subunit'));
    }

    public function list_item(Request $request)
    {
        $tanggal = $request->tanggal;
        $departemen = $request->departemen;
        $unit = $request->unit;
        $subunit = $request->subunit;
        $items = Item::where('status', 1)->get();
        return view('pages.harian-proses.input-harian.list-item',compact('items', 'tanggal', 'departemen', 'unit', 'subunit'));
    }

    public function input_harian(Request $request)
    {
        $rules =array(
            'in' => 'required',
            'out' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        
        $tanggal = $request->tanggal;
        // $select_departemen = $request->departemen;
        $select_departemen = MasterDepartemenHarianProses::where('id', $request->departemen)->first();
        // $select_unit = $request->unit;
        $select_unit = MasterUnit::where('id', $request->unit)->first();
        // $select_subunit = $request->subunit;
        $select_subunit = MasterSubUnit::where('id', $request->subunit)->first();

        foreach($request->in as $key=> $item)
        {
            $array[] = $item; 
        }
        $ins = Item::findMany($array);

        foreach($request->out as $key=> $item)
        {
            $array2[] = $item; 
        }
        $items = Item::where('status', 1)->get();
        $ins = Item::findMany($array);
        $count_ins = Item::findMany($array)->count();
        $start_array_in = $count_ins - 1;
        // dd($start_array_in);
        $outs = Item::findMany($array2);

        // dd($outs);
        $departemen = MasterDepartemenHarianProses::where('status', 1)->get();
        $unit = MasterUnit::where('status', 1)->get();

        $subunit = MasterSubUnit::where('status', 1)->get();
        $master_item = MasterItem::where('status', 1)->get();
        $ket_bahan = MasterKeteranganBahan::where('status', 1)->get();
        $type_bahan = MasterTypeBahan::where('status', 1)->get();
        // $diameter = 
        // $tinggi = 
        // $lebar = 
        // $panjang = 
        $warna = MasterWarna::where('status', 1)->get();
        $group_kualitas = MasterGroupKualitas::where('status', 1)->get();
        $kualitas = MasterKualitas::where('status', 1)->get();
        $jenis_kayu = MasterJenisKayu::where('status', 1)->get();
        $ket_proses = MasterKeteranganProses::where('status', 1)->get();
                
        // dd($master_item);
        $supplier = MasterSupplier::where('status', 1)->get();
        $product = MasterProduct::where('status', 1)->get();
        $no_po = MasterNoPo::where('status', 1)->get();
        $pemborong = MasterPemborong::where('status', 1)->get();
        $shift = MasterShift::where('status', 1)->get();

        return view('pages.harian-proses.input-harian.input',compact('items', 'ins', 'start_array_in', 'outs', 'departemen', 'unit', 'subunit', 'master_item', 'ket_bahan', 'type_bahan', 'warna', 'group_kualitas', 'kualitas', 'jenis_kayu', 'ket_proses', 'tanggal', 'select_departemen', 'select_unit', 'select_subunit', 'supplier', 'product', 'no_po', 'pemborong', 'shift'));

    }
    public function json_departemen(Request $request)
    {
        $items = MasterUnit::where('status', 1)->where('master_departemen_harian_proses_id', $request->id)->pluck('name', 'id');
        $count = MasterUnit::where('status', 1)->where('master_departemen_harian_proses_id', $request->id)->count();
        // $result = array( 'items' => $items, 'count' => $count );
        return Response::json($items);
        
    }
    public function json_unit(Request $request)
    {
        $items = MasterSubUnit::where('status', 1)->where('master_units_id', $request->id)->pluck('name', 'id');
        return Response::json($items);
        
    }
    public function json_item(Request $request)
    {
        // $items = Item::where('status', 1)->where('id', 1)->get();
        $items = Item::where('status', 1)->where('id', $request->no_part_item_in)->get();
        foreach ( $items as $item) 
        {
            // dd($item->id);
            $data = array('id' => $item->id,'no_part_item' => $item->no_part_item,'master_items_id' => $item->master_items_id,'master_keterangan_bahans_id' => $item->master_keterangan_bahans_id,'master_type_bahans_id' => $item->master_type_bahans_id,'diameter' => $item->diameter,'tinggi' => $item->tinggi,'lebar' => $item->lebar,'panjang' => $item->panjang,'master_warnas_id' => $item->master_warnas_id,'master_group_kualitas_id' => $item->master_group_kualitas_id,'master_kualitas_id' => $item->master_kualitas_id,'master_jenis_kayus_id' => $item->master_jenis_kayus_id,'master_keterangan_proses_id' => $item->master_keterangan_proses_id,'status' => $item->status);
            break;
        }
        return Response::json($data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
        //input harian in
        $array = 0;
        $ot = 1;
        // $tahun = Carbon::parse($item);
        //     dd($tahun->format('Y'));
        foreach($request->tanggal_in as $item){
            $in = new InputHarianIn();
            $in->tanggal = $request->tanggal_in[$array];
            $in->no_process = $request->no_process_in[$array];
            $in->departemen = $request->departemen_in[$array];
            $in->unit = $request->unit_in[$array];
            $in->sub_unit = $request->subunit_in[$array];
            $in->no_part_item = $request->no_part_item_in[$array];
            $in->item = $request->item_in[$array];
            $in->shift = $request->shift_in[$array];
            $in->keterangan_bahan = $request->keterangan_bahan_in[$array];
            $in->type_bahan = $request->type_bahan_in[$array];
            $in->jenis_kayu = $request->jenis_kayu_in[$array];
            $in->group_kualitas = $request->group_kualitas_in[$array];
            $in->kualitas = $request->kualitas_in[$array];
            $in->warna = $request->warna_in[$array];
            $in->diameter = $request->diameter_in[$array];
            $in->keterangan_proses = $request->keterangan_proses_in[$array];
            $in->tinggi = $request->tinggi_in[$array];
            $in->lebar = $request->lebar_in[$array];
            $in->panjang = $request->panjang_in[$array];
            $in->pcs = $request->pcs_in[$array];
            $in->m1 = ($request->panjang_in[$array]*$request->pcs_in[$array])/1000;
            $in->m2 = ($request->panjang_in[$array]*$request->lebar_in[$array]*$request->pcs_in[$array])/1000000;
            $in->m3 = ($request->panjang_in[$array]*$request->lebar_in[$array]*$request->tinggi_in[$array]*$request->pcs_in[$array])/1000000000;
            $in->master_suppliers_id = $request->supplier_in[$array];
            $in->no_kiriman = $request->no_kiriman_in[$array];
            $in->master_products_id = $request->po_in[$array];
            $in->master_no_pos_id = $request->no_po_in[$array];
            $in->no_pallet = $request->no_pallet_in[$array];
            $in->nampan = $request->nampan_in[$array];
            $in->operator = $request->operator_in[$array];
            $in->harian_tetap = $request->harian_tetap_in[$array];
            $in->harian_lepas = $request->harian_lepas_in[$array];
            $in->borong = $request->borong_in[$array];
            $in->master_pemborongs_id = $request->pemborong_in[$array];
            $in->memo = $request->memo_in[$array];
            $in->ot = $request->ot_in.$ot;
            $tahun = Carbon::parse($item);
            $in->tahun = $tahun->format('Y');
            $in->keterangan = $request->keterangan_in[$array];
            $in->save();

            $array = $array + 1;
            $ot = $ot + 1;

            activity('input_harian')
            ->performedOn($in)
            ->causedBy(Auth::user())
            ->log('created');
        }

        //input harian out
        $array2 = 0;
        $ot2 = 1;
        // $tahun = Carbon::parse($item);
        //     dd($tahun->format('Y'));
        foreach($request->tanggal_out as $item){
            $out = new InputHarianOut();
            $out->tanggal = $request->tanggal_out[$array2];
            $out->no_process = $request->no_process_out[$array2];
            $out->departemen = $request->departemen_out[$array2];
            $out->unit = $request->unit_out[$array2];
            $out->sub_unit = $request->subunit_out[$array2];
            $out->no_part_item = $request->no_part_item_out[$array2];
            $out->item = $request->item_out[$array2];
            $out->shift = $request->shift_out[$array2];
            $out->keterangan_bahan = $request->keterangan_bahan_out[$array2];
            $out->type_bahan = $request->type_bahan_out[$array2];
            $out->jenis_kayu = $request->jenis_kayu_out[$array2];
            $out->group_kualitas = $request->group_kualitas_out[$array2];
            $out->kualitas = $request->kualitas_out[$array2];
            $out->warna = $request->warna_out[$array2];
            $out->diameter = $request->diameter_out[$array2];
            $out->keterangan_proses = $request->keterangan_proses_out[$array2];
            $out->tinggi = $request->tinggi_out[$array2];
            $out->lebar = $request->lebar_out[$array2];
            $out->panjang = $request->panjang_out[$array2];
            $out->pcs = $request->pcs_out[$array2];
            $out->m1 = ($request->panjang_out[$array2]*$request->pcs_out[$array2])/1000;
            $out->m2 = ($request->panjang_out[$array2]*$request->lebar_out[$array2]*$request->pcs_out[$array2])/1000000;
            $out->m3 = ($request->panjang_out[$array2]*$request->lebar_out[$array2]*$request->tinggi_out[$array2]*$request->pcs_out[$array2])/1000000000;
            $out->master_suppliers_id = $request->supplier_out[$array2];
            $out->no_kiriman = $request->no_kiriman_out[$array2];
            $out->master_products_id = $request->po_out[$array2];
            $out->master_no_pos_id = $request->no_po_out[$array2];
            $out->no_pallet = $request->no_pallet_out[$array2];
            $out->nampan = $request->nampan_out[$array2];
            $out->operator = $request->operator_out[$array2];
            $out->harian_tetap = $request->harian_tetap_out[$array2];
            $out->harian_lepas = $request->harian_lepas_out[$array2];
            $out->borong = $request->borong_out[$array2];
            $out->master_pemborongs_id = $request->pemborong_out[$array2];
            $out->memo = $request->memo_out[$array2];
            $out->ot = $request->ot_out.$ot2;
            $tahun = Carbon::parse($item);
            $out->tahun = $tahun->format('Y');
            $out->keterangan = $request->keterangan_out[$array2];
            $out->save();

            $array2 = $array2 + 1;
            $ot2 = $ot2 + 1;
            activity('input_harian')
            ->performedOn($out)
            ->causedBy(Auth::user())
            ->log('created');
        }
        
        return redirect('input-harian')->with('flash_message', 'Data Telah Berhasil Di input!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InputHarian  $inputHarian
     * @return \Illuminate\Http\Response
     */
    public function show(InputHarian $inputHarian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InputHarian  $inputHarian
     * @return \Illuminate\Http\Response
     */
    public function edit(InputHarian $inputHarian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InputHarian  $inputHarian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InputHarian $inputHarian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InputHarian  $inputHarian
     * @return \Illuminate\Http\Response
     */
    public function destroy(InputHarian $inputHarian)
    {
        //
    }
}
