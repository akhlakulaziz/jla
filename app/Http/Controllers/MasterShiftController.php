<?php

namespace App\Http\Controllers;

use App\Models\MasterShift;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterShift = MasterShift::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterShift = MasterShift::orderBy('name', 'asc')->get();
        }
        return view('pages.master-shift.index', compact('masterShift'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-shift.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterShift = MasterShift::create($requestData);
        activity('master_shift')
            ->performedOn($masterShift)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-shift')->with('flash_message', 'Master Shift added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterShift  $masterShift
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterShift = MasterShift::findOrFail($id);
        return view('pages.master-shift.show', compact('masterShift'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterShift  $masterShift
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterShift = MasterShift::findOrFail($id);
        return view('pages.master-shift.edit', compact('masterShift'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterShift  $masterShift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterShift = MasterShift::findOrFail($id);
        $masterShift->update($requestData);

        activity('master_shift')
            ->performedOn($masterShift)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-shift')->with('flash_message', 'Master Shift updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterShift  $masterShift
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterShift = MasterShift::find($id);
        MasterShift::destroy($id);
        activity('master_shift')
            ->performedOn($masterShift)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-shift')->with('flash_message', 'Master Master Shift deleted!');
    }
}
