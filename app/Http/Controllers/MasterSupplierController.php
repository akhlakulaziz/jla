<?php

namespace App\Http\Controllers;

use App\Models\MasterSupplier;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterSupplier = MasterSupplier::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterSupplier = MasterSupplier::orderBy('name', 'asc')->get();
        }
        return view('pages.master-supplier.index', compact('masterSupplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterSupplier = MasterSupplier::create($requestData);
        activity('master_supplier')
            ->performedOn($masterSupplier)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-supplier')->with('flash_message', 'Master Supplier added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterSupplier  $masterSupplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterSupplier = MasterSupplier::findOrFail($id);
        return view('pages.master-supplier.show', compact('masterSupplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterSupplier  $masterSupplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterSupplier = MasterSupplier::findOrFail($id);
        return view('pages.master-supplier.edit', compact('masterSupplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterSupplier  $masterSupplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterSupplier = MasterSupplier::findOrFail($id);
        $masterSupplier->update($requestData);

        activity('master_supplier')
            ->performedOn($masterSupplier)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-supplier')->with('flash_message', 'Master Supplier updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterSupplier  $masterSupplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterSupplier = MasterSupplier::find($id);
        MasterSupplier::destroy($id);
        activity('master_supplier')
            ->performedOn($masterSupplier)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-supplier')->with('flash_message', 'Master Master Supplier deleted!');
    }
}
