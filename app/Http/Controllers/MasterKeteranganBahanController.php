<?php

namespace App\Http\Controllers;

use App\Models\MasterKeteranganBahan;
use Illuminate\Http\Request;
use Auth;
use Validator;

class MasterKeteranganBahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterketeranganbahan = MasterKeteranganBahan::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterketeranganbahan = MasterKeteranganBahan::orderBy('name', 'asc')->get();
        }
        // dd("halo");
        return view('pages.master-keterangan-bahan.index', compact('masterketeranganbahan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-keterangan-bahan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterketeranganbahan = MasterKeteranganBahan::create($requestData);
        activity('master_keterangan-bahan')
            ->performedOn($masterketeranganbahan)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-keterangan-bahan')->with('flash_message', 'Master Keterangan Bahan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterKeteranganBahan  $masterKeteranganBahan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterketeranganbahan = MasterKeteranganBahan::findOrFail($id);
        return view('pages.master-keterangan-bahan.show', compact('masterketeranganbahan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterKeteranganBahan  $masterKeteranganBahan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterketeranganbahan = MasterKeteranganBahan::findOrFail($id);
        return view('pages.master-keterangan-bahan.edit', compact('masterketeranganbahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterKeteranganBahan  $masterKeteranganBahan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterketeranganbahan = MasterKeteranganBahan::findOrFail($id);
        $masterketeranganbahan->update($requestData);

        activity('master_keterangan-bahan')
            ->performedOn($masterketeranganbahan)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-keterangan-bahan')->with('flash_message', 'Master Keterangan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterKeteranganBahan  $masterKeteranganBahan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterketeranganbahan = MasterKeteranganBahan::find($id);
        MasterKeteranganBahan::destroy($id);
        activity('master_keterangan-bahan')
            ->performedOn($masterketeranganbahan)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-keterangan-bahan')->with('flash_message', 'Master Keterangan bahan deleted!');
    }
}
