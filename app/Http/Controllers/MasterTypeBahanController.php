<?php

namespace App\Http\Controllers;

use App\Models\MasterTypeBahan;
use Illuminate\Http\Request;
use Auth;
use Validator;


class MasterTypeBahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $masterTypeBahan = MasterTypeBahan::where('name', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $masterTypeBahan = MasterTypeBahan::orderBy('name', 'asc')->get();
        }
        return view('pages.master-type-bahan.index', compact('masterTypeBahan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master-type-bahan.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        $masterTypeBahan = MasterTypeBahan::create($requestData);
        activity('master_type_bahan')
            ->performedOn($masterTypeBahan)
            ->causedBy(Auth::user())
            ->log('created');
        return redirect('master-type-bahan')->with('flash_message', 'Master Type Bahan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterTypeBahan  $masterTypeBahan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $masterTypeBahan = MasterTypeBahan::findOrFail($id);
        return view('pages.master-type-bahan.show', compact('masterTypeBahan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterTypeBahan  $masterTypeBahan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterTypeBahan = MasterTypeBahan::findOrFail($id);
        return view('pages.master-type-bahan.edit', compact('masterTypeBahan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterTypeBahan  $masterTypeBahan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =array(
            'name' => 'required',
            'status' => 'required',
        );

        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return redirect()->back();
        }
        $requestData = $request->all();
        
        $masterTypeBahan = MasterTypeBahan::findOrFail($id);
        $masterTypeBahan->update($requestData);

        activity('master_type_bahan')
            ->performedOn($masterTypeBahan)
            ->causedBy(Auth::user())
            ->log('updated');

        return redirect('master-type-bahan')->with('flash_message', 'Master Type Bahan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterTypeBahan  $masterTypeBahan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masterTypeBahan = MasterTypeBahan::find($id);
        MasterTypeBahan::destroy($id);
        activity('master_type_bahan')
            ->performedOn($masterTypeBahan)
            ->causedBy(Auth::user())
            ->log('deleted');

        return redirect('master-type-bahan')->with('flash_message', 'Master Master Type Bahan deleted!');
    }
}
