<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['pages.roles.form','pages.users.modal'], function($view) {
            $view->with('permissions', Permission::pluck('name', 'id'));
            $view->with('list_role', Role::pluck('name', 'id'));
        });
        view()->composer(['pages.business.owners.modal'], function($view) {
            $view->with('list_users', User::pluck('name', 'id'));
        });


    }
}
