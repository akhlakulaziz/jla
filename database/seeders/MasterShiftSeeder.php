<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterShift;

class MasterShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterShift::create([
            'name' => 'Shift 1',
            'status' => 1,
        ]);
        MasterShift::create([
            'name' => 'Shift 2',
            'status' => 1,
        ]);
    }
}
