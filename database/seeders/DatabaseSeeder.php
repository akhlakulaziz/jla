<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(ConfigTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DepartemenSeeder::class);

        $this->call(MasterDepartemenHarianProsesSeeder::class);
        $this->call(MasterGroupKualitasSeeder::class);
        $this->call(MasterItemSeeder::class);
        $this->call(MasterJenisKayuSeeder::class);
        $this->call(MasterJenisPekerjaanSeeder::class);
        $this->call(MasterKeteranganBahanSeeder::class);
        $this->call(MasterKeteranganPekerjaanSeeder::class);
        $this->call(MasterKeteranganProsesSeeder::class);
        $this->call(MasterKualitasSeeder::class);
        $this->call(MasterLokasiSeeder::class);
        $this->call(MasterNoPoSeeder::class);
        $this->call(MasterPemborongsSeeder::class);
        $this->call(MasterProductSeeder::class);
        $this->call(MasterShiftSeeder::class);
        $this->call(MasterSubUnitSeeder::class);
        $this->call(MasterSuppliersSeeder::class);
        $this->call(MasterTypeBahanSeeder::class);
        $this->call(MasterUnitSeeder::class);
        $this->call(MasterWarnaSeeder::class);
        $this->call(MasterTenagaKerjaSeeder::class);
        $this->call(MasterStatusTenagaKerjaSeeder::class);

        // $this->call(MasterBidangUsahaTableSeeder::class);
        // $this->call(MasterPositionEmploeesTableSeeder::class);
        // $this->call(MasterTypeAssetsTableSeeder::class);
        // $this->call(MasterBadanUsahaTableSeeder::class);
        // $this->call(MasterTypeBpjsTableSeeder::class);
        // $this->call(BusinessesTableSeeder::class);
        // $this->call(BusinessCompanionsTableSeeder::class);
        // $this->call(BusinessOwnersTableSeeder::class);
        // $this->call(BusinessAssetsTableSeeder::class);
        // $this->call(BusinessKaryawanTableSeeder::class);
        
        // Fitur Baru
        // $this->call(MasterBlockchainTableSeeder::class);
        // $this->call(PermissionTableSeederNFT::class);
        
    }
}
