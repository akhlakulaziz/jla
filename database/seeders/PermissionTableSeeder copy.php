<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'admin']);
        $user = Role::create(['name' => 'user']);

        $permissions = [
        
            ['id' => 1, 'name' => 'users-list',],
            ['id' => 2, 'name' => 'users-create',],
            ['id' => 3, 'name' => 'users-edit',],
            ['id' => 4, 'name' => 'users-delete',],
            ['id' => 5, 'name' => 'users-view',],

            ['id' => 6, 'name' => 'roles-list',],
            ['id' => 7, 'name' => 'roles-create',],
            ['id' => 8, 'name' => 'roles-edit',],
            ['id' => 9, 'name' => 'roles-delete',],
            ['id' => 10, 'name' => 'roles-view',],
        ];

    

        foreach ($permissions as $item) {
            Permission::create($item);
        }

        $user_permissions = [1];

        $user->syncPermissions($user_permissions);
        $admin->syncPermissions(Permission::all());
    }
}
