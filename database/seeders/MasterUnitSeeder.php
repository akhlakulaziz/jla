<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterUnit;

class MasterUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterUnit::create([
            'name' => 'SAWMILL JLA2',
            'master_departemen_harian_proses_id' => 1,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PROCESSING JLA 2',
            'master_departemen_harian_proses_id' => 2,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'AIR DRY / KD JLA1',
            'master_departemen_harian_proses_id' => 3,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PROCESSING JLA 1',
            'master_departemen_harian_proses_id' => 2,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'CORE',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PRESS',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PRODUKSI 1 A',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PRODUKSI 1 B JLA 1',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PRODUKSI 1 B JLA 2',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PRODUKSI 2',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'FINISHING 1',
            'master_departemen_harian_proses_id' => 5,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'FINISHING 2',
            'master_departemen_harian_proses_id' => 5,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'AIR DRY / KD JLA2',
            'master_departemen_harian_proses_id' => 3,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'MOSAIC',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PRODUKSI GUDANG',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'WAREHOUSE JLA1',
            'master_departemen_harian_proses_id' => 6,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PURCHASING',
            'master_departemen_harian_proses_id' => 6,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'ADMIN ENGINEERING',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'BENGKEL',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'LISTRIK JLA2',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'MEKANIK JLA2',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'SAW DOCTOR JLA2',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'LISTRIK JLA1',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'MEKANIK JLA1',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'SAW DOCTOR JLA1',
            'master_departemen_harian_proses_id' => 7,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'QC JLA1',
            'master_departemen_harian_proses_id' => 8,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'QC JLA2',
            'master_departemen_harian_proses_id' => 8,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'R & D',
            'master_departemen_harian_proses_id' => 9,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'GA',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'KEBERSIHAN JLA2',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'KEBERSIHAN JLA1',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'POLIKLINIK JLA2',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'POLIKLINIK JLA1',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'SECURITY JLA2',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'SECURITY JLA1',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'SUPPORTING JLA2',
            'master_departemen_harian_proses_id' => 11,
            'master_lokasis_id' => 2,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'WALL CLADDING',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'PURCHASING & GRADE',
            'master_departemen_harian_proses_id' => 10,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'MOZAIC',
            'master_departemen_harian_proses_id' => 4,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
        MasterUnit::create([
            'name' => 'WAREHOUSE JLA2',
            'master_departemen_harian_proses_id' => 6,
            'master_lokasis_id' => 1,
            'status' => 1,
        ]);
    }
}
