<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterKeteranganProses;

class MasterKeteranganProsesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterKeteranganProses::create([
            'name' => 'DOUBLE END',
            'status' => 1,
        ]);
        MasterKeteranganProses::create([
            'name' => 'T&G',
            'status' => 1,
        ]);
    }
}
