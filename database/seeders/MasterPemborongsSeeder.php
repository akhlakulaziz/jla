<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterPemborong;

class MasterPemborongsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterPemborong::create([
            'name' => 'C',
            'status' => 1,
        ]);
        MasterPemborong::create([
            'name' => 'GLOBAL',
            'status' => 1,
        ]);
        MasterPemborong::create([
            'name' => 'NON BORONG',
            'status' => 1,
        ]);
        MasterPemborong::create([
            'name' => 'TETAP',
            'status' => 1,
        ]);
    }
}
