<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterSubUnit;

class MasterSubUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSubUnit::create([
            'name' => 'BANDSAW 28-3',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT 1',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT 2',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT 3',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT 4',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT 5',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT 6',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'KUANGYUNG',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE PLANNER 2 (HINOKI)',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MULTIRIP OGAM (PO-220)',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOILER HOT WATER & CONDITIONING',
            'master_units_id' => 3,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOILER HOT WATER',
            'master_units_id' => 3,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 620-1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOZAIC LINE 1',
            'master_units_id' => 5,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOZAIC LINE 2',
            'master_units_id' => 5,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE DRUMSAW 1',
            'master_units_id' => 5,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE DRUMSAW 2',
            'master_units_id' => 5,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE DRUMSAW 3',
            'master_units_id' => 5,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE DRUMSAW 4',
            'master_units_id' => 5,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS A',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN PRESS HF',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS HPC 160',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN PRESS HPC-250',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS OTT',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CONDITIONING',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 02 (SYC-423)',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 06 (SYC615-PR1)',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'T&G TORWEGGE',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING BM',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING FM',
            'master_units_id' => 9,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING COSTA',
            'master_units_id' => 9,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING BUTTFERING',
            'master_units_id' => 9,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 623-2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING UNIMAT-17A',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'T&G CELASCHI',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DN SHI TEH 1',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DN SHI TEH 2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DN DANGKAERT',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN PARAFIN 2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DEMPUL PRODUKSI 2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'UNIT KEROK',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'COLORING',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'HAND SANDING',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING WOOD BSA',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING SEALER BSA',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CURTAIN COATER',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PACKING & WRAPPING JLA 1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PEMBUATAN PALLET EXPORT',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'UV LINE 2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PACKING & WRAPPING JLA 2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PALLET PADAT',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PALLET AIR DRY / KD',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 36-4 (ROBINSON)',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 36-9 (ST-9)',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TREATMENT 1',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TREATMENT 2',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW CD',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW PONY',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 36-1',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 36-2',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 36-3',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 42-1',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE PLANNER 1 (KCL 600)',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE RIP',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR PRESS',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PALLET PADAT',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PALLET PADAT',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'WIRE SYC-6S (SYC615-PR2)',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DRIP PRODUKSI 2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TAMBAL PRODUKSI 2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PLANNER PALLET',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSSCUT PALLET',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING WADKIN 03',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN CKM - FINGERJOINT',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MULTIRIP EXTREMA',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 623-4',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING WADKIN 04',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'COLORING JLA 2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SHRINGKING KELOMPOK JLA2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSSCUT 9',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MULTIRIP 2',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 28-2',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT (PROD 2)',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 620-2',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOILER STEAM',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TREATMENT BESAR (1)',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'COLORING JLA2',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PALLET OBAT',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW AUTO BESAR',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT - 01',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'WOODMIZER',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DIMTER',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 28-1',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN PRESS A BOTTOM',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'UNIT KEROK KELOMPOK',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PACKING KELOMPOK JLA1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PACKING KELOMPOK JLA2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SHRINGKING KELOMPOK JLA1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 623-1',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PLANNER MULTI 1',
            'master_units_id' => 14,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PLANNER MULTI 2',
            'master_units_id' => 14,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW 44',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'WIRE SYC-6S',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG FINISHING JLA1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG FINISHING JLA2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG RM SAWMILL',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG KD',
            'master_units_id' => 3,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG RM PROCESSING JLA1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG RM PEMBAHANAN',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG MOZAIC',
            'master_units_id' => 14,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG PRODUKSI 1',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG NON MOVING PRODUKSI JLA1',
            'master_units_id' => 9,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG (PROD 2)',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG PRESS',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOTTOM CROSS OMGA',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'WIRE SYC 615-PR1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'HASIL SORTIR PROCESSING JLA2',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG PACKING JLA1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG PACKING JLA2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOTTOM PLANNER 04',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG RM PURCHASING',
            'master_units_id' => 38,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR FINISHING JLA 2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS PANEL MANUAL',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING VIET',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GRADE BALOK',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GRADE LOG',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GRADE KERING HASIL KD',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PALLET VEENER AKASIA',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR KERING JLA1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GRADE KERING VENEER JLA1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'HASIL GRADE DIMTER',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR PROCESSING JLA2',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SANDING MASTER',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG NON MOVING (PROD 2)',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'T & G TORWEGGE',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SQUARING',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SHRINGKING JLA2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SHRINGKING JLA1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'JUMPSAW 1',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'JUMPSAW 2',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER NON KARET 1',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER NON KARET 2',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER NON KARET 3',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER NON KARET 4',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DEMPUL PRODUKSI 1',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DRIP PRODUKSI 1',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TAMBAL PRODUKSI 1',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER KARET 1',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER KARET 2',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER KARET 3',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER KARET 4',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG NON MOVING (PROD 1)',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'STENNER PK-20',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN SHENG JIN',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'LMC 623 SPEED MAC',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GRADE DIMTER',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING RM KD',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'FORKLIFT RM AIR DRY / KD',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC KD / AIR DRY',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR RM KD',
            'master_units_id' => 13,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'WAREHOUSE',
            'master_units_id' => 16,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'WAREHOUSE',
            'master_units_id' => 40,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR WAREHOUSE',
            'master_units_id' => 40,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PURCHASING',
            'master_units_id' => 17,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN ENGINEERING',
            'master_units_id' => 18,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BENGKEL',
            'master_units_id' => 19,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ELECTRIC',
            'master_units_id' => 20,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MEKANIK',
            'master_units_id' => 21,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SAW DOCTOR JLA2',
            'master_units_id' => 22,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ELECTRIC',
            'master_units_id' => 23,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MEKANIK',
            'master_units_id' => 24,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SAW DOCTOR JLA1',
            'master_units_id' => 25,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'QC',
            'master_units_id' => 26,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'QC',
            'master_units_id' => 27,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'R & D',
            'master_units_id' => 28,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'LABORATORIUM',
            'master_units_id' => 28,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING F&P 1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'FORKLIFT F&P',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR F&P 1',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING F&P 2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR F&P 2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GENERAL AFFAIR',
            'master_units_id' => 29,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'LIMBAH',
            'master_units_id' => 29,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR GA',
            'master_units_id' => 29,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'KEBERSIHAN',
            'master_units_id' => 30,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'KEBERSIHAN',
            'master_units_id' => 31,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'POLIKLINIK',
            'master_units_id' => 32,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'POLIKLINIK',
            'master_units_id' => 33,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SECURITY',
            'master_units_id' => 34,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SECURITY',
            'master_units_id' => 35,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING HR',
            'master_units_id' => 36,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DRIVER',
            'master_units_id' => 36,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC MOZAIC',
            'master_units_id' => 39,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'FORKLIFT PRODUKSI',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SOPIR BP',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC PRODUKSI GUDANG',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC PRODUKSI 1B JLA1',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC PRODUKSI 1A',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR PRODUKSI GUDANG',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR PRODUKSI 1A',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR PRESS',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING PRODUKSI 1',
            'master_units_id' => 6,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING RM PROCESSING 1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC RM PROCESSING 1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR RM PROCESSING 1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'FORKLIFT RM PROCESSING 1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC F&P 2',
            'master_units_id' => 12,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR PRODUKSI 2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC PRODUKSI 2',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TELLY LAPANGAN',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'FORKLIFT RM PROCESSING',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR RM PROCESSING 2',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING RM PROCESSING 2',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PPIC RM GRADE & SAWMILL',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ADMIN & SUPPORTING RM GRADE & SM',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SUPERVISOR RM GRADE & SAWMILL',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'FORKLIFT RM GRADE & SAWMILL',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TELLY LAPANGAN',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER NON KARET 5',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER KARET 5',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DN SHI TEH 1 JLA 1',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MULTIRIP ACOSTA',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER KARET 6',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER KARET 7',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE PLANNER 3 (EC-610M)',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER NON KARET 6',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR VENEER NON KARET 7',
            'master_units_id' => 15,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE CROSSCUT 1',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE CROSSCUT 2',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE RIP 1',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE SANDING BRUSH',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN PRESS KUNTO',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING WITKIN',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN PRESS PARJO',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR LEM (SCRUB LEM)',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOTTOM PLANNER 01',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PLANNER WALL CLADDING',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SINGLE RIP 2',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE CROSSCUT 3',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 720M',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR HATI (BERSIH HATI)',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SCHROEDER',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS MANUAL 1',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 415',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR KULIT (BERSIH KULIT)',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE PLANNER 600-610',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS MANUAL 2',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSSCUT (PROD 1B)',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SQUARING 2',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING UNIMAT SUPER 4',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING LMC 623 SPEED MAC',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'RUNNING SAW',
            'master_units_id' => 8,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOTTOM GROOVING',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MESIN ROLL',
            'master_units_id' => 11,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'QC',
            'master_units_id' => 26,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'R & D',
            'master_units_id' => 28,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GRADE SAWN TIMBER',
            'master_units_id' => 38,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'HASIL GRADE BALOK',
            'master_units_id' => 38,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'HASIL GRADE LOG',
            'master_units_id' => 38,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'TREATMENT KECIL (2)',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DOUBLE PLANNER HINOKI KCL-400',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DEMPUL RM 2',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DRIP RM 2',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MULTIRIP SCHROEDER',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS MANUAL 3',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS MANUAL 4',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS MANUAL 5',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS MANUAL 6',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PRESS MANUAL 7',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'ASSEMBLING WALLCLADDING',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PERCELAN WALLCLADDING',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'LONG JEN CIRCLE',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR MOZAIC',
            'master_units_id' => 39,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PERCELAN BBI',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'SORTIR WALL CLADDING',
            'master_units_id' => 2,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'DEMPUL / AMPLAS WALL CLADDING',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'WIRE BRUSH MANUAL',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'GUDANG WALLCLADDING',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT WALLCLADDING',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'POTONG MULTI 1',
            'master_units_id' => 39,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'POTONG MULTI 2',
            'master_units_id' => 39,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BANDSAW AUTO KECIL',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'HAND SANDING WALL CLADDING',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'PASSANG BOTTOM',
            'master_units_id' => 37,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'JUMPSAW 1',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'JUMPSAW 2',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'POTONG OMGA',
            'master_units_id' => 4,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOR XIANG YOU A',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'BOR XIANG YOU B',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CELASCHI P40',
            'master_units_id' => 10,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'RIPPING MULTI 1',
            'master_units_id' => 39,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'RIPPING MULTI 2',
            'master_units_id' => 39,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT - 02',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT - 03',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 623-3',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'CROSS CUT - 04',
            'master_units_id' => 1,
            'status' => 1,
        ]);
        MasterSubUnit::create([
            'name' => 'MOULDING SYC 623-C',
            'master_units_id' => 7,
            'status' => 1,
        ]);
        
    }
}
