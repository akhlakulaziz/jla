<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeederNFT extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nft = Role::create(['name' => 'nft']);

        $permissions = [

            ['id' => 64, 'name' => 'kelola-nft-list',],
            ['id' => 65, 'name' => 'nft-create',],
            ['id' => 66, 'name' => 'nft-edit',],
            ['id' => 67, 'name' => 'nft-delete',],
            
            ['id' => 68, 'name' => 'list-nft-list',],
            ['id' => 69, 'name' => 'leaderboard-nft-list',],
            ['id' => 70, 'name' => 'jadwal-list',],

            ['id' => 71, 'name' => 'master-blockchain-list',],
            ['id' => 72, 'name' => 'master-blockchain-view',],
            ['id' => 73, 'name' => 'master-blockchain-create',],
            ['id' => 74, 'name' => 'master-blockchain-edit',],
            ['id' => 75, 'name' => 'master-blockchain-delete',],
            
        ];

        foreach ($permissions as $item) {
            Permission::create($item);
        }
        
        $nft_permisions = [64, 65, 66, 67, 68, 69, 70];

        $nft->syncPermissions($nft_permisions);
    }
}
