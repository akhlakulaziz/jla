<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterTenagaKerja;

class MasterTenagaKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterTenagaKerja::create([
            'nik' => '33256478955443',
            'name' => 'steven',
            'status' => 1,
        ]);
        MasterTenagaKerja::create([
            'nik' => '33265479965466',
            'name' => 'joni',
            'status' => 1,
        ]);
        MasterTenagaKerja::create([
            'nik' => '3326547889654',
            'name' => 'alex',
            'status' => 1,
        ]);
    }
}
