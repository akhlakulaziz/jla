<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterItem;

class MasterItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterItem::create([
            'name' => 'SOLID',
            'status' => 1,
        ]);
        MasterItem::create([
            'name' => 'SOLID FJ',
            'status' => 1,
        ]);
    }
}
