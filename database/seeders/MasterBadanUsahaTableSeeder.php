<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterBadanUsaha;

class MasterBadanUsahaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterBadanUsaha::create([
            'name' => 'Belum Ada',
            'status' => 1,
        ]);
        MasterBadanUsaha::create([
            'name' => 'UD',
            'status' => 1,
        ]);
        MasterBadanUsaha::create([
            'name' => 'CV',
            'status' => 1,
        ]);
        MasterBadanUsaha::create([
            'name' => 'PT',
            'status' => 1,
        ]);

      
    }
}
