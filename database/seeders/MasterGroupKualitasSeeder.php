<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterGroupKualitas;

class MasterGroupKualitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterGroupKualitas::create([
            'name' => 'AFKIR',
            'status' => 1,
        ]);
        MasterGroupKualitas::create([
            'name' => 'BAIK',
            'status' => 1,
        ]);
        MasterGroupKualitas::create([
            'name' => 'REPRO',
            'status' => 1,
        ]);
    }
}
