<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterKualitas;

class MasterKualitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterKualitas::create([
            'name' => 'BAIK',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'COKLAT',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'POTONG',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'PUTIH',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'RUSTIC',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'AFKIR',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'BLUESTIN',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'DEMPUL',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'POTONG',
            'status' => 1,
        ]);
        MasterKualitas::create([
            'name' => 'TU PANJANG',
            'status' => 1,
        ]);
    }
}
