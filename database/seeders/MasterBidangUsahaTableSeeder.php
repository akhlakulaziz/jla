<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterBidangUsaha;

class MasterBidangUsahaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterBidangUsaha::create([
            'name' => 'Backend Developer',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Produksi dan Percetakan',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Jasa Percetakan',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Blangko Undangan',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Perlengkapan Undangan',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Blangko Amplop',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Blangko Cover Yasin',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Undangan Online',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Souvenir dan Merchandise',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Logistik dan Transportasi',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Toko Undangan',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Desain Grafis',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Ilustrasi',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Animasi',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Presentasi',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Musik Digital',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Media Sosial',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Website, Aplikasi dan Perangkat Lunak',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Pendidikan Industri Kreatif',
            'status' => 1,
        ]);
        MasterBidangUsaha::create([
            'name' => 'Trading dan Manajemen Investasi',
            'status' => 1,
        ]);

    }
}
