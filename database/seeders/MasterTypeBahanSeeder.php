<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterTypeBahan;

class MasterTypeBahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterTypeBahan::create([
            'name' => 'SOLID',
            'status' => 1,
        ]);
    }
}
