<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterKeteranganBahan;

class MasterKeteranganBahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterKeteranganBahan::create([
            'name' => 'SOLID',
            'status' => 1,
        ]);
        MasterKeteranganBahan::create([
            'name' => 'FINGERJOINT',
            'status' => 1,
        ]);
    }
}
