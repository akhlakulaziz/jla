<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterJenisKayu;

class MasterJenisKayuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterJenisKayu::create([
            'name' => 'KARET',
            'status' => 1,
        ]);
        MasterJenisKayu::create([
            'name' => 'AKASIA',
            'status' => 1,
        ]);
    }
}
