<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $administrator = Role::create(['name' => 'administrator']);
        $admin = Role::create(['name' => 'admin']);
        // $user = Role::create(['name' => 'koordinator']);
        $harian_proses = Role::create(['name' => 'harian-proses']);

        $permissions = [
        
            ['id' => 1, 'name' => 'users-list',],
            ['id' => 2, 'name' => 'users-create',],
            ['id' => 3, 'name' => 'users-edit',],
            ['id' => 4, 'name' => 'users-delete',],
            ['id' => 5, 'name' => 'users-reset-password',],
            ['id' => 6, 'name' => 'users-view',],

            ['id' => 7, 'name' => 'roles-list',],
            ['id' => 8, 'name' => 'roles-create',],
            ['id' => 9, 'name' => 'roles-edit',],
            ['id' => 10, 'name' => 'roles-delete',],
            ['id' => 11, 'name' => 'roles-view',],

            //master harian proses
            ['id' => 12, 'name' => 'master-item-list',],
            ['id' => 13, 'name' => 'master-item-view',],
            ['id' => 14, 'name' => 'master-item-create',],
            ['id' => 15, 'name' => 'master-item-edit',],
            ['id' => 16, 'name' => 'master-item-delete',],

            ['id' => 17, 'name' => 'master-keterangan-bahan-list',],
            ['id' => 18, 'name' => 'master-keterangan-bahan-view',],
            ['id' => 19, 'name' => 'master-keterangan-bahan-create',],
            ['id' => 20, 'name' => 'master-keterangan-bahan-edit',],
            ['id' => 21, 'name' => 'master-keterangan-bahan-delete',],

            ['id' => 22, 'name' => 'master-type-bahan-list',],
            ['id' => 23, 'name' => 'master-type-bahan-view',],
            ['id' => 24, 'name' => 'master-type-bahan-create',],
            ['id' => 25, 'name' => 'master-type-bahan-edit',],
            ['id' => 26, 'name' => 'master-type-bahan-delete',],

            ['id' => 27, 'name' => 'master-warna-list',],
            ['id' => 28, 'name' => 'master-warna-view',],
            ['id' => 29, 'name' => 'master-warna-create',],
            ['id' => 30, 'name' => 'master-warna-edit',],
            ['id' => 31, 'name' => 'master-warna-delete',],

            ['id' => 32, 'name' => 'master-group-kualitas-list',],
            ['id' => 33, 'name' => 'master-group-kualitas-view',],
            ['id' => 34, 'name' => 'master-group-kualitas-create',],
            ['id' => 35, 'name' => 'master-group-kualitas-edit',],
            ['id' => 36, 'name' => 'master-group-kualitas-delete',],

            ['id' => 37, 'name' => 'master-kualitas-list',],
            ['id' => 38, 'name' => 'master-kualitas-view',],
            ['id' => 39, 'name' => 'master-kualitas-create',],
            ['id' => 40, 'name' => 'master-kualitas-edit',],
            ['id' => 41, 'name' => 'master-kualitas-delete',],

            ['id' => 42, 'name' => 'master-jenis-kayu-list',],
            ['id' => 43, 'name' => 'master-jenis-kayu-view',],
            ['id' => 44, 'name' => 'master-jenis-kayu-create',],
            ['id' => 45, 'name' => 'master-jenis-kayu-edit',],
            ['id' => 46, 'name' => 'master-jenis-kayu-delete',],

            ['id' => 47, 'name' => 'master-keterangan-proses-list',],
            ['id' => 48, 'name' => 'master-keterangan-proses-view',],
            ['id' => 49, 'name' => 'master-keterangan-proses-create',],
            ['id' => 50, 'name' => 'master-keterangan-proses-edit',],
            ['id' => 51, 'name' => 'master-keterangan-proses-delete',],

            ['id' => 52, 'name' => 'master-departemen-list',],
            ['id' => 53, 'name' => 'master-departemen-view',],
            ['id' => 54, 'name' => 'master-departemen-create',],
            ['id' => 55, 'name' => 'master-departemen-edit',],
            ['id' => 56, 'name' => 'master-departemen-delete',],

            ['id' => 57, 'name' => 'master-unit-list',],
            ['id' => 58, 'name' => 'master-unit-view',],
            ['id' => 59, 'name' => 'master-unit-create',],
            ['id' => 60, 'name' => 'master-unit-edit',],
            ['id' => 61, 'name' => 'master-unit-delete',],

            ['id' => 62, 'name' => 'master-sub-unit-list',],
            ['id' => 63, 'name' => 'master-sub-unit-view',],
            ['id' => 64, 'name' => 'master-sub-unit-create',],
            ['id' => 65, 'name' => 'master-sub-unit-edit',],
            ['id' => 66, 'name' => 'master-sub-unit-delete',],

            ['id' => 67, 'name' => 'master-supplier-list',],
            ['id' => 68, 'name' => 'master-supplier-view',],
            ['id' => 69, 'name' => 'master-supplier-create',],
            ['id' => 70, 'name' => 'master-supplier-edit',],
            ['id' => 71, 'name' => 'master-supplier-delete',],

            ['id' => 72, 'name' => 'master-product-list',],
            ['id' => 73, 'name' => 'master-product-view',],
            ['id' => 74, 'name' => 'master-product-create',],
            ['id' => 75, 'name' => 'master-product-edit',],
            ['id' => 76, 'name' => 'master-product-delete',],

            ['id' => 77, 'name' => 'master-nomer-po-list',],
            ['id' => 78, 'name' => 'master-nomer-po-view',],
            ['id' => 79, 'name' => 'master-nomer-po-create',],
            ['id' => 80, 'name' => 'master-nomer-po-edit',],
            ['id' => 81, 'name' => 'master-nomer-po-delete',],

            ['id' => 82, 'name' => 'master-pemborong-list',],
            ['id' => 83, 'name' => 'master-pemborong-view',],
            ['id' => 84, 'name' => 'master-pemborong-create',],
            ['id' => 85, 'name' => 'master-pemborong-edit',],
            ['id' => 86, 'name' => 'master-pemborong-delete',],

            ['id' => 87, 'name' => 'master-jenis-pekerjaan-list',],
            ['id' => 88, 'name' => 'master-jenis-pekerjaan-view',],
            ['id' => 89, 'name' => 'master-jenis-pekerjaan-create',],
            ['id' => 90, 'name' => 'master-jenis-pekerjaan-edit',],
            ['id' => 91, 'name' => 'master-jenis-pekerjaan-delete',],

            ['id' => 92, 'name' => 'master-keterangan-pekerjaan-list',],
            ['id' => 93, 'name' => 'master-keterangan-pekerjaan-view',],
            ['id' => 94, 'name' => 'master-keterangan-pekerjaan-create',],
            ['id' => 95, 'name' => 'master-keterangan-pekerjaan-edit',],
            ['id' => 96, 'name' => 'master-keterangan-pekerjaan-delete',],

            ['id' => 97, 'name' => 'master-lokasi-list',],
            ['id' => 98, 'name' => 'master-lokasi-view',],
            ['id' => 99, 'name' => 'master-lokasi-create',],
            ['id' => 100, 'name' => 'master-lokasi-edit',],
            ['id' => 101, 'name' => 'master-lokasi-delete',],

            ['id' => 102, 'name' => 'master-shift-list',],
            ['id' => 103, 'name' => 'master-shift-view',],
            ['id' => 104, 'name' => 'master-shift-create',],
            ['id' => 105, 'name' => 'master-shift-edit',],
            ['id' => 106, 'name' => 'master-shift-delete',],

            ['id' => 107, 'name' => 'master-tenaga-kerja-list',],
            ['id' => 108, 'name' => 'master-tenaga-kerja-view',],
            ['id' => 109, 'name' => 'master-tenaga-kerja-create',],
            ['id' => 110, 'name' => 'master-tenaga-kerja-edit',],
            ['id' => 111, 'name' => 'master-tenaga-kerja-delete',],

            //master hrd
            ['id' => 112, 'name' => 'master-departemen-hrd-list',],
            ['id' => 113, 'name' => 'master-departemen-hrd-view',],
            ['id' => 114, 'name' => 'master-departemen-hrd-create',],
            ['id' => 115, 'name' => 'master-departemen-hrd-edit',],
            ['id' => 116, 'name' => 'master-departemen-hrd-delete',],
            
            ['id' => 117, 'name' => 'master-bagian-list',],
            ['id' => 118, 'name' => 'master-bagian-view',],
            ['id' => 119, 'name' => 'master-bagian-create',],
            ['id' => 120, 'name' => 'master-bagian-edit',],
            ['id' => 121, 'name' => 'master-bagian-delete',],
        
            // ['id' => 12, 'name' => 'list-business-list',],
            // ['id' => 13, 'name' => 'business-list',],
            // ['id' => 14, 'name' => 'business-create',],
            // ['id' => 15, 'name' => 'business-edit',],
            // ['id' => 16, 'name' => 'business-delete',],

            // ['id' => 17, 'name' => 'employess-list',],
            // ['id' => 18, 'name' => 'employess-create',],
            // ['id' => 19, 'name' => 'employess-edit',],
            // ['id' => 20, 'name' => 'employess-delete',],

            // ['id' => 21, 'name' => 'owners-list',],
            // ['id' => 22, 'name' => 'owners-create',],
            // ['id' => 23, 'name' => 'owners-edit',],
            // ['id' => 24, 'name' => 'owners-delete',],

            // ['id' => 25, 'name' => 'companions-list',],
            // ['id' => 26, 'name' => 'companions-create',],
            // ['id' => 27, 'name' => 'companions-edit',],
            // ['id' => 28, 'name' => 'companions-delete',],

            // ['id' => 29, 'name' => 'assets-list',],
            // ['id' => 30, 'name' => 'assets-create',],
            // ['id' => 31, 'name' => 'assets-edit',],
            // ['id' => 32, 'name' => 'assets-delete',],

            // ['id' => 33, 'name' => 'master-bidang-usaha-list',],
            // ['id' => 34, 'name' => 'master-bidang-usaha-view',],
            // ['id' => 35, 'name' => 'master-bidang-usaha-create',],
            // ['id' => 36, 'name' => 'master-bidang-usaha-edit',],
            // ['id' => 37, 'name' => 'master-bidang-usaha-delete',],

            // ['id' => 38, 'name' => 'master-position-employess-list',],
            // ['id' => 39, 'name' => 'master-position-emploees-view',],
            // ['id' => 40, 'name' => 'master-position-emploees-create',],
            // ['id' => 41, 'name' => 'master-position-emploees-edit',],
            // ['id' => 42, 'name' => 'master-position-emploees-delete',],

            // ['id' => 43, 'name' => 'master-type-assets-list',],
            // ['id' => 44, 'name' => 'master-type-assets-view',],
            // ['id' => 45, 'name' => 'master-type-assets-create',],
            // ['id' => 46, 'name' => 'master-type-assets-edit',],
            // ['id' => 47, 'name' => 'master-type-assets-delete',],

            // ['id' => 48, 'name' => 'master-type-bpjs-list',],
            // ['id' => 49, 'name' => 'master-type-bpjs-view',],
            // ['id' => 50, 'name' => 'master-type-bpjs-create',],
            // ['id' => 51, 'name' => 'master-type-bpjs-edit',],
            // ['id' => 52, 'name' => 'master-type-bpjs-delete',],

            // ['id' => 53, 'name' => 'master-badan-usaha-list',],
            // ['id' => 54, 'name' => 'master-badan-usaha-view',],
            // ['id' => 55, 'name' => 'master-badan-usaha-create',],
            // ['id' => 56, 'name' => 'master-badan-usaha-edit',],
            // ['id' => 57, 'name' => 'master-badan-usaha-delete',],

            ['id' => 122, 'name' => 'setting-list',],

            ['id' => 123, 'name' => 'activity-log-list',],

            // ['id' => 60, 'name' => 'manage-business-finance',],

            // ['id' => 61, 'name' => 'business-galeries',],
            // ['id' => 62, 'name' => 'business-galeries-store',],
            // ['id' => 63, 'name' => 'business-galeries-delete',],


            //harian proses
            ['id' => 124, 'name' => 'item-list',],
            ['id' => 125, 'name' => 'item-create',],
            ['id' => 126, 'name' => 'item-edit',],
            ['id' => 127, 'name' => 'item-delete',],

            ['id' => 128, 'name' => 'input-harian-list',],
            ['id' => 129, 'name' => 'input-harian-create',],
            ['id' => 130, 'name' => 'input-harian-edit',],
            ['id' => 131, 'name' => 'input-harian-delete',],

            ['id' => 132, 'name' => 'harian-in-list',],
            ['id' => 133, 'name' => 'harian-in-create',],
            ['id' => 134, 'name' => 'harian-in-edit',],
            ['id' => 135, 'name' => 'harian-in-delete',],

            ['id' => 136, 'name' => 'harian-out-list',],
            ['id' => 137, 'name' => 'harian-out-create',],
            ['id' => 138, 'name' => 'harian-out-edit',],
            ['id' => 139, 'name' => 'harian-out-delete',],

            ['id' => 140, 'name' => 'work-time-list',],
            ['id' => 141, 'name' => 'work-time-create',],
            ['id' => 142, 'name' => 'work-time-edit',],
            ['id' => 143, 'name' => 'work-time-delete',],

            ['id' => 144, 'name' => 'stock-item-list',],
            ['id' => 145, 'name' => 'stock-item-create',],
            ['id' => 146, 'name' => 'stock-item-edit',],
            ['id' => 147, 'name' => 'stock-item-delete',],

        ];

        foreach ($permissions as $item) {
            Permission::create($item);
        }

        // $user_permissions = [12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 60, 61, 62, 63];
        $harian_proses_permissions = [124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147];

        // $user->syncPermissions($user_permissions);
        $harian_proses->syncPermissions($harian_proses_permissions);
        // $administrator->syncPermissions(Permission::all());
        $admin->syncPermissions(Permission::all());
    }
}
