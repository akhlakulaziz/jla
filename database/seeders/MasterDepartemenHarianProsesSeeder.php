<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterDepartemenHarianProses;


class MasterDepartemenHarianProsesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterDepartemenHarianProses::create([
            'name' => 'RAW MATERIAL SAWMILL',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'RAW MATERIAL PROCESSING',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'RAW MATERIAL AIR DRY / KD',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'PRODUCTION',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'FINISHING AND PACKAGING',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'A F T',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'ENGINEERING & MAINTENANCE',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'QUALITY CONTROL',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'RESEARCH & DEVELOPMENT',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'RAW MATERIAL PURCHASING & GRADE',
            'status' => 1,
        ]);
        MasterDepartemenHarianProses::create([
            'name' => 'HUMAN RESOURCE',
            'status' => 1,
        ]);
    }
}
