<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterWarna;

class MasterWarnaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterWarna::create([
            'name' => 'CHERRY',
            'status' => 1,
        ]);
        MasterWarna::create([
            'name' => 'EXPRESSO',
            'status' => 1,
        ]);
        MasterWarna::create([
            'name' => 'GREEN',
            'status' => 1,
        ]);
        MasterWarna::create([
            'name' => 'GREY',
            'status' => 1,
        ]);
        MasterWarna::create([
            'name' => 'TEAK',
            'status' => 1,
        ]);
    }
}
