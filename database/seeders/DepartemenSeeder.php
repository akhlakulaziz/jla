<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterDepartemen;

class DepartemenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterDepartemen::create([
            'name' => 'RM PURCHASING & GRADE',
            'status' => 1,
        ]);
        MasterDepartemen::create([
            'name' => 'RM SAWMILL',
            'status' => 1,
        ]);
        MasterDepartemen::create([
            'name' => 'FINISHING & PACKING 2',
            'status' => 1,
        ]);
    }
}
