<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterSupplier;

class MasterSuppliersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSupplier::create([
            'name' => 'AB',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'AMA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'AZZ',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'BAB',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'BC',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'BF',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'BWC',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'CBA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'CMA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'CMC',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'COA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'COC',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'COD',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'DD',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'DDA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'DMA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'DOA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'DPA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'DUA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'DWB',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'EMA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'EOA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'EOB',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'EOC',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'EOD',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FAA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FB',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FIA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FOA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FRA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FTA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FTB',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FTC',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FTD',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FUA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'FWA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'GEA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'GNA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'GOA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'GTA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'MA',
            'status' => 1,
        ]);
        MasterSupplier::create([
            'name' => 'SB',
            'status' => 1,
        ]);
    }
}
