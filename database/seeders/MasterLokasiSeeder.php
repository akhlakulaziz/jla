<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterLokasi;

class MasterLokasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterLokasi::create([
            'name' => 'JLA 1',
            'status' => 1,
        ]);
        MasterLokasi::create([
            'name' => 'JLA 2',
            'status' => 1,
        ]);
    }
}
