<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterKeteranganPekerjaan;

class MasterKeteranganPekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterKeteranganPekerjaan::create([
            'name' => 'GANTI BAHAN DAN SETTING MESIN',
            'status' => 1,
        ]);
        MasterKeteranganPekerjaan::create([
            'name' => 'GANTI BAHAN LL 451',
            'status' => 1,
        ]);
        MasterKeteranganPekerjaan::create([
            'name' => 'GANTI BAUT',
            'status' => 1,
        ]);
        MasterKeteranganPekerjaan::create([
            'name' => 'PERSIAPAN',
            'status' => 1,
        ]);
        MasterKeteranganPekerjaan::create([
            'name' => 'SETING HASIL TIDAK RAPAT DAN STEP',
            'status' => 1,
        ]);
    }
}
