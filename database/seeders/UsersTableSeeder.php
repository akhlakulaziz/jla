<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Carbon\Carbon;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;
use App\Models\Team;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $administrator = User::create([
        //     'name' => 'administrator',
        //     'email' => 'administrator@gmail.com',
        //     'email_verified_at' => now(),
        //     'phone' => '0857474543' ,
        //     'address' => 'jalan tanjung sari',
        //     'gender' => '1',
        //     'npwp' => '123453345',
        //     'nik' => 'K35443245345',
        //     'place_of_birth' => 'semarang',
        //     'password' => Hash::make('secret'),
        // ]);


        $admin = User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'phone' => '08574743536' ,
            'address' => 'jalan tanjung sari 1',
            'gender' => '1',
            'npwp' => '123453435',
            'nik' => 'K3543345345',
            'place_of_birth' => 'semarang',
            'password' => Hash::make('secret'),
        ]);

    
        // $user = User::create([
        //     'name' => 'user',
        //     'email' => 'user@gmail.com',
        //     'email_verified_at' => now(),
        //     'phone' => '0273943292346',
        //     'address' => 'jalan tanjung sari 2',
        //     'gender' => '1',
        //     'npwp' => '1454354543',
        //     'nik' => 'L253523525',
        //     'place_of_birth' => 'semarang satu',
        //     'password' => Hash::make('secret'),
        // ]);

        $harian_proses = User::create([
            'name' => 'harian proses',
            'email' => 'harianproses@gmail.com',
            'email_verified_at' => now(),
            'phone' => '0273943292346',
            'address' => 'semarang',
            'gender' => '1',
            'npwp' => '1454354543',
            'nik' => 'L253523525',
            'place_of_birth' => 'semarang satu',
            'password' => Hash::make('secret'),
        ]);

        // $administrator->assignRole('administrator');
        $admin->assignRole('admin');
        // $user->assignRole('koordinator');
        $harian_proses->assignRole('harian-proses');
    }
}
