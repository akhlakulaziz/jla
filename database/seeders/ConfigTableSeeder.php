<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{
  
    public function run()
    {
        $data = [
            'app.name' =>'APPS JLA',
            'app.url' => 'https://apps.jla.com',
            'mail.from.name' => 'wide',
            'mail.from.address' => 'akhlakulaziz@gmail.com',
            'mail.default' => 'smtp',
            'mail.mailers.smtp.host' => 'smtp.gmail.com',
            'mail.mailers.smtp.port' => '587',
            'mail.mailers.smtp.username' => 'akhlakulaziz@gmail.com',
            'mail.mailers.smtp.password' => '1qazxsw2@akhlakul12345',
            'mail.mailers.smtp.encryption' => 'tls',
            'mail.from.app_logo' =>'logo.png',
            'mail.from.app_favicon' =>'favicon.ico',
            'mail.from.app_description' =>'Wood Parquet Flooring  & Component Industry',

        ];

        foreach ($data as $key => $value) {
            $config = \App\Models\Configs::firstOrCreate(['key' => $key]);
            $config->value = $value;
            $config->save();
        }
    }
}
