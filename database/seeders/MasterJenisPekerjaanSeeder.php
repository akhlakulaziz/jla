<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterJenisPekerjaan;

class MasterJenisPekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterJenisPekerjaan::create([
            'name' => 'BAHAN',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'ELEKTRONIK',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'GERGAJI',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'ISTIRAHAT',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'KERJA',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'LAIN - LAIN',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'MANAJEMEN',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'MEKANIKAL',
            'status' => 1,
        ]);
        MasterJenisPekerjaan::create([
            'name' => 'SDM',
            'status' => 1,
        ]);
    }
}
