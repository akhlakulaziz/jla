<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterNoPo;

class MasterNoPoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterNoPo::create([
            'name' => 'LL 412',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 413',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 437',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 438',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 451',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 453',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 459',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 461',
            'master_products_id' => 1,
            'status' => 1,
        ]);
        MasterNoPo::create([
            'name' => 'LL 464',
            'master_products_id' => 1,
            'status' => 1,
        ]);
    }
}
