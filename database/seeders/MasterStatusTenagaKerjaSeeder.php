<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterStatusTenagaKerja;

class MasterStatusTenagaKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterStatusTenagaKerja::create([
            'name' => 'Harian Lepas',
            'status' => 1,
        ]);
        MasterStatusTenagaKerja::create([
            'name' => 'Borong',
            'status' => 1,
        ]);
        MasterStatusTenagaKerja::create([
            'name' => 'Tetap',
            'status' => 1,
        ]);
        MasterStatusTenagaKerja::create([
            'name' => 'Kontrak',
            'status' => 1,
        ]);
        MasterStatusTenagaKerja::create([
            'name' => 'Magang',
            'status' => 1,
        ]);
        MasterStatusTenagaKerja::create([
            'name' => 'Percobaan',
            'status' => 1,
        ]);
    }
}
