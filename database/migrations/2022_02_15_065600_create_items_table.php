<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('no_part_item')->nullable();
            $table->bigInteger('master_items_id')->nullable()->unsigned();
            $table->bigInteger('master_keterangan_bahans_id')->nullable()->unsigned();
            $table->bigInteger('master_type_bahans_id')->nullable()->unsigned();
            $table->float('diameter')->nullable();;
            $table->float('tinggi')->nullable();;
            $table->float('lebar')->nullable();;
            $table->float('panjang')->nullable();;
            $table->bigInteger('master_warnas_id')->nullable()->unsigned();
            $table->bigInteger('master_group_kualitas_id')->nullable()->unsigned();
            $table->bigInteger('master_kualitas_id')->nullable()->unsigned();
            $table->bigInteger('master_jenis_kayus_id')->nullable()->unsigned();
            $table->bigInteger('master_keterangan_proses_id')->nullable()->unsigned();
            $table->boolean('status')->nullable();

            $table->foreign('master_items_id')->references('id')->on('master_items')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_keterangan_bahans_id')->references('id')->on('master_keterangan_bahans')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_type_bahans_id')->references('id')->on('master_type_bahans')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_warnas_id')->references('id')->on('master_warnas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_group_kualitas_id')->references('id')->on('master_group_kualitas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_kualitas_id')->references('id')->on('master_kualitas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_jenis_kayus_id')->references('id')->on('master_jenis_kayus')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_keterangan_proses_id')->references('id')->on('master_keterangan_proses')->onDelete('cascade')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
