<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInputHarianOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_harian_outs', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal')->nullable();
            $table->string('no_process')->nullable();
            $table->string('departemen')->nullable();
            $table->string('unit')->nullable();
            $table->string('sub_unit')->nullable();
            $table->string('no_part_item');
            $table->string('item')->nullable();
            $table->string('shift')->nullable();
            $table->string('keterangan_bahan')->nullable();
            $table->string('type_bahan')->nullable();
            $table->string('jenis_kayu')->nullable();
            $table->string('group_kualitas')->nullable();
            $table->string('kualitas')->nullable();
            $table->string('warna')->nullable();
            $table->string('diameter')->nullable();
            $table->string('keterangan_proses')->nullable();
            $table->string('tinggi')->nullable();
            $table->string('lebar')->nullable();
            $table->string('panjang')->nullable();
            $table->string('pcs')->nullable();
            $table->float('m1')->nullable();
            $table->float('m2')->nullable();
            $table->float('m3')->nullable();
            $table->bigInteger('master_suppliers_id')->nullable()->unsigned();
            $table->bigInteger('no_kiriman')->nullable();
            $table->bigInteger('master_products_id')->nullable()->unsigned();
            $table->bigInteger('master_no_pos_id')->nullable()->unsigned();
            $table->bigInteger('no_pallet')->nullable();
            $table->bigInteger('nampan')->nullable();
            $table->string('operator')->nullable();
            $table->bigInteger('harian_tetap')->nullable();
            $table->bigInteger('harian_lepas')->nullable();
            $table->bigInteger('borong')->nullable();
            $table->bigInteger('master_pemborongs_id')->nullable()->unsigned();
            $table->string('memo')->nullable();
            $table->boolean('ot')->nullable();
            $table->string('tahun')->nullable();
            $table->string('keterangan')->nullable();

            $table->foreign('master_suppliers_id')->references('id')->on('master_suppliers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_products_id')->references('id')->on('master_products')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_no_pos_id')->references('id')->on('master_no_pos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_pemborongs_id')->references('id')->on('master_pemborongs')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_harian_outs');
    }
}
