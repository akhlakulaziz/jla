<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterSubUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_sub_units', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedInteger('master_units_id')->nullable()->unsigned();
            $table->boolean('status')->nullable();
            // $table->foreign('master_units_id')->references('id')->on('master_units_id')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_sub_units');
    }
}
