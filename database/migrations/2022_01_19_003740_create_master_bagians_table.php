<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterBagiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_bagians', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('master_departemens_id')->nullable()->unsigned();
            $table->boolean('status')->nullable();
            $table->foreign('master_departemens_id')->references('id')->on('master_departemens')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_bagians');
    }
}
