<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterTenagaKerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_tenaga_kerjas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('nik')->nullable();
            $table->string('name');
            $table->bigInteger('master_status_tenaga_kerjas_id')->nullable()->unsigned();
            $table->boolean('status')->nullable();
            // $table->foreign('master_status_tenaga_kerjas_id')->references('id')->on('master_status_tenaga_kerjas')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_tenaga_kerjas');
    }
}
