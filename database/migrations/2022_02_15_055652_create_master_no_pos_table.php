<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterNoPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_no_pos', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('master_products_id')->nullable()->unsigned();
            $table->boolean('status')->nullable();
            // $table->foreign('master_products_id')->references('id')->on('master_products_id')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_no_pos');
    }
}
