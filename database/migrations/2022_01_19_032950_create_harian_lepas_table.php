<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHarianLepasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harian_lepas', function (Blueprint $table) {
            $table->id();
            $table->date('star_period_month')->nullable();
            $table->date('end_period_month')->nullable();
            $table->date('star_period_week')->nullable();
            $table->date('end_period_week')->nullable();
            $table->date('tanggal')->nullable();
            $table->string('name');
            $table->bigInteger('master_departemens_id')->nullable()->unsigned();
            $table->bigInteger('master_bagians_id')->nullable()->unsigned();
            $table->Integer('jml_tenaga')->nullable();
            $table->enum('jla',['1', '2'])->comment('1 = JLA 1, 2 = JLA 2');
            $table->enum('status',['1', '2'])->comment('1 = harian lepas, 2 = borong harian lepas');
            $table->string('mandor');
            $table->enum('shift',['1', '2'])->comment('1 = shift 1, 2 = shift 2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harian_lepas');
    }
}
