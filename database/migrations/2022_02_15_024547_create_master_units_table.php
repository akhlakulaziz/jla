<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_units', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('master_departemen_harian_proses_id')->nullable()->unsigned();
            $table->bigInteger('master_lokasis_id')->nullable()->unsigned();
            $table->boolean('status')->nullable();
            $table->foreign('master_departemen_harian_proses_id')->references('id')->on('master_departemen_harian_proses')->onDelete('cascade')->onUpdate('cascade');
            // $table->foreign('master_lokasis_id')->references('id')->on('master_lokasis')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_units');
    }
}
