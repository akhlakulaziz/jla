{{-- =============================================Start New====================================================== --}}
<nav class="navbar navbar-light navbar-vertical navbar-expand-xl">
    <script>
        var navbarStyle = localStorage.getItem("navbarStyle");
        if (navbarStyle && navbarStyle !== 'transparent') {
            document.querySelector('.navbar-vertical').classList.add(`navbar-${navbarStyle}`);
        }
    </script>
    <div class="d-flex align-items-center">
        <div class="toggle-icon-wrapper">
            <button class="btn navbar-toggler-humburger-icon navbar-vertical-toggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Toggle Navigation">
                <span class="navbar-toggle-icon">
                    <span class="toggle-line"></span>
                </span>
            </button>
        </div>
        <a class="navbar-brand" href="index.html">
            <div class="d-flex align-items-center py-3">
                {{-- <img class="me-2" src="assets/img/icons/spot-illustrations/falcon.png" alt="" width="40" /> --}}
                <span class="font-sans-serif">{{ config('app.name', 'Laravel') }}</span>
            </div>
        </a>
    </div>
    <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
        <div class="navbar-vertical-content scrollbar">
            <ul class="navbar-nav flex-column mb-3" id="navbarVerticalNav">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('dashboard')}}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-chart-pie"></span>
                            </span>
                            <span class="nav-link-text ps-1">Dashboard</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('flowchart')}}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-sitemap"></span>
                            </span>
                            <span class="nav-link-text ps-1">Flow Proses</span>
                        </div>
                    </a>
                </li>
                @canany(['item-list'])
                <li class="nav-item">
                    <!-- label-->
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Harian Proses
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    
                    <!-- parent pages-->
                    <a class="nav-link" href="{{route('item.index')}}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-layer-group"></span>
                            </span>
                            <span class="nav-link-text ps-1">Item</span>
                        </div>
                    </a>

                    <!-- parent pages-->
                    <a class="nav-link" href="{{route('input-harian.index')}}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-calendar-plus"></span>
                            </span>
                            <span class="nav-link-text ps-1">Input Harian</span>
                        </div>
                    </a>
                    <!-- parent pages-->
                    <a class="nav-link" href="{{route('input-harian-in.index')}}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-calendar-plus"></span>
                            </span>
                            <span class="nav-link-text ps-1">Harian In</span>
                        </div>
                    </a>
                    <!-- parent pages-->
                    <a class="nav-link" href="{{route('input-harian-out.index')}}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-calendar-minus"></span>
                            </span>
                            <span class="nav-link-text ps-1">Harian Out</span>
                        </div>
                    </a>

                    <!-- parent pages-->
                    <a class="nav-link" href="#" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-clock"></span>
                            </span>
                            <span class="nav-link-text ps-1">Work time</span>
                        </div>
                    </a>

                    <!-- parent pages-->
                    <a class="nav-link" href="#" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-cubes"></span>
                            </span>
                            <span class="nav-link-text ps-1">Stock Item</span>
                        </div>
                    </a>
                    
                </li>
                @endcanany
                @canany(['master-item-list', 'master-keterangan-bahan-list', 'master-type-bahan-list', 'master-warna-list', 'master-group-kualitas-list', 'master-kualitas-list', 'master-jenis-kayu-list', 'master-keterangan-proses-list', 'master-departemen-list', 'master-unit-list', 'master-sub-unit-list', 'master-supplier-list', 'master-product-list', 'master-nomer-po-list', 'master-pemborong-list', 'master-jenis-pekerjaan-list', 'master-keterangan-pekerjaan-list', 'master-lokasi-list', 'master-shift-list', 'master-tenaga-kerja-list', 'master-departemen-list', 'master-bagian-list',])
                <li class="nav-item">
                    <!-- label-->
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Master Harian Proses
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    
                    <!-- Master Harian Proses-->
                    <a class="nav-link dropdown-indicator" href="#harian" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="email">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-database"></span>
                            </span>
                            <span class="nav-link-text ps-1">Master Harian Proses</span>
                        </div>
                    </a>
                    <ul class="nav collapse false" id="harian">
                        <li class="nav-item">
                            @can ('master-item-list')
                            <a class="nav-link" href="{{route('master-item.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Item</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-keterangan-bahan-list')
                            <a class="nav-link" href="{{route('master-keterangan-bahan.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Keterangan Bahan</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-type-bahan-list')
                            <a class="nav-link" href="{{route('master-type-bahan.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Type Bahan</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-warna-list')
                            <a class="nav-link" href="{{route('master-warna.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Warna</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-group-kualitas-list')
                            <a class="nav-link" href="{{route('master-group-kualitas.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Group Kualitas</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-kualitas-list')
                            <a class="nav-link" href="{{route('master-kualitas.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Kualitas</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-jenis-kayu-list')
                            <a class="nav-link" href="{{route('master-jenis-kayu.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Jenis Kayu</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-keterangan-proses-list')
                            <a class="nav-link" href="{{route('master-keterangan-proses.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Keterangan Proses</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-departemen-list')
                            <a class="nav-link" href="{{route('master-departemen-harian-proses.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Departemen</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-unit-list')
                            <a class="nav-link" href="{{route('master-unit.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Unit</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-sub-unit-list')
                            <a class="nav-link" href="{{route('master-sub-unit.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Sub Unit</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-supplier-list')
                            <a class="nav-link" href="{{route('master-supplier.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Supplier</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-product-list')
                            <a class="nav-link" href="{{route('master-product.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Product</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-nomer-po-list')
                            <a class="nav-link" href="{{route('master-no-po.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Nomer PO</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-pemborong-list')
                            <a class="nav-link" href="{{route('master-pemborong.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Pemborong</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-jenis-pekerjaan-list')
                            <a class="nav-link" href="{{route('master-jenis-pekerjaan.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Jenis Pekerjaan</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-keterangan-pekerjaan-list')
                            <a class="nav-link" href="{{route('master-keterangan-pekerjaan.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Keterangan Pekerjaan</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-lokasi-list')
                            <a class="nav-link" href="{{route('master-lokasi.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Lokasi</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-shift-list')
                            <a class="nav-link" href="{{route('master-shift.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Shift</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-tenaga-kerja-list')
                            <a class="nav-link" href="{{route('master-tenaga-kerja.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Tenaga Kerja</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-tenaga-kerja-list')
                            <a class="nav-link" href="{{route('master-status-tenaga-kerja.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Status Tenaga Kerja</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                    </ul>
                    @endcanany
                    @canany(['master-departemen-hrd-list', 'master-bagian-list',])
                    <!-- Master Harian Proses-->
                    <a class="nav-link dropdown-indicator" href="#hrd" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="email">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-database"></span>
                            </span>
                            <span class="nav-link-text ps-1">Master HRD</span>
                        </div>
                    </a>
                    <ul class="nav collapse false" id="hrd">
                        <li class="nav-item">
                            @can ('master-departemen-hrd-list')
                            <a class="nav-link" href="{{route('master-departemen.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Departemen</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('master-bagian-list')
                            <a class="nav-link" href="{{route('master-bagian.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Bagian</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                    </ul>

                    
                </li>
                @endcanany
                @canany(['users-list', 'roles-list',])
                <li class="nav-item">
                    <!-- label-->
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Pengaturan Sistem
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    <!-- Master Harian Proses-->
                    <a class="nav-link dropdown-indicator" href="#pengaturan_sitem" role="button" data-bs-toggle="collapse" aria-expanded="false" aria-controls="email">
                        <div class="d-flex align-items-center">
                            <span class="nav-link-icon">
                                <span class="fas fa-user"></span>
                            </span>
                            <span class="nav-link-text ps-1">Akun</span>
                        </div>
                    </a>
                    <ul class="nav collapse false" id="pengaturan_sitem">
                        <li class="nav-item">
                            @can ('users-list')
                            <a class="nav-link" href="{{route('users.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Pengguna</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                        <li class="nav-item">
                            @can ('roles-list')
                            <a class="nav-link" href="{{route('roles.index')}}" aria-expanded="false">
                                <div class="d-flex align-items-center">
                                    <span class="nav-link-text ps-1">Akses</span>
                                </div>
                            </a>
                            @endcan
                            <!-- more inner pages-->
                        </li>
                    </ul>
                    @can ('activity-log-list')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('activity-log.index')}}" role="button" aria-expanded="false">
                            <div class="d-flex align-items-center">
                                <span class="nav-link-icon">
                                    <span class="fas fa-list"></span>
                                </span>
                                <span class="nav-link-text ps-1">Log Aktivitas</span>
                            </div>
                        </a>
                    </li>
                    @endcan
                    @can ('setting-list')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('settings.index')}}" role="button" aria-expanded="false">
                            <div class="d-flex align-items-center">
                                <span class="nav-link-icon">
                                    <span class="fas fa-cogs"></span>
                                </span>
                                <span class="nav-link-text ps-1">Pengaturan</span>
                            </div>
                        </a>
                    </li>
                    @endcan
                </li>
                @endcanany
            
            </ul>
        </div>
    </div>
</nav>
{{-- =============================================End New====================================================== --}}