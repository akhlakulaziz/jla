{{-- ==================================Start New========================================================= --}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- ======================================== Start sama seperti Codebase===================================== --}}
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- ======================================== End sama seperti Codebase===================================== --}}


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href={{asset('assets2/img/favicons/apple-touch-icon.png')}}>
    <link rel="icon" type="image/png" sizes="32x32" href={{asset('assets2/img/favicons/favicon-32x32.png')}}>
    <link rel="icon" type="image/png" sizes="16x16" href={{asset('assets2/img/favicons/favicon-16x16.png')}}>
    <link rel="shortcut icon" type="image/x-icon" href={{asset('assets2/img/favicons/favicon.ico')}}>
    <link rel="manifest" href={{asset('assets2/img/favicons/manifest.json')}}>
    <meta name="msapplication-TileImage" content={{asset('assets2/img/favicons/mstile-150x150.png')}}>
    <meta name="theme-color" content="#ffffff">
    <script src={{asset('assets2/js/config.js')}}></script>
    <script src={{asset('assets2/vendors/overlayscrollbars/OverlayScrollbars.min.js')}}></script>


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">
    <link href={{asset('assets2/vendors/overlayscrollbars/OverlayScrollbars.min.css')}} rel="stylesheet">
    <link href={{asset('assets2/css/theme-rtl.min.css')}} rel="stylesheet" id="style-rtl">
    <link href={{asset('assets2/css/theme.min.css')}} rel="stylesheet" id="style-default">
    <link href={{asset('assets2/css/user-rtl.min.css')}} rel="stylesheet" id="user-style-rtl">
    <link href={{asset('assets2/css/user.min.css')}} rel="stylesheet" id="user-style-default">
    <script>
    var isRTL = JSON.parse(localStorage.getItem('isRTL'));
    if (isRTL) {
        var linkDefault = document.getElementById('style-default');
        var userLinkDefault = document.getElementById('user-style-default');
        linkDefault.setAttribute('disabled', true);
        userLinkDefault.setAttribute('disabled', true);
        document.querySelector('html').setAttribute('dir', 'rtl');
    } else {
        var linkRTL = document.getElementById('style-rtl');
        var userLinkRTL = document.getElementById('user-style-rtl');
        linkRTL.setAttribute('disabled', true);
        userLinkRTL.setAttribute('disabled', true);
    }
    </script>
{{-- ==================================End New========================================================= --}}

@livewireStyles

