    {{-- <script src="{{asset('assets/js/core/jquery.min.js')}}"></script> --}}
    <script src="{{asset('assets/js/codebase.core.min.js')}}"></script>
    {{-- <script src="{{ mix('js/app.js') }}" defer></script> --}}
    <script src="{{asset('assets/js/codebase.app.min.js')}}"></script>

{{-- =============================================Start New====================================================== --}}
    <script src={{asset('assets2/vendors/popper/popper.min.js')}}></script>
    <script src={{asset('assets2/vendors/bootstrap/bootstrap.min.js')}}></script>
    <script src={{asset('assets2/vendors/anchorjs/anchor.min.js')}}></script>
    <script src={{asset('assets2/vendors/is/is.min.js')}}></script>
    <script src={{asset('assets2/vendors/echarts/echarts.min.js')}}></script>
    <script src={{asset('assets2/vendors/fontawesome/all.min.js')}}></script>
    <script src={{asset('assets2/vendors/lodash/lodash.min.js')}}></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
    <script src={{asset('assets2/vendors/list.js/list.min.js')}}></script>
    <script src={{asset('assets2/js/theme.js')}}></script>
    
{{-- =============================================End New====================================================== --}}