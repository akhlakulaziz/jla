{{-- <footer id="page-footer">
  <div class="content py-20 font-size-sm clearfix">
      <div class="float-left">
          <a class="font-w600" href="{{config('app.url')}}" target="_blank">{{config('app.name')}}</a> &copy; <span class="js-year-copy"></span>
      </div>
  </div>
</footer> --}}

{{-- =============================================Start New====================================================== --}}
    <footer class="footer">
        <div class="row g-0 justify-content-between fs--1 mt-4 mb-3">
        <div class="col-12 col-sm-auto text-center">
            <a class="font-w600" href="{{config('app.url')}}" target="_blank">{{config('app.name')}}</a> &copy; <span class="js-year-copy"></span>
        </div>
        </div>
    </footer>
{{-- =============================================End New====================================================== --}}
