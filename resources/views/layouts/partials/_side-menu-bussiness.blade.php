<div class="js-inbox-nav d-none d-md-block">
    <div class="block">
        <div class="block-header block-header-default bg-gray">
            <h3 class="block-title">Menu</h3>
            <div class="block-options">
            </div>
        </div>
        @canany(['business-edit', 'business-galeries', 'employess-list','owners-list', 'companions-list', 'assets-list', 'business-delete', 'manage-business-finance',])
        <div class="block-content">
            <ul class="nav nav-pills flex-column push">
                <li class="nav-item">
                @can ('business-edit')
                    <a class="nav-link d-flex align-items-center justify-content-between {{ request()->is('business/*/settings') ? ' active' : '' }}" href="{{route('business.settings', [$business->uuid])}}">
                        <span>Kelola Data Usaha</span>
                    </a>
                @endcan
                </li>
                <li class="nav-item">
                    @can ('business-galeries')
                        <a class="nav-link d-flex align-items-center justify-content-between {{ request()->is('business/*/galery') ? ' active' : '' }}" href="{{route('business.galeries', [$business->uuid])}}">
                            <span>Galeri Usaha</span>
                        </a>
                    @endcan
                </li>
                @can ('employess-list')
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center justify-content-between {{ request()->is('business/*/employess') ? ' active' : '' }}" href="{{route('business.employess', [$business->uuid])}}">
                        <span>Karyawan</span>
                    </a>
                @endcan
                </li>
                @can ('owners-list')
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center justify-content-between {{ request()->is('business/*/owners') ? ' active' : '' }}" href="{{route('business.owners', [$business->uuid])}}">
                        <span>Kepemilikan</span>
                    </a>
                @endcan
                @can ('companions-list')
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center justify-content-between {{ request()->is('business/*/companion') ? ' active' : '' }}" href="{{route('business.companions', [$business->uuid])}}">
                        <span>Pendamping</span>
                    </a>
                @endcan
                @can ('assets-list')
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center justify-content-between {{ request()->is('business/*/assets') ? ' active' : '' }}" href="{{route('business.assets', [$business->uuid])}}">
                        <span>Aset</span>
                    </a>
                @endcan
                @can ('manage-business-finance')
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex align-items-center justify-content-between {{ request()->is('*/manage-business-finance') ? ' active' : '' }}" href="{{route('business.manage-business-finance', [$business->uuid])}}">
                        <span>Kelola Keuangan Usaha</span>
                    </a>
                </li>
                @endcan
            </ul>
        @endcanany
        </div>
    </div>
</div>