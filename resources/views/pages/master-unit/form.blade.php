<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Nama', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('master_departemen_harian_proses_id') ? 'has-error' : ''}}">
    {!! Form::label('master_departemen_harian_proses_id', 'Departemen', ['class' => 'control-label']) !!}
    <select class="form-control" id="master_departemen_harian_proses_id" name="master_departemen_harian_proses_id">
        @foreach($departemen as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('master_departemen_harian_proses_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('master_lokasis_id') ? 'has-error' : ''}}">
    {!! Form::label('master_lokasis_id', 'Lokasi', ['class' => 'control-label']) !!}
    <select class="form-control" id="master_lokasis_id" name="master_lokasis_id">
        @foreach($lokasi as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('master_departemen_harian_proses_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('status', (["1" => "Aktif", "2" => "Tidak Aktif"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
 
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Edit' : 'Tambah', ['class' => 'btn btn-primary']) !!}
</div>
