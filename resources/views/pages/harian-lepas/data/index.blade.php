<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

{{-- ============================ Start Aset Tanggal Periode=============================== --}}
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
{{-- ============================ End Aset Tanggal Periode=============================== --}}
<x-app-layout>
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Dashboard</a>
        <a class="breadcrumb-item" href="{{route('business.index')}}">Kelola Data Hl</a>
        <span class="breadcrumb-item active">Data Harian Lepas</span>
    </nav>
    <a href="{{route('dashboard')}}" title="Back"><button class="btn btn-dark btn-md mb-2">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</button>
    </a>
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <!-- Message List -->
                <div class="block">
                    <div class="block-header block-header-default bg-gray">
                        <div class="block-title">
                            <strong>Data Harian Lepas</strong> 
                        </div>
                    </div>
                    <div class="block-content">
                    @can ('companions-create')
                        <button type="button" class="btn btn-dark btn-sm" data-toggle="modal" onClick="create()" data-target="#modal-hl"><i class="si si-plus"></i> Add New</button>
                    @endcan
                        <div class="table-responsive" style="min-height: ">
                            <table class="table table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">#</th>
                                        <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                                        <th style="width: 30%;">Periode bulan</th>
                                        <th style="width: 15%;">Periode Minggu</th>
                                        <th style="width: 15%;">Tanggal</th>
                                        <th style="width: 15%;">Hari</th>
                                        <th style="width: 15%;">nama</th>
                                        <th style="width: 15%;">Hari</th>
                                        
                                        <th class="text-center" style="width: 100px;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($hl as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name}}</td>
                                            
                                            <td>
                                                @canany(['companions-edit', 'companions-delete',])
                                                @can ('companions-edit')
                                                <button type="button" onClick="edit({{ json_encode($item) }})"  title="Edit" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-hl">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i> 
                                                </button>
                                                @endcan
                                                @can ('companions-delete')
                                                <button type="button" onClick="remove({{ $item->id}})" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-info-hl" title="Hapus hl">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                                @endcan
                                                @endcanany
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

        @include('pages.harian-lepas.data.modal')
        {{-- @include('pages.harian-lepas.data.delete') --}}
    </x-app-layout>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

{{-- ============================ Start Aset Tanggal Periode=============================== --}}
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
{{-- ============================ End Aset Tanggal Periode=============================== --}}