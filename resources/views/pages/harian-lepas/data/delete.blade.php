 <!-- ends: .modal-info-warning -->
 <div class="modal fade" id="modal-delete-business" data-backdrop="static"  tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-transparent mb-0">
                <div class="block-header">
                    <h3 class="block-title" id="modal-title">
                        Hapus Bisnis
                    </h3>
                    {{-- <p>Are you sure you want to delete your account? Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.</p> --}}
                    <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                    </div>
                </div>
                <div class="block-content">
                    <p>Anda yakin ingin menghapus bisnis Anda? Setelah akun Anda dihapus, semua sumber daya dan datanya akan dihapus secara permanen. Silakan masukkan kata sandi Anda untuk mengonfirmasi bahwa Anda ingin menghapus akun Anda secara permanen.</p> 
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id_business" id="id_business">
                <button type="button" class="btn btn-danger text-uppercase"  id="hapus" onclick="destroy()">
                    Hapus Bisnis
                </button>
                <button type="button" class="btn btn-outline-secondary text-uppercase" data-dismiss="modal">Tutup</button>
                
            </div>
        </div>
    </div>
</div>
 

<script>

    function remove(id){
        $('#id_business').val(id);
    }

    function destroy(){
        var id =$('#id_business').val();
        createOverlay("process...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "/business/destroy/"+id,
            data: {
                '_token' : '{{ csrf_token() }}',
                _method: 'POST',
                id: id,
            },

           
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('business.index') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }


</script>
