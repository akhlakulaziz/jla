{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<style>
    .select2-container{
        width: 100% !important;
    }
</style>
<div class="modal fade" id="modal-hl" data-backdrop="static"  tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-transparent mb-0">
                <div class="block-header">
                    <h3 class="block-title" id="modal-title">
                    </h3>
                    <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                    </div>
                </div>
                <div class="block-content" style="padding: 0px 0px 1px;">
                    <form class="form-horizontal" id="form" action="#">
                        <div class="modal-body">
                            <input type="hidden" class="form-control" id="id" disabled>
                            <div class="form-group d-inline-block w-100">
                                <strong >Periode Minggu</strong>
                                <input type="text" class="form-control" name="daterange" id="periode_bulan" value="" />
                            </div>
                            <div class="form-group d-inline-block w-100">
                                <strong >Periode Minggu</strong>
                                <input type="text" class="form-control" name="daterange" id="periode_minggu" value="" />
                            </div>
                            <div id="form_tanggal" class="form-group {{ $errors->has('tanggal') ? 'has-error' : ''}}">
                                {!! Form::label('tanggal', 'Tanggal', ['class' => 'control-label']) !!}
                                {!! Form::date('tanggal', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                {!! Form::label('name', 'Nama', ['class' => 'control-label']) !!}
                                {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group d-inline-block w-100">
                                <label class="mr-sm-4" for="inlineFormCustomSelect">Departemen</label><br>
                                <select class="form-control"  id="master_departemens_id" name="master_departemens_id">
                                    <option value="">Pilih</option>
                                    @foreach ($departemen as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group d-inline-block w-100">
                                <label class="mr-sm-4" for="inlineFormCustomSelect">Bagian</label><br>
                                <select class="form-control"  id="master_bagians_id" name="master_bagians_id">
                                    <option value="">Pilih</option>
                                    @foreach ($bagian as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group {{ $errors->has('jla') ? 'has-error' : ''}}">
                                {!! Form::label('jla', 'JLA', ['class' => 'control-label']) !!}
                                {!! Form::select('jla', (["1" => "JLA 1", "2" => "JLA 2"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Pabrik JLA']) !!}
                                {!! $errors->first('jla', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                                {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
                                {!! Form::select('status', (["1" => "Harian Lepas", "2" => "Borong Harian Lepas"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Status']) !!}
                                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('mandor') ? 'has-error' : ''}}">
                                {!! Form::label('mandor', 'Nama Mandor', ['class' => 'control-label']) !!}
                                {!! Form::text('mandor', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('mandor', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('shift') ? 'has-error' : ''}}">
                                {!! Form::label('shift', 'Shift', ['class' => 'control-label']) !!}
                                {!! Form::select('shift', (["1" => "Shift 1", "2" => "Shift 2"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Shift']) !!}
                                {!! $errors->first('shift', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary text-uppercase" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-dark text-uppercase"  id="create" onclick="store()">
                    Simpan
                </button>
                <button type="button" class="btn btn-dark text-uppercase"  id="update" onclick="updated()">
                    Edit
                </button>
            </div>
        </div>
    </div>
</div>

<script>
// $(document).ready(function() {
//     $('#user_id').select2();
// });

    function edit(data){
        // console.log(data);
        $('#modal-title').text('Edit Data');
        $('#id').val(data.id);
        $('#business_id').val(data.business_id);
        $('#user_id').val(data.user_id);
        $('#user_id').select2();
        $('#position_type').val(data.position_type);
        $('#status').val(data.status);
        $("#create").hide(); 
        $("#update").show();
        // $('#modal-basic').modal('show');
    }
    
    function create() {
        $('#modal-title').text('Add Data');
        $('#form').trigger("reset");
        $("#periode_bulan").show();
        $("#periode_minggu").show();
        $("#tanggal").show();
        $("#name").show();
        $("#master_departemens_id").show();
        $('#master_departemens_id').select2();
        $("#master_bagians_id").show();
        $('#master_bagians_id').select2();
        $("#jla").show(); 
        $("#status").show(); 
        $("#mandor").show(); 
        $("#shift").show(); 
        $("#update").hide();
        $("#create").show();
        // $("#modal-basic").modal('show');

        $('input[name="daterange"]').daterangepicker({
            locale: {format: 'YYYY-MM-DD'},
        });
    }



    function store() {

        var periode_bulan = $('input[id="periode_bulan"]').val();
        var periode_minggu = $('input[id="periode_minggu"]').val();
        var tanggal = $('#tanggal').val();
        var name = $('#name').val();
        var master_departemens_id = $('#master_departemens_id').val();
        var master_bagians_id = $('#master_bagians_id').val();
        var jla = $('#jla').val();
        var status = $('#status').val();
        var mandor = $('#mandor').val();
        var shift = $('#shift').val();
        createOverlay("process...");

        $.ajax({
            type : "POST",
            url: "{!! route('harian-lepas.store') !!}",
            data: {
                '_token' : '{{ csrf_token() }}',
                'periode_bulan' :periode_bulan,
                'periode_minggu' :periode_minggu,
                'tanggal' :tanggal,
                'name' :name,
                'master_departemens_id' :master_departemens_id,
                'master_bagians_id' :master_bagians_id,
                'jla' :jla,
                'status' :status,
                'mandor' :mandor,
                'shift' :shift,
            },
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{route('harian-lepas.index')}}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }

    function updated(){
        var id = $('#id').val();
        var business_id = $('#business_id').val();
        var user_id = $('#user_id').val();
        var position_type = $('#position_type').val();
        var status = $('#status').val();
        createOverlay("Proses...");
        $.ajax({
            type : "PUT",
            url: "{!! route('harian-lepas.update',["id"]) !!}",
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'id' : id,
                'business_id' :business_id,
                'user_id' :user_id,
                'position_type' :position_type,
                'status' :status,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                            window.location = "{{route('harian-lepas.index')}}";
                    }, 500);              
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });

    }

</script> 