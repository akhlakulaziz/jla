<x-app-layout>
    <div class="card mb-3">
        <div class="card-header">
            <div class="row flex-between-end">
                <div class="col-auto align-self-center">
                    <h5 class="mb-0">Master Departemen</h5>
                </div>
                <div class="col-auto ms-auto">
                    <div class="nav nav-pills nav-pills-falcon flex-grow-1" role="tablist">
                        {!! Form::open(['method' => 'GET', 'route' => 'master-departemen.index', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Pencarian" value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search" style="color: white"></i>
                                    </button>
                                </span>
                            </div>
                        {!! Form::close() !!}
                        {{-- <button class="btn btn-sm active" data-bs-toggle="pill" data-bs-target="#dom-316cb649-6a3e-4ec3-9de8-7d83e880873f" type="button" role="tab" aria-controls="dom-316cb649-6a3e-4ec3-9de8-7d83e880873f" aria-selected="true" id="tab-dom-316cb649-6a3e-4ec3-9de8-7d83e880873f">Preview</button>
                        <button class="btn btn-sm" data-bs-toggle="pill" data-bs-target="#dom-972124bd-19ed-4bd0-9178-e6e8515f5d06" type="button" role="tab" aria-controls="dom-972124bd-19ed-4bd0-9178-e6e8515f5d06" aria-selected="false" id="tab-dom-972124bd-19ed-4bd0-9178-e6e8515f5d06">Code</button> --}}
                        
                    </div>
                </div>
            </div>
            <div class="col-auto align-self-center">
                @can ('master-departemen-hrd-create') 
                    <a href="{{ route('master-departemen.create') }}" class="btn btn-success btn-sm" title="Add New MasterDepartemen">
                        {{-- <button class="btn btn-falcon-success btn-sm" type="button"> --}}
                            <span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span>
                            <span class="ms-1">New</span>
                        {{-- </button> --}}
                    </a>
                @endcan
                {{-- <button class="btn btn-falcon-success btn-sm" type="button">
                    <span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span>
                    <span class="ms-1">New</span>
                </button> --}}
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="tab-content">
                <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-55d552bf-cdbd-40f9-856d-410188578fda" id="dom-55d552bf-cdbd-40f9-856d-410188578fda">
                    {{-- <div id="tableExample2" data-list='{"valueNames":["name","Status","Aksi"],"page":5,"pagination":true}'> --}}
                        <div class="table-responsive scrollbar">
                            <table class="table table-bordered table-striped fs--1 mb-0">
                                <thead class="bg-200 text-900">
                                    <tr>
                                        <th class="sort" data-sort="no">Nomer</th>
                                        <th class="sort" data-sort="nama">Nama</th>
                                        <th class="sort" data-sort="status">Status</th>
                                        <th class="sort" data-sort="aksi">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="list">
                                    @foreach($masterdepartemen as $item)
                                        <tr>
                                            <td class="nama">{{ $loop->iteration }}</td>
                                            <td class="status">{{ $item->name }}</td>
                                            <td class="aksi">@if($item->status==2)
                                                Tidak Aktif
                                                @elseif($item->status==1)
                                                Aktif
                                                @endif
                                            </td>
                                            <td>
                                                @canany(['master-departemen-hrd-view', 'master-departemen-hrd-edit', 'master-departemen-hrd-delete',])
                                                    @can ('master-departemen-hrd-view')
                                                    <a href="{{ route('master-departemen.show',[$item->id]) }}" title="View MasterDepartemen"><button class="btn btn-info btn-sm"><i class="fas fa-eye" aria-hidden="true"></i> Detail</button></a>
                                                    @endcan
                                                    @can ('master-departemen-hrd-edit')
                                                    <a href="{{ route('master-departemen.edit',[$item->id]) }}" title="Edit Masterdepartemen"><button class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true"></i> Edit</button></a>
                                                    @endcan
                                                    @can ('master-departemen-hrd-delete')
                                                    {!! Form::open([
                                                        'method'=>'DELETE',
                                                        'route' => ['master-departemen.destroy', $item->id],
                                                        'style' => 'display:inline'
                                                    ]) !!}
                                                        {!! Form::button('<i class="fas fa-trash-alt" aria-hidden="true"></i> Hapus', array(
                                                                'type' => 'submit',
                                                                'class' => 'btn btn-danger btn-sm',
                                                                'title' => 'Delete Masterdepartemen',
                                                                'onclick'=>'return confirm("Confirm delete?")'
                                                        )) !!}
                                                    {!! Form::close() !!}
                                                    @endcan
                                                @endcan
                                                </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{-- <div class="d-flex justify-content-center mt-3">
                            <button class="btn btn-sm btn-falcon-default me-1" type="button" title="Previous" data-list-pagination="prev"><span class="fas fa-chevron-left"></span></button>
                            <ul class="pagination mb-0"></ul>
                            <button class="btn btn-sm btn-falcon-default ms-1" type="button" title="Next" data-list-pagination="next"><span class="fas fa-chevron-right"> </span></button>
                        </div> --}}
            </div>
        </div>
    </div>
</x-app-layout>