<x-app-layout>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Log Aktivitas</h5>
               </div>
               
               <div class="col-auto ms-auto">
                  <div class="nav nav-pills nav-pills-falcon flex-grow-1 mt-2" role="tablist">
                     <form action="{{ route('activity-log.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="table-responsive scrollbar">
                     <table class="table">
                        <thead>
                           <tr>
                              <th>
                                 <span class="userDatatable-title">#</span>
                              </th>
                              <th>
                                 <span class="userDatatable-title">Nama</span>
                              </th>
                              <th>
                                 <span class="userDatatable-title">Nama log</span>
                              </th>
                              <th>
                                 <span class="userDatatable-title">Model</span>
                              </th>
                              <th>
                                 <span class="userDatatable-title">Metode</span>
                              </th>
                              <th>
                                 <span class="userDatatable-title">Waktu</span>
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach ($activitys as $item)
                              <tr>
                                 <td>
                                    <div class="userDatatable-content">
                                       {{ $loop->iteration }}
                                    </div>
                                 </td> 
                                 <td>
                                    <div class="userDatatable-content">
                                       {{ $item->causer->name }}
                                    </div>
                                 </td>
                                 
                                 <td>
                                    <div class="userDatatable-content">
                                       {{ $item->log_name }}
                                    </div>
                                 </td>
                                 <td>
                                    <div class="userDatatable-content">
                                       {{ $item->subject_type }}
                                    </div>
                                 </td>
                                 
                                 <td>
                                    <div class="userDatatable-content">
                                       @if ($item->description=="created")
                                          <span class="badge badge-pill badge-primary">Tambah</span>
                                       @elseif ($item->description=="updated")
                                          <span class="badge badge-pill badge-warning">Edit</span>
                                       @elseif ($item->description=="deleted")
                                          <span class="badge badge-pill badge-danger">Hapus</span>
                                       @endif

                                    </div>
                                 </td>
                                 <td>
                                    <div class="userDatatable-content">
                                       {{ \Carbon\Carbon::parse($item->created_at)->diffForHumans() }}
                                    </div>
                                 </td>

                              </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div>{{ $activitys->appends(['search' => Request::get('search')])->links('component.pagination') }} </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>