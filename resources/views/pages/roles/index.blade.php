{{-- <x-app-layout>
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="{{route('dashboard')}}">Dashboard</a>
        <span class="breadcrumb-item">Akun</span>
        <span class="breadcrumb-item active">Akses</span>
    </nav>
    <!-- Full Table -->
    <div class="block">
        <div class="block-header block-header-default bg-gray">
            <h3 class="block-title">Tabel Akses</h3>
                <div class="col-lg-10">
                    <div class="d-flex align-items-center">
                        <div class="d-flex align-items-center col-lg-9">
                            @can ('roles-create')
                                <a href="{{ route('roles.create') }}" class="btn btn-dark"><i class="si si-plus"></i> Tambah Akses</a>
                            @endcan
                        </div>
                        <div class="col-lg-3">
                            <form action="{{ route('roles.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                                <span data-feather="search"></span>
                                <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                        <tr>
                            <th>Akes</th>
                            <th style="width: 25%;">Perijinan Akses</th>
                            <th style="width: 25%;">Jumlah Pengguna</th>
                            <th class="text-center" style="width: 20%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $item)
                            <tr>
                                <td>{{ ucwords($item->name) }}</td>
                                <td>
                                    @if($item->id == 1)
                                        All
                                    @else
                                        @if($item->permissions->count())
                                            @foreach($item->permissions as $permission)
                                                {{ ucwords($permission->name) }}
                                            @endforeach
                                        @else
                                            None
                                        @endif
                                    @endif
                                </td>
                                
                                <td>{{ $item->users->count() }}</td>
                                
                                @canany(['roles-edit','roles-delete'])
                                    <td class="text-center">
                                        <div class="btn-group">
                                            @can ('roles-edit')
                                                <a href="{{ route('roles.edit',[$item->id]) }}" title="Edit Role"><button class="btn btn-sm btn-secondary"><i class="fa fa-pencil"></i></button></a>
                                            @endcan
                                            @if($item->id != 1)
                                                @can ('roles-delete')
                                                    <button type="button" onClick="remove({{ $item->id}})" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-info-confirmed" title="Delete">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    @include('pages.roles.delete')
                                                @endcan
                                            @endif
                                            </div>
                                        </div>
                                    </td>
                                @endcanany
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Full Table -->



</x-app-layout> --}}

<x-app-layout>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header border-bottom">
                <div class="row flex-between-end">
                    <div class="col-auto align-self-center">
                    <h5 class="mb-0" data-anchor="data-anchor">Akses</h5>
                    </div>
                    
                    <div class="col-auto ms-auto">
                    <div class="nav nav-pills nav-pills-falcon flex-grow-1 mt-2" role="tablist">
                        <form action="{{ route('roles.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                            <span data-feather="search"></span>
                            <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                        </form>
                    </div>
                    </div>
                </div>
                @can ('roles-create')
                    <a href="{{ route('roles.create') }}" class="btn btn-dark"><i class="si si-plus"></i> Tambah Akses</a>
                @endcan
            </div>
            <div class="card-body pt-0">
                <div class="tab-content">
                    <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                    <div class="table-responsive scrollbar">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Akes</th>
                                    <th style="width: 25%;">Perijinan Akses</th>
                                    <th style="width: 25%;">Jumlah Pengguna</th>
                                    <th class="text-center" style="width: 20%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roles as $item)
                                    <tr>
                                        <td>{{ ucwords($item->name) }}</td>
                                        <td>
                                            @if($item->id == 1)
                                                All
                                            @else
                                                @if($item->permissions->count())
                                                    @foreach($item->permissions as $permission)
                                                        {{ ucwords($permission->name) }}
                                                    @endforeach
                                                @else
                                                    None
                                                @endif
                                            @endif
                                        </td>
                                        
                                        <td>{{ $item->users->count() }}</td>
                                        
                                        @canany(['roles-edit','roles-delete'])
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    @can ('roles-edit')
                                                        <a href="{{ route('roles.edit',[$item->id]) }}" title="Edit Role"><button class="btn btn-sm btn-secondary"><i class="fas fa-pencil-alt"></i></button></a>
                                                    @endcan
                                                    @if($item->id != 1)
                                                        @can ('roles-delete')
                                                            <button type="button" onClick="remove({{ $item->id}})" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-info-confirmed" title="Delete">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                            @include('pages.roles.delete')
                                                        @endcan
                                                    @endif
                                                    </div>
                                                </div>
                                            </td>
                                        @endcanany
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</x-app-layout>