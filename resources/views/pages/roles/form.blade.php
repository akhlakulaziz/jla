<div class="form-group {{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Nama: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('permission') ? 'has-error' : ''}}">
    {!! Form::label('permission', 'Perijinan Akses', ['class' => 'control-label']) !!}
    @if(count($permissions) > 0)
        @foreach($permissions as $key => $value)
        <br>
        <label class="css-control css-control-sm css-control-primary css-switch">
            {!! Form::checkbox ('permission[]', $key,  isset($role->id) ?  in_array($key, $rolePermissions) ? true : null : null,array('class' => 'css-control-input')) !!} <span class="css-control-indicator"></span> {{ $value }}</label>
            @endforeach
        {!! $errors->first('permission', '<p class="help-block">:message</p>') !!}
    @else
        <p>Tidak ada opsi izin, buat dulu...!</p>
    @endif
</div>
<div class="button-group d-flex pt-25 justify-content-end  mb-20">

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Tambah', ['class' => 'btn btn-dark btn-default btn-squared text-capitalize radius-md shadow2']) !!}
</div>

