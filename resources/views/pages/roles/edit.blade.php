{{-- <x-app-layout>
   <nav class="breadcrumb bg-white push">
      <a class="breadcrumb-item" href="{{route('dashboard')}}">Dasbor</a>
      <span class="breadcrumb-item">Akun</span>
      <a class="breadcrumb-item" href="{{route('roles.index')}}">Akses</a>
      <span class="breadcrumb-item active">Tambah Akses</span>
   </nav>
   <div class="row">
      <div class="col-md-12">
         <!-- Default Elements -->
         <div class="block">
            <div class="block-header block-header-default bg-gray">
                  <h3 class="block-title">Edit Akses</h3>
            </div>
            <div class="block-content">
            
               @if ($errors->any())
                  <ul class="alert alert-danger">
                     @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                     @endforeach
                  </ul>
               @endif
                  {!! Form::model($role, [
                        'method' => 'PATCH',
                        'route' => ['roles.update', $role->id],
                        'class' => 'form-horizontal'
                  ]) !!}

                  @include ('pages.roles.form', ['formMode' => 'edit'])

                  {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
</x-app-layout> --}}

<x-app-layout>
   <div class="card mb-3">
      <div class="card-header">
         <div class="row flex-between-end">
            <div class="col-auto align-self-center">
                  <h5 class="mb-0">Edit Akses</h5>
            </div>
         </div>
         <div class="col-auto align-self-center">
            <a href="{{ route('roles.index') }}" class="btn btn-dark btn-sm" title="Back">
                  <span class="fa fa-arrow-left" data-fa-transform="shrink-3 down-2"></span>
                  <span class="ms-1">Back</span>
            </a>
         </div>
      </div>
      <div class="card-body pt-0">
         <div class="tab-content">
            <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-55d552bf-cdbd-40f9-856d-410188578fda" id="dom-55d552bf-cdbd-40f9-856d-410188578fda">
                  <div class="table-responsive scrollbar">
                     @if ($errors->any())
                        <ul class="alert alert-danger">
                              @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                              @endforeach
                        </ul>
                     @endif

                     {!! Form::model($role, [
                        'method' => 'PATCH',
                        'route' => ['roles.update', $role->id],
                        'class' => 'form-horizontal'
                     ]) !!}

                     @include ('pages.roles.form', ['formMode' => 'edit'])

                     {!! Form::close() !!}
                  </div>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>