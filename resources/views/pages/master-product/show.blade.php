<x-app-layout>
    <div class="card mb-3">
        <div class="card-header">
            <div class="row flex-between-end">
                <div class="col-auto align-self-center">
                    <h5 class="mb-0">Detail Master Product</h5>
                </div>
                <a href="{{ route('master-product.index') }}" title="Back">
                    <button class="btn btn-dark btn-md mb-2"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                </a>
                <div class="col-auto ms-auto">
                    <div class="nav nav-pills nav-pills-falcon flex-grow-1" role="tablist">
                        {{-- @canany(['master-product-edit', 'master-product-delete',]) --}}
                            @can ('master-product-edit')
                                <a href="{{ route('master-product.edit',[$masterProduct->id]) }}" title="Edit MasterProduct"><button class="btn btn-primary btn-sm" style="color: white;"><i class="fas fa-edit" aria-hidden="true"></i> Edit</button></a>
                            @endcan
                            @can ('master-product-delete')
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'route' => ['master-product.destroy', $masterProduct->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fas fa-trash-alt" aria-hidden="true"></i> Hapus', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Delete MasterProduct',
                                            'style' => 'color: white;',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    ))!!}
                                {!! Form::close() !!}
                            @endcan
                        {{-- @endcanany --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="tab-content">
                <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-55d552bf-cdbd-40f9-856d-410188578fda" id="dom-55d552bf-cdbd-40f9-856d-410188578fda">
                    <div class="table-responsive scrollbar">
                        <table class="table table-bordered table-striped fs--1 mb-0">
                            <tbody class="list">
                                    <tr>
                                        <tr><th> Nama </th><td> {{ $masterProduct->name }} </td></tr>
                                        <tr><th> Status </th><td>   @if ($masterProduct->status==1)
                                            Aktif
                                        @elseif ($masterProduct->status==2)
                                            Tidak Aktif
                                        @endif </td></tr>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</x-app-layout>