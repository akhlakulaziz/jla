<x-app-layout>
    <div class="row">
        <div class="col-md-4">
            <x-jet-section-title>
                <x-slot name="title">Umum</x-slot>
                <x-slot name="description">
                    <span class="small">
                        Pengaturan umum seperti, judul situs, deskripsi situs, alamat dan sebagainya.
                    </span>
                </x-slot>
            </x-jet-section-title>
        </div>
        <div class="col-md-8">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="w-md-75">
                        <form>
                                <div class="form-group ">
                                    <label for="name">Nama app</label>
                                    <input type="text" class="form-control form-control-lg" id="name" value="{{config('app.name')}}">
                                </div>
                                <div class="form-group ">
                                    <label for="url">Url app</label>
                                    <input type="text" class="form-control form-control-lg" id="url" value="{{config('app.url')}}">
                                </div>
                                <div class="form-group ">
                                    <label for="url">Deskripsi app</label>
                                    <textarea class="form-control form-control-lg" name="description" id="description" cols="48" rows="5">{{config('mail.from.app_description')}}</textarea>
                                </div>
                                
                                <div class="form-group ">
        
                                    <label >Logo app</label>
                                    <div class="col-md-8">
                                        <div id="logo_b_image_preview" class="d-inline-block p-3 preview">
                                            <img height="50px" src="{{asset('image/'.config('mail.from.app_logo'))}}">
                                        </div>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="logo" id="logo" data-toggle="custom-file-input">
                                        <label class="custom-file-label" for="logo">Pilih file</label>
                                    </div>
                                    <small id="passwordHelpInline2" class="fs-13 mt-2">
                                        Catatan: Unggah logo dengan <span class="color-dark">teks hitam dan latar belakang transparan dalam  format .png</span> dan <span class="color-dark">294x50(WxH)</span> pixels.
                                        <span class="color-dark">Tinggi</span> harus diperbaiki,<span class="color-dark">lebar</span> menurut anda  <span class="color-dark">rasio aspek.</span>
                                    </small>                                    
                                </div>
                                <div class="form-group ">
        
                                    <label >Tambah Favicon</label>
                                    <div class="col-md-8">
                                        <div id="logo_b_image_preview" class="d-inline-block p-3 preview">
                                            <img height="50px" src="{{asset('image/'.config('mail.from.app_favicon'))}}">
                                        </div>
                                    </div>
        
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="favicon" id="favicon" data-toggle="custom-file-input">
                                        <label class="custom-file-label" for="favicon">Plih file</label>
                                    </div>
                                    <small id="passwordHelpInline3" class="fs-13 mt-2">
                                        Catatan : Unggah logo dengan resolusi <span class="color-dark">32x32</span> pixels dan ekstensi <span class="color-dark">.png</b> atau <span class="color-dark">.gif</span> atau <span class="color-dark">.ico</span>
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <div class="d-flex align-items-baseline">
                                <button class="btn btn-dark text-uppercase" type="button" onclick="updateGeneral()">Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <x-jet-section-border />
        <div class="row">
            <div class="col-md-4">
                <x-jet-section-title>
                    <x-slot name="title">Konfigurasi Email</x-slot>
                    <x-slot name="description">
                        <span class="small">
                            Email Pengaturan SMTP, notifikasi dan lain-lain yang berhubungan dengan email.
                        </span>
                    </x-slot>
                </x-jet-section-title>
            </div>
            <div class="col-md-8">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <div class="w-md-75">
                            <form>
                                <div class="form-group mb-20">
                                    <label for="mail_name">Nama Surat</label>
                                    <input type="text" class="form-control" id="mail_name"  value="{{config('mail.from.name')}}">
                                    <small class="font-14 text-muted">Ini akan menjadi nama tampilan untuk email yang Anda kirim.</small>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email45">Alamat Surat</label>
                                    <input type="text" class="form-control" id="mail_address" value="{{config('mail.from.address')}}">
                                    <small class="font-14 text-muted">Email ini akan digunakan untuk korespondensi "Formulir Kontak".</small>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email45">Pengirim Surat</label>
                                    <input type="text" class="form-control" id="mail_driver" value="{{config('mail.default')}}">
                                    <small class="font-14 text-muted">Anda dapat memilih pengirim apa pun yang Anda inginkan untuk pengaturan Mail Anda. Mantan. SMTP, Mailgun, Mandrill, SparkPost, Amazon SES dll. Tambahkan pengirim tunggal saja..</small>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email45">Mail HOST</label>
                                    <input type="text" class="form-control" id="mail_host" value="{{config('mail.mailers.smtp.host')}}">
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email45">Mail PORT</label>
                                    <input type="text" class="form-control" id="mail_port" value="{{config('mail.mailers.smtp.port')}}">
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email45">Mail Username</label>
                                    <input type="text" class="form-control" id="mail_username" value="{{config('mail.mailers.smtp.username')}}">
                                    <small class="font-14 text-muted">Tambahkan id email Anda yang ingin Anda konfigurasikan untuk mengirim email.</small>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email45">Mail Password</label>
                                    <input type="password" class="form-control" id="mail_password" value="{{config('mail.mailers.smtp.password')}}">
                                    <small class="font-14 text-muted">Tambahkan kata sandi email yang ingin Anda konfigurasikan untuk mengirim email.</small>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email45">Email</label>
                                    <input type="text" class="form-control" id="mail_encryption"  value="{{config('mail.mailers.smtp.encryption')}}">
                                    <small class="font-14 text-muted">Gunakan tls jika situs Anda menggunakan protokol HTTP dan ssl jika situs Anda menggunakan protokol HTTPS.</small>
                                </div>
                                <div class="form-group mb-1 mt-2">
                                    <div class="text-left">
                                        <small class="text-muted font-13"><strong>Catatan penting:</strong></small>
                                        <small class="text-muted font-13">JIKA Anda menggunakan konfigurasi GMAIL untuk Mail, pastikan Anda telah menyelesaikan proses berikut sebelum memperbarui: <br>
                                            1. Pergi ke <a href="https://myaccount.google.com/security" class="text-blue-600" target="_blank">Akun Saya</a>  dari Akun Google Anda, Anda ingin mengonfigurasi dan Masuk <br>
                                            2. Scroll ke bawah ke Akses aplikasi yang kurang  dan AKTIFKAN<br>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <div class="d-flex align-items-baseline">
                                <button class="btn btn-dark text-uppercase" type="button" onclick="updateConfiguration()">Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            <script>
                function updateGeneral(){
                    
                    var name = $('#name').val();
                    var url = $('#url').val();
                    var description =$('#description').val();
                    var logo = $('#logo').prop('files')[0];
                    var favicon = $('#favicon').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('_token', '{{ csrf_token() }}');
                    form_data.append('name', name);
                    form_data.append('url', url);
                    form_data.append('description',description);
                    form_data.append('logo', logo);
                    form_data.append('favicon', favicon);
                    createOverlay("process...");
                    $.ajax({
                        type : "POST",
                        url: "{!! route('settings.store') !!}",
                        dataType: 'json',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        success: function(data){
                            gOverlay.hide();
                            if(data["status"] == "success") {            
                                toastr.success(data["message"]);
                                setTimeout(function(){ 
                                    window.location = "{{ route('settings.index') }}";
                                }, 500);            
                            }else {
                                toastr.error(data["message"]);
                            }
                        },
                        error: function(error) {
                            alert("Server/network error\r\n" + error);
                        }
                    });
                }
            
                function updateConfiguration(){
                    
                    var mail_name = $('#mail_name').val();
                    var mail_address = $('#mail_address').val();
                    var mail_driver =$('#mail_driver').val();
                    var mail_host =$('#mail_host').val();
                    var mail_port =$('#mail_port').val();
                    var mail_username =$('#mail_username').val();
                    var mail_password =$('#mail_password').val();
                    var mail_encryption =$('#mail_encryption').val();
            
                    var form_data = new FormData();
                    form_data.append('_token', '{{ csrf_token() }}');
                    form_data.append('mail_name', mail_name);
                    form_data.append('mail_address', mail_address);
                    form_data.append('mail_driver',mail_driver);
                    form_data.append('mail_host', mail_host);
                    form_data.append('mail_port', mail_port);
                    form_data.append('mail_username', mail_username);
                    form_data.append('mail_password', mail_password);
                    form_data.append('mail_encryption', mail_encryption);
                    createOverlay("process...");
                    $.ajax({
                        type : "POST",
                        url: "{!! route('settings.mail') !!}",
                        dataType: 'json',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        success: function(data){
                            gOverlay.hide();
                            if(data["status"] == "success") {            
                                toastr.success(data["message"]);          
                            }else {
                                toastr.error(data["message"]);
                            }
                        },
                        error: function(error) {
                            alert("Server/network error\r\n" + error);
                        }
                    });
                }
                
            
                
            </script>
        
        
        </x-app-layout>
        
            
                    
                        
            
                    