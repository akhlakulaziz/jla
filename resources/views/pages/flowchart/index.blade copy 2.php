
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('image/'.config('mail.from.app_favicon'))}}">
    <link rel="icon" type="image/png" sizes="192x192" href="">
    <link rel="apple-touch-icon" sizes="180x180" href="">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700&display=swap">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" id="css-main" href="{{asset('assets/css/codebase.min.css')}}">
</head>
<body>
    <h2>SOLID WOOD FLOORING</h2>
        <div class="post-info">
            <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
            </span>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                <i class="bi bi-square-fill fa-stack-2x"></i>
                <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MULTID- RUMSAW</p></span>
            </span>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                <i class="bi bi-square-fill fa-stack-2x"></i>
                <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>Planner Schroeder LH4</p></span>
            </span>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                <i class="bi bi-square-fill fa-stack-2x"></i>
                <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">MULTI RIPSAW Schroeder LS</p></span>
            </span>
        </div>
        <div class="post-info">
            &emsp;&nbsp;<i class="bi bi-arrow-down fa-3x" width="32" height="32"></i>
        </div>
        {{-- ========================Star Baris satu================================ --}}
        <div class="post-info">
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">Sanding Costa</p></span>
                </span>
            </a>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">GDG. Non Moving Prod.2</p></span>
                </span>
            </a>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">T&G Celaschi</p></span>
                </span>
            </a>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">Celaschi P-40</p></span>
                </span>
            </a>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
            </span>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i><text style="color: black">OK</text>
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Gudang</p></span>
                </span>
            </a>
            

        </div>
        {{-- ========================End Baris satu================================ --}}
        {{-- ========================Star Baris Dua================================ --}}
        <div class="post-info">
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<i class="bi bi-arrow-down fa-3x" width="32" height="32"></i>
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<i class="bi bi-arrow-up fa-3x" width="32" height="32"></i>
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;<i class="bi bi-arrow-down fa-3x" width="32" height="32"></i>
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;<i class="bi bi-arrow-down fa-3x" width="32" height="32"></i><text style="color: black">Not OK</text>
            &emsp;&emsp;&emsp;&emsp;&emsp;<i class="bi bi-arrow-up fa-3x" width="32" height="32"></i>
        </div>
        {{-- ========================End Baris Dua================================ --}}
        {{-- ========================Star Baris Tiga================================ --}}
        <div class="post-info">
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            &nbsp;
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 2</p></span>
                </span>
            </a>
            &emsp;<i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-bar-up fa-3x" width="32" height="32"></i>
            &emsp;&emsp;&emsp;&emsp;&emsp;
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Moulding SYC-6S</p></span>
                </span>
            </a>
            &emsp;&emsp;&emsp;
            <a href="{{route('dashboard')}}" title="Back">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 3</p></span>
                </span>
            </a>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            {{-- <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i> --}}
            <i class="bi bi-arrow-up fa-3x" width="32" height="32"></i>
        </div>
        {{-- ========================End Baris Tiga================================ --}}
        {{-- ========================Star Baris Empat================================ --}}
        <div class="post-info">
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<i class="bi bi-arrow-return-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            &nbsp;<i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            &nbsp;<i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            &nbsp;<i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            &nbsp;<i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
            <i class="bi bi-arrow-up fa-3x" width="32" height="32"></i>
        </div>
        {{-- ========================End Baris Empat================================ --}}
</body>
</html>
