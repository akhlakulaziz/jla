
<!DOCTYPE html>
<html lang="en" style="width: 10000px; height: 2000px">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    {{-- <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> --}}
    {{-- <link rel="shortcut icon" href="{{asset('image/'.config('mail.from.app_favicon'))}}">
    <link rel="icon" type="image/png" sizes="192x192" href="">
    <link rel="apple-touch-icon" sizes="180x180" href=""> --}}

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700&display=swap">
    {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
    <link rel="stylesheet" id="css-main" href="{{asset('assets/css/codebase.min.css')}}">
    <style>
        .fa-3x{
            color: black;
        }
        .verikal_center{
            /*mengatur border bagian kiri tag div */
            border-left: 6px solid black;
            /* mengatur tinggi tag div*/
            height: 200px;
            /*mengatur lebar tag div*/
            width : 2px;
        }
        hr.style5 {
            background-color: #fff;
            border-top: 2px dashed #8c8b8b;
        }

    </style>
</head>
<body>
    <center><h2>SOLID WOOD FLOORING</h2></center><br>
        {{-- Start merah --}}
        <div>
            {{-- ====================================Start Baris Satu =============================================--}}
            <div style="margin-left: 20px;">
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Start</p></span>
                    </span>
                </a>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Penerimaan Kayu</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Grade Log</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Hoist</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png" style="margin-top: -15px"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/left-down2.png" style="margin-left: -56px; margin-top: 45px; margin-right: 14px;"/>
                {{-- <img src="https://img.icons8.com/ios-filled/38/000000/left-down.png" style="margin-left: 1px;"/> --}}
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Bandsaw Pony</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: 382px;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Boiler Steam   </p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/up-right--v1.png" style=" margin-left: 110px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dimter</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png"/>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-left: 233px; margin-top: 45px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M72.24,13.76c-15.11719,0 -27.52,12.40281 -27.52,27.52v93.31l-25.93437,-25.93437c-1.30344,-1.33031 -3.07719,-2.08281 -4.945,-2.08281c-2.795,0 -5.30781,1.69312 -6.36938,4.28656c-1.04812,2.59344 -0.43,5.57656 1.58563,7.525l42.70437,42.70437l42.70438,-42.70437c2.6875,-2.6875 2.6875,-7.04125 0,-9.72875c-2.6875,-2.6875 -7.05469,-2.6875 -9.74219,0l-26.24344,26.25687v-93.6325c0,-7.68625 6.07375,-13.76 13.76,-13.76h86c2.48594,0.04031 4.78375,-1.26312 6.03344,-3.41312c1.26313,-2.13656 1.26313,-4.79719 0,-6.93375c-1.24969,-2.15 -3.5475,-3.45344 -6.03344,-3.41313z"></path></g></g></svg>
                <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="115" height="48" style="margin-top: 17px; margin-left: -16px;"/>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: 36px; margin-left: -15px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M58.265,6.73219c-1.78719,0.05375 -3.48031,0.80625 -4.73,2.08281l-42.70437,42.70438l42.70437,42.70437c1.72,1.80063 4.28656,2.52625 6.70531,1.89469c2.40531,-0.63156 4.28656,-2.51281 4.91812,-4.91812c0.63156,-2.41875 -0.09406,-4.98531 -1.89469,-6.70531l-26.015,-26.015h93.47125c7.68625,0 13.76,6.07375 13.76,13.76v86c-0.04031,2.48594 1.26313,4.78375 3.41313,6.03344c2.13656,1.26313 4.79719,1.26313 6.93375,0c2.15,-1.24969 3.45344,-3.5475 3.41312,-6.03344v-86c0,-15.11719 -12.40281,-27.52 -27.52,-27.52h-93.6325l26.17625,-26.17625c2.02906,-1.97531 2.64719,-4.99875 1.54531,-7.61906c-1.11531,-2.60687 -3.70875,-4.27312 -6.54406,-4.1925z"></path></g></g></svg>
                <text style="color: red;">Not OK</text>
                {{-- halo --}}
            </div>
            {{-- ====================================End Baris Satu =============================================--}}
            {{-- ====================================Start Baris Dua =============================================--}}
            <div style="margin-left: 257px; margin-top: 10px;">
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Grade Sawntimber</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <img src="https://img.icons8.com/material/40/000000/forward-arrow--v1.png" style="margin-left: -41px; margin-top: -120px; margin-right: -3px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png" style="margin-top: 25px;"/>

                <img src="https://img.icons8.com/ios-filled/38/000000/up-right.png" style="margin-left: 59px; margin-right: -3px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Bandsaw 36-3</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Treatment  I</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">AIR DRY/KD</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>KILN DRY</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">GRADE KERING</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">CROSS CUT 1</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>D. Planner 1</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Gudang</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/48/000000/long-arrow-right.png"/>
            </div>
            {{-- ====================================End Baris Dua =============================================--}}
            {{-- ====================================Start Baris Satu =============================================--}}
            <div style="margin-left: 257px; margin-top: 10px;">
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="230" style="margin-left: -61px; margin-top: -186px; margin-right: 9px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Grade Balok</p></span>
                </span>
                {{-- <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 149px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="230" style="margin-left: -61px; margin-top: -186px; margin-right: 9px;"/> --}}
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: 190px;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">BS.Auto Kecil</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-left: 1px"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="260" style="margin-left: -39px; margin-top: -186px; margin-right: 9px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 0px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="110" style="margin-left: -61px; margin-top: -92px; margin-right: 9px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Treatment  II</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="130" style="margin-left: -39px; margin-top: -83px; margin-right: 9px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/left-down2.png" style="margin-top: -13px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="100" height="38" style="margin-left: -8px; margin-top: -41px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-left.png" style="margin-top: -56px; margin-left: -15px"/>
                {{-- haloo --}}
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Boiler Hot Water</p></span>
                </span>
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: 15px; margin-top: -46px;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">----------<br>Karet <br>Non Karet</p></span>
                </span>

                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 11px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">CROSS CUT 2</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-left: 2px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 18px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="110" style="margin-left: -61px; margin-top: -92px; margin-right: 9px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>D. Planner 2   </p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="130" style="margin-left: -38px; margin-top: -83px; margin-right: 9px;"/>
                {{-- <img src="https://img.icons8.com/ios-filled/38/fa314a/long-arrow-up.png"/> --}}
                <img src="https://img.icons8.com/ios-filled/38/fa314a/left-up2--v2.png" style="margin-top: -73px; margin-left: 20px;"/>
                <img src="https://img.icons8.com/material/38/fa314a/horizontal-line--v1.png" width="520" height="38" style="margin-top: -44px; margin-left: -56px;"/>
            </div>
            {{-- ====================================End Baris Dua =============================================--}}
            <div style="margin-left: 257px; margin-top: 10px;">
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-down.png" style="margin-top: -46px; margin-left: 64px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="160" style="margin-left: 78px; margin-top: -190px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: -32px; margin-top: -35px;"/>
                <img src="https://img.icons8.com/material/38/000000/horizontal-line--v1.png" width="285" height="38" style="margin-left: -33px; margin-top: -18px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-left: -52px; margin-top: -47px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="100" style="margin-left: -39px; margin-top: -139px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/left-down2.png" style="margin-left: 246px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Multi Rip Extrema</p></span>
                </span>
                <img src="https://img.icons8.com/material/38/000000/reply-arrow--v1.png" style="margin-left: 9px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="130" style="margin-left: 20px; margin-top: -90px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-left.png" style="margin-left: -61px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 47px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">CROSS CUT 3</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
            </div>
            <div style="margin-left: 257px; margin-top: 10px;">
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="160" style="margin-left: 59px; margin-top: -67px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="370" style="margin-left: 12px; margin-top: -366px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-up.png" style="margin-left: -46px; margin-top: -35px;"/>
                
                <img src="https://img.icons8.com/ios-filled/38/000000/left-down2.png" style="margin-left: 52px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-left.png" style="margin-top: -41px; margin-left: -21px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="350" style="margin-left: -32px; margin-top: -367px;"/>

                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-up.png" style="margin-top: -36px; margin-left: -37px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="280" style="margin-left: -46px; margin-top: -322px;"/>

                <img src="https://img.icons8.com/material/38/000000/forward-arrow--v1.png" style="margin-left: -21px; margin-top: -24px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-top: -61px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="140" style="margin-left: -39px; margin-top: -178px;"/>
                <img src="https://img.icons8.com/material/38/000000/forward-arrow--v1.png" style="margin-left: 26px; margin-top: -24px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-top: -66px; margin-left: -8px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="80" style="margin-left: -39px; margin-top: -158px;"/>
                
                <img src="https://img.icons8.com/ios-filled/38/000000/left-down2.png" style="margin-left: 198px; margin-top: 30px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-left.png"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: 7px;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Bandsaw 28</p></span>
                </span>
                <img src="https://img.icons8.com/material/38/000000/reply-arrow--v1.png" style="margin-left: 9px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 100px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">CROSS CUT 4</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
            </div>
            <div style="margin-left: 257px; margin-top: 10px;">
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: 118px; margin-top: -103px;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Loader Log</p></span>
                </span>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="470" style="margin-left: -10px; margin-top: -497px; margin-right: -41px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png" style="margin-top: -103px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-top: -103px;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Bs. Auto Besar</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-up.png" style="margin-top: 44px; margin-left: -66px; margin-right: 28px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png" style="margin-top: -103px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-top: -103px;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Woodmizer</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/left-up2--v2.png" style="margin-left: 280px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="190" style="margin-left: -54px; margin-top: -198px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: 2px;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Bandsaw 26</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-left.png" style="margin-left: 9px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="220" style="margin-left: -33px; margin-top: -196px;"/>

                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 82px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">CROSS CUT 5</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
            </div>
            <div style="margin-left: 257px; margin-top: 10px;">
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="180" style="margin-left: 59px; margin-top: -175px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png"  style="margin-left: -33px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="106" height="48" style="margin-top: 17px; margin-left: -13px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="180" style="margin-left: -6px; margin-top: -252px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png"  style="margin-left: -32px; margin-top: -70px; margin-right: -55px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png" style="margin-left: -10px; margin-top: 16px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Bandsaw 42-1</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="48" height="48" style="margin-left: -18px;"/>
                <img src="https://img.icons8.com/ios-filled/34/000000/right-up2.png" style="margin-left: -13px; margin-top: -25px;"/>
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="98" style="margin-left: -38px; margin-top: -129px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: 580px;"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="670" style="margin-left: -61px; margin-top: -579px; margin-right: 9px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: red;">
                    <i class="bi bi-square-fill fa-stack-2x" width="48" height="190"></i>
                    <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">CROSS CUT 6</p></span>
                </span>
                <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
                <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="670" style="margin-left: -38px; margin-top: -579px; margin-right: 9px;"/>
            </div>
            
        </div>
        <h4 style="margin-left: 100px; margin-bottom: 1px">JLA 2</h4>
        
        <img src="https://img.icons8.com/ios-filled/38/000000/left-down2.png" style="margin-left: 234px;"/>
        <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="768" height="48" style="margin-top: -29px; margin-left: -55px;"/>
        <img src="https://img.icons8.com/ios-filled/38/000000/down-left.png" style="margin-top: -44px; margin-left: -57px;"/>
        <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="280" style="margin-left: -32px; margin-top: -290px;"/>

        {{-- End merah --}}
        
        <div>
            <div style="margin-left: 1810px; margin-top: -630px; margin-bottom: 125px">
                {{-- <h4>JLA 2</h4> --}}
                {{-- ========================Star Baris satu================================ --}}
                <div class="post-info">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i><text style="color: black">OK</text>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black"><br>LMC 623</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i></i><text style="color: black">OK</text>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i></i><text style="color: black">OK</text>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">Sanding Costa</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">GDG. Non Moving Prod.2</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">T&G Celaschi</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">Celaschi P-40</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i><text style="color: black">OK</text>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Gudang</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png" style="margin-left: -42px; margin-top: 41px;"/>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Brush Ott</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Color Superfici</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Brush 1 Superfici</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Brush 2 Fengchaou</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Infra Red  Superfici 3 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Putty Barberan</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer 1 Barberan</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer 2 Barberan</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">SAND. Tagliabue</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Scotch Brite Feng Chaou</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer Superfici</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Superfici 2 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer TA Sane</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">SAND. Chia Lung</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Top Coat 1 Fengchaou</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Top Coat 2 Fengchaou</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">FIN. Product</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Sortir</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>PACKING</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: red;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">END (Stuffing)</p></span>
                        </span>
                    </a>

                </div>
                {{-- ========================End Baris satu================================ --}}

                {{-- ========================Star Baris Dua================================ --}}
                <div class="post-info" style="margin-left: 36px">
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: -15px;"></i>
                    <img src="https://img.icons8.com/material/38/000000/forward-arrow--v1.png" style="margin-left: 18px;"/>
                    <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" style="margin-left: -10px; margin-top: -15px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-top: -44px; margin-left: -34px"/>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32"></i>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 104px; color: red;"></i><text style="color: red;">Not OK</text>
                    <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 61px"></i>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 88px"></i>
                    <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 85px"></i>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 85px"></i>
                    <i class="bi bi-arrow-down-left fa-3x" style="margin-left: 55px; color: red;"></i>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style=" margin-left: -13px; color: red;"></i><text style="color: red;">Not OK</text>
                    <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 68px;"></i>
                    <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style=""></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="200" height="32" style="margin-top: -21px; margin-left: -32px"/>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Manual Color</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -47px;"/>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 115px;">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Start</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sample Machine</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>End</p></span>
                        </span>
                    </a>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 60px;">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Cross Cut</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Planner</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Pallet Export</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 1930px"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="2270" height="32" style="margin-top: -20px; margin-left: -2092px"/>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -44px; margin-left: -102px;"/>
                    
                </div>
                {{-- ========================End Baris Dua================================ --}}
                {{-- ========================Star Baris Tiga================================ --}}
                <div class="post-info" style="margin-left: 7px; margin-top: -24px">
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: 19px; margin-top: -31px; margin-bottom: -21px"/>
                    <i class="bi bi-arrow-90deg-down fa-3x" style="margin-left: 50px"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i><text style="color: black">OK</text>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 5px">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 1</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 15px"></i>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 16px; margin-right: 1px;"/>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 82px">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 2</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 8px"></i>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 7px"/>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 80px">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Moulding SYC-6S</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="color: red"></i>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: -4px">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 3</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    {{-- <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i> --}}
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: 4px; margin-top: -31px; margin-bottom: -21px"/>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -45px; margin-left: -41px"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-up.png" style="margin-left: 900px"/>
                    <a href="{{route('dashboard')}}" title="Back">
                        <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: 2200px;">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Afkir</p></span>
                        </span>
                    </a>
                </div>
                {{-- ========================End Baris Tiga================================ --}}
                {{-- ========================Star Baris Empat================================ --}}
                <div class="post-info" style="margin-left: 173px">
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="color: red;"></i><text style="color: red;">Not OK</text>
                    <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: 80px"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="285" height="32" style="margin-top: -21px; margin-left: -32px"/>
                    <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: -22px"></i>
                    <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: 225px"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="555" height="32" style="margin-top: -21px; margin-left: -301px; "/>
                    <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="150px" style="margin-left:-245px; margin-right:210px; margin-top:-65px;"/>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -45px; margin-left: -32px"/>


                </div>
                {{-- ========================End Baris Empat================================ --}}
                {{-- ========================Start Baris Lima================================ --}}
                <div class="post-info" style="margin-left: 145px">
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="250px" style="margin-left: -68px; margin-top: -305px;"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="250px" style="margin-left: -5px; margin-top: -154px;"/>
                    <a href="{{route('dashboard')}}" title="Back" style="margin-left: 13px">
                        <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                            <i class="bi bi-square-fill fa-stack-2x"></i>
                            <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Cross Cut</p></span>
                        </span>
                    </a>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="250" height="32" style="margin-top: -25px; margin-left: -32px"/>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -48px; margin-left: -30px"/>
                    <img src="https://img.icons8.com/material/48/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left:441px; margin-top:-80px">
                    <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="880px" height="32px" style="margin-left: -830px; margin-top: 41px;"/>
                    <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="725px" height="32px" style="margin-left: -884px; margin-top: 18px;"/>
                    <img src="https://img.icons8.com/ios-filled/48/fa314a/vertical-line.png" width="32px" height="360px" style="margin-left: -79px; margin-top: -311px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/fa314a/long-arrow-up.png" style="margin-left: -39px; margin-top: -653px;"/>
                </div>
                {{-- ========================End Baris Lima================================ --}}
                {{-- ========================Start Baris Lima================================ --}}
                <div class="post-info">
                    <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 71px"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -40px; margin-top: -165px;"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="50px" style="margin-left: -4px; margin-top: -97px; "/>
                    <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: -23px;"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="380" height="32" style="margin-top: -20px; margin-left: -32px"/>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -44px; margin-left: -37px"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="400px" style="margin-top: -366px; margin-left: -29px;"/>
                </div>
                {{-- ========================End Baris Lima================================ --}}
                {{-- ========================Star Baris Enam================================ --}}
                <div class="post-info">
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="700px" style="margin-top: -390px; margin-left: 26px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: -10px; margin-top: -220px;">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                </div>
                {{-- ========================End Baris Enam================================ --}}

                {{-- ========================Start Baris Tujuh================================ --}}
                <div class="post-info" style="margin-top: -191px; margin-left: 2px;">
                    <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: 35px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MULTID- RUMSAW</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PLANNER Schroeder LH4</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">MULTI RIPSAW Schroeder LS</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">GUDANG MOVING PROD.2</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png"/>
                </div>
                {{-- ========================End Baris Tujuh================================ --}}
                <p style="margin-top: -104px; margin-left:210px; color:blue">MOZAIC LINE 1</p>
                <p style="margin-top: 87px; margin-left:210px; color:blue">MOZAIC LINE 2</p>

                {{-- ========================Start Baris Delapan================================ --}}        
                <div class="post-info" style="margin-top: -26px;  margin-left: 2px;">
                    <img src="https://img.icons8.com/ios-filled/48/000000/down-left.png" style="margin-left: -3px; margin-top: -40px;"/>
                    <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: -13px; margin-top: -10px"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MULTID- RUMSAW</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PLANNER Schroeder LH4</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">MULTI RIPSAW Schroeder LS</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png" style="margin-top: -25px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-top: -55px;"/>
                </div>
                {{-- ========================End Baris Delapan================================ --}}
            </div>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="500" style="margin-top: -1187px; margin-left: 908px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="300" style="margin-top: -465px; margin-left: -52px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-top: -200px; margin-left: -32px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="800" height="48" style="margin-top: -184px; margin-left: -51px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="800" height="48" style="margin-top: -184px; margin-left: -74px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="800" height="48" style="margin-top: -184px; margin-left: -74px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="840" height="48" style="margin-top: -184px; margin-left: -74px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-top: -211px; margin-left: -47px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="500" style="margin-top: -1058px; margin-left: -40px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="300" style="margin-top: -465px; margin-left: -51px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="1520" height="48" style="margin-left: -3581px; margin-top: -374px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/left-down2.png" style="margin-left: -1501px; margin-top: -338px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="250" style="margin-left: 1931px; margin-top: -316px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/down-left.png" style="margin-left: -61px; margin-top: -99px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="700" height="48" style="margin-left: -695px; margin-top: -81px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/left-down2.png" style="margin-left: -713px; margin-top: -47px;"/>
            
            <hr class="style5">
            <hr class="style5">
            {{-- +++++++++++++++++++++++++++++++++++++++++++++++JLA 2+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --}}
            {{-- Start merah --}}
            <h4 style="margin-left: 100px;">JLA 1</h4>
            <div style="margin-left: 20px; margin-top: 50px;">
                <div>
                    <img src="https://img.icons8.com/ios-filled/38/fa314a/left-down2.png" style="margin-left: 31px; margin-top: -43px;"/>
                    <img src="https://img.icons8.com/material/48/000000/forward-arrow--v1.png" style="margin-left: 381px; margin-top: -120px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="1050" height="38" style="margin-left: -70px; margin-top: -140px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="1050" height="38" style="margin-left: -70px; margin-top: -140px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="1100" height="38" style="margin-left: -70px; margin-top: -140px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png" style="margin-top: -112px; margin-left: -49px;"/>

                    <img src="https://img.icons8.com/material/38/fa314a/horizontal-line--v1.png" width="1000" height="38" style="margin-top: -71px; margin-left: -3519px;"/>
                    <img src="https://img.icons8.com/material/38/fa314a/horizontal-line--v1.png" width="1000" height="38" style="margin-top: -71px; margin-left: -184px;"/>
                    <img src="https://img.icons8.com/material/38/fa314a/horizontal-line--v1.png" width="1000" height="38" style="margin-top: -71px; margin-left: -184px;"/>
                    <img src="https://img.icons8.com/material/38/fa314a/horizontal-line--v1.png" width="370" height="38" style="margin-top: -71px; margin-left: -184px;"/>
                    <img src="https://img.icons8.com/material/38/fa314a/reply-arrow--v1.png" style="margin-top: -57px; margin-left: -50px;"/>
                </div>
                <div>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 30%; color: black"><br>KD / Conditioning</p></span>
                    </span>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">   Boiler Hot Water</p></span>
                    </span>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Kuang Yung</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png"/>
                    <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="550" style="margin-left: -102px; margin-top: -550px;"/>

                    <img src="https://img.icons8.com/ios-filled/38/000000/up-right--v1.png" style="margin-left: 237px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 38%; color: black">MOULDING SYC 620-1</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png" style="margin-left: 1px;"/>

                </div>
                <div style="margin-top: 10px;">
                    <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-down.png" style="margin-left: 21px; margin-top: -47px;"/>
                    <img src="https://img.icons8.com/material/38/000000/forward-arrow--v1.png" style="margin-left: 67px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: 4px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Bandsaw<br>36-5 </p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png" style="margin-left: 13px;"/>
                    <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="200" height="48" style="margin-left: -15px;"/>
                    <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="500" style="margin-left: -137px; margin-top: -461px;"/>
                    <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="230" style="margin-left: 17px; margin-top: -208px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png" style="margin-left: 5px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">MOULD.<br>UNIMAT SUPER-4</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-right.png" style="margin-left: 10px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">GRADE KERING<br>----------</p></span>
                    </span>

                </div>
                <div style="margin-top: 10px; margin-left: 90px;">
                    <img src="https://img.icons8.com/material/38/fa314a/forward-arrow--v1.png" style="margin-top: -59px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png"/>
                    <img src="https://img.icons8.com/ios-filled/24/000000/vertical-line.png" width="48" height="100" style="margin-left: -61px; margin-right: 11px; margin-top: -100px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Bandsaw<br>36-9 (ST-9)</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
                    <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="205" style="margin-top: -185px; margin-left: -38px;"/>
                    <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="225" style="margin-top: -185px; margin-left: 154px;"/>
                    <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: -31px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">D. PLANNER KCL-600</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png"/>
                    <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="205" style="margin-top: -185px; margin-left: -39px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red; margin-top: -50px; margin-left: -1px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Karet<br>Akasia</p></span>
                    </span>
                </div>
                <div style="margin-top: 10px;">
                    <img src="https://img.icons8.com/material/48/000000/vertical-line.png" width="48" height="200" style="margin-top: -224px; margin-left: 16px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: red; margin-left: -70px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Squaring</p></span>
                    </span>
                    <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png"/>
                </div>
            </div>
            <img src="https://img.icons8.com/ios-filled/38/fa314a/right-up2.png" style="margin-top: -300px; margin-left: 2787px;"/>
            <img src="https://img.icons8.com/ios-filled/38/fa314a/vertical-line.png" width="38" height="300" style="margin-top: -546px; margin-left: -34px;"/>
            {{-- End merah --}}
            <div style="margin-left: 1810px; margin-top: -395px;">
                {{-- <h4>JLA 1</h4> --}}
                {{-- ========================Start Baris Satu================================ --}}
                <div class="post-info">
                    <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 8px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS A BOTTOM (WILLY)</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 8px"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="140" height="32" style="margin-top: -25px; margin-left: -25px"/>
                    <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32" style="margin-left: -16px; margin-top: -4px;"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="170px" style="margin-left: -30px; margin-bottom: -120px"/>

                    <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 730px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING SYC-623/1</p></span>
                    </span>
                    <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="110px" style="margin-left: -30px; margin-bottom: -103px"/>
                </div> 
                {{-- ========================End Baris Satu================================ --}}
                {{-- ********************************************jla1 baris1******************************************************** --}}
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -23px; margin-left: 1710px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M86,21.5c-19.70487,0 -35.83333,16.12847 -35.83333,35.83333v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-11.95747 9.54253,-21.5 21.5,-21.5h64.5v-14.33333z"></path></g></g></svg>
                <img src="	https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="100" height="32" style="margin-top: -49px; margin-left: -19px;"/>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -31px; margin-left: -22px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>
                
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -23px; margin-left: 258px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M86,21.5c-19.70487,0 -35.83333,16.12847 -35.83333,35.83333v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-11.95747 9.54253,-21.5 21.5,-21.5h64.5v-14.33333z"></path></g></g></svg>
                <img src="	https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="100" height="32" style="margin-top: -49px; margin-left: -19px;"/>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -31px; margin-left: -22px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>


                <img src="https://img.icons8.com/material/38/000000/forward-arrow--v1.png" style="margin-left: 225px; margin-top: -53px;"/>
                <img src="https://img.icons8.com/material/38/000000/horizontal-line--v1.png" width="490" height="38" style="margin-top: -70px; margin-left: -71px;"/>
                <img src="https://img.icons8.com/material/38/000000/right-down2--v1.png" style="margin-top: -45px; margin-left: -68px;"/>

                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -23px; margin-left: -406px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M86,21.5c-19.70487,0 -35.83333,16.12847 -35.83333,35.83333v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-11.95747 9.54253,-21.5 21.5,-21.5h64.5v-14.33333z"></path></g></g></svg>
                <img src="	https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="270" height="32" style="margin-top: -49px; margin-left: -30px;"/>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -31px; margin-left: -38px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>

                {{-- ********************************************jla1 baris1******************************************************** --}}
                {{-- ========================Start Baris Dua================================ --}}
                <div class="post-info" style="margin-top: -61px">
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="35" height="32" style="margin-top: -25px"/>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: -8px"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MESIN CKM</p></span>
                    </span>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="135px" style="margin-left: -145px; margin-bottom: -85px"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="90px" style="margin-left: -55px; margin-right: 21px; margin-top: 51px;"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="130px" style="margin-left: -55px; margin-top: -130px;"/>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 137px"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS KUOMING</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="118px" style="margin-left: -40px; margin-top: 70px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>JUMPSAW 1</p></span>
                    </span>
                    <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="130px" style="margin-left: -30px; margin-bottom: -113px"/>
                    <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 265px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MESIN PRESS<br>A</p></span>
                    </span>
                    
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style="fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M21.5,21.5v14.33333h64.5c11.95747,0 21.5,9.54253 21.5,21.5v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-19.70487 -16.12847,-35.83333 -35.83333,-35.83333z"></path></g></g></svg>
                    <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: -30px; margin-bottom: -118px">
                    <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 115px;"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="120px" style="margin-left: -57px; margin-top: -131px; margin-right: 24px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING SYC-423</p></span>
                    </span>
                    <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -30px; margin-bottom: -113px"/>
                    <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: 39px; margin-bottom: -96px">
                    <i class="bi bi-arrow-90deg-right fa-3x" style="color:red; margin-left: -23px; margin-bottom: -50px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>GUDANG MOVING<br>A</p></span>
                    </span>
                    <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: 112px; margin-bottom: -96px">
                    <i class="bi bi-arrow-90deg-left fa-3x" style="color:red; margin-left: -57px; margin-bottom: -50px;"></i>
                    <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="140px" height="32px" style="margin-top: -44px; margin-left: -165px; margin-right: -63px;"/><text style="color: red">Not OK</text>
                    <i class="bi bi-arrow-90deg-right fa-3x" style="color:black; margin-left: 28px;"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="100" height="32" style="margin-top: -43px; margin-left: -26px;"/></i>
                    <text style="margin-left: -45px;">OK</text>
                    <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png"  width="48" height="180" style="margin-top: -247px; margin-left: -4px; margin-right: -49px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>UNIT KEROK</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING WOOD</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>HAND SANDING</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>UNIT COLORING PU</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>CURTAIN COATER</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING SEALER</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>CROSS & ROLL</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PALLET PADAT</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>FINISHING PRODUCT</p></span>
                    </span>
                    <img src="https://img.icons8.com/material/38/000000/horizontal-line--v1.png" width="490" height="30" style="margin-top: 59px; margin-left: -71px;"/>
                    <img src="https://img.icons8.com/material/38/000000/horizontal-line--v1.png" width="300" height="30" style="margin-top: 59px; margin-left: -90px;"/>
                    <img src="https://img.icons8.com/material/38/000000/right-down2--v1.png" style="margin-top: 85px; margin-left: -45px;"/>

                </div>
                {{-- ========================End Baris Dua================================ --}}
                {{-- ========================Start Baris Tiga================================ --}}
                <div class="post-info" style="margin-top: -30px;">
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="180px" style="margin-left: -3px; margin-top: -54px; margin-right: -31px;"/>
                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 30px;"></i>

                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>GRE-CON</p></span>
                    </span>
                    <i class="bi bi-arrow-up-right fa-3x" style="margin-top: -130px;"></i>
                    <i class="bi bi-arrow-90deg-right fa-3x" style="color:red; margin-left: 61px; margin-bottom: -50px;"></i>
                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 2px;"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="118px" style="margin-left: -57px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>JUMPSAW 2</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <img src="https://img.icons8.com/ios-filled/38/fa314a/right-down2.png" style="margin-top: 57px; margin-left: -18px;"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="130px" style="margin-left: -60px; margin-bottom: -66px; margin-right: -2px;"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>GUDANG NON MOVING</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -130px; margin-left: 10px; margin-right: -51px"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>
                    <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="440px" height="32px" style="margin-top: -143px; margin-left: -381px; margin-right: -63px;"/>
                    
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Sortir</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -43px; margin-top: -150px"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="81px" style="margin-left: -32px; margin-top: 45px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS HPC.160</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 10px; color:red"></i>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 90px"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -43px; margin-top: -150px"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="81px" style="margin-left: -32px; margin-top: 45px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING SYC-623/2</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 10px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>M.TORWEGGE</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                    </span>

                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>DEMPUL</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>WIRE SYC 615</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>UNIT COLORING UV</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>DOUBLE BRUSHED</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>AIR JET DRIER</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PUTTY</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER ALOX</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER 1</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING 1 (SHENG SHIENG)</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>BUFFER ROLL</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER 2</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER 3</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>BUFFER ROLL</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>TOP COAT 1</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>TOP COAT 2</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Sortir</span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PACKING</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: red">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>END (STUFFING)</p></span>
                    </span>
                </div>
                {{-- ========================End Baris Tiga================================ --}}
                {{-- ========================Start Baris Emapat================================ --}}
                <div class="post-info" style=" margin-top: -1px; margin-left: 209px;">
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="100px" style="margin-left: -212px; margin-top: -104px; margin-right: 179px"/>
                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: -202px;"></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>STENNER PK-20</p></span>
                    </span>
                    <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING BM</p></span>
                    </span>
                    <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: -35px; margin-top: -125px;">
                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 24px;"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="70px" style="margin-left: -56px; margin-top: -100px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>DOUBLE END</p></span>
                    </span>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 2px; margin-right: -2px;"/>
                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 273px;"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="70px" style="margin-left: -57px; margin-top: -100px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS HPC.250</p></span>
                    </span>
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-left: 3px; margin-top: -50px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M114.66667,21.5l-35.83333,35.83333h28.66667v57.33333c0,11.95747 -9.54253,21.5 -21.5,21.5h-64.5v14.33333h64.5c19.70486,0 35.83333,-16.12847 35.83333,-35.83333v-57.33333h28.66667z"></path></g></g></svg>
                    <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: -32px; margin-top: -180px">

                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 115px;"></i>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="70px" style="margin-left: -57px; margin-top: -100px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING UNIMAT 318</p></span>
                    </span>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 3px;"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="145px" style="margin-top: -151px; margin-left: -29px;"/>
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 2435px; margin-top: -50px"></i>

                </div>
                {{-- ========================End Baris Empat================================ --}}
                {{-- ========================Star Baris Lima================================ --}}
                <div class="post-info" style="">
                    <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 80px;"></i>
                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 115px;"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="460" height="32" style="margin-top: -21px; margin-left: -29px;"/>
                    <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -45px; margin-left: -35px;"/>
                    <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="180px" style="margin-left: -29px; margin-top: -180px"/>
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: 2950px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>Afkir</p></span>
                    </span>
                </div>
                {{-- ========================End Baris Lima================================ --}}
                {{-- ========================Star Baris Enam================================ --}}
                <div class="post-info" style="margin-top: -30px;">
                    <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 96px;"></i>
                    <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="1000" height="32" style="margin-top: -21px; margin-left: -47px;"/>

                </div>
                {{-- ========================End Baris Enam================================ --}}
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="600px" style="margin-left: 1046px; margin-top: -615px; "/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="650px" style="margin-top: -1629px; margin-left: -34px;"/>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -111px; margin-left: -40px;"/>
                <img src="https://img.icons8.com/material/38/000000/reply-arrow--v1.png" width="38" height="38" style="margin-left: -46px; margin-top: -2162px;"/>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="1000" height="32" style="margin-top: -2179px; margin-left: -979px;"/>
                
                {{-- <img src="https://img.icons8.com/ios-filled/24/000000/long-arrow-up.png" width="30px" height="30px" style="margin-top: -797px; margin-left: 795px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="90px" style="margin-left: -34px; margin-top: -711px;"/> --}}
                <img src="https://img.icons8.com/ios-filled/24/000000/long-arrow-down.png" width="30px" height="30px" style="margin-top: -615px; margin-left: 795px;"/>

                <img src="https://img.icons8.com/ios-filled/24/000000/long-arrow-up.png" width="30px" height="30px" style="margin-top: -533px; margin-left: -35px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="50px" style="margin-top: -475px; margin-left: -33px;"/>
                <img src="https://img.icons8.com/material/35/000000/horizontal-line--v1.png" width="155px" height="35px" style="margin-left: -159px; margin-top: -436px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="190px" style="margin-top: -594px; margin-left: -158px;"/>
                <img src="https://img.icons8.com/material/24/000000/reply-arrow--v1.png" width="30px" height="30px" style="margin-top: -765px; margin-left: -43px;"/>
                {{-- <img src="https://img.icons8.com/material/38/fa314a/horizontal-line--v1.png" width="520" height="38" style="margin-top: -44px; margin-left: -56px;"/> --}}
                <img src="https://img.icons8.com/ios-filled/48/fa314a/long-arrow-down.png" style="margin-left: -236px; margin-top: -395px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/vertical-line.png" width="48px" height="170px" style="margin-top: -248px; margin-left: -52px;"/>
                <img src="https://img.icons8.com/ios-filled/38/fa314a/down-left.png" style="margin-left: -60px; margin-top: -76px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/horizontal-line.png" width="600px" height="48px" style="margin-top: -61px; margin-left: -603px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/horizontal-line.png" width="600px" height="48px" style="margin-top: -61px; margin-left: -1143px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/horizontal-line.png" width="600px" height="48px" style="margin-top: -61px; margin-left: -1143px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/horizontal-line.png" width="600px" height="48px" style="margin-top: -61px; margin-left: -1143px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/horizontal-line.png" width="600px" height="48px" style="margin-top: -61px; margin-left: -1143px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/horizontal-line.png" width="600px" height="48px" style="margin-top: -61px; margin-left: -1143px;"/>
                <img src="https://img.icons8.com/ios-filled/38/fa314a/left-up2--v2.png" style="margin-top: -90px; margin-left: -593px;"/>
                <img src="https://img.icons8.com/ios-filled/48/fa314a/vertical-line.png" width="48px" height="300px" style="margin-left: -55px; margin-top: -376px;"/>
                {{-- hitam --}}
                <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48px" height="180px" style="margin-left: 1808px; margin-top: -202px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/down-left.png" style="margin-top: -33px; margin-left: -60px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="600px" height="38px" style="margin-left: -592px; margin-top: -18px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="600px" height="38px" style="margin-left: -1118px; margin-top: -18px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="600px" height="38px" style="margin-left: -1118px; margin-top: -18px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="350px" height="38px" style="margin-left: -906px; margin-top: -18px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/left-up2--v2.png" style="margin-left: -368px; margin-top: -47px;"/>
                <img src="https://img.icons8.com/ios-filled/38/000000/vertical-line.png" width="38x" height="200px" style="margin-left: -49px; margin-top: -204px;"/>
                
            </div>
            {{-- hitam bawah --}}
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="38x" height="200px" style="margin-top: -262px; margin-left: 112px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/down-right.png" style="margin-left: -28px; margin-top: -50px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="600px" height="38px" style="margin-left: -41px; margin-top: -35px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="600px" height="38px" style="margin-left: -79px; margin-top: -35px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="600px" height="38px" style="margin-left: -79px; margin-top: -35px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="600px" height="38px" style="margin-left: -79px; margin-top: -35px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/horizontal-line.png" width="292px" height="38px" style="margin-left: -79px; margin-top: -35px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/right-up2.png" style="margin-top: -64px; margin-left: -29px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/vertical-line.png" width="38px" height="120px" style="margin-left: -34px; margin-top: -153px;"/>

            {{-- merah lurus atas --}}
            <img src="https://img.icons8.com/ios-filled/48/fa314a/vertical-line.png"  width="38px" height="220px" style="margin-left: -237px; margin-top: -303px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="750" style="margin-left: -645px; margin-top: -761px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-down.png" style="margin-left: -47px; margin-top: -71px;"/>
            
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="1000" style="margin-left: 1302px; margin-top: -2244px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-down.png" style="margin-top: -1310px; margin-left: -47px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/long-arrow-up.png" style="margin-top: -3090px; margin-left: 12px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="1050" style="margin-top: -2131px; margin-left: -46px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/left-up2--v2.png" style="margin-top: -1161px; margin-left: -40px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/horizontal-line.png" width="810" height="48" style="margin-left: -58px; margin-top: -1134px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/up-left--v1.png" style="margin-top: -1120px; margin-left: -40px;"/>
            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="100" style="margin-top: -1010px; margin-left: -33px;"/>

            <img src="https://img.icons8.com/ios-filled/48/000000/vertical-line.png" width="48" height="1300" style="margin-top: -1817px; margin-left: 1831px;"/>
            <img src="https://img.icons8.com/ios-filled/38/000000/right-down2.png" style="margin-top: -3031px; margin-left: -55px;"/>

            <img src="https://img.icons8.com/ios-filled/24/000000/left-down2.png" style="margin-left: -2092px; margin-top: -700px;"/>
            <img src="https://img.icons8.com/ios-filled/24/000000/horizontal-line.png"  width="410" height="24" style="margin-left: -31px; margin-top: -718px;"/>
            <img src="https://img.icons8.com/ios-filled/24/000000/right-up2.png" style="margin-left: -21px; margin-top: -736px;"/>
            <img src="https://img.icons8.com/ios-filled/24/000000/vertical-line.png" width="24" height="50" style="margin-left: -22px; margin-top: -770px;"/>
            <img src="https://img.icons8.com/ios-filled/24/000000/long-arrow-up.png" style="margin-left: -28px; margin-top: -826px;"/>
        </div>
</body>
</html>
