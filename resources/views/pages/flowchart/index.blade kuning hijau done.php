
<!DOCTYPE html>
<html lang="en" style="width: 5000px; height: 5000px">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    {{-- <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> --}}
    {{-- <link rel="shortcut icon" href="{{asset('image/'.config('mail.from.app_favicon'))}}">
    <link rel="icon" type="image/png" sizes="192x192" href="">
    <link rel="apple-touch-icon" sizes="180x180" href=""> --}}

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700&display=swap">
    {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
    <link rel="stylesheet" id="css-main" href="{{asset('assets/css/codebase.min.css')}}">
    <style>
        .fa-3x{
            color: black;
        }
        .verikal_center{
            /*mengatur border bagian kiri tag div */
            border-left: 6px solid black;
            /* mengatur tinggi tag div*/
            height: 200px;
            /*mengatur lebar tag div*/
            width : 2px;
        }
        hr.style5 {
            background-color: #fff;
            border-top: 2px dashed #8c8b8b;
        }

    </style>
</head>
<body>
    <center><h2>SOLID WOOD FLOORING</h2></center><br>
        {{-- <div style="margin-left: -154px;">
            <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                <i class="bi bi-square-fill fa-stack-2x"></i>
                <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>Start</p></span>
            </span>
        </div> --}}
        <div>
            <h4>JLA 2</h4>
            {{-- <div class="post-info">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MULTID- RUMSAW</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>Planner Schroeder LH4</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">MULTI RIPSAW Schroeder LS</p></span>
                </span>
            </div>
            <div class="post-info">
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 20px"></i>
            </div> --}}
            {{-- ========================Star Baris satu================================ --}}
            <div class="post-info">
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i><text style="color: black">OK</text>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black"><br>LMC 623</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i></i><text style="color: black">OK</text>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i></i><text style="color: black">OK</text>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">Sanding Costa</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">GDG. Non Moving Prod.2</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">T&G Celaschi</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p style="font-size: 40%; color: black">Celaschi P-40</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i><text style="color: black">OK</text>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Gudang</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Brush Ott</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Color Superfici</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Brush 1 Superfici</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Brush 2 Fengchaou</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Infra Red  Superfici 3 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Putty Barberan</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer 1 Barberan</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer 2 Barberan</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">SAND. Tagliabue</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Scotch Brite Feng Chaou</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer Superfici</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Superfici 2 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sealer TA Sane</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">SAND. Chia Lung</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Top Coat 1 Fengchaou</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Top Coat 2 Fengchaou</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">UV Lamp Fengchaou 2 Lamp</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">FIN. Product</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Sortir</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13);">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>PACKING</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: red;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">END (Stuffing)</p></span>
                    </span>
                </a>

            </div>
            {{-- ========================End Baris satu================================ --}}

            {{-- ========================Star Baris Dua================================ --}}
            <div class="post-info" style="margin-left: 36px">
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: -15px"></i>
                <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 22px; margin-top: 500px;"></i>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 6px"/>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32"></i>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 104px; color: red;"></i><text style="color: red;">Not OK</text>
                <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 61px"></i>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 88px"></i>
                <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 85px"></i>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 85px"></i>
                <i class="bi bi-arrow-down-left fa-3x" style="margin-left: 55px; color: red;"></i>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style=" margin-left: -13px; color: red;"></i><text style="color: red;">Not OK</text>
                <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 68px;"></i>
                <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style=""></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="200" height="32" style="margin-top: -21px; margin-left: -32px"/>
                <a href="{{route('dashboard')}}" title="Back">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Manual Color</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -47px;"/>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 115px;">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Start</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Sample Machine</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>End</p></span>
                    </span>
                </a>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 60px;">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Cross Cut</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Planner</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 18px;">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: -17px; margin-top: 8px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black">Pallet Export</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 1930px"></i>

                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="2270" height="32" style="margin-top: -20px; margin-left: -2092px"/>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -44px; margin-left: -102px;"/>
                
                {{-- halo --}}
            </div>
            {{-- ========================End Baris Dua================================ --}}
            {{-- ========================Star Baris Tiga================================ --}}
            <div class="post-info" style="margin-left: 7px; margin-top: -24px">
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: 19px; margin-top: -31px; margin-bottom: -21px"/>
                <i class="bi bi-arrow-90deg-down fa-3x" style="margin-left: 50px"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i><text style="color: black">OK</text>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 5px">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 1</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 15px"></i>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 17px"/>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 82px">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 2</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 8px"></i>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 7px"/>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 80px">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Moulding SYC-6S</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="color: red"></i>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: -4px">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Dempul 3</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                {{-- <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i> --}}
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: 4px; margin-top: -31px; margin-bottom: -21px"/>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -45px; margin-left: -41px"/>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 1px">
                    <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: 3138px;">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Afkir</p></span>
                    </span>
                </a>
            </div>
            {{-- ========================End Baris Tiga================================ --}}
            {{-- ========================Star Baris Empat================================ --}}
            <div class="post-info" style="margin-left: 173px">
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="color: red;"></i><text style="color: red;">Not OK</text>
                <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: 80px"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="285" height="32" style="margin-top: -21px; margin-left: -32px"/>
                <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: -22px"></i>
                <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: 225px"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="555" height="32" style="margin-top: -21px; margin-left: -301px; "/>
                <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="150px" style="margin-left:-245px; margin-right:210px; margin-top:-65px;"/>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -45px; margin-left: -32px"/>


            </div>
            {{-- ========================End Baris Empat================================ --}}
            {{-- ========================Start Baris Lima================================ --}}
            <div class="post-info" style="margin-left: 145px">
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="250px" style="margin-left: -68px; margin-top: -305px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="250px" style="margin-left: -5px; margin-top: -154px;"/>
                <a href="{{route('dashboard')}}" title="Back" style="margin-left: 13px">
                    <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                        <i class="bi bi-square-fill fa-stack-2x"></i>
                        <span class="fa fa-stack-1x"><p class="text-center" style="font-size: 40%; color: black"><br>Cross Cut</p></span>
                    </span>
                </a>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="250" height="32" style="margin-top: -25px; margin-left: -32px"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="300px" style="margin-left: -23px; margin-top: -250px;"/>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -48px; margin-left: -40px"/>
                <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left:441px; margin-top:-80px">
                <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="880px" height="32px" style="margin-left: -830px; margin-top: 41px;"/>
            </div>
            {{-- ========================End Baris Lima================================ --}}
            {{-- ========================Start Baris Lima================================ --}}
            <div class="post-info">
                <i class="bi bi-arrow-up fa-3x" width="32" height="32" style="margin-left: 71px"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -40px; margin-top: -165px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="50px" style="margin-left: -4px; margin-top: -97px; "/>
                <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: -23px;"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="380" height="32" style="margin-top: -23px; margin-left: -32px"/>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -48px; margin-left: -37px"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="100px" style="margin-top: -139px; margin-left: -27px;"/>
            </div>
            {{-- ========================End Baris Lima================================ --}}
            {{-- ========================Star Baris Enam================================ --}}
            <div class="post-info">
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="700px" style="margin-top: -390px; margin-left: 26px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: -10px; margin-top: -220px;">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
            </div>
            {{-- ========================End Baris Enam================================ --}}

            {{-- ========================Start Baris Tujuh================================ --}}
            <div class="post-info" style="margin-top: -175px; margin-left: 2px;">
                <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: 35px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MULTID- RUMSAW</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PLANNER Schroeder LH4</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">MULTI RIPSAW Schroeder LS</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">GUDANG MOVING PROD.2</p></span>
                </span>
            </div>
            {{-- ========================End Baris Tujuh================================ --}}
            <p style="margin-top: -104px; margin-left:210px; color:blue">MOZAIC LINE 1</p>
            <p style="margin-top: 87px; margin-left:210px; color:blue">MOZAIC LINE 2</p>

            {{-- ========================Start Baris Delapan================================ --}}        
            <div class="post-info" style="margin-top: -26px;  margin-left: 2px;">
                <i class="bi bi-arrow-return-right fa-3x" width="32" height="32" style="margin-left: 35px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MULTID- RUMSAW</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PLANNER Schroeder LH4</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black">MULTI RIPSAW Schroeder LS</p></span>
                </span>
                <i class="bi bi-arrow-up-right fa-3x"></i>
            </div>
            {{-- ========================End Baris Delapan================================ --}}
        </div>
        <hr class="style5">
        <hr class="style5">
        {{-- +++++++++++++++++++++++++++++++++++++++++++++++JLA 2+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --}}
        <div>
            <h4>JLA 1</h4>
            {{-- ========================Start Baris Satu================================ --}}
            <div class="post-info">
                <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 8px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS A BOTTOM (WILLY)</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 8px"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="140" height="32" style="margin-top: -25px; margin-left: -25px"/>
                <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32" style="margin-left: -16px; margin-top: -4px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="170px" style="margin-left: -30px; margin-bottom: -120px"/>

                <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 730px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING SYC-623/1</p></span>
                </span>
                <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="110px" style="margin-left: -30px; margin-bottom: -103px"/>
            </div> 
            {{-- ========================End Baris Satu================================ --}}
            {{-- ********************************************jla1 baris1******************************************************** --}}
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -23px; margin-left: 1710px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M86,21.5c-19.70487,0 -35.83333,16.12847 -35.83333,35.83333v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-11.95747 9.54253,-21.5 21.5,-21.5h64.5v-14.33333z"></path></g></g></svg>
            <img src="	https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="100" height="32" style="margin-top: -49px; margin-left: -19px;"/>
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -31px; margin-left: -22px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>
            
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -23px; margin-left: 258px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M86,21.5c-19.70487,0 -35.83333,16.12847 -35.83333,35.83333v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-11.95747 9.54253,-21.5 21.5,-21.5h64.5v-14.33333z"></path></g></g></svg>
            <img src="	https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="100" height="32" style="margin-top: -49px; margin-left: -19px;"/>
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -31px; margin-left: -22px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>


            <img src="https://img.icons8.com/material/38/000000/forward-arrow--v1.png" style="margin-left: 225px; margin-top: -53px;"/>
            <img src="https://img.icons8.com/material/38/000000/horizontal-line--v1.png" width="490" height="38" style="margin-top: -70px; margin-left: -71px;"/>
            <img src="https://img.icons8.com/material/38/000000/right-down2--v1.png" style="margin-top: -45px; margin-left: -68px;"/>

            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -23px; margin-left: -406px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M86,21.5c-19.70487,0 -35.83333,16.12847 -35.83333,35.83333v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-11.95747 9.54253,-21.5 21.5,-21.5h64.5v-14.33333z"></path></g></g></svg>
            <img src="	https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="270" height="32" style="margin-top: -49px; margin-left: -30px;"/>
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -31px; margin-left: -38px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>

            {{-- ********************************************jla1 baris1******************************************************** --}}
            {{-- ========================Start Baris Dua================================ --}}
            <div class="post-info" style="margin-top: -61px">
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="35" height="32" style="margin-top: -25px"/>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: -8px"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MESIN CKM</p></span>
                </span>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="135px" style="margin-left: -145px; margin-bottom: -85px"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="90px" style="margin-left: -55px; margin-right: 21px; margin-top: 51px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="130px" style="margin-left: -55px; margin-top: -130px;"/>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 137px"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS KUOMING</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="118px" style="margin-left: -40px; margin-top: 70px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>JUMPSAW 1</p></span>
                </span>
                <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="130px" style="margin-left: -30px; margin-bottom: -113px"/>
                <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 265px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MESIN PRESS<br>A</p></span>
                </span>
                
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style="fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M21.5,21.5v14.33333h64.5c11.95747,0 21.5,9.54253 21.5,21.5v57.33333h-28.66667l35.83333,35.83333l35.83333,-35.83333h-28.66667v-57.33333c0,-19.70487 -16.12847,-35.83333 -35.83333,-35.83333z"></path></g></g></svg>
                <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: -30px; margin-bottom: -118px">
                <i class="bi bi-arrow-90deg-right fa-3x" style="margin-left: 115px;"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="120px" style="margin-left: -57px; margin-top: -131px; margin-right: 24px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING SYC-423</p></span>
                </span>
                <img src="https://img.icons8.com/material/24/000000/right-down2--v1.png" width="35" height="32"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -30px; margin-bottom: -113px"/>
                <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: 39px; margin-bottom: -96px">
                <i class="bi bi-arrow-90deg-right fa-3x" style="color:red; margin-left: -23px; margin-bottom: -50px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>GUDANG MOVING<br>A</p></span>
                </span>
                <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: 112px; margin-bottom: -96px">
                <i class="bi bi-arrow-90deg-left fa-3x" style="color:red; margin-left: -57px; margin-bottom: -50px;"></i>
                <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="140px" height="32px" style="margin-top: -44px; margin-left: -165px; margin-right: -63px;"/><text style="color: red">Not OK</text>
                <i class="bi bi-arrow-90deg-right fa-3x" style="color:black; margin-left: 28px;"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="100" height="32" style="margin-top: -43px; margin-left: -26px;"/></i>
                <text style="margin-left: -45px;">OK</text>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>UNIT KEROK</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING WOOD</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>HAND SANDING</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>UNIT COLORING PU</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>CURTAIN COATER</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING SEALER</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>CROSS & ROLL</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PALLET PADAT</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" style="margin-top: -130px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>FINISHING PRODUCT</p></span>
                </span>
                <img src="https://img.icons8.com/material/38/000000/horizontal-line--v1.png" width="490" height="30" style="margin-top: 59px; margin-left: -71px;"/>
                <img src="https://img.icons8.com/material/38/000000/horizontal-line--v1.png" width="300" height="30" style="margin-top: 59px; margin-left: -90px;"/>
                <img src="https://img.icons8.com/material/38/000000/right-down2--v1.png" style="margin-top: 85px; margin-left: -45px;"/>

                {{-- halo --}}

            </div>
            {{-- ========================End Baris Dua================================ --}}
            {{-- ========================Start Baris Tiga================================ --}}
            <div class="post-info" style="margin-top: -30px;">
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="180px" style="margin-left: -3px; margin-top: -54px; margin-right: -31px;"/>
                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 30px;"></i>

                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>GRE-CON</p></span>
                </span>
                <i class="bi bi-arrow-up-right fa-3x" style="margin-top: -130px;"></i>
                <i class="bi bi-arrow-90deg-right fa-3x" style="color:red; margin-left: 61px; margin-bottom: -50px;"></i>
                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 2px;"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="118px" style="margin-left: -57px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>JUMPSAW 2</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="130px" style="margin-left: -36px; margin-bottom: -66px; margin-right: -2px;"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>GUDANG NON MOVING</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-top: -130px; margin-left: 10px; margin-right: -51px"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M52.26628,16.43294l-33.73372,33.73372l33.73372,33.73372l10.13411,-10.13411l-16.43294,-16.43294h61.53255c15.91269,0 28.66667,12.75398 28.66667,28.66667v64.5h14.33333v-64.5c0,-23.66165 -19.33835,-43 -43,-43h-61.53255l16.43294,-16.43294z"></path></g></g></svg>
                <img src="https://img.icons8.com/material/48/fa314a/horizontal-line--v1.png" width="440px" height="32px" style="margin-top: -143px; margin-left: -381px; margin-right: -63px;"/>
                
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Sortir</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -43px; margin-top: -150px"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="81px" style="margin-left: -32px; margin-top: 45px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS HPC.160</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 10px; color:red"></i>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 90px"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="150px" style="margin-left: -43px; margin-top: -150px"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="81px" style="margin-left: -32px; margin-top: 45px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING SYC-623/2</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style="margin-left: 10px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>M.TORWEGGE</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Check</span>
                </span>

                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>DEMPUL</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>WIRE SYC 615</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>UNIT COLORING UV</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>DOUBLE BRUSHED</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>AIR JET DRIER</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PUTTY</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER ALOX</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER 1</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING 1 (SHENG SHIENG)</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>BUFFER ROLL</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER 2</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SEALER 3</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>BUFFER ROLL</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>TOP COAT 1</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>TOP COAT 2</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="fa bi bi-diamond-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x " style="font-size: 20px; color: black">Sortir</span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13)">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PACKING</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: red">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>END (STUFFING)</p></span>
                </span>
            </div>
            {{-- ========================End Baris Tiga================================ --}}
            {{-- ========================Start Baris Emapat================================ --}}
            <div class="post-info" style=" margin-top: -1px; margin-left: 209px;">
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="100px" style="margin-left: -212px; margin-top: -104px; margin-right: 179px"/>
                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: -202px;"></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>STENNER PK-20</p></span>
                </span>
                <i class="bi bi-arrow-right fa-3x" width="32" height="32" style=""></i>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 10px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>SANDING BM</p></span>
                </span>
                <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: -35px; margin-top: -125px;">
                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 24px;"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="70px" style="margin-left: -56px; margin-top: -100px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>DOUBLE END</p></span>
                </span>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 2px; margin-right: -2px;"/>
                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 273px;"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="70px" style="margin-left: -57px; margin-top: -100px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>PRESS HPC.250</p></span>
                </span>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="38" height="38" viewBox="0 0 172 172" style=" fill:#000000; margin-left: 3px; margin-top: -50px;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M114.66667,21.5l-35.83333,35.83333h28.66667v57.33333c0,11.95747 -9.54253,21.5 -21.5,21.5h-64.5v14.33333h64.5c19.70486,0 35.83333,-16.12847 35.83333,-35.83333v-57.33333h28.66667z"></path></g></g></svg>
                <img src="https://img.icons8.com/material/24/fa314a/vertical-line.png" width="30px" height="145px" style="margin-left: -32px; margin-top: -180px">

                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 115px;"></i>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="70px" style="margin-left: -57px; margin-top: -100px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: #ffff04; margin-left: 32px">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>MOULDING UNIMAT 318</p></span>
                </span>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -50px; margin-left: 3px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="145px" style="margin-top: -151px; margin-left: -29px;"/>
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 2435px; margin-top: -50px"></i>

                {{-- halo --}}
            </div>
            {{-- ========================End Baris Empat================================ --}}
            {{-- ========================Star Baris Lima================================ --}}
            <div class="post-info" style="">
                <i class="bi bi-arrow-down fa-3x" width="32" height="32" style="margin-left: 80px;"></i>
                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 115px;"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="460" height="32" style="margin-top: -21px; margin-left: -29px;"/>
                <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -45px; margin-left: -35px;"/>
                <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="180px" style="margin-left: -29px; margin-top: -180px"/>
                <span class="fa-stack fa-3x fa-lg" style="color: rgb(13, 192, 13); margin-left: 2950px;">
                    <i class="bi bi-square-fill fa-stack-2x"></i>
                    <span class="fa fa-stack-1x"><p style="font-size: 30%; color: black"><br>Afkir</p></span>
                </span>
                {{-- halo --}}
            </div>
            {{-- ========================End Baris Lima================================ --}}
            {{-- ========================Star Baris Enam================================ --}}
            <div class="post-info" style="margin-top: -30px;">
                <i class="bi bi-arrow-return-right fa-3x" style="margin-left: 96px;"></i>
                <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="1000" height="32" style="margin-top: -21px; margin-left: -47px;"/>

            </div>
            {{-- ========================End Baris Enam================================ --}}
            <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="500px" style="margin-left: 1046px; margin-top: -522px; "/>
            <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="660px" style="margin-top: -1297px; margin-left: -34px;"/>
            <img src="{{ asset('image/arrow-up-1.png') }}" width="32" height="32" style="margin-top: -111px; margin-left: -40px;"/>
            <img src="https://img.icons8.com/material/38/000000/reply-arrow--v1.png" width="38" height="38" style="margin-left: -46px; margin-top: -1859px;"/>
            <img src="https://img.icons8.com/ios/100/000000/horizontal-line.png" width="1000" height="32" style="margin-top: -1873px; margin-left: -979px;"/>
            {{-- halo --}}
            
            <img src="https://img.icons8.com/ios-filled/24/000000/long-arrow-up.png" width="30px" height="30px" style="margin-top: -797px; margin-left: 795px;"/>
            <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="90px" style="margin-left: -34px; margin-top: -711px;"/>
            <img src="https://img.icons8.com/ios-filled/24/000000/long-arrow-down.png" width="30px" height="30px" style="margin-top: -615px; margin-left: -33px;"/>

            <img src="https://img.icons8.com/ios-filled/24/000000/long-arrow-up.png" width="30px" height="30px" style="margin-top: -533px; margin-left: -35px;"/>
            <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="50px" style="margin-top: -475px; margin-left: -33px;"/>
            <img src="https://img.icons8.com/material/35/000000/horizontal-line--v1.png" width="155px" height="35px" style="margin-left: -159px; margin-top: -436px;"/>
            <img src="https://img.icons8.com/material/24/000000/vertical-line.png" width="30px" height="190px" style="margin-top: -594px; margin-left: -158px;"/>
            <img src="https://img.icons8.com/material/24/000000/reply-arrow--v1.png" width="30px" height="30px" style="margin-top: -765px; margin-left: -43px;"/>
        </div>
</body>
</html>
