<x-app-layout>
    <div class="row">
        <div class="col-md-8">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title"> Bussiness
                    </h3>

                    {{-- <a href="#">
                        <h3 class="block-title font-size-default text-primary">
                            See More
                        </h3>
                    </a> --}}
                </div>
            </div>
            <div class="row items-push">
                @foreach ($business as $item)
                <div class="col-md-6 col-xl-3">
                    <div class="block block-rounded text-center">
                        <div class="block-content block-content-full">
                            @if (!empty($item->image))
                                <img src="{{ asset('profil_usaha/'. $item->image) }}" class="img-avatar"  alt="" srcset="" width="100px" height="100px">
                            @else
                                <img src="{{ asset('profil_usaha/dummy-image.png') }}" alt="" class="img-avatar">
                            @endif
                        </div>
                        <div class="block-content block-content-full block-content-sm bg-body-light">
                            <a href="{{ route('show_details',[$item->uuid] )}}">
                                <div class="font-size-sm text-muted">{{ $item->name }}</div>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title"> Profil
                    </h3>
                </div>
                <div class="content content-full text-center">
                    <a class="img-link mb-3" href="be_pages_generic_profile.html">
                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{ $pemilik->profile_photo_url }}" alt="">
                    </a>
                    <h2 class="h5 ">
                        {{ $pemilik->name }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <!-- END User Profile -->

    <div class="row">
        <div class="col-md-8">

        </div>

        <div class="col-md-4">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title"> Biodata
                    </h3>
                </div>
                <div class="block-content">
                    <div class="font-w600 mb-5">Email</div>
                    <div class="font-size-sm text-muted mb-2">{{ $pemilik->email }}</div>
                    
                    <div class="font-w600 mb-5">Kontak</div>
                    <div class="font-size-sm text-muted mb-2">{{ $pemilik->phone }}</div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>