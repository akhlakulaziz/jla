<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Nama', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('master_products_id') ? 'has-error' : ''}}">
    {!! Form::label('master_products_id', 'Product', ['class' => 'control-label']) !!}
    <select class="form-control" id="master_products_id" name="master_products_id">
        @foreach($product as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('master_products_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('status', (["1" => "Aktif", "2" => "Tidak Aktif"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
 
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Edit' : 'Tambah', ['class' => 'btn btn-primary']) !!}
</div>
