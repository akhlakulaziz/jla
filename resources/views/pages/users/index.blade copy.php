<x-app-layout>
   <!-- Page Content -->
   <nav class="breadcrumb bg-white push">
      <a class="breadcrumb-item" href="{{route('dashboard')}}">Dashboard</a>
      <span class="breadcrumb-item">Akun</span>
      <span class="breadcrumb-item active">Pengguna</span>
   </nav>
<div class="block">
      <div class="block-header block-header-default bg-gray">
         <h3 class="block-title">Tabel Pengguna</h3>
         <div class="col-lg-10">
            <div class="d-flex align-items-center">
               <div class="d-flex align-items-center col-lg-9">
                  @can ('users-create')   
                     <button type="button" class="btn btn-dark me-2" data-toggle="modal" onClick="create()" data-target="#modal-users"><i class="si si-plus"></i> Tambah Pengguna</button>
                  @endcan
                  <a href="{{ route('UserExport') }}" type="button" class="btn btn-success me-2">
                     <i class="fa fa-download" aria-hidden="true"></i> Download 
                  </a>
                  @can ('users-delete')
                     @if(count($trashed) != 0)
                        <a href="{{ route('users.trash') }}" type="button" class="btn btn-warning me-2">
                           <i class="fa fa-trash" aria-hidden="true"></i> Tempat Sampah 
                        </a>
                     @endif
                  @endcan
               </div>
               <div class="col-lg-3">
                  <form action="{{ route('users.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                     <span data-feather="search"></span>
                     <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div class="block-content block-content-full">
         <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/tables_datatables.js -->
         <div class="table-responsive">
            <table id="userTable" class="table table-striped table-vcenter">
               <thead>
                  <tr>
                     <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                     <th>Name</th>
                     <th style="width: 30%;">Email</th>
                     <th style="width: 15%;">Nomor HP</th>
                     <th style="width: 15%;">Jenis Kelamin</th>
                     <th style="width: 15%;">Tempat Lahir</th>
                     <th style="width: 15%;">Tanggal Lahir</th>
                     <th style="width: 15%;">Akses</th>
                     @canany(['users-edit', 'users-delete','user-reset-password',])
                        <th class="text-center" style="width: 100px;">Aksi</th>
                     @endcanany
                  </tr>
               </thead>
               <tbody>
                  @foreach ($users as $user)
                  <tr>
                     <td class="text-center">
                        <img class="img-avatar img-avatar48" src="{{$user->profile_photo_url}}" alt="">
                     </td>
                     <td class="font-w600">{{$user->name}}</td>
                     <td>{{$user->email}}</td>
                     <td>{{$user->phone}}</td>
                     <td>@if($user->gender==2) Perempuan @elseif($user->gender==1) Laki-Laki @endif</td>
                     <td>{{$user->place_of_birth}}</td>
                     <td>{{$user->date_of_birth}}</td>
                     <td>
                        @if(!empty($user->getRoleNames()))
                           @foreach($user->getRoleNames() as $key =>$v)
                              <span class="badge badge-success">{{ $v }}</span>
                           @endforeach
                        @endif
                     {{-- <span class="badge badge-success">VIP</span> --}}
                     </td>
                     @canany(['users-edit', 'users-delete','user-reset-password',])
                     <td class="text-center">
                     <div class="btn-group">
                        @can ('users-edit')
                           <button type="button" onClick="edit({{ json_encode($user) }})" title="Edit" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-users">
                              <i class="fa fa-pencil"></i> 
                           </button>
                        @endcan
                        @can ('users-edit')
                           <button type="button" onClick="login({{ json_encode($user) }})" title="Login As" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-login-sebagai">
                              <i class="fa fa-sign-in"></i>
                           </button>
                        @endcan
                        @can ('users-reset-password')
                           <button type="button" onClick="change({{ $user->id}})" title="Reset Password" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-password">
                              <i class="fa fa-key"></i>
                           </button>
                        @endcan
                        @can ('users-delete')
                           @if($user->id != 1)
                              <button type="button" onClick="remove({{ $user->id}})" title="Delete Pengguna" class="btn btn-sm btn-secondary " data-toggle="modal" data-target="#modal-info-confirmed">
                                 <i class="fa fa-trash"></i>
                              </button>
                           @endif
                        @endcan
                        
                        </div>
                     </td>
                     @endcanany
                  </tr>
                  
                  @endforeach
               </tbody>
            </table>
            <div>{{ $users->appends(['search' => Request::get('search')])->links('component.pagination') }} </div>
         </div> 
   </div>
</div>


   @include('pages.users.modal')
   @include('pages.users.modal-login')
   @include('pages.users.delete')
   @include('pages.users.change-password')
</x-app-layout>
