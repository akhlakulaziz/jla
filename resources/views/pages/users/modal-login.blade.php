<!-- ends: .modal-info-warning -->
{{-- <div class="modal fade" id="modal-login-sebagai" data-backdrop="static"  tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-transparent mb-0">
                <div class="block-header">
                    <h3 class="block-title" id="modal-title-login"></h3>
                    <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                    </div>
                </div>
                <div class="block-content">
                    <p>
                        Apakah Anda yakin ingin Login Sebagai "<span id="name-login" style="color: black;font-weight:600"></span>" ?
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="id-login">
                <button type="button" class="btn btn-info text-uppercase" id="change" onclick="loginAs()">
                    Login
                </button>
                <button type="button" class="btn btn-outline-danger text-uppercase" data-dismiss="modal">Batal</button>
                
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="modal-login-sebagai" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h4 class="mb-1" id="modal-title"></h4>
                </div>
                <div class="p-4 pb-0">
                    <form class="form-horizontal" id="form" action="#">
                        <div class="modal-body">
                            <p>
                                Apakah Anda yakin ingin Login Sebagai "<span id="name-login" style="color: black;font-weight:600"></span>" ?
                            </p>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="id-login">
                <button type="button" class="btn btn-info text-uppercase" id="change" onclick="loginAs()">
                    Login
                </button>
                <button type="button" class="btn btn-outline-danger text-uppercase" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script>
    function login(data){
        $('#modal-title-login').text('Login Pengguna');
        $('#id-login').val(data.id);
        $('#name-login').text(data.name);
    }

    function loginAs(){
        var id =$('#id-login').val();
        var url = "{{ route('user.login-as',":id") }}";
        url = url.replace(':id', id);

        var form_data = new FormData();
            form_data.append('_token', '{{ csrf_token() }}');
            form_data.append('id', id);

        createOverlay("process...");
        $.ajax({
            type: 'GET',
            url: url,
            contentType : false,
            processData : false,
            data: form_data,

            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('dashboard') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }


</script>
