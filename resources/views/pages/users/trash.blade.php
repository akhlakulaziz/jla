{{-- <x-app-layout>
   <!-- Page Content -->
   <nav class="breadcrumb bg-white push">
      <a class="breadcrumb-item" href="{{route('dashboard')}}">Dashboard</a>
      <span class="breadcrumb-item">Akun</span>
      <span class="breadcrumb-item active">Tempat Sampah</span>
   </nav>
   <div class="block">
         <div class="block-header block-header-default bg-gray">
            <h3 class="block-title">Tempat Sampah</h3>
            <div class="col-lg-10">
               <div class="d-flex align-items-center">
                  <div class="d-flex align-items-center col-lg-9">
                     <a href="{{ route('users.index') }}" type="button" class="btn btn-dark mr-2">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali
                     </a>
                     @if(count($users) != 0)
                        <a href="{{ route('users.restoreAll') }}" type="button" class="btn btn-warning">
                           <i class="fa fa-mail-reply-all" aria-hidden="true"></i> Pulihkan Semua
                        </a>
                     @endif
                  </div>
                  <div class="col-lg-3">
                     <form action="{{ route('users.trash') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="block-content block-content-full">
            <div class="table-responsive">
               <table id="userTable" class="table table-striped table-vcenter">
                  <thead>
                     <tr>
                        <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                        <th>Name</th>
                        <th style="width: 25%;">Email</th>
                        <th style="width: 15%;">Jenis Kelamin</th>
                        <th style="width: 15%;">Tempat Lahir</th>
                        <th style="width: 15%;">Akses</th>
                        @canany(['users-delete'])
                           <th class="text-center" style="width: 35%;">Aksi</th>
                        @endcanany
                     </tr>
                  </thead>
                  <tbody>
                     @foreach ($users as $user)
                     <tr>
                        <td class="text-center">
                           <img class="img-avatar img-avatar48" src="{{$user->profile_photo_url}}" alt="">
                        </td>
                        <td class="font-w600">{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>@if($user->gender==2) Perempuan @elseif($user->gender==1) Laki-Laki @endif</td>
                        <td>{{$user->place_of_birth}}</td>
                        <td>
                           @if(!empty($user->getRoleNames()))
                              @foreach($user->getRoleNames() as $key =>$v)
                                 <span class="badge badge-success">{{ $v }}</span>
                              @endforeach
                           @endif
                        </td>
                        @canany(['users-edit', 'users-delete'])
                        <td class="text-center">
                           <div class="btn-group">
                              @can ('users-delete')
                                 <a href="{{ route('users.restore',[$user->id]) }}" type="button" class="btn btn-warning">
                                    <i class="fa fa-mail-reply" aria-hidden="true"></i> Pulihkan
                                 </a>
                                 <button type="button" onClick="force({{$user->id}})" class="btn btn-danger " data-toggle="modal" data-target="#modal-force-confirmed" title="Hapus Paksa">
                                    <i class="fa fa-trash" aria-hidden="true"></i> Hapus Paksa
                                 </button>
                              @endcan
                           </div>
                        </td>
                        @endcanany
                     </tr>
                     
                     @endforeach
                  </tbody>
               </table>
               <div>{{ $users->appends(['search' => Request::get('search')])->links('component.pagination') }} </div>
            </div> 
      </div>
   </div>

   @include('pages.users.force-delete')
</x-app-layout> --}}

<x-app-layout>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Tempat Sampah</h5>
               </div>
               
               {{-- <div class="col-auto ms-auto">
                  <div class="nav nav-pills nav-pills-falcon flex-grow-1 mt-2" role="tablist">
                     <form action="{{ route('users.trash') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                     </form>
                  </div>
               </div> --}}
            </div>
            <a href="{{ route('users.index') }}" type="button" class="btn btn-dark mr-2">
               <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali
            </a>
            @if(count($users) != 0)
               <a href="{{ route('users.restoreAll') }}" type="button" class="btn btn-warning">
                  <i class="fas fa-reply-all" aria-hidden="true"></i> Pulihkan Semua
               </a>
            @endif
            
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="table-responsive scrollbar">
                     <table class="table">
                        <thead>
                           <tr>
                              <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                              <th>Name</th>
                              <th style="width: 25%;">Email</th>
                              <th style="width: 15%;">Jenis Kelamin</th>
                              <th style="width: 15%;">Tempat Lahir</th>
                              <th style="width: 15%;">Akses</th>
                              @canany(['users-delete'])
                                 <th class="text-center" style="width: 35%;">Aksi</th>
                              @endcanany
                           </tr>
                        </thead>
                        <tbody>
                           @foreach ($users as $user)
                     <tr>
                        <td class="text-center">
                           <img class="img-avatar img-avatar48" src="{{$user->profile_photo_url}}" alt="">
                        </td>
                        <td class="font-w600">{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>@if($user->gender==2) Perempuan @elseif($user->gender==1) Laki-Laki @endif</td>
                        <td>{{$user->place_of_birth}}</td>
                        <td>
                           @if(!empty($user->getRoleNames()))
                              @foreach($user->getRoleNames() as $key =>$v)
                                 <span>{{ $v }}</span>
                              @endforeach
                           @endif
                        </td>
                        @canany(['users-edit', 'users-delete'])
                        <td class="text-center">
                           <div class="btn-group">
                              @can ('users-delete')
                                 <a href="{{ route('users.restore',[$user->id]) }}" type="button" class="btn btn-warning">
                                    <i class="fas fa-reply" aria-hidden="true"></i> Pulihkan
                                 </a>
                                 <button type="button" onClick="force({{$user->id}})" class="btn btn-danger " data-toggle="modal" data-target="#modal-force-confirmed" title="Hapus Paksa">
                                    <i class="fa fa-trash" aria-hidden="true"></i> Hapus Paksa
                                 </button>
                              @endcan
                           </div>
                        </td>
                        @endcanany
                     </tr>
                     
                     @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @include('pages.users.force-delete')
</x-app-layout>