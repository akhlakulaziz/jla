{{-- <div class="modal fade" id="modal-password" data-backdrop="static"  tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-transparent mb-0">
                <div class="block-header">
                    <h3 class="block-title" id="modal-title">
                        Reset Password
                    </h3>
                    <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                    </div>
                </div>
                <div class="block-content">
                    <form>
                        <div class="form-group mb-1">
                            <input type="hidden" class="form-control" id="idUser" name="id" disabled>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                                {!! Form::label('password', 'Password Baru: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="las la-lock color-gray" id="icon-password"></span>
                                    <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" name="password" id="password2">
                                </div>
                            
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ''}}">
                                {!! Form::label('password_confirmation', 'Konfirmasi Password Baru: ', ['class' => 'control-label','id'=>'label-password']) !!}
                                <div class="with-icon">
                                    <span class="las la-lock color-gray" id="icon-password"></span>
                                    <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" name="password_confirmation" id="password_confirmation">
                                </div>
                                {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                            </div> 
                    </form>
                </div>
            </div>
            <div class="modal-footer">
              
                <button type="button" class="btn btn-outline-secondary text-uppercase" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-dark text-uppercase"  id="change" onclick="updatePassword()">
                    Edit
                </button>
        </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="modal-password" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h4 class="mb-1" id="modal-title"></h4>
                </div>
                <div class="p-4 pb-0">
                    <form class="form-horizontal" id="form" action="#">
                        <div class="modal-body">
                            <input type="hidden" class="form-control" id="idUser" name="id" disabled>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                                {!! Form::label('password', 'Password Baru: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="las la-lock color-gray" id="icon-password"></span>
                                    <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" name="password" id="password2">
                                </div>
                            
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ''}}">
                                {!! Form::label('password_confirmation', 'Konfirmasi Password Baru: ', ['class' => 'control-label','id'=>'label-password']) !!}
                                <div class="with-icon">
                                    <span class="las la-lock color-gray" id="icon-password"></span>
                                    <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" name="password_confirmation" id="password_confirmation">
                                </div>
                                {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary text-uppercase" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-dark text-uppercase"  id="change" onclick="updatePassword()">
                    Edit
                </button>
            </div>
        </div>
    </div>
</div>

<script> 
    function change(id){
        $('#modal-title').text('New Password');
        $('#idUser').val(id);
    }

    function updatePassword() {
    var id = $('#idUser').val();
    var password2 = $('#password2').val();
    var password_confirmation = $('#password_confirmation').val();

    var form_data = new FormData();
    form_data.append('_token', '{{ csrf_token() }}');
    form_data.append('id', id);
    form_data.append('password', password2);
    form_data.append('password_confirmation', password_confirmation);
    createOverlay("process...");

    $.ajax({
        type : "POST",
        url: "{!! route('update.password',["id"]) !!}",
        dataType: 'json',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        success: function(data){
            gOverlay.hide();
            if(data["status"] == "success") {            
                toastr.success(data["message"]); 
                doc_val_check = "";
                $('#password2').val(doc_val_check);
                $('#password_confirmation').val(doc_val_check);
                setTimeout(function(){ 
                    window.location = "{{ route('users.index') }}";
                }, 500); 
            }else {
                toastr.error(data["message"]);
            }
        },
        error: function(error) {
            alert("Server/network error\r\n" + error);
        }
    });
    }
</script>