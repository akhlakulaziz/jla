{{-- <div class="modal fade" id="modal-users" data-backdrop="static"  tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-transparent mb-0">
                <div class="block-header">
                    <h3 class="block-title" id="modal-title">
                    </h3>
                    <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                    </div>
                </div>
                <div class="block-content" style="padding: 0px 0px 1px;">
                    <form class="form-horizontal" id="form" action="#">
                        <div class="modal-body">
                            <input type="hidden" class="form-control" id="id" disabled>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                                {!! Form::label('name', 'Nama: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::text('name', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'name','required' => 'required','placeholder'=>'Contoh: Duran Clayton']) !!}
                                </div>
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                                {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="lar la-envelope color-gray"></span>
                                    {!! Form::email('email', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'email','required' => 'required','placeholder'=>'Contoh: username@email.com']) !!}
                                </div>
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
                                {!! Form::label('phone', 'Nomor HP: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::number('phone', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'phone','required' => 'required','placeholder'=>'Contoh: 08xxxxx']) !!}
                                </div>
                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
                                {!! Form::label('address', 'Alamat: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::textarea('address', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'address','required' => 'required','placeholder'=>'Contoh : Jl. Tanjungsari 1']) !!}
                                </div>
                                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                                {!! Form::label('gender', 'Jenis Kelamin', ['class' => 'control-label']) !!}
                                {!! Form::select('gender', (["1" => "Laki-laki", "2" => "Perempuan"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Jenis Kelamin']) !!}
                                {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('npwp') ? ' has-error' : ''}}">
                                {!! Form::label('npwp', 'NPWP: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::number('npwp', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'npwp','required' => 'required','placeholder'=>'Contoh: 564xxxxx']) !!}
                                </div>
                                {!! $errors->first('npwp', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('nik') ? ' has-error' : ''}}">
                                {!! Form::label('nik', 'NIK: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::number('nik', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'nik','required' => 'required','placeholder'=>'Contoh: P003xxxxx']) !!}
                                </div>
                                {!! $errors->first('nik', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : ''}}">
                                {!! Form::label('place_of_birth', 'Tempat Lahir: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::text('place_of_birth', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'place_of_birth','required' => 'required','placeholder'=>'Contoh : Magelang']) !!}
                                </div>
                                {!! $errors->first('place_of_birth', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : ''}}">
                                {!! Form::label('date_of_birth', 'Tanggal Lahir', ['class' => 'control-label']) !!}
                                {!! Form::date('date_of_birth', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                                {!! Form::label('password', 'Password: ', ['class' => 'control-label','id'=>'label-password']) !!}
                                <div class="with-icon">
                                    <span class="las la-lock color-gray" id="icon-password"></span>
                                    <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" id="password">
                                </div>
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('JRU') ? 'has-error' : ''}}">
                                {!! Form::label('JRU', 'Keanggotaan', ['class' => 'control-label']) !!}
                                {!! Form::select('JRU', (["1" => "JRU", "2" => "Non JRU"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Keanggotaan']) !!}
                                {!! $errors->first('JRU', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
                                {!! Form::label('role', 'Akses: ', ['class' => 'control-label']) !!}
                                {!! Form::select('roles[]', $list_role, isset($user_roles) ? $user_roles : [], ['class' => 'form-control', 'multiple' => true,'id'=>'roles']) !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary text-uppercase" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-dark text-uppercase"  id="create" onclick="store()">
                    Simpan
                </button>
                <button type="button" class="btn btn-dark text-uppercase"  id="update" onclick="updated()">
                    Edit
                </button>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="modal-users" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h4 class="mb-1" id="modal-title"></h4>
                </div>
                <div class="p-4 pb-0">
                    <form class="form-horizontal" id="form" action="#">
                        <div class="modal-body">
                            <input type="hidden" class="form-control" id="id" disabled>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                                {!! Form::label('name', 'Nama: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::text('name', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'name','required' => 'required','placeholder'=>'Contoh: Duran Clayton']) !!}
                                </div>
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                                {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="lar la-envelope color-gray"></span>
                                    {!! Form::email('email', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'email','required' => 'required','placeholder'=>'Contoh: username@email.com']) !!}
                                </div>
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
                                {!! Form::label('phone', 'Nomor HP: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::number('phone', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'phone','required' => 'required','placeholder'=>'Contoh: 08xxxxx']) !!}
                                </div>
                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
                                {!! Form::label('address', 'Alamat: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::textarea('address', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'address','required' => 'required','placeholder'=>'Contoh : Jl. Tanjungsari 1']) !!}
                                </div>
                                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                                {!! Form::label('gender', 'Jenis Kelamin', ['class' => 'control-label']) !!}
                                {!! Form::select('gender', (["1" => "Laki-laki", "2" => "Perempuan"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Jenis Kelamin']) !!}
                                {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('npwp') ? ' has-error' : ''}}">
                                {!! Form::label('npwp', 'NPWP: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::number('npwp', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'npwp','required' => 'required','placeholder'=>'Contoh: 564xxxxx']) !!}
                                </div>
                                {!! $errors->first('npwp', '<p class="help-block">:message</p>') !!}
                            </div>
                            {{-- <div class="form-group{{ $errors->has('nik') ? ' has-error' : ''}}">
                                {!! Form::label('nik', 'NIK: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::number('nik', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'nik','required' => 'required','placeholder'=>'Contoh: P003xxxxx']) !!}
                                </div>
                                {!! $errors->first('nik', '<p class="help-block">:message</p>') !!}
                            </div> --}}
                            <div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : ''}}">
                                {!! Form::label('place_of_birth', 'Tempat Lahir: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                    {!! Form::text('place_of_birth', null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'place_of_birth','required' => 'required','placeholder'=>'Contoh : Magelang']) !!}
                                </div>
                                {!! $errors->first('place_of_birth', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : ''}}">
                                {!! Form::label('date_of_birth', 'Tanggal Lahir', ['class' => 'control-label']) !!}
                                {!! Form::date('date_of_birth', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                                {!! Form::label('password', 'Password: ', ['class' => 'control-label','id'=>'label-password']) !!}
                                <div class="with-icon">
                                    <span class="las la-lock color-gray" id="icon-password"></span>
                                    <input type="password" class="form-control  ih-medium ip-gray radius-xs b-light" id="password">
                                </div>
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                            {{-- <div class="form-group {{ $errors->has('JRU') ? 'has-error' : ''}}">
                                {!! Form::label('JRU', 'Keanggotaan', ['class' => 'control-label']) !!}
                                {!! Form::select('JRU', (["1" => "JRU", "2" => "Non JRU"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Keanggotaan']) !!}
                                {!! $errors->first('JRU', '<p class="help-block">:message</p>') !!}
                            </div> --}}
                            <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
                                {!! Form::label('role', 'Akses: ', ['class' => 'control-label']) !!}
                                {!! Form::select('roles[]', $list_role, isset($user_roles) ? $user_roles : [], ['class' => 'form-control', 'multiple' => true,'id'=>'roles']) !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="button" id="create" onclick="store()">Simpan</button>
                <button class="btn btn-primary" type="button" id="update" onclick="updated()">Update</button>
            </div>
        </div>
    </div>
</div>
<script>
    function edit(data){
    
        var role = [];
        Object.values(data.roles).forEach(val => {
            role.push(val.id);
        });
        $('#modal-title').text('Edit Data');
        $('#id').val(data.id);
        $('#name').val(data.name);
        $('#email').val(data.email);
        $('#phone').val(data.phone);
        $('#address').val(data.address);
        $('#gender').val(data.gender);
        $('#npwp').val(data.npwp);
        // $('#nik').val(data.nik);
        $('#place_of_birth').val(data.place_of_birth);
        $('#date_of_birth').val(data.date_of_birth);
        // $('#JRU').val(data.JRU);
        $('#roles').val(role);
        $("#password").hide(); 
        $("#label-password").hide();
        $("#icon-password").hide();
        $("#create").hide();
        $("#update").show();
    }
    
    function create() {
        $('#modal-title').text('Add Data Item');
        $('#form').trigger("reset");
        $("#update").hide();
        $("#create").show();
        $("#modal-basic").modal('show');
    }




function store() {

        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var address = $('#address').val();
        var gender = $('#gender').val();
        var npwp = $('#npwp').val();
        // var nik = $('#nik').val();
        var place_of_birth = $('#place_of_birth').val();
        var date_of_birth = $('#date_of_birth').val();
        var password = $('#password').val();
        // var jru = $('#JRU').val();
        var roles = $('#roles').val();
        
        createOverlay("Process...");

        $.ajax({
            type : "POST",
            url: "{!! route('users.store') !!}",
            data: {
                '_token' : '{{ csrf_token() }}',
                'name' :name,
                'email' :email,
                'phone' :phone,
                'address' :address,
                'gender' :gender,
                'npwp' :npwp,
                // 'nik' :nik,
                'place_of_birth' :place_of_birth,
                'date_of_birth' :date_of_birth,
                'password' :password,
                // 'JRU' :jru,
                'roles' :roles,
            },
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('users.index') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }

    function updated(){

        var id = $('#id').val();
        var name = $('#name').val();
        var phone = $('#phone').val();
        var address = $('#address').val();
        var gender = $('#gender').val();
        var npwp = $('#npwp').val();
        // var nik = $('#nik').val();
        var place_of_birth = $('#place_of_birth').val();
        var date_of_birth = $('#date_of_birth').val();
        // var jru = $('#JRU').val();
        var roles = $('#roles').val();
    
        createOverlay("Proses...");
        $.ajax({
            type : "PUT",
            url: "users/"+id,
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'id' : id,
                'name' :name,
                'phone' :phone,
                'address' :address,
                'gender' :gender,
                'npwp' :npwp,
                // 'nik' :nik,
                'place_of_birth' :place_of_birth,
                'date_of_birth' :date_of_birth,
                // 'JRU' :jru,
                'roles' :roles,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                            window.location = "{{ route('users.index') }}";
                    }, 500);              
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
        
    }

</script> 