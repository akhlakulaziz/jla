<x-app-layout>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Tabel Pengguna</h5>
               </div>
               
               <div class="col-auto ms-auto">
                  <div class="nav nav-pills nav-pills-falcon flex-grow-1 mt-2" role="tablist">
                     <form action="{{ route('users.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-auto align-self-center" style="margin-bottom: 10px;">
               <button type="button" class="btn btn-success btn-sm" title="Add New User" data-toggle="modal" onClick="create()" data-target="#modal-users"><i class="si si-plus"></i> Tambah Pengguna</button>
            </div>
            <div class="col-auto align-self-center">
               @can ('users-delete')
                  @if(count($trashed) != 0)
                     <a href="{{ route('users.trash') }}" type="button" class="btn btn-warning me-2">
                        <i class="fa fa-trash" aria-hidden="true"></i> Tempat Sampah 
                     </a>
                  @endif
               @endcan
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="table-responsive scrollbar">
                     <table class="table">
                        <thead>
                           <tr>
                              <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                              <th>Name</th>
                              <th style="width: 30%;">Email</th>
                              <th style="width: 15%;">Nomor HP</th>
                              <th style="width: 15%;">Jenis Kelamin</th>
                              <th style="width: 15%;">Tempat Lahir</th>
                              <th style="width: 15%;">Tanggal Lahir</th>
                              <th style="width: 15%;">Akses</th>
                              @canany(['users-edit', 'users-delete','user-reset-password',])
                                 <th class="text-center" style="width: 100px;">Actions</th>
                              @endcanany
                           </tr>
                        </thead>
                        <tbody>
                           @foreach ($users as $user)
                  <tr>
                     <td class="text-center">
                        <img class="img-avatar img-avatar48" src="{{$user->profile_photo_url}}" alt="">
                     </td>
                     <td class="font-w600">{{$user->name}}</td>
                     <td>{{$user->email}}</td>
                     <td>{{$user->phone}}</td>
                     <td>@if($user->gender==2) Perempuan @elseif($user->gender==1) Laki-Laki @endif</td>
                     <td>{{$user->place_of_birth}}</td>
                     <td>{{$user->date_of_birth}}</td>
                     <td>
                        @if(!empty($user->getRoleNames()))
                           @foreach($user->getRoleNames() as $key =>$v)
                              <span>{{ $v }}</span>
                           @endforeach
                        @endif
                     {{-- <span class="badge badge-success">VIP</span> --}}
                     </td>
                     @canany(['users-edit', 'users-delete','user-reset-password',])
                     <td class="text-center">
                     <div class="btn-group">
                        @can ('users-edit')
                           <button type="button" onClick="edit({{ json_encode($user) }})" title="Edit" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-users">
                              <i class="fas fa-pencil-alt"></i> 
                           </button>
                        @endcan
                        @can ('users-edit')
                           <button type="button" onClick="login({{ json_encode($user) }})" title="Login As" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-login-sebagai">
                              <i class="fas fa-sign-in-alt"></i>
                           </button>
                        @endcan
                        @can ('users-reset-password')
                           <button type="button" onClick="change({{ $user->id}})" title="Reset Password" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-password">
                              <i class="fa fa-key"></i>
                           </button>
                        @endcan
                        @can ('users-delete')
                           @if($user->id != 1)
                              <button type="button" onClick="remove({{ $user->id}})" title="Delete Pengguna" class="btn btn-sm btn-secondary " data-toggle="modal" data-target="#modal-info-confirmed">
                                 <i class="fa fa-trash"></i>
                              </button>
                           @endif
                        @endcan
                        
                        </div>
                     </td>
                     @endcanany
                  </tr>
                  
                  @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @include('pages.users.modal')
   @include('pages.users.modal-login')
   @include('pages.users.delete')
   @include('pages.users.change-password')
</x-app-layout>
