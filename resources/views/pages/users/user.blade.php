<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Nomor HP</th>
        <th>Alamat</th>
        <th>Gender</th>
        <th>NPWP</th>
        {{-- <th>NIK</th> --}}
        <th>Tempat Lahir</th>
        <th>Tanggal Lahir</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->phone }}</td>
            <td>{{ $item->address }}</td>
            <td>@if($item->gender==2) Perempuan @elseif($item->gender==1) Laki-Laki @endif</td>
            <td>{{ $item->npwp }}</td>
            {{-- <td>{{ $item->nik }}</td> --}}
            <td>{{ $item->place_of_birth }}</td>
            <td>{{ $item->date_of_birth }}</td>
        </tr>
    @endforeach
    </tbody>
</table>