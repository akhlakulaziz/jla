<div class="form-group {{ $errors->has('nik') ? 'has-error' : ''}}">
    {!! Form::label('nik', 'NIK', ['class' => 'control-label']) !!}
    {!! Form::number('nik', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nik', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Nama', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('master_status_tenaga_kerjas_id') ? 'has-error' : ''}}">
    {!! Form::label('master_status_tenaga_kerjas_id', 'Product', ['class' => 'control-label']) !!}
    <select class="form-control" id="master_status_tenaga_kerjas_id" name="master_status_tenaga_kerjas_id">
        @foreach($master_status_tenaga_kerjas_id as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('master_status_tenaga_kerjas_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
    {!! Form::select('status', (["1" => "Aktif", "2" => "Tidak Aktif"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Edit' : 'Tambah', ['class' => 'btn btn-primary']) !!}
</div>
