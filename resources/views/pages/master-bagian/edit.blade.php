
{{-- <x-app-layout>
    <nav class="breadcrumb bg-white push">
        <span class="breadcrumb-item">Bidang Bagian</span>
        <span class="breadcrumb-item active">Edit</span>
    </nav>
    <a href="{{ route('master-bagian.index') }}" title="Back"><button class="btn btn-dark btn-md mb-2">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</button>
    </a>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Edit Master Bagian</h3>
        </div>
        <div class="block-content">
            <div class="row item-push">
                <div class="col-lg-3">
                    <p>
                        
                    </p>
                </div>
                <div class="col-lg-7 offset-lg-1">
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($masterbagian, [
                    'method' => 'PATCH',
                    'route' => ['master-bagian.update', $masterbagian->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('pages.master-bagian.form', ['formMode' => 'edit'])

                {!! Form::close() !!}
                </div>
            </div>
            
        </div>
    </div>

</x-app-layout> --}}

<x-app-layout>
    <div class="card mb-3">
        <div class="card-header">
            <div class="row flex-between-end">
                <div class="col-auto align-self-center">
                    <h5 class="mb-0">Edit Master Bagian</h5>
                </div>
            </div>
            <div class="col-auto align-self-center">
                <a href="{{ route('master-bagian.index') }}" class="btn btn-dark btn-sm" title="Back">
                    <span class="fa fa-arrow-left" data-fa-transform="shrink-3 down-2"></span>
                    <span class="ms-1">Back</span>
                </a>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="tab-content">
                <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-55d552bf-cdbd-40f9-856d-410188578fda" id="dom-55d552bf-cdbd-40f9-856d-410188578fda">
                    <div class="table-responsive scrollbar">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($masterbagian, [
                            'method' => 'PATCH',
                            'route' => ['master-bagian.update', $masterbagian->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('pages.master-bagian.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>