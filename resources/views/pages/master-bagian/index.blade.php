<x-app-layout>
    <div class="card mb-3">
        <div class="card-header">
            <div class="row flex-between-end">
                <div class="col-auto align-self-center">
                    <h5 class="mb-0">Master Bagian</h5>
                </div>
                <div class="col-auto ms-auto">
                    <div class="nav nav-pills nav-pills-falcon flex-grow-1" role="tablist">
                        {!! Form::open(['method' => 'GET', 'route' => 'master-bagian.index', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Pencarian" value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search" style="color: white"></i>
                                    </button>
                                </span>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-auto align-self-center">
                @can ('master-bagian-create') 
                    <a href="{{ route('master-bagian.create') }}" class="btn btn-success btn-sm" title="Add New MasterBagian">
                            <span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span>
                            <span class="ms-1">New</span>
                    </a>
                @endcan
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="tab-content">
                <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-55d552bf-cdbd-40f9-856d-410188578fda" id="dom-55d552bf-cdbd-40f9-856d-410188578fda">
                    <div class="table-responsive scrollbar">
                        <table class="table table-bordered table-striped fs--1 mb-0">
                            <thead class="bg-200 text-900">
                                <tr>
                                    <th class="sort" data-sort="no">Nomer</th>
                                    <th class="sort" data-sort="nama">Nama</th>
                                    <th class="sort" data-sort="nama">Departemen</th>
                                    <th class="sort" data-sort="status">Status</th>
                                    <th class="sort" data-sort="aksi">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach($masterbagian as $item)
                                    <tr>
                                        <td class="nama">{{ $loop->iteration }}</td>
                                        <td class="status">{{ $item->name }}</td>
                                        <td class="status">{{ $item->departemen->name }}</td>
                                        <td class="aksi">@if($item->status==2)
                                            Tidak Aktif
                                            @elseif($item->status==1)
                                            Aktif
                                            @endif
                                        </td>
                                        <td>
                                            @canany(['master-bagian-view', 'master-bagian-edit', 'master-bagian-delete',])
                                                @can ('master-bagian-view')
                                                <a href="{{ route('master-bagian.show',[$item->id]) }}" title="View MasterBagian"><button class="btn btn-info btn-sm"><i class="fas fa-eye" aria-hidden="true"></i> Detail</button></a>
                                                @endcan
                                                @can ('master-bagian-edit')
                                                <a href="{{ route('master-bagian.edit',[$item->id]) }}" title="Edit MasterBagian"><button class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true"></i> Edit</button></a>
                                                @endcan
                                                @can ('master-bagian-delete')
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'route' => ['master-bagian.destroy', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fas fa-trash-alt" aria-hidden="true"></i> Hapus', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-sm',
                                                            'title' => 'Delete MasterBagian',
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                                @endcan
                                            @endcan
                                            </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                        
            </div>
        </div>
    </div>
</x-app-layout>