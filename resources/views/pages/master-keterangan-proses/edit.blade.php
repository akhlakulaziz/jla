<x-app-layout>
    <div class="card mb-3">
        <div class="card-header">
            <div class="row flex-between-end">
                <div class="col-auto align-self-center">
                    <h5 class="mb-0">Edit Master Keterangan Proses</h5>
                </div>
            </div>
            <div class="col-auto align-self-center">
                <a href="{{ route('master-keterangan-proses.index') }}" class="btn btn-dark btn-sm" title="Back">
                    <span class="fa fa-arrow-left" data-fa-transform="shrink-3 down-2"></span>
                    <span class="ms-1">Back</span>
                </a>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="tab-content">
                <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-55d552bf-cdbd-40f9-856d-410188578fda" id="dom-55d552bf-cdbd-40f9-856d-410188578fda">
                    <div class="table-responsive scrollbar">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($masterKeteranganProses, [
                            'method' => 'PATCH',
                            'route' => ['master-keterangan-proses.update', $masterKeteranganProses->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('pages.master-keterangan-proses.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>