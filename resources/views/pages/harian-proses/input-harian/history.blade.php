<x-app-layout>
   <div class="col-lg-12">
      <div class="col-auto align-self-center" style="margin-bottom: 5px;">
         <a href="javascript:history.go(-1)" title="Back">
            <button class="btn btn-dark btn-md mb-2"><i class="fa fa-arrow-left" aria-hidden="true" value="Back"></i> Back</button>
         </a>
         <form action="{{ route('input-harian.list') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input class="form-control" id="tanggal" name="tanggal" type="hidden" value="{{$tanggal}}"/>
            <input class="form-control" id="departemen" name="departemen" type="hidden" value="{{$departemen}}"/>
            <input class="form-control" id="unit" name="unit" type="hidden" value="{{$unit}}"/>
            <input class="form-control" id="subunit" name="subunit" type="hidden" value="{{$subunit}}"/>
            <button class="btn btn-success text-uppercase" data-toggle="tooltip" title="New"><i class="fas fa-plus"></i> New</button>
         </form>
         {{-- @can ('master-bidang-usaha-create') 
            <a href="{{ route('master-departemen-harian-proses.create') }}" class="btn btn-success btn-sm" title="Add New MasterDepartemenHarianProses">
               <span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span>
               <span class="ms-1">New</span>
            </a>
         @endcan --}}
         
      </div>
      <div class="card" style="margin-bottom: 50px;">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Harian In</h5>
               </div>
               
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="card-body pt-0">
                     <div class="tab-content">
                        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                           <div class="table-responsive scrollbar">
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th style="width: 10%;">No</th>
                                       <th scope="col">Tanggal</th>
                                       <th scope="col">No_Process</th>
                                       <th scope="col">Departemen</th>
                                       <th scope="col">Unit</th>
                                       <th scope="col">Sub_Unit</th>
                                       <th scope="col">No_Part_Item</th>
                                       <th scope="col">Item</th>
                                       <th scope="col">Shift</th>
                                       <th scope="col">Keterangan_Bahan</th>
                                       <th scope="col">Type_Bahan</th>
                                       <th scope="col">Jenis_Kayu</th>
                                       <th scope="col">Group_Kualitas</th>
                                       <th scope="col">Kualitas</th>
                                       <th scope="col">Warna</th>
                                       <th scope="col">Diameter</th>
                                       <th scope="col">Keterangan_Proses</th>
                                       <th scope="col">Tinggi</th>
                                       <th scope="col">Lebar</th>
                                       <th scope="col">Panjang</th>
                                       <th scope="col">Pcs</th>
                                       <th scope="col">M1</th>
                                       <th scope="col">M2</th>
                                       <th scope="col">M3</th>
                                       <th scope="col">Suppliers</th>
                                       <th scope="col">No_Kiriman</th>
                                       <th scope="col">Products</th>
                                       <th scope="col">No_Po</th>
                                       <th scope="col">No_Pallet</th>
                                       <th scope="col">Nampan</th>
                                       <th scope="col">Operator</th>
                                       <th scope="col">Harian_Tetap</th>
                                       <th scope="col">Harian_Lepas</th>
                                       <th scope="col">Borong</th>
                                       <th scope="col">Pemborong</th>
                                       <th scope="col">Memo</th>
                                       <th scope="col">OT</th>
                                       <th scope="col">Tahun</th>
                                       <th scope="col">Keterangan</th>
                                       <th scope="col">Created_At</th>
                                       <th class="text-end" scope="col">Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($ins as $item)
                                       <tr>
                                          <td>{{ $loop->iteration }}</td>
                                          <td>{{\Carbon\Carbon::parse($item->tanggal)->format('d M Y')}}</td>
                                          <td>{{ $item->no_process }}</td>
                                          <td>{{ $item->departemen }}</td>
                                          <td>{{ $item->unit }}</td>
                                          <td>{{ $item->sub_unit }}</td>
                                          <td>{{ $item->no_part_item }}</td>
                                          <td>{{ $item->item }}</td>
                                          <td>{{ $item->master_shift->name }}</td>
                                          <td>{{ $item->keterangan_bahan }}</td>
                                          <td>{{ $item->type_bahan }}</td>
                                          <td>{{ $item->jenis_kayu }}</td>
                                          <td>{{ $item->group_kualitas }}</td>
                                          <td>{{ $item->kualitas }}</td>
                                          <td>{{ $item->warna }}</td>
                                          <td>{{ $item->diameter }}</td>
                                          <td>{{ $item->keterangan_proses }}</td>
                                          <td>{{ $item->tinggi }}</td>
                                          <td>{{ $item->lebar }}</td>
                                          <td>{{ $item->panjang }}</td>
                                          <td>{{ $item->pcs }}</td>
                                          <td>{{ $item->m1 }}</td>
                                          <td>{{ $item->m2 }}</td>
                                          <td>{{ $item->m3 }}</td>
                                          <td>{{ $item->suppliers->name  }}</td>
                                          <td>{{ $item->no_kiriman }}</td>
                                          <td>{{ $item->product->name  }}</td>
                                          <td>{{ $item->no_product->name  }}</td>
                                          <td>{{ $item->no_pallet }}</td>
                                          <td>{{ $item->nampan }}</td>
                                          <td>{{ $item->operator }}</td>
                                          <td>{{ $item->harian_tetap }}</td>
                                          <td>{{ $item->harian_lepas }}</td>
                                          <td>{{ $item->borong }}</td>
                                          <td>{{ $item->pemborong->name  }}</td>
                                          <td>{{ $item->memo }}</td>
                                          <td>
                                             @if ($item->ot == 1)
                                                Ada
                                             @else
                                                Tidak Ada
                                             @endif
                                          </td>
                                          <td>{{ $item->tahun }}</td>
                                          <td>{{ $item->keterangan }}</td>
                                          <td>{{\Carbon\Carbon::parse($item->created_at)->format('d M Y')}}</td>
                                          <td class="text-end">
                                             <div>
                                                <button class="btn p-0" type="button" onClick="edit({{ json_encode($item) }})" title="Edit" data-bs-toggle="modal" data-bs-target="#item-modal"><span class="text-500 fas fa-edit"></span></button>
                                                <button class="btn p-0 ms-2" type="button" onClick="remove({{ json_encode($item->id) }})" title="Delete" data-bs-toggle="modal" data-bs-target="#modal-info-confirmed"><span class="text-500 fas fa-trash-alt"></span></button>
                                             </div>
                                          </td>
                                       </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Harian Out</h5>
               </div>
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="card-body pt-0">
                     <div class="tab-content">
                        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                           <div class="table-responsive scrollbar">
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th style="width: 10%;">No</th>
                                       <th scope="col">Tanggal</th>
                                       <th scope="col">No_Process</th>
                                       <th scope="col">Departemen</th>
                                       <th scope="col">Unit</th>
                                       <th scope="col">Sub_Unit</th>
                                       <th scope="col">No_Part_Item</th>
                                       <th scope="col">Item</th>
                                       <th scope="col">Shift</th>
                                       <th scope="col">Keterangan_Bahan</th>
                                       <th scope="col">Type_Bahan</th>
                                       <th scope="col">Jenis_Kayu</th>
                                       <th scope="col">Group_Kualitas</th>
                                       <th scope="col">Kualitas</th>
                                       <th scope="col">Warna</th>
                                       <th scope="col">Diameter</th>
                                       <th scope="col">Keterangan_Proses</th>
                                       <th scope="col">Tinggi</th>
                                       <th scope="col">Lebar</th>
                                       <th scope="col">Panjang</th>
                                       <th scope="col">Pcs</th>
                                       <th scope="col">M1</th>
                                       <th scope="col">M2</th>
                                       <th scope="col">M3</th>
                                       <th scope="col">Suppliers</th>
                                       <th scope="col">No_Kiriman</th>
                                       <th scope="col">Products</th>
                                       <th scope="col">No_Po</th>
                                       <th scope="col">No_Pallet</th>
                                       <th scope="col">Nampan</th>
                                       <th scope="col">Operator</th>
                                       <th scope="col">Harian_Tetap</th>
                                       <th scope="col">Harian_Lepas</th>
                                       <th scope="col">Borong</th>
                                       <th scope="col">Pemborong</th>
                                       <th scope="col">Memo</th>
                                       <th scope="col">OT</th>
                                       <th scope="col">Tahun</th>
                                       <th scope="col">Keterangan</th>
                                       <th scope="col">Created_At</th>
                                       <th class="text-end" scope="col">Actions</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($outs as $item)
                                       <tr>
                                          <td>{{ $loop->iteration }}</td>
                                          <td>{{\Carbon\Carbon::parse($item->tanggal)->format('d M Y')}}</td>
                                          <td>{{ $item->no_process }}</td>
                                          <td>{{ $item->departemen }}</td>
                                          <td>{{ $item->unit }}</td>
                                          <td>{{ $item->sub_unit }}</td>
                                          <td>{{ $item->no_part_item }}</td>
                                          <td>{{ $item->item }}</td>
                                          <td>{{ $item->master_shift->name }}</td>
                                          <td>{{ $item->keterangan_bahan }}</td>
                                          <td>{{ $item->type_bahan }}</td>
                                          <td>{{ $item->jenis_kayu }}</td>
                                          <td>{{ $item->group_kualitas }}</td>
                                          <td>{{ $item->kualitas }}</td>
                                          <td>{{ $item->warna }}</td>
                                          <td>{{ $item->diameter }}</td>
                                          <td>{{ $item->keterangan_proses }}</td>
                                          <td>{{ $item->tinggi }}</td>
                                          <td>{{ $item->lebar }}</td>
                                          <td>{{ $item->panjang }}</td>
                                          <td>{{ $item->pcs }}</td>
                                          <td>{{ $item->m1 }}</td>
                                          <td>{{ $item->m2 }}</td>
                                          <td>{{ $item->m3 }}</td>
                                          <td>{{ $item->suppliers->name  }}</td>
                                          <td>{{ $item->no_kiriman }}</td>
                                          <td>{{ $item->product->name  }}</td>
                                          <td>{{ $item->no_product->name  }}</td>
                                          <td>{{ $item->no_pallet }}</td>
                                          <td>{{ $item->nampan }}</td>
                                          <td>{{ $item->operator }}</td>
                                          <td>{{ $item->harian_tetap }}</td>
                                          <td>{{ $item->harian_lepas }}</td>
                                          <td>{{ $item->borong }}</td>
                                          <td>{{ $item->pemborong->name  }}</td>
                                          <td>{{ $item->memo }}</td>
                                          <td>
                                             @if ($item->ot == 1)
                                                Ada
                                             @else
                                                Tidak Ada
                                             @endif
                                          </td>
                                          <td>{{ $item->tahun }}</td>
                                          <td>{{ $item->keterangan }}</td>
                                          <td>{{\Carbon\Carbon::parse($item->created_at)->format('d M Y')}}</td>
                                          <td class="text-end">
                                             <div>
                                                <button class="btn p-0" type="button" onClick="edit({{ json_encode($item) }})" title="Edit" data-bs-toggle="modal" data-bs-target="#item-modal"><span class="text-500 fas fa-edit"></span></button>
                                                <button class="btn p-0 ms-2" type="button" onClick="remove({{ json_encode($item->id) }})" title="Delete" data-bs-toggle="modal" data-bs-target="#modal-info-confirmed"><span class="text-500 fas fa-trash-alt"></span></button>
                                             </div>
                                          </td>
                                       </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>
