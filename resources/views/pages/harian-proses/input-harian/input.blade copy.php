<x-app-layout>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5>Input Harian Proses</h5>
               </div>
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <form action="{{ route('input-harian.store') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                     <div class="table-responsive scrollbar">
                        <h5 class="mb-0" data-anchor="data-anchor">Input Harian In</h5>
                        @foreach($ins as $in)
                     
                        <div class="card-header border-bottom">
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Process</label>
                                 <input class="form-control" id="no_process_in[]" name="no_process_in[]" type="text" value="halo" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tanggal</label>
                                 <input class="form-control" id="tanggal_in[]" name="tanggal_in[]" type="date" value="{{$tanggal}}" readonly/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Departemen</label>
                                 <input class="form-control" id="departemen_in[]" name="departemen_in[]" type="text" value="{{$select_departemen->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Unit</label>
                                 <input class="form-control" id="unit_in[]" name="unit_in[]" type="text" value="{{$select_unit->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Sub Unit</label>
                                 <input class="form-control" id="subunit_in[]" name="subunit_in[]" type="text" value="{{$select_subunit->name}}" readonly style="width: 200px"/>
                              </div>
                           </div>
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Part Item</label>
                                 <input class="form-control" id="no_part_item_in[]" name="no_part_item_in[]" type="text" value="{{$in->no_part_item}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Item</label>
                                 <input class="form-control" id="item_in[]" name="item_in[]" type="text" value="{{$in->item->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Shift</label>
                                 <select class="form-select" id="shift_in[]" name="shift_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Shift</option>
                                    @foreach ($shift as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Bahan</label>
                                 <input class="form-control" id="keterangan_bahan_in[]" name="keterangan_bahan_in[]" type="text" value="{{$in->ket_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Type Bahan</label>
                                 <input class="form-control" id="type_bahan_in[]" name="type_bahan_in[]" type="text" value="{{$in->type_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Jenis Kayu</label>
                                 <input class="form-control" id="jenis_kayu_in[]" name="jenis_kayu_in[]" type="text" value="{{ $in->jenis_kayu->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Group Kualitas</label>
                                 <input class="form-control" id="group_kualitas_in[]" name="group_kualitas_in[]" type="text" value="{{ $in->group_kualitas->name }}" readonly style="width: 200px"/>
                              </div>                           
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Kualitas</label>
                                 <input class="form-control" id="kualitas_in[]" name="kualitas_in[]" type="text" value="{{ $in->kualitas->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Warna</label>
                                 <input class="form-control" id="warnas_in[]" name="warna_in[]" type="text" value="{{ $in->warna->name  }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Diameter</label>
                                 <input class="form-control" id="diameter_in[]" name="diameter_in[]" type="text" value="{{ $in->diameter }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Proses</label>
                                 <input class="form-control" id="keterangan_proses_in[]" name="keterangan_proses_in[]" type="text" value="{{ $in->ket_proses->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tinggi</label>
                                 <input class="form-control" id="tinggi_in[]" name="tinggi_in[]" type="text" value="{{ $in->tinggi }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Lebar</label>
                                 <input class="form-control" id="lebar_in[]" name="lebar_in[]" type="text" value="{{ $in->lebar }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Panjang</label>
                                 <input class="form-control" id="panjang_in[]" name="panjang_in[]" type="text" value="{{ $in->panjang }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pcs</label>
                                 <input class="form-control" id="pcs_in[]" name="pcs_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Supplier</label>
                                 <select class="form-select" id="supplier_in[]" name="supplier_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Supplier</option>
                                    @foreach ($supplier as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Kiriman</label>
                                 <input class="form-control" id="no_kiriman_in[]" name="no_kiriman_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO</label>
                                 <select class="form-select" id="po_in[]" name="po_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO</option>
                                    @foreach ($product as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO Nomer</label>
                                 <select class="form-select" id="no_po_in[]" name="no_po_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO Nomer</option>
                                    @foreach ($no_po as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Pallet</label>
                                 <input class="form-control" id="no_pallet_in[]" name="no_pallet_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nampan</label>
                                 <input class="form-control" id="nampan_in[]" name="nampan_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Operator</label>
                                 <input class="form-control" id="operator_in[]" name="operator_in[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Tetap</label>
                                 <input class="form-control" id="harian_tetap_in[]" name="harian_tetap_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Lepas</label>
                                 <input class="form-control" id="harian_lepas_in[]" name="harian_lepas_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Borong</label>
                                 <input class="form-control" id="borong_in[]" name="borong_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pemborong</label>
                                 <select class="form-select" id="pemborong_in[]" name="pemborong_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Pemborong</option>
                                    @foreach ($pemborong as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Memo</label>
                                 <input class="form-control" id="memo_in[]" name="memo_in[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3" style="margin-left: 23px; margin-right: 23px;">
                                 <label class="form-label" for="basic-form-dob">OT</label>
                                 <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-e0d869aa-3441-496b-9fc3-0333faf70522" id="dom-e0d869aa-3441-496b-9fc3-0333faf70522">
                                    <div class="form-check">
                                       <input class="form-check-input" id="flexRadioDefault1" type="radio" id="ot_in{{ $loop->iteration }}" name="ot_in{{ $loop->iteration }}" value="1"/>
                                       <label class="form-check-label" for="flexRadioDefault1">Ada</label>
                                    </div>
                                    <div class="form-check">
                                       <input class="form-check-input" id="flexRadioDefault2" type="radio" id="ot_in{{ $loop->iteration }}" name="ot_in{{ $loop->iteration }}" value="2" checked="" />
                                       <label class="form-check-label" for="flexRadioDefault2">Tidak Ada</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Keterangan</label>
                                 <input class="form-control" id="keterangan_in[]" name="keterangan_in[]" type="text" style="width: 200px"/>
                              </div>
                           </div>
                        </div>
                        @endforeach
                     </div>

                     <div class="table-responsive scrollbar">
                        <h5 class="mb-0" data-anchor="data-anchor">Input Harian Out</h5>
                        @foreach($outs as $out)
                     
                        <div class="card-header border-bottom">
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Process</label>
                                 <input class="form-control" id="no_process_out[]" name="no_process_out[]" type="text" value="halo" readonly  style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tanggal</label>
                                 <input class="form-control" id="tanggal_out[]" name="tanggal_out[]" type="date" value="{{$tanggal}}" readonly/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Departemen</label>
                                 <input class="form-control" id="departemen_out[]" name="departemen_out[]" type="text" value="{{$select_departemen->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Unit</label>
                                 <input class="form-control" id="unit_out[]" name="unit_out[]" type="text" value="{{$select_unit->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Sub Unit</label>
                                 <input class="form-control" id="subunit_out[]" name="subunit_out[]" type="text" value="{{$select_subunit->name}}" readonly style="width: 200px"/>
                              </div>
                           </div>
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Part Item</label>
                                 <input class="form-control" id="no_part_item_out[]" name="no_part_item_out[]" type="text" value="{{$out->no_part_item}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Item</label>
                                 <input class="form-control" id="item_out[]" name="item_out[]" type="text" value="{{$out->item->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Shift</label>
                                 <select class="form-select" id="shift_out[]" name="shift_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Shift</option>
                                    @foreach ($shift as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Bahan</label>
                                 <input class="form-control" id="keterangan_bahan_out[]" name="keterangan_bahan_out[]" type="text" value="{{$out->ket_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Type Bahan</label>
                                 <input class="form-control" id="type_bahan_out[]" name="type_bahan_out[]" type="text" value="{{$out->type_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Jenis Kayu</label>
                                 <input class="form-control" id="jenis_kayu_out[]" name="jenis_kayu_out[]" type="text" value="{{ $out->jenis_kayu->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Group Kualitas</label>
                                 <input class="form-control" id="group_kualitas_out[]" name="group_kualitas_out[]" type="text" value="{{ $out->group_kualitas->name }}" readonly style="width: 200px"/>
                              </div>                           
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Kualitas</label>
                                 <input class="form-control" id="kualitas_out[]" name="kualitas_out[]" type="text" value="{{ $out->kualitas->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Warna</label>
                                 <input class="form-control" id="warnas_out[]" name="warna_out[]" type="text" value="{{ $out->warna->name  }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Diameter</label>
                                 <input class="form-control" id="diameter_out[]" name="diameter_out[]" type="text" value="{{ $out->diameter }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Proses</label>
                                 <input class="form-control" id="keterangan_proses_out[]" name="keterangan_proses_out[]" type="text" value="{{ $out->ket_proses->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tinggi</label>
                                 <input class="form-control" id="tinggi_out[]" name="tinggi_out[]" type="text" value="{{ $out->tinggi }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Lebar</label>
                                 <input class="form-control" id="lebar_out[]" name="lebar_out[]" type="text" value="{{ $out->lebar }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Panjang</label>
                                 <input class="form-control" id="panjang_out[]" name="panjang_out[]" type="text" value="{{ $out->panjang }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pcs</label>
                                 <input class="form-control" id="pcs_out[]" name="pcs_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Supplier</label>
                                 <select class="form-select" id="supplier_out[]" name="supplier_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Supplier</option>
                                    @foreach ($supplier as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Kiriman</label>
                                 <input class="form-control" id="no_kiriman_out[]" name="no_kiriman_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO</label>
                                 <select class="form-select" id="po_out[]" name="po_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO</option>
                                    @foreach ($product as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO Nomer</label>
                                 <select class="form-select" id="no_po_out[]" name="no_po_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO Nomer</option>
                                    @foreach ($no_po as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Pallet</label>
                                 <input class="form-control" id="no_pallet_out[]" name="no_pallet_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nampan</label>
                                 <input class="form-control" id="nampan_out[]" name="nampan_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Operator</label>
                                 <input class="form-control" id="operator_out[]" name="operator_out[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Tetap</label>
                                 <input class="form-control" id="harian_tetap_out[]" name="harian_tetap_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Lepas</label>
                                 <input class="form-control" id="harian_lepas_out[]" name="harian_lepas_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Borong</label>
                                 <input class="form-control" id="borong_out[]" name="borong_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pemborong</label>
                                 <select class="form-select" id="pemborong_out[]" name="pemborong_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Pemborong</option>
                                    @foreach ($pemborong as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Memo</label>
                                 <input class="form-control" id="memo_out[]" name="memo_out[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3" style="margin-left: 23px; margin-right: 23px;">
                                 <label class="form-label" for="basic-form-dob">OT</label>
                                 <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-e0d869aa-3441-496b-9fc3-0333faf70522" id="dom-e0d869aa-3441-496b-9fc3-0333faf70522">
                                    <div class="form-check">
                                       <input class="form-check-input" id="flexRadioDefault1" type="radio" id="ot_out{{ $loop->iteration }}[]" name="ot_out{{ $loop->iteration }}[]" value="on"/>
                                       <label class="form-check-label" for="flexRadioDefault1">Ada</label>
                                    </div>
                                    <div class="form-check">
                                       <input class="form-check-input" id="flexRadioDefault2" type="radio" id="ot_out{{ $loop->iteration }}[]" name="ot_out{{ $loop->iteration }}[]" value="off" checked="" />
                                       <label class="form-check-label" for="flexRadioDefault2">Tidak Ada</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Keterangan</label>
                                 <input class="form-control" id="keterangan_out[]" name="keterangan_out[]" type="text" style="width: 200px"/>
                              </div>
                           </div>
                        </div>
                        @endforeach
                     </div>
                     <button class="btn btn-primary text-uppercase" data-toggle="tooltip" title="Save">Post</button>
                  </form>




                     {{-- <div class="table-responsive scrollbar"><br>
                        <h5 class="mb-0" data-anchor="data-anchor">Input Harian Out</h5>
                        @foreach($outs as $in)
                     
                        <div class="card-header border-bottom">
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Process</label>
                                 <input class="form-control" id="tanggal" name="tanggal" type="text" value="halo" disabled/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tanggal</label>
                                 <input class="form-control" id="tanggal" name="tanggal" type="date" value="{{$tanggal}}" disabled/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="departemen">Departemen</label>
                                 <select class="form-select" id="departemen" name="departemen" aria-label="Default select example" disabled>
                                    <option selected="selected" value="{{$select_departemen->id}}">{{$select_departemen->name}}</option>
                                    @foreach ($departemen as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="unit">Unit</label>
                                 <select class="form-select" id="unit" name="unit" aria-label="Default select example" disabled>
                                       <option selected="selected" value="{{$select_unit->id}}">{{$select_unit->name}}</option>
                                       @foreach ($unit as $item)
                                          <option value="{{$item->id}}">{{$item->name}}</option>
                                       @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="subunit">Sub Unit</label>
                                 <select class="form-select" id="subunit" name="subunit" aria-label="Default select example" disabled>
                                    <option selected="selected" value="{{$select_subunit->id}}">{{$select_subunit->name}}</option>
                                    @foreach ($subunit as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Part Item</label>
                                 <input class="form-control" id="no_part_item" name="no_part_item" type="text" value="{{$in->no_part_item}}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Item</label>
                                 <input class="form-control" id="item" name="item" type="text" value="{{$in->item->name}}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Shift</label>
                                 <input class="form-control" id="shift" name="shift" type="text" value="halo" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Bahan</label>
                                 <input class="form-control" id="master_keterangan_bahans_id" name="master_keterangan_bahans_id" type="text" value="{{$in->ket_bahan->name}}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Type Bahan</label>
                                 <input class="form-control" id="master_type_bahans_id" name="master_type_bahans_id" type="text" value="{{$in->type_bahan->name}}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Jenis Kayu</label>
                                 <input class="form-control" id="master_jenis_kayus_id" name="master_jenis_kayus_id" type="text" value="{{ $in->jenis_kayu->name }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Group Kualitas</label>
                                 <input class="form-control" id="master_group_kualitas_id" name="master_group_kualitas_id" type="text" value="{{ $in->group_kualitas->name }}" disabled style="width: 200px"/>
                              </div>                           
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Kualitas</label>
                                 <input class="form-control" id="master_kualitas_id" name="master_kualitas_id" type="text" value="{{ $in->kualitas->name }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Warna</label>
                                 <input class="form-control" id="master_warnas_id" name="master_warnas_id" type="text" value="{{ $in->warna->name  }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Diameter</label>
                                 <input class="form-control" id="diameter" name="diameter" type="text" value="{{ $in->diameter }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Proses</label>
                                 <input class="form-control" id="master_keterangan_proses_id" name="master_keterangan_proses_id" type="text" value="{{ $in->ket_proses->name }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tinggi</label>
                                 <input class="form-control" id="tinggi" name="tinggi" type="text" value="{{ $in->tinggi }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Lebar</label>
                                 <input class="form-control" id="lebar" name="lebar" type="text" value="{{ $in->lebar }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Panjang</label>
                                 <input class="form-control" id="panjang" name="panjang" type="text" value="{{ $in->panjang }}" disabled style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pcs</label>
                                 <input class="form-control" id="pcs" name="pcs" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Supplier</label>
                                 <select class="form-select" id="supplier" name="supplier" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Supplier</option>
                                    @foreach ($supplier as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Kiriman</label>
                                 <input class="form-control" id="no_kiriman" name="no_kiriman" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO</label>
                                 <select class="form-select" id="po" name="po" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO</option>
                                    @foreach ($product as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO Nomer</label>
                                 <select class="form-select" id="no_po" name="no_po" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO Nomer</option>
                                    @foreach ($no_po as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Pallet</label>
                                 <input class="form-control" id="no_pallet" name="no_pallet" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nampan</label>
                                 <input class="form-control" id="nampan" name="nampan" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Operator</label>
                                 <input class="form-control" id="operator" name="operator" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Tetap</label>
                                 <input class="form-control" id="harian_tetap" name="harian_tetap" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Lepas</label>
                                 <input class="form-control" id="harian_lepas" name="harian_lepas" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Borong</label>
                                 <input class="form-control" id="borong" name="borong" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pemborong</label>
                                 <select class="form-select" id="pemborong" name="pemborong" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Pemborong</option>
                                    @foreach ($pemborong as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Memo</label>
                                 <input class="form-control" id="memo" name="memo" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3" style="margin-left: 23px; margin-right: 23px;">
                                 <label class="form-label" for="basic-form-dob">OT</label>
                                 <input class="form-check-input" id="ot" name="ot" type="checkbox"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Keterangan</label>
                                 <input class="form-control" id="tanggal" name="tanggal" type="text" style="width: 200px"/>
                              </div>
                           </div>
                        </div>
                        @endforeach
                     </div> --}}
               </div>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>
