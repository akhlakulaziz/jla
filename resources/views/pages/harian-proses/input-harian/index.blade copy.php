{{-- <form action="{{ route('input-harian.index') }}" method="POST">
   @csrf
   <div class="mb-3">
      <input class="form-control" name="tanggal" type="date" id="tanggal">
   </div>
   <button id="educationAdd" class="btn btn-primary" data-toggle="tooltip" title="Save"><i class="fa fa-save"></i></button>
</form> --}}
<x-app-layout>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Item</h5>
               </div>
               
               <div class="col-auto ms-auto mb-3">
                  <div class="nav nav-pills nav-pills-falcon flex-grow-1 mt-3" role="tablist">
                     <form action="{{ route('input-harian.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                     </form>
                  </div>
               </div>
            </div>
         
            
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  
                  <div class="card-header border-bottom">
                     <div class="col-auto align-self-center">
                        <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                           <form action="{{ route('input-harian.store') }}" method="POST">
                           @csrf
                           <div class="mb-3">
                              <label class="form-label" for="basic-form-dob">Tanggal</label>
                              <input class="form-control" name="tanggal" id="tanggal" type="date" />
                           </div>
                           <div class="mb-3">
                              <label class="form-label" for="departemen">Departemen</label>
                              <select class="form-select" id="departemen" name="departemen">
                                 <option selected="selected" value="">Pilih Departemen</option>
                                 @foreach ($departemen as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                           <div class="mb-3">
                              <label class="form-label" for="unit">Unit</label>
                              <select class="form-select" id="unit" name="unit">
                                    <option selected="selected" value="">Pilih Unit</option>
                                    @foreach ($unit as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                              </select>
                           </div>
                           <div class="mb-3">
                              <label class="form-label" for="subunit">Sub Unit</label>
                              <select class="form-select" id="subunit" name="subunit" aria-label="Default select example">
                                 <option selected="selected" value="">Pilih Sub Unit</option>
                                 @foreach ($subunit as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                           {{-- <button id="educationAdd" class="btn btn-primary" data-toggle="tooltip" title="Save"><i class="fa fa-save"></i></button> --}}
                           <button class="btn btn-primary text-uppercase" data-toggle="tooltip" title="Save">Post</button>
                           </form>
                        </div>
                     </div>
                  </div>
                  
                  <div class="table-responsive scrollbar">
                     <table class="table">
                        <thead>
                           <tr>
                              <th style="width: 10%;">No</th>
                              <th scope="col">No Part Item</th>
                              <th scope="col">Item</th>
                              <th scope="col">Ket Bahan</th>
                              <th scope="col">Type Bahan</th>
                              <th scope="col">Diameter</th>
                              <th scope="col">Tinggi</th>
                              <th scope="col">Lebar</th>
                              <th scope="col">Panjang</th>
                              <th scope="col">Warna</th>
                              <th scope="col">Group Kualitas</th>
                              <th scope="col">Kualitas</th>
                              <th scope="col">Jenis Kayu</th>
                              <th scope="col">Ket Proses</th>
                              <th scope="col">Status</th>
                              <th scope="col">In</th>
                              <th scope="col">Out</th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($items as $item)
                              <tr>
                                 <td>{{ $loop->iteration }}</td>
                                 <td>{{ $item->no_part_item }}</td>
                                 <td>{{ $item->item->name }}</td>
                                 <td>{{ $item->ket_bahan->name }}</td>
                                 <td>{{ $item->type_bahan->name  }}</td>
                                 <td>{{ $item->diameter }}</td>
                                 <td>{{ $item->tinggi }}</td>
                                 <td>{{ $item->lebar }}</td>
                                 <td>{{ $item->panjang }}</td>
                                 <td>{{ $item->warna->name  }}</td>
                                 <td>{{ $item->group_kualitas->name }}</td>
                                 <td>{{ $item->kualitas->name }}</td>
                                 <td>{{ $item->jenis_kayu->name }}</td>
                                 <td>{{ $item->ket_proses->name }}</td>
                                 <td>
                                    @if ($item->status == 1)
                                       Active
                                    @else
                                       Inactive
                                    @endif
                                 </td>
                                 <td>
                                    <div class="form-check">
                                       <input class="form-check-input" id="in" type="checkbox" value="{{ $item->id }}" />
                                    </div>
                                 </td>
                                 <td>
                                    <div class="form-check">
                                       <input class="form-check-input" id="out" type="checkbox" value="{{ $item->id }}" />
                                    </div>
                                 </td>
                              </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
                  {{-- <button id="educationAdd" class="btn btn-primary" data-toggle="tooltip" title="Save"><i class="fa fa-save"></i></button> --}}
                  {{-- <button class="btn btn-primary text-uppercase" data-toggle="tooltip" title="Save">Post</button>
                  </form> --}}
               </div>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>
