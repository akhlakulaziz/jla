<link href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css" rel="stylesheet" />
<x-app-layout>
   <div class="col-lg-12">
      {{-- ============table item============= --}}
      <div class="card" style="margin-bottom: 25px;">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Item</h5>
               </div>
               
               {{-- <div class="col-auto ms-auto">
                  <div class="nav nav-pills nav-pills-falcon flex-grow-1 mt-2" role="tablist">
                     <form action="{{ route('item.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                     </form>
                  </div>
               </div> --}}
            </div>
            {{-- <div class="col-auto align-self-center">
               @can ('master-bidang-usaha-create')
                     <a href="#" class="btn btn-success btn-sm" title="Add New MasterItem" data-bs-toggle="modal" onClick="create()" data-bs-target="#item-modal">
                     <span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span>
                     <span class="ms-1">New</span>
                  </a>
               @endcan
               
            </div> --}}
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="table-responsive scrollbar">
                     <table id="example" class="table">
                        <thead>
                           <tr>
                              <th style="width: 10%;">No</th>
                              <th scope="col">In</th>
                              <th scope="col">Out</th>
                              <th scope="col">Jenis Barang</th>
                              {{-- <th scope="col">No Part Item</th>
                              <th scope="col">Item</th>
                              <th scope="col">Ket Bahan</th>
                              <th scope="col">Type Bahan</th>
                              <th scope="col">Diameter</th>
                              <th scope="col">Tinggi</th>
                              <th scope="col">Lebar</th>
                              <th scope="col">Panjang</th>
                              <th scope="col">Warna</th>
                              <th scope="col">Group Kualitas</th>
                              <th scope="col">Kualitas</th>
                              <th scope="col">Jenis Kayu</th>
                              <th scope="col">Ket Proses</th> --}}
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($items as $item)
                              <tr>
                                 <td>{{ $loop->iteration }}</td>
                                 <td><button type="button" title="in" class="btn btn-primary btn-sm" id="in" name="in" onClick="harian_in({{ json_encode($item) }})">IN</button></td>
                                 <td><button type="button" title="in" class="btn btn-danger btn-sm" id="out" name="out" onClick="harian_out({{ $item }})">OUT</button></td>
                                 <td>{{ $item->no_part_item }}.{{ $item->item->name }}.{{ $item->ket_bahan->name }}.{{ $item->type_bahan->name  }}.{{ $item->jenis_kayu->name }}.{{ $item->group_kualitas->name }}.{{ $item->kualitas->name }}.{{ $item->warna->name }}.{{ $item->diameter }}.{{ $item->ket_proses->name }}.{{ $item->tinggi }}.{{ $item->lebar }}.{{ $item->panjang }}</td>
                                 {{-- <td>{{ $item->no_part_item }}</td> --}}
                                 {{-- <td>{{ $item->item->name }}</td>
                                 <td>{{ $item->ket_bahan->name }}</td>
                                 <td>{{ $item->type_bahan->name  }}</td>
                                 <td>{{ $item->diameter }}</td>
                                 <td>{{ $item->tinggi }}</td>
                                 <td>{{ $item->lebar }}</td>
                                 <td>{{ $item->panjang }}</td>
                                 <td>{{ $item->warna->name  }}</td>
                                 <td>{{ $item->group_kualitas->name }}</td>
                                 <td>{{ $item->kualitas->name }}</td>
                                 <td>{{ $item->jenis_kayu->name }}</td>
                                 <td>{{ $item->ket_proses->name }}</td> --}}
                              </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>

      {{-- ============Form Input In============= --}}
      <form action="{{ route('input-harian.store') }}" method="POST" enctype="multipart/form-data">
         @csrf
         <div class="card" style="margin-bottom: 25px;">
            <div class="card-header border-bottom">
               <div class="row flex-between-end">
                  <div class="col-auto align-self-center">
                     <h5>Input Harian Proses</h5>
                  </div>
               </div>
            </div>
            <div class="card-body pt-0">
               <div class="tab-content">
                  <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                     <div class="table-responsive scrollbar">
                        <h5 class="mb-0" data-anchor="data-anchor">Input Harian In</h5>
                        @foreach($ins as $in)
                           <div class="card-header border-bottom">
                              <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                                 <div class="mb-3">
                                    <label class="form-label" for="basic-form-dob">Tanggal</label>
                                    <input class="form-control" id="tanggal_in[]" name="tanggal_in[]" type="date" value="{{$tanggal}}" readonly/>
                                 </div>
                                 <div class="mb-3">
                                    <label class="form-label" for="basic-form-dob">Departemen</label>
                                    <input class="form-control" id="departemen_in[]" name="departemen_in[]" type="text" value="{{$select_departemen->name}}" readonly style="width: 200px"/>
                                 </div>
                                 <div class="mb-3">
                                    <label class="form-label" for="basic-form-dob">Unit</label>
                                    <input class="form-control" id="unit_in[]" name="unit_in[]" type="text" value="{{$select_unit->name}}" readonly style="width: 200px"/>
                                 </div>
                                 <div class="mb-3">
                                    <label class="form-label" for="basic-form-dob">Sub Unit</label>
                                    <input class="form-control" id="subunit_in[]" name="subunit_in[]" type="text" value="{{$select_subunit->name}}" readonly style="width: 200px"/>
                                 </div>
                              </div>
                              <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                                 <div class="mb-3">
                                    <label class="form-label" for="basic-form-dob">No Process</label>
                                    <input class="form-control" id="no_process_in[]" name="no_process_in[]" type="text" value="halo" readonly style="width: 200px"/>
                                 </div>
                                 <div class="mb-3">
                                    <label class="form-label" for="basic-form-dob">Shift</label>
                                    <select class="form-select" id="shift_in[]" name="shift_in[]" aria-label="Default select example" style="width: 200px">
                                       <option selected="selected" value="">Pilih Shift</option>
                                       @foreach ($shift as $item)
                                          <option value="{{$item->id}}">{{$item->name}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                                 <div class="mb-3">
                                    <label class="form-label" for="basic-form-dob" style="margin-left: 15px;">OT</label><br>
                                    <input class="form-check-input" id="ot_in[]" name="ot_in[]" type="checkbox" style="margin-left: 15px;"/>
                                 </div>
                                 {{-- <div class="mb-3" style="margin-left: 23px; margin-right: 23px;">
                                    <label class="form-label" for="basic-form-dob">OT</label>
                                    <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-e0d869aa-3441-496b-9fc3-0333faf70522" id="dom-e0d869aa-3441-496b-9fc3-0333faf70522">
                                       <div class="form-check">
                                          <input class="form-check-input" id="flexRadioDefault1" type="radio" id="ot_in{{ $loop->iteration }}" name="ot_in{{ $loop->iteration }}" value="1"/>
                                          <label class="form-check-label" for="flexRadioDefault1">Ada</label>
                                       </div>
                                       <div class="form-check">
                                          <input class="form-check-input" id="flexRadioDefault2" type="radio" id="ot_in{{ $loop->iteration }}" name="ot_in{{ $loop->iteration }}" value="2" checked="" />
                                          <label class="form-check-label" for="flexRadioDefault2">Tidak Ada</label>
                                       </div>
                                    </div>
                                 </div> --}}
                              </div>
                           </div>
                           @break;
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
         </div>

         {{-- @foreach($ins as $in) --}}
            <div class="row g-3 mb-3">
               <div class="col-xxl-12">
                  <div class="card overflow-hidden mb-3">
                     <div class="card-header audience-chart-header p-0 bg-light scrollbar-overlay">
                        <ul class="nav nav-tabs border-0 chart-tab flex-nowrap" id="audience-chart-tab" role="tablist">
                           <li class="nav-item" role="presentation">
                              <a class="nav-link mb-0 active" id="harian-in-tab" data-bs-toggle="tab" href="#harian-in" role="tab" aria-controls="harian-in" aria-selected="true">
                                 <div class="audience-tab-item p-2 pe-4">
                                    <h5 class="text-800 text-nowrap">Input harian</h5>
                                 </div>
                              </a>
                           </li>
                           <li class="nav-item" role="presentation">
                              <a class="nav-link mb-0" id="tenaga-kerja-in-tab" data-bs-toggle="tab" href="#tenaga-kerja-in" role="tab" aria-controls="tenaga-kerja-in" aria-selected="false">
                                 <div class="audience-tab-item p-2 pe-4">
                                    <h5 class="text-800">Tenaga Kerja</h5>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                     <div class="card-body">
                        <div class="tab-content">
                           <div class="tab-pane active" id="harian-in" role="tabpanel" aria-labelledby="harian-in-tab">
                              {{-- IN --}}
                              <h5 class="text-800">Input</h5>
                              @foreach($ins as $in)
                                 <div class="table-responsive scrollbar">
                                    <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">No Part Item</label>
                                          <input class="form-control" id="no_part_item_in[]" name="no_part_item_in[]" type="text" value="{{$in->no_part_item}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Item</label>
                                          <input class="form-control" id="item_in[]" name="item_in[]" type="text" value="{{$in->item->name}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Ket Bahan</label>
                                          <input class="form-control" id="keterangan_bahan_in[]" name="keterangan_bahan_in[]" type="text" value="{{$in->ket_bahan->name}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Type Bahan</label>
                                          <input class="form-control" id="type_bahan_in[]" name="type_bahan_in[]" type="text" value="{{$in->type_bahan->name}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Jenis Kayu</label>
                                          <input class="form-control" id="jenis_kayu_in[]" name="jenis_kayu_in[]" type="text" value="{{ $in->jenis_kayu->name }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Group Kualitas</label>
                                          <input class="form-control" id="group_kualitas_in[]" name="group_kualitas_in[]" type="text" value="{{ $in->group_kualitas->name }}" readonly style="width: 200px"/>
                                       </div>                           
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Kualitas</label>
                                          <input class="form-control" id="kualitas_in[]" name="kualitas_in[]" type="text" value="{{ $in->kualitas->name }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Warna</label>
                                          <input class="form-control" id="warnas_in[]" name="warna_in[]" type="text" value="{{ $in->warna->name  }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Diameter</label>
                                          <input class="form-control" id="diameter_in[]" name="diameter_in[]" type="text" value="{{ $in->diameter }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Ket Proses</label>
                                          <input class="form-control" id="keterangan_proses_in[]" name="keterangan_proses_in[]" type="text" value="{{ $in->ket_proses->name }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Tinggi</label>
                                          <input class="form-control" id="tinggi_in[]" name="tinggi_in[]" type="text" value="{{ $in->tinggi }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Lebar</label>
                                          <input class="form-control" id="lebar_in[]" name="lebar_in[]" type="text" value="{{ $in->lebar }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Panjang</label>
                                          <input class="form-control" id="panjang_in[]" name="panjang_in[]" type="text" value="{{ $in->panjang }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Pcs</label>
                                          <input class="form-control" id="pcs_in[]" name="pcs_in[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Supplier</label>
                                          <select class="form-select" id="supplier_in[]" name="supplier_in[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih Supplier</option>
                                             @foreach ($supplier as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">No Kiriman</label>
                                          <input class="form-control" id="no_kiriman_in[]" name="no_kiriman_in[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">PO</label>
                                          <select class="form-select" id="po_in[]" name="po_in[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih PO</option>
                                             @foreach ($product as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">PO Nomer</label>
                                          <select class="form-select" id="no_po_in[]" name="no_po_in[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih PO Nomer</option>
                                             @foreach ($no_po as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">No Pallet</label>
                                          <input class="form-control" id="no_pallet_in[]" name="no_pallet_in[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Nampan</label>
                                          <input class="form-control" id="nampan_in[]" name="nampan_in[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Operator</label>
                                          <input class="form-control" id="operator_in[]" name="operator_in[]" type="text" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Harian Tetap</label>
                                          <input class="form-control" id="harian_tetap_in[]" name="harian_tetap_in[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Harian Lepas</label>
                                          <input class="form-control" id="harian_lepas_in[]" name="harian_lepas_in[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Borong</label>
                                          <input class="form-control" id="borong_in[]" name="borong_in[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Pemborong</label>
                                          <select class="form-select" id="pemborong_in[]" name="pemborong_in[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih Pemborong</option>
                                             @foreach ($pemborong as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Memo</label>
                                          <input class="form-control" id="memo_in[]" name="memo_in[]" type="text" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Keterangan</label>
                                          <input class="form-control" id="keterangan_in[]" name="keterangan_in[]" type="text" style="width: 200px"/>
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                              <div id="dynamicin">

                              </div>



                              {{-- OUT --}}
                              <h5 class="text-800">Output</h5>
                              @foreach($outs as $out)
                                 <div class="table-responsive scrollbar">
                                    <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">No Part Item</label>
                                          <input class="form-control" id="no_part_item_out[]" name="no_part_item_out[]" type="text" value="{{$out->no_part_item}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Item</label>
                                          <input class="form-control" id="item_out[]" name="item_out[]" type="text" value="{{$out->item->name}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Ket Bahan</label>
                                          <input class="form-control" id="keterangan_bahan_out[]" name="keterangan_bahan_out[]" type="text" value="{{$out->ket_bahan->name}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Type Bahan</label>
                                          <input class="form-control" id="type_bahan_out[]" name="type_bahan_out[]" type="text" value="{{$out->type_bahan->name}}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Jenis Kayu</label>
                                          <input class="form-control" id="jenis_kayu_out[]" name="jenis_kayu_out[]" type="text" value="{{ $out->jenis_kayu->name }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Group Kualitas</label>
                                          <input class="form-control" id="group_kualitas_out[]" name="group_kualitas_out[]" type="text" value="{{ $out->group_kualitas->name }}" readonly style="width: 200px"/>
                                       </div>                           
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Kualitas</label>
                                          <input class="form-control" id="kualitas_out[]" name="kualitas_out[]" type="text" value="{{ $out->kualitas->name }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Warna</label>
                                          <input class="form-control" id="warnas_out[]" name="warna_out[]" type="text" value="{{ $out->warna->name  }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Diameter</label>
                                          <input class="form-control" id="diameter_out[]" name="diameter_out[]" type="text" value="{{ $out->diameter }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Ket Proses</label>
                                          <input class="form-control" id="keterangan_proses_out[]" name="keterangan_proses_out[]" type="text" value="{{ $out->ket_proses->name }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Tinggi</label>
                                          <input class="form-control" id="tinggi_out[]" name="tinggi_out[]" type="text" value="{{ $out->tinggi }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Lebar</label>
                                          <input class="form-control" id="lebar_out[]" name="lebar_out[]" type="text" value="{{ $out->lebar }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Panjang</label>
                                          <input class="form-control" id="panjang_out[]" name="panjang_out[]" type="text" value="{{ $out->panjang }}" readonly style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Pcs</label>
                                          <input class="form-control" id="pcs_out[]" name="pcs_out[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Supplier</label>
                                          <select class="form-select" id="supplier_out[]" name="supplier_out[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih Supplier</option>
                                             @foreach ($supplier as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">No Kiriman</label>
                                          <input class="form-control" id="no_kiriman_out[]" name="no_kiriman_out[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">PO</label>
                                          <select class="form-select" id="po_out[]" name="po_out[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih PO</option>
                                             @foreach ($product as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">PO Nomer</label>
                                          <select class="form-select" id="no_po_out[]" name="no_po_out[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih PO Nomer</option>
                                             @foreach ($no_po as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">No Pallet</label>
                                          <input class="form-control" id="no_pallet_out[]" name="no_pallet_out[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Nampan</label>
                                          <input class="form-control" id="nampan_out[]" name="nampan_out[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Operator</label>
                                          <input class="form-control" id="operator_out[]" name="operator_out[]" type="text" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Harian Tetap</label>
                                          <input class="form-control" id="harian_tetap_out[]" name="harian_tetap_out[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Harian Lepas</label>
                                          <input class="form-control" id="harian_lepas_out[]" name="harian_lepas_out[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Borong</label>
                                          <input class="form-control" id="borong_out[]" name="borong_out[]" type="number" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Pemborong</label>
                                          <select class="form-select" id="pemborong_out[]" name="pemborong_out[]" aria-label="Default select example" style="width: 200px">
                                             <option selected="selected" value="">Pilih Pemborong</option>
                                             @foreach ($pemborong as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Memo</label>
                                          <input class="form-control" id="memo_out[]" name="memo_out[]" type="text" style="width: 200px"/>
                                       </div>
                                       <div class="mb-3">
                                          <label class="form-label" for="basic-form-dob">Keterangan</label>
                                          <input class="form-control" id="keterangan_out[]" name="keterangan_out[]" type="text" style="width: 200px"/>
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                           </div>
                           
                           {{-- Tenaga Kerja --}}
                           <div class="tab-pane" id="tenaga-kerja-in" role="tabpanel" aria-labelledby="tenaga-kerja-in-tab">
                              @foreach($ins as $in)
                              <div class="table-responsive scrollbar">
                                 <div class="d-flex add-contact__form my-sm-0 my-2">
                                    <div class="mb-3" style="margin-right: 25px;">
                                       <label class="form-label" for="basic-form-dob">Nama Pemborong</label>
                                       <input class="form-control" id="nama_pemborong[]" name="nama_pemborong[]" type="text" style="width: 200px;"/>
                                    </div>
                                    <div class="mb-3" style="margin-right: 25px;">
                                       <label class="form-label" for="basic-form-dob">Nama Operator</label>
                                       <input class="form-control" id="nama_operator[]" name="nama_operator[]" type="text" style="width: 200px;"/>
                                    </div>
                                    <div class="mb-3" style="margin-right: 25px;">
                                       <div id="dynamicht">
                                          <label class="form-label" for="basic-form-dob">Nama HT</label>
                                          <input class="form-control" id="nama_ht[]" name="nama_ht[]" type="text" style="width: 200px;"/>
                                       </div>
                                       <button type="button" name="add" id="dynamic-arht" class="btn btn-outline-primary">Tambah HT</button>
                                       {{-- <button type="button" class="btn btn-outline-primary" onClick="harian_tetap({{ json_encode($loop->iteration) }})">Tambah HT</button> --}}
                                    </div>
                                    <div class="mb-3" style="margin-right: 25px;">
                                       <div id="dynamichl">
                                          <label class="form-label" for="basic-form-dob">Nama HL</label>
                                          <input class="form-control" id="nama_hl[]" name="nama_hl[]" type="text" style="width: 200px;"/>
                                       </div>
                                       <button type="button" name="add" id="dynamic-arhl" class="btn btn-outline-primary">Tambah HL</button>
                                       {{-- <button type="button" class="btn btn-outline-primary" onClick="harian_lepas({{ json_encode($loop->iteration) }})">Tambah HT</button> --}}
                                    </div>
                                    <div class="mb-3">
                                       <div id="dynamicbrg">
                                          <label class="form-label" for="basic-form-dob">Nama Borong</label>
                                          <input class="form-control" id="nama_borong[]" name="nama_borong[]" type="text" style="width: 200px"/>
                                       </div>
                                       <button type="button" name="add" id="dynamic-arbrg" class="btn btn-outline-primary">Tambah Borong</button>
                                       {{-- <button type="button" class="btn btn-outline-primary" onClick="borong({{ json_encode($loop->iteration) }})">Tambah HT</button> --}}
                                    </div>
                                 </div>
                              </div>
                              @break;
                              @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         {{-- @endforeach --}}

         {{-- <div class="card" style="margin-bottom: 25px;">
            <div class="card-header border-bottom">
               <div class="row flex-between-end">
                  <div class="col-auto align-self-center">
                     <h5>Input Harian Proses</h5>
                  </div>
               </div>
            </div>
            <div class="card-body pt-0">
               <div class="tab-content">
                  <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                     <div class="table-responsive scrollbar">
                        <h5 class="mb-0" data-anchor="data-anchor">Input Harian In</h5>
                        @foreach($ins as $in)
                        <div class="card-header border-bottom">
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nama Pemborong</label>
                                 <input class="form-control" id="nama_pemborong[]" name="nama_pemborong[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nama Operator</label>
                                 <input class="form-control" id="nama_operator[]" name="nama_operator[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nama HT</label>
                                 <input class="form-control" id="nama_hl[]" name="nama_hl[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nama HL</label>
                                 <input class="form-control" id="nama_ht[]" name="nama_ht[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nama Borong</label>
                                 <input class="form-control" id="nama_borong[]" name="nama_borong[]" type="text" style="width: 200px"/>
                              </div>
                           </div>
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Part Item</label>
                                 <input class="form-control" id="no_part_item_in[]" name="no_part_item_in[]" type="text" value="{{$in->no_part_item}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Item</label>
                                 <input class="form-control" id="item_in[]" name="item_in[]" type="text" value="{{$in->item->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Bahan</label>
                                 <input class="form-control" id="keterangan_bahan_in[]" name="keterangan_bahan_in[]" type="text" value="{{$in->ket_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Type Bahan</label>
                                 <input class="form-control" id="type_bahan_in[]" name="type_bahan_in[]" type="text" value="{{$in->type_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Jenis Kayu</label>
                                 <input class="form-control" id="jenis_kayu_in[]" name="jenis_kayu_in[]" type="text" value="{{ $in->jenis_kayu->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Group Kualitas</label>
                                 <input class="form-control" id="group_kualitas_in[]" name="group_kualitas_in[]" type="text" value="{{ $in->group_kualitas->name }}" readonly style="width: 200px"/>
                              </div>                           
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Kualitas</label>
                                 <input class="form-control" id="kualitas_in[]" name="kualitas_in[]" type="text" value="{{ $in->kualitas->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Warna</label>
                                 <input class="form-control" id="warnas_in[]" name="warna_in[]" type="text" value="{{ $in->warna->name  }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Diameter</label>
                                 <input class="form-control" id="diameter_in[]" name="diameter_in[]" type="text" value="{{ $in->diameter }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Proses</label>
                                 <input class="form-control" id="keterangan_proses_in[]" name="keterangan_proses_in[]" type="text" value="{{ $in->ket_proses->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tinggi</label>
                                 <input class="form-control" id="tinggi_in[]" name="tinggi_in[]" type="text" value="{{ $in->tinggi }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Lebar</label>
                                 <input class="form-control" id="lebar_in[]" name="lebar_in[]" type="text" value="{{ $in->lebar }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Panjang</label>
                                 <input class="form-control" id="panjang_in[]" name="panjang_in[]" type="text" value="{{ $in->panjang }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pcs</label>
                                 <input class="form-control" id="pcs_in[]" name="pcs_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Supplier</label>
                                 <select class="form-select" id="supplier_in[]" name="supplier_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Supplier</option>
                                    @foreach ($supplier as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Kiriman</label>
                                 <input class="form-control" id="no_kiriman_in[]" name="no_kiriman_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO</label>
                                 <select class="form-select" id="po_in[]" name="po_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO</option>
                                    @foreach ($product as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO Nomer</label>
                                 <select class="form-select" id="no_po_in[]" name="no_po_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO Nomer</option>
                                    @foreach ($no_po as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Pallet</label>
                                 <input class="form-control" id="no_pallet_in[]" name="no_pallet_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nampan</label>
                                 <input class="form-control" id="nampan_in[]" name="nampan_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Operator</label>
                                 <input class="form-control" id="operator_in[]" name="operator_in[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Tetap</label>
                                 <input class="form-control" id="harian_tetap_in[]" name="harian_tetap_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Lepas</label>
                                 <input class="form-control" id="harian_lepas_in[]" name="harian_lepas_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Borong</label>
                                 <input class="form-control" id="borong_in[]" name="borong_in[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pemborong</label>
                                 <select class="form-select" id="pemborong_in[]" name="pemborong_in[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Pemborong</option>
                                    @foreach ($pemborong as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Memo</label>
                                 <input class="form-control" id="memo_in[]" name="memo_in[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Keterangan</label>
                                 <input class="form-control" id="keterangan_in[]" name="keterangan_in[]" type="text" style="width: 200px"/>
                              </div>
                           </div>
                        </div>
                        @endforeach
                        
                     </div>
                  </div>
               </div>
            </div>
         </div> --}}

         {{-- ============Form Input Out============= --}}
         {{-- <div class="card" style="margin-bottom: 25px;">
            <div class="card-header border-bottom">
               <div class="row flex-between-end">
                  <div class="col-auto align-self-center">
                     <h5>Input Harian Proses</h5>
                  </div>
               </div>
            </div>
            <div class="card-body pt-0">
               <div class="tab-content">
                  <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                     <div class="table-responsive scrollbar">
                        <h5 class="mb-0" data-anchor="data-anchor">Input Harian Out</h5>
                        @foreach($outs as $out)
                     
                        <div class="card-header border-bottom">
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Process</label>
                                 <input class="form-control" id="no_process_out[]" name="no_process_out[]" type="text" value="halo" readonly  style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tanggal</label>
                                 <input class="form-control" id="tanggal_out[]" name="tanggal_out[]" type="date" value="{{$tanggal}}" readonly/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Departemen</label>
                                 <input class="form-control" id="departemen_out[]" name="departemen_out[]" type="text" value="{{$select_departemen->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Unit</label>
                                 <input class="form-control" id="unit_out[]" name="unit_out[]" type="text" value="{{$select_unit->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Sub Unit</label>
                                 <input class="form-control" id="subunit_out[]" name="subunit_out[]" type="text" value="{{$select_subunit->name}}" readonly style="width: 200px"/>
                              </div>
                           </div>
                           <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Part Item</label>
                                 <input class="form-control" id="no_part_item_out[]" name="no_part_item_out[]" type="text" value="{{$out->no_part_item}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Item</label>
                                 <input class="form-control" id="item_out[]" name="item_out[]" type="text" value="{{$out->item->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Shift</label>
                                 <select class="form-select" id="shift_out[]" name="shift_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Shift</option>
                                    @foreach ($shift as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Bahan</label>
                                 <input class="form-control" id="keterangan_bahan_out[]" name="keterangan_bahan_out[]" type="text" value="{{$out->ket_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Type Bahan</label>
                                 <input class="form-control" id="type_bahan_out[]" name="type_bahan_out[]" type="text" value="{{$out->type_bahan->name}}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Jenis Kayu</label>
                                 <input class="form-control" id="jenis_kayu_out[]" name="jenis_kayu_out[]" type="text" value="{{ $out->jenis_kayu->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Group Kualitas</label>
                                 <input class="form-control" id="group_kualitas_out[]" name="group_kualitas_out[]" type="text" value="{{ $out->group_kualitas->name }}" readonly style="width: 200px"/>
                              </div>                           
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Kualitas</label>
                                 <input class="form-control" id="kualitas_out[]" name="kualitas_out[]" type="text" value="{{ $out->kualitas->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Warna</label>
                                 <input class="form-control" id="warnas_out[]" name="warna_out[]" type="text" value="{{ $out->warna->name  }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Diameter</label>
                                 <input class="form-control" id="diameter_out[]" name="diameter_out[]" type="text" value="{{ $out->diameter }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Ket Proses</label>
                                 <input class="form-control" id="keterangan_proses_out[]" name="keterangan_proses_out[]" type="text" value="{{ $out->ket_proses->name }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Tinggi</label>
                                 <input class="form-control" id="tinggi_out[]" name="tinggi_out[]" type="text" value="{{ $out->tinggi }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Lebar</label>
                                 <input class="form-control" id="lebar_out[]" name="lebar_out[]" type="text" value="{{ $out->lebar }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Panjang</label>
                                 <input class="form-control" id="panjang_out[]" name="panjang_out[]" type="text" value="{{ $out->panjang }}" readonly style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pcs</label>
                                 <input class="form-control" id="pcs_out[]" name="pcs_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Supplier</label>
                                 <select class="form-select" id="supplier_out[]" name="supplier_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Supplier</option>
                                    @foreach ($supplier as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Kiriman</label>
                                 <input class="form-control" id="no_kiriman_out[]" name="no_kiriman_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO</label>
                                 <select class="form-select" id="po_out[]" name="po_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO</option>
                                    @foreach ($product as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">PO Nomer</label>
                                 <select class="form-select" id="no_po_out[]" name="no_po_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih PO Nomer</option>
                                    @foreach ($no_po as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">No Pallet</label>
                                 <input class="form-control" id="no_pallet_out[]" name="no_pallet_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Nampan</label>
                                 <input class="form-control" id="nampan_out[]" name="nampan_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Operator</label>
                                 <input class="form-control" id="operator_out[]" name="operator_out[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Tetap</label>
                                 <input class="form-control" id="harian_tetap_out[]" name="harian_tetap_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Harian Lepas</label>
                                 <input class="form-control" id="harian_lepas_out[]" name="harian_lepas_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Borong</label>
                                 <input class="form-control" id="borong_out[]" name="borong_out[]" type="number" style="width: 200px"/>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Pemborong</label>
                                 <select class="form-select" id="pemborong_out[]" name="pemborong_out[]" aria-label="Default select example" style="width: 200px">
                                    <option selected="selected" value="">Pilih Pemborong</option>
                                    @foreach ($pemborong as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Memo</label>
                                 <input class="form-control" id="memo_out[]" name="memo_out[]" type="text" style="width: 200px"/>
                              </div>
                              <div class="mb-3" style="margin-left: 23px; margin-right: 23px;">
                                 <label class="form-label" for="basic-form-dob">OT</label>
                                 <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-e0d869aa-3441-496b-9fc3-0333faf70522" id="dom-e0d869aa-3441-496b-9fc3-0333faf70522">
                                    <div class="form-check">
                                       <input class="form-check-input" id="flexRadioDefault1" type="radio" id="ot_out{{ $loop->iteration }}[]" name="ot_out{{ $loop->iteration }}[]" value="on"/>
                                       <label class="form-check-label" for="flexRadioDefault1">Ada</label>
                                    </div>
                                    <div class="form-check">
                                       <input class="form-check-input" id="flexRadioDefault2" type="radio" id="ot_out{{ $loop->iteration }}[]" name="ot_out{{ $loop->iteration }}[]" value="off" checked="" />
                                       <label class="form-check-label" for="flexRadioDefault2">Tidak Ada</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="mb-3">
                                 <label class="form-label" for="basic-form-dob">Keterangan</label>
                                 <input class="form-control" id="keterangan_out[]" name="keterangan_out[]" type="text" style="width: 200px"/>
                              </div>
                           </div>
                        </div>
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
         </div> --}}
         {{-- <div id="dynamicAddRemove">
         </div>
         <button type="button" name="add" id="dynamic-ar" class="btn btn-outline-primary">Add Subject</button> --}}
         <button id="post" class="btn btn-primary text-uppercase" data-toggle="tooltip" title="Save">Post</button>
      </form>

      {{-- <form action="{{ route('input-harian.store') }}" method="POST">
         @csrf
         <div id="dynamictk"> --}}
            {{-- <select class="form-select" id="itemm" name="itemm" onchange="isi_otomatis()" aria-label="Default select example" style="width: 200px">
               <option selected="selected" value="">Pilih Item</option>
               @foreach ($items as $item)
                  <option value="{{$item->id}}">{{$item->no_part_item}}</option>
               @endforeach
            </select> --}}
            {{-- <input class="form-control" id="diameterr" name="diameterr" type="text" readonly style="width: 200px"/><br> --}}

            {{-- <input type="text" name="addMoreInputFieldstk[0][subject]" placeholder="Enter subject" class="form-control" style="width: 200px"/>
         </div>
         <button type="button" name="add" id="dynamic-artk" class="btn btn-outline-primary">Add Subject</button>
         <button id="post" class="btn btn-primary text-uppercase" data-toggle="tooltip" title="Save">Post</button>
      </form> --}}
   </div>
</x-app-layout>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
{{-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>

{{-- Start data table --}}
<script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>

{{-- End data table --}}

{{-- Dynamic form --}}
<script type="text/javascript">
   function isi_otomatis(){
      var itemm = $("#itemm").val();
      $.ajax({
            url: "{!! route('input-harian.json-item') !!}",
            data:"itemm="+itemm,
      }).success(function (items) {
            
            $('#diameterr').val(items.diameter);
      });
   }

   $(document).ready(function () {
      $('#navigation').formNavigation();
   });

   // move up down cursor when input
   var elements = document.getElementsByClassName("navigation");
   var currentIndex = 0;

   document.onkeydown = function(e) {
      switch (e.keyCode) {
      case 38:
         currentIndex = (currentIndex == 0) ? elements.length - 1 : --currentIndex;
         elements[currentIndex].focus();
         break;
      case 40:
         currentIndex = ((currentIndex + 1) == elements.length) ? 0 : ++currentIndex;
         elements[currentIndex].focus();
         break;
      }
   };
   
      // function harian_tetap(data){
      //    console.log(data);
      //    $("#dynamicht"+data).append('<tr><td><input type="text" name="addMoreInputFieldsht[' + data + '][]" class="form-control" style="width: 200px;"/></td><td><button type="button" class="btn btn-outline-danger remove-input-fieldht"><span class="fas fa-trash"></span></button></td></tr>');
      // }

      // function harian_lepas(data){
      //    console.log(data);
      //    $("#dynamichl"+data).append('<tr><td><input type="text" name="addMoreInputFieldshl[' + data + '][]" class="form-control" style="width: 200px;"/></td><td><button type="button" class="btn btn-outline-danger remove-input-fieldht"><span class="fas fa-trash"></span></button></td></tr>');
      // }

      // function borong(data){
      //    console.log(data);
      //    $("#dynamicbrg"+data).append('<tr><td><input type="text" name="addMoreInputFieldsbrg[' + data + '][]" class="form-control" style="width: 200px;"/></td><td><button type="button" class="btn btn-outline-danger remove-input-fieldht"><span class="fas fa-trash"></span></button></td></tr>');
      // }

      $(document).ready( function () {
      $('#example').DataTable();
      } );

      // dinamic HT
      var ht = 0;
      $("#dynamic-arht").click(function () {
         ++ht;
         $("#dynamicht").append('<tr><td><input type="text" name="nama_ht[' + ht + ']" class="form-control" style="width: 200px;"/></td><td><button type="button" class="btn btn-outline-danger remove-input-fieldht"><span class="fas fa-trash"></span></button></td></tr>');
      });
      $(document).on('click', '.remove-input-fieldht', function () {
         $(this).parents('tr').remove();
      });

      // dinamic HL
      var hl = 0;
      $("#dynamic-arhl").click(function () {
         ++hl;
         $("#dynamichl").append('<tr><td><input type="text" name="nama_hl[' + hl + ']" class="form-control" style="width: 200px;"/></td><td><button type="button" class="btn btn-outline-danger remove-input-fieldhl"><span class="fas fa-trash"></span></button></td></tr>');
      });
      $(document).on('click', '.remove-input-fieldhl', function () {
         $(this).parents('tr').remove();
      });

      // dinamic Borong
      var brg = 0;
      $("#dynamic-arbrg").click(function () {
         ++brg;
         $("#dynamicbrg").append('<tr><td><input type="text" name="nama_borong[' + brg + ']" class="form-control" style="width: 200px;"/></td><td><button type="button" class="btn btn-outline-danger remove-input-fieldbrg"><span class="fas fa-trash"></span></button></td></tr>');
      });
      $(document).on('click', '.remove-input-fieldbrg', function () {
         $(this).parents('tr').remove();
      });





      var i = '<?php echo json_encode($start_array_in); ?>';
      $("#dynamic-ar").click(function () {
         ++i;

            $("#dynamicAddRemove").append(
               '<span>'+
                  '<div class="card-header border-bottom">'+
                     '<div class="d-flex align-items-center add-contact__form my-sm-0 my-2">'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">No Process</label>'+
                           '<input class="form-control" id="no_process_in[' + i + ']" name="no_process_in[' + i + ']" type="text" value="halo" readonly style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Tanggal</label>'+
                           '<input class="form-control" id="tanggal_in[' + i + ']" name="tanggal_in[' + i + ']" type="date" value="{{$tanggal}}" readonly/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Departemen</label>'+
                           '<input class="form-control" id="departemen_in[' + i + ']" name="departemen_in[' + i + ']" type="text" value="{{$select_departemen->name}}" readonly style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Unit</label>'+
                           '<input class="form-control" id="unit_in[' + i + ']" name="unit_in[' + i + ']" type="text" value="{{$select_unit->name}}" readonly style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Sub Unit</label>'+
                           '<input class="form-control" id="subunit_in[' + i + ']" name="subunit_in[' + i + ']" type="text" value="{{$select_subunit->name}}" readonly style="width: 200px"/>'+
                        '</div>'+
                     '</div>'+
                     '<div class="d-flex align-items-center add-contact__form my-sm-0 my-2">'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">No Part Item</label>'+
                           '<select class="form-select" id="no_part_item_in'+i+'" name="no_part_item_in[' + i + ']" onchange="otomatis()" aria-label="Default select example" style="width: 200px">'+
                              '<option selected="selected" value="">Pilih Item</option>'+
                              '@foreach ($items as $item)'+
                                 '<option value="{{$item->id}}">{{$item->no_part_item}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Item</label>'+
                           '<select class="form-select" id="item_in'+i+'" name="item_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Item</option>'+
                              '@foreach ($master_item as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Shift</label>'+
                           '<select class="form-select" id="shift_in" name="shift_in[' + i + ']" aria-label="Default select example" style="width: 200px">'+
                              '<option selected="selected" value="">Pilih Shift</option>'+
                              '@foreach ($shift as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Ket Bahan</label>'+
                           '<select class="form-select" id="keterangan_bahan_in'+i+'" name="keterangan_bahan_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Ket Bahan</option>'+
                              '@foreach ($ket_bahan as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Type Bahan</label>'+
                           '<select class="form-select" id="type_bahan_in'+i+'" name="type_bahan_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Type Bahan</option>'+
                              '@foreach ($type_bahan as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Jenis Kayu</label>'+
                           '<select class="form-select" id="jenis_kayu_in'+i+'" name="jenis_kayu_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Jenis Kayu</option>'+
                              '@foreach ($jenis_kayu as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Group Kualitas</label>'+
                           '<select class="form-select" id="group_kualitas_in'+i+'" name="group_kualitas_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Group Kualitas</option>'+
                              '@foreach ($group_kualitas as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Kualitas</label>'+
                           '<select class="form-select" id="kualitas_in'+i+'" name="kualitas_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Kualitas</option>'+
                              '@foreach ($kualitas as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Warna</label>'+
                           '<select class="form-select" id="warnas_in'+i+'" name="warnas_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Warna</option>'+
                              '@foreach ($warna as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Diameter</label>'+
                           '<input class="form-control" id="diameter_in'+i+'" name="diameter_in[' + i + ']" type="text" readonly style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Ket Proses</label>'+
                           '<select class="form-select" id="keterangan_proses_in'+i+'" name="keterangan_proses_in[' + i + ']" aria-label="Default select example" style="width: 200px" disabled="true">'+
                              '<option selected="selected" value="">Pilih Ket Proses</option>'+
                              '@foreach ($ket_proses as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Tinggi</label>'+
                           '<input class="form-control" id="tinggi_in'+i+'" name="tinggi_in[' + i + ']" type="text" readonly style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Lebar</label>'+
                           '<input class="form-control" id="lebar_in'+i+'" name="lebar_in[' + i + ']" type="text" readonly style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Panjang</label>'+
                           '<input class="form-control" id="panjang_in'+i+'" name="panjang_in[' + i + ']" type="text" readonly style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Pcs</label>'+
                           '<input class="form-control" id="pcs_in" name="pcs_in[' + i + ']" type="number" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Supplier</label>'+
                           '<select class="form-select" id="supplier_in" name="supplier_in[' + i + ']" aria-label="Default select example" style="width: 200px">'+
                              '<option selected="selected" value="">Pilih Supplier</option>'+
                              '@foreach ($supplier as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">No Kiriman</label>'+
                           '<input class="form-control" id="no_kiriman_in" name="no_kiriman_in[' + i + ']" type="number" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">PO</label>'+
                           '<select class="form-select" id="po_in" name="po_in[' + i + ']" aria-label="Default select example" style="width: 200px">'+
                              '<option selected="selected" value="">Pilih PO</option>'+
                              '@foreach ($product as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">PO Nomer</label>'+
                           '<select class="form-select" id="no_po_in" name="no_po_in[' + i + ']" aria-label="Default select example" style="width: 200px">'+
                              '<option selected="selected" value="">Pilih PO Nomer</option>'+
                              '@foreach ($no_po as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">No Pallet</label>'+
                           '<input class="form-control" id="no_pallet_in" name="no_pallet_in[' + i + ']" type="number" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Nampan</label>'+
                           '<input class="form-control" id="nampan_in" name="nampan_in[' + i + ']" type="number" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Operator</label>'+
                           '<input class="form-control" id="operator_in" name="operator_in[' + i + ']" type="text" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Harian Tetap</label>'+
                           '<input class="form-control" id="harian_tetap_in" name="harian_tetap_in[' + i + ']" type="number" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Harian Lepas</label>'+
                           '<input class="form-control" id="harian_lepas_in" name="harian_lepas_in[' + i + ']" type="number" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Borong</label>'+
                           '<input class="form-control" id="borong_in" name="borong_in[' + i + ']" type="number" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Pemborong</label>'+
                           '<select class="form-select" id="pemborong_in" name="pemborong_in[' + i + ']" aria-label="Default select example" style="width: 200px">'+
                              '<option selected="selected" value="">Pilih Pemborong</option>'+
                              '@foreach ($pemborong as $item)'+
                                 '<option value="{{$item->id}}">{{$item->name}}</option>'+
                              '@endforeach'+
                           '</select>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Memo</label>'+
                           '<input class="form-control" id="memo_in" name="memo_in[' + i + ']" type="text" style="width: 200px"/>'+
                        '</div>'+
                        '<div class="mb-3" style="margin-left: 23px; margin-right: 23px;">'+
                           '<label class="form-label" for="basic-form-dob">OT</label>'+
                           '<div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-e0d869aa-3441-496b-9fc3-0333faf70522" id="dom-e0d869aa-3441-496b-9fc3-0333faf70522">'+
                              '<div class="form-check">'+
                                 '<input class="form-check-input" id="flexRadioDefault1" type="radio" id="ot_in" name="ot_in[' + i + ']" value="1"/>'+
                                 '<label class="form-check-label" for="flexRadioDefault1">Ada</label>'+
                              '</div>'+
                              '<div class="form-check">'+
                                 '<input class="form-check-input" id="flexRadioDefault2" type="radio" id="ot_in" name="ot_in[' + i + ']" value="2" checked="" />'+
                                 '<label class="form-check-label" for="flexRadioDefault2">Tidak Ada</label>'+
                              '</div>'+
                           '</div>'+
                        '</div>'+
                        '<div class="mb-3">'+
                           '<label class="form-label" for="basic-form-dob">Keterangan</label>'+
                           '<input class="form-control" id="keterangan_in" name="keterangan_in[' + i + ']" type="text" style="width: 200px"/>'+
                        '</div>'+
                     '</div>'+
                     '<button type="button" class="btn btn-outline-danger remove-input-field">Delete</button>'+
                  '</div>'+
               '</span>'
            )
            
      });

      $(document).on('click', '.remove-input-field', function () {
         $(this).parents('span').remove();
      });

      function harian_in(data1){
         console.log(data);
         $("#dynamicin").append(
            '<div class="table-responsive scrollbar">'+
               '<div class="d-flex align-items-center add-contact__form my-sm-0 my-2">'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">No Part Item</label>'+
                     '<input class="form-control" id="no_part_item_in[]" name="no_part_item_in[]" type="text" value="'+data.no_part_item+'" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Item</label>'+
                     '<input class="form-control" id="item_in[]" name="item_in[]" type="text" value="" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Ket Bahan</label>'+
                     '<input class="form-control" id="keterangan_bahan_in[]" name="keterangan_bahan_in[]" type="text" value="{{$in->ket_bahan->name}}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Type Bahan</label>'+
                     '<input class="form-control" id="type_bahan_in[]" name="type_bahan_in[]" type="text" value="{{$in->type_bahan->name}}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Jenis Kayu</label>'+
                     '<input class="form-control" id="jenis_kayu_in[]" name="jenis_kayu_in[]" type="text" value="{{ $in->jenis_kayu->name }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Group Kualitas</label>'+
                     '<input class="form-control" id="group_kualitas_in[]" name="group_kualitas_in[]" type="text" value="{{ $in->group_kualitas->name }}" readonly style="width: 200px"/>'+
                  '</div>'+           
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Kualitas</label>'+
                     '<input class="form-control" id="kualitas_in[]" name="kualitas_in[]" type="text" value="{{ $in->kualitas->name }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Warna</label>'+
                     '<input class="form-control" id="warnas_in[]" name="warna_in[]" type="text" value="{{ $in->warna->name  }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Diameter</label>'+
                     '<input class="form-control" id="diameter_in[]" name="diameter_in[]" type="text" value="{{ $in->diameter }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Ket Proses</label>'+
                     '<input class="form-control" id="keterangan_proses_in[]" name="keterangan_proses_in[]" type="text" value="{{ $in->ket_proses->name }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Tinggi</label>'+
                     '<input class="form-control" id="tinggi_in[]" name="tinggi_in[]" type="text" value="{{ $in->tinggi }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Lebar</label>'+
                     '<input class="form-control" id="lebar_in[]" name="lebar_in[]" type="text" value="{{ $in->lebar }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Panjang</label>'+
                     '<input class="form-control" id="panjang_in[]" name="panjang_in[]" type="text" value="{{ $in->panjang }}" readonly style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Pcs</label>'+
                     '<input class="form-control" id="pcs_in[]" name="pcs_in[]" type="number" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Supplier</label>'+
                     '<select class="form-select" id="supplier_in[]" name="supplier_in[]" aria-label="Default select example" style="width: 200px">'+
                        '<option selected="selected" value="">Pilih Supplier</option>'+
                        // '@foreach ($supplier as $item)'+
                        //    '<option value="{{$item->id}}">{{$item->name}}</option>'+
                        // '@endforeach'+
                     '</select>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">No Kiriman</label>'+
                     '<input class="form-control" id="no_kiriman_in[]" name="no_kiriman_in[]" type="number" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">PO</label>'+
                     '<select class="form-select" id="po_in[]" name="po_in[]" aria-label="Default select example" style="width: 200px">'+
                        '<option selected="selected" value="">Pilih PO</option>'+
                        // '@foreach ($product as $item)'+
                        //    '<option value="{{$item->id}}">{{$item->name}}</option>'+
                        // '@endforeach'+
                     '</select>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">PO Nomer</label>'+
                     '<select class="form-select" id="no_po_in[]" name="no_po_in[]" aria-label="Default select example" style="width: 200px">'+
                        '<option selected="selected" value="">Pilih PO Nomer</option>'+
                        // '@foreach ($no_po as $item)'+
                        //    '<option value="{{$item->id}}">{{$item->name}}</option>'+
                        // '@endforeach'+
                     '</select>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">No Pallet</label>'+
                     '<input class="form-control" id="no_pallet_in[]" name="no_pallet_in[]" type="number" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Nampan</label>'+
                     '<input class="form-control" id="nampan_in[]" name="nampan_in[]" type="number" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Operator</label>'+
                     '<input class="form-control" id="operator_in[]" name="operator_in[]" type="text" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Harian Tetap</label>'+
                     '<input class="form-control" id="harian_tetap_in[]" name="harian_tetap_in[]" type="number" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Harian Lepas</label>'+
                     '<input class="form-control" id="harian_lepas_in[]" name="harian_lepas_in[]" type="number" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Borong</label>'+
                     '<input class="form-control" id="borong_in[]" name="borong_in[]" type="number" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Pemborong</label>'+
                     '<select class="form-select" id="pemborong_in[]" name="pemborong_in[]" aria-label="Default select example" style="width: 200px">'+
                        '<option selected="selected" value="">Pilih Pemborong</option>'+
                        // '@foreach ($pemborong as $item)'+
                        //    '<option value="{{$item->id}}">{{$item->name}}</option>'+
                        // '@endforeach'+
                     '</select>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Memo</label>'+
                     '<input class="form-control" id="memo_in[]" name="memo_in[]" type="text" style="width: 200px"/>'+
                  '</div>'+
                  '<div class="mb-3">'+
                     '<label class="form-label" for="basic-form-dob">Keterangan</label>'+
                     '<input class="form-control" id="keterangan_in[]" name="keterangan_in[]" type="text" style="width: 200px"/>'+
                  '</div>'+
               '</div>'+
            '</div>'
         )
      }

      function harian_out(data){
         console.log(data);
      
      }

      function otomatis(){
         var no_part_item_in = $("#no_part_item_in"+i).val();
         $.ajax({
               url: "{!! route('input-harian.json-item') !!}",
               data:"no_part_item_in="+no_part_item_in,
         }).success(function (items) {
               $('#item_in'+i).val(items.master_items_id);
               $('#keterangan_bahan_in'+i).val(items.master_keterangan_bahans_id);
               $('#type_bahan_in'+i).val(items.master_type_bahans_id);
               $('#jenis_kayu_in'+i).val(items.master_jenis_kayus_id);
               $('#group_kualitas_in'+i).val(items.master_group_kualitas_id);
               $('#kualitas_in'+i).val(items.master_kualitas_id);
               $('#warnas_in'+i).val(items.master_warnas_id );
               $('#keterangan_proses_in'+i).val(items.master_keterangan_proses_id);
               $('#diameter_in'+i).val(items.diameter);
               $('#tinggi_in'+i).val(items.tinggi);
               $('#lebar_in'+i).val(items.lebar);
               $('#panjang_in'+i).val(items.panjang);
         });
      }
      $('#post').click(function() {
         $('#item_in'+i).attr('disabled', false);
         $('#keterangan_bahan_in'+i).attr('disabled', false);
         $('#type_bahan_in'+i).attr('disabled', false);
         $('#jenis_kayu_in'+i).attr('disabled', false);
         $('#group_kualitas_in'+i).attr('disabled', false);
         $('#kualitas_in'+i).attr('disabled', false);
         $('#warnas_in'+i).attr('disabled', false);
         $('#keterangan_proses_in'+i).attr('disabled', false);
      });

</script>

