{{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> --}}
<script src="{{ asset('js/app.js') }}" defer></script>
<style>
   .select2-container{
       width: 100% !important;
   }
</style>

<x-app-layout>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Item</h5>
               </div>
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <form action="{{ route('input-harian.history') }}" method="POST" enctype="multipart/form-data">
                     @csrf
                     <div class="card-header border-bottom">
                        <div class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                           <div class="mb-3">
                              <label class="form-label" for="basic-form-dob">Tanggal</label>
                              <input class="form-control" id="tanggal" name="tanggal" type="date" />
                           </div>
                           {{-- <div class="mb-3">
                              <label class="form-label" for="departemen">Departemen</label>
                              <select class="form-select" id="departemen" name="departemen" aria-label="Default select example"  onchange="opt_departemen()" style="width: 220px">
                                 <option selected="selected" value="">Pilih Departemen</option>
                                 @foreach ($departemen as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                 @endforeach
                              </select>
                           </div> --}}
                           <div class="mb-3">
                              <label class="form-label" for="departemen">Departemen</label>
                              <select class="form-select" id="departemen" name="departemen" aria-label="Default select example" style="width: 220px">
                                 <option selected="selected" value="">Pilih Departemen</option>
                                 @foreach ($departemen as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                           {{-- <input class="form-control" id="halo" name="halo" type="text" /> --}}
                           <div class="mb-3">
                              <label class="form-label" for="unit">Unit</label>
                              <select class="form-select" id="unit" name="unit" aria-label="Default select example" style="width: 250px">
                                    <option selected="selected" value="">Pilih Unit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </option>
                                    @foreach ($unit as $item)
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                              </select>
                           </div>
                           <div class="mb-3">
                              <label class="form-label" for="subunit">Sub Unit</label>
                              <select class="form-select" id="subunit" name="subunit" aria-label="Default select example" style="width: 220px">
                                 <option selected="selected" value="">Pilih Sub Unit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                 @foreach ($subunit as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        <button class="btn btn-primary text-uppercase" data-toggle="tooltip" title="Save">Post</button>
                     </div>
                  </form>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>

      // $(function () {
      //    $('#departemen').on('change', function () {
      //       axios.post('{{ route('input-harian.json-departemen') }}', {id: $(this).val()})
      //             .then(function (response) {
      //                $('#unit').empty();
      //                $.each(response.data, function (id, name) {
      //                   $('#unit').append(
      //                      new Option(name, id));
      //                })
      //             });
      //    });
      // });

      // $(function () {
      //    $('#unit').on('change', function () {
      //       axios.post('{{ route('input-harian.json-unit') }}', {id: $(this).val()})
      //             .then(function (response) {
      //                $('#subunit').empty();
      //                $.each(response.data, function (id, name) {
      //                   $('#subunit').append(new Option(name, id))
      //                })
      //             });
      //    });
      // });

   $(document).ready(function() {
   $('#departemen').select2();
   });


   $(document).ready(function() {
   $('#unit').select2();
   });

   $(document).ready(function() {
   $('#subunit').select2();
   });

</script>
