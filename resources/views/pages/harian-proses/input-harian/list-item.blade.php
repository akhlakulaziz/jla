<x-app-layout>
   <div class="col-lg-12">
      <div class="col-auto align-self-center" style="margin-bottom: 5px;">
         <form action="{{ route('input-harian.input') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input class="form-control" id="tanggal" name="tanggal" type="hidden" value="{{$tanggal}}"/>
            <input class="form-control" id="departemen" name="departemen" type="hidden" value="{{$departemen}}"/>
            <input class="form-control" id="unit" name="unit" type="hidden" value="{{$unit}}"/>
            <input class="form-control" id="subunit" name="subunit" type="hidden" value="{{$subunit}}"/>
      </div>
      {{-- <a href="javascript:history.go(-1)" title="Back">
         <button class="btn btn-dark btn-md mb-2"><i class="fa fa-arrow-left" aria-hidden="true" value="Back"></i> Back</button>
      </a> --}}
      <div class="card" style="margin-bottom: 50px;">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Harian In</h5>
               </div>
               
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="card-body pt-0">
                     <div class="tab-content">
                        <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                           <div class="table-responsive scrollbar">
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th style="width: 10%;">No</th>
                                       <th scope="col">No Part Item</th>
                                       <th scope="col">Item</th>
                                       <th scope="col">Ket Bahan</th>
                                       <th scope="col">Type Bahan</th>
                                       <th scope="col">Diameter</th>
                                       <th scope="col">Tinggi</th>
                                       <th scope="col">Lebar</th>
                                       <th scope="col">Panjang</th>
                                       <th scope="col">Warna</th>
                                       <th scope="col">Group Kualitas</th>
                                       <th scope="col">Kualitas</th>
                                       <th scope="col">Jenis Kayu</th>
                                       <th scope="col">Ket Proses</th>
                                       <th scope="col">Status</th>
                                       <th scope="col">In</th>
                                       <th scope="col">Out</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($items as $item)
                                       <tr>
                                          <td>{{ $loop->iteration }}</td>
                                          <td>{{ $item->no_part_item }}</td>
                                          <td>{{ $item->item->name }}</td>
                                          <td>{{ $item->ket_bahan->name }}</td>
                                          <td>{{ $item->type_bahan->name  }}</td>
                                          <td>{{ $item->diameter }}</td>
                                          <td>{{ $item->tinggi }}</td>
                                          <td>{{ $item->lebar }}</td>
                                          <td>{{ $item->panjang }}</td>
                                          <td>{{ $item->warna->name  }}</td>
                                          <td>{{ $item->group_kualitas->name }}</td>
                                          <td>{{ $item->kualitas->name }}</td>
                                          <td>{{ $item->jenis_kayu->name }}</td>
                                          <td>{{ $item->ket_proses->name }}</td>
                                          <td>
                                             @if ($item->status == 1)
                                                Active
                                             @else
                                                Inactive
                                             @endif
                                          </td>
                                          <td>
                                             <div class="form-check">
                                                <input class="form-check-input" id="in[]" name="in[]" type="checkbox" value="{{ $item->id }}" />
                                             </div>
                                          </td>
                                          <td>
                                             <div class="form-check">
                                                <input class="form-check-input" id="out[]" name="out[]" type="checkbox" value="{{ $item->id }}" />
                                             </div>
                                          </td>
                                       </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                           <button class="btn btn-primary text-uppercase" data-toggle="tooltip" title="Save">Post</button>
                        </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>
