<div class="modal fade" id="modal-info-confirmed" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h4 class="mb-1">Hapus Data</h4>
                </div>
                <div class="p-4 pb-0">
                    <p>Apakah Anda yakin ingin menghapus data?</p> 
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="id">
                <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger text-uppercase"  id="change" onclick="destroy()">
                    Hapus
                </button>
            </div>
        </div>
    </div>
</div>

<script>

    function remove(id){
        $('#id').val(id);

    }

    function destroy(){
        var id =$('#id').val();

        createOverlay("process...");
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                id: id,
            },
            url: "/input-harian-out/"+id,
            
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{route('input-harian-out.index')}}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }


</script>
