<div class="modal fade" id="item-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h4 class="mb-1" id="modal-title"></h4>
                </div>
                <div class="p-4 pb-0">
                    <form id="form" action="#">
                        <input type="hidden" class="form-control" id="id" disabled>
                        <div class="mb-3">
                            <label class="col-form-label" for="shift">Shift:</label>
                            <select class="form-control" id="shift" name="shift">
                                <option value="">Pilih</option>
                                @foreach ($shift as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="pcs">Pcs:</label>
                            <input type="number" class="form-control" id="pcs">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="supplier">Supplier:</label>
                            <select class="form-control" id="supplier" name="supplier">
                                <option value="">Pilih</option>
                                @foreach ($supplier as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="no_kiriman">No Kiriman:</label>
                            <input type="number" class="form-control" id="no_kiriman">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="po">PO:</label>
                            <select class="form-control" id="po" name="po">
                                <option value="">Pilih PO</option>
                                @foreach ($product as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="po_no">PO Nomer:</label>
                            <select class="form-control" id="po_no" name="po_no">
                                <option value="">Pilih PO Nomer</option>
                                @foreach ($no_po as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="no_pallet">No Pallet:</label>
                            <input type="number" class="form-control" id="no_pallet">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="nampan">Nampan:</label>
                            <input type="number" class="form-control" id="nampan">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="operator">Operator:</label>
                            <input type="text" class="form-control" id="operator">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="harian_tetap">Harian Tetap:</label>
                            <input type="number" class="form-control" id="harian_tetap">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="harian_lepas">Harian Lepas:</label>
                            <input type="number" class="form-control" id="harian_lepas">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="borong">Borong:</label>
                            <input type="number" class="form-control" id="borong">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="pemborong">Pemborong:</label>
                            <select class="form-control" id="pemborong" name="pemborong">
                                <option value="">Pilih</option>
                                @foreach ($pemborong as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="memo">Memo:</label>
                            <input type="text" class="form-control" id="memo">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="ot">Ot:</label>
                            <select class="form-control" id="ot" name="ot">
                                <option value="">Pilih</option>
                                <option value="1">Ada</option>
                                <option value="2">Tidak Ada</option>
                            </select>
                        </div>
                        {{-- <div class="mb-3">
                            <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-e0d869aa-3441-496b-9fc3-0333faf70522" id="dom-e0d869aa-3441-496b-9fc3-0333faf70522">
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault1" type="radio" id="ot" name="ot" value="1"/>
                                    <label class="form-check-label" for="flexRadioDefault1">Ada</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault2" type="radio" id="ot" name="ot" value="2" checked/>
                                    <label class="form-check-label" for="flexRadioDefault2">Tidak Ada</label>
                                </div>
                            </div>
                        </div> --}}
                        <div class="mb-3">
                            <label class="col-form-label" for="keterangan">Keterangan:</label>
                            <input type="text" class="form-control" id="keterangan">
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="button" id="update" onclick="updated()">Update</button>
            </div>
        </div>
    </div>
</div>

<script>
    function edit(data){
        // console.log(data.ot);

        $('#form').trigger("reset");
        $('#modal-title').text('Edit Data');
        $('#id').val(data.id);
        $('#shift').val(data.shift);
        $('#pcs').val(data.pcs);
        $('#supplier').val(data.master_suppliers_id);
        $('#no_kiriman').val(data.no_kiriman);
        $('#po').val(data.master_products_id);
        $('#po_no').val(data.master_no_pos_id);
        $('#no_pallet').val(data.no_pallet);
        $('#nampan').val(data.nampan);
        $('#operator').val(data.operator);
        $('#harian_tetap').val(data.harian_tetap);
        $('#harian_lepas').val(data.harian_lepas);
        $('#borong').val(data.borong);
        $('#pemborong').val(data.master_pemborongs_id);
        $('#memo').val(data.memo);
        $('#ot').val(data.ot);
        $('#keterangan').val(data.keterangan);
        $("#update").show();
        $('#modal-basic').modal('show');
    }

    function updated(){
        var id = $('#id').val();
        var shift = $('#shift').val();
        var pcs = $('#pcs').val();
        var supplier = $('#supplier').val();
        var no_kiriman = $('#no_kiriman').val();
        var po = $('#po').val();
        var po_no = $('#po_no').val();
        var no_pallet = $('#no_pallet').val();
        var nampan = $('#nampan').val();
        var operator = $('#operator').val();
        var harian_tetap = $('#harian_tetap').val();
        var harian_lepas = $('#harian_lepas').val();
        var borong = $('#borong').val();
        var pemborong = $('#pemborong').val();
        var memo = $('#memo').val();
        var ot = $('#ot').val();
        var keterangan = $('#keterangan').val();
        createOverlay("Proses...");
        $.ajax({
            type : "PUT",
            url: "{!! route('input-harian-out.update',["id"]) !!}",
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'id' : id,
                'shift' :shift,
                'pcs' :pcs,
                'supplier' :supplier,
                'no_kiriman' :no_kiriman,
                'po' :po,
                'po_no' :po_no,
                'no_pallet' :no_pallet,
                'nampan' :nampan,
                'operator' :operator,
                'harian_tetap' :harian_tetap,
                'harian_lepas' :harian_lepas,
                'borong' :borong,
                'pemborong' :pemborong,
                'memo' :memo,
                'ot' :ot,
                'keterangan' :keterangan,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                            window.location = "{{route('input-harian-out.index')}}";
                    }, 500);              
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }

</script>
