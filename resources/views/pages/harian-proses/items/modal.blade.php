{{-- <style>
    .select2-container{
        width: 100% !important;
    }
</style> --}}
{{-- <div class="modal fade" id="modal-owners" data-backdrop="static"  tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-transparent mb-0">
                <div class="block-header">
                    <h3 class="block-title" id="modal-title1">
                    </h3>
                    <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                    </div>
                </div>
                
                <div class="block-content" style="padding: 0px 0px 1px;">
                    <form class="form-horizontal" id="form" action="#">
                        <div class="modal-body">
                            <input type="hidden" class="form-control" id="id" disabled>
                            <input type="hidden" class="form-control" id="business_id" value="{{$business->id}}">

                            <div class="form-group{{ $errors->has('type_user') ? ' has-error' : ''}}">
                                {!! Form::label('type_user', 'Type User: ', ['class' => 'control-label']) !!}
                                {!! Form::select('type_user',(["1" => "User", "2" => "Bisnis"]), null, ['class' => 'form-control  ih-medium ip-gray radius-xs b-light', 'id'=>'type_user', 'onchange'=>'myFunction1()', 'placeholder' => 'Pilih Salah Satu']) !!}
                                {!! $errors->first('type_user', '<p class="help-block">:message</p>') !!}
                            </div> 

                            <div class="form-group" id="form_user">
                                <label class="mr-sm-4" for="inlineFormCustomSelect">Nama</label><br>
                                <select class="form-control" id="user_id" name="user_id">
                                    <option value="">Pilih</option>
                                    @foreach ($users as $item)
                                        @if($item->JRU == 1)
                                            <option value="{{$item->id}}">{{$item->name}} (JRU)</option>
                                        @elseif ($item->JRU == 2)
                                            <option value="{{$item->id}}">{{$item->name}} (Non Personil)</option>
                                        @endif
                                    @endforeach
                                </select>
                                <small class="font-14 text-muted">Catatan : Apabila nama user sebagai <strong>NON PERSONIL</strong> belum ada, silahkan buat
                                    @can ('owners-create')
                                        <a type="button" data-toggle="modal" onClick="create2()" data-target="#modal-users" data-dismiss="modal" style="color: #1e0fbe"><u><strong>disini</strong></u></a>
                                    @endcan
                                . </small>
                            </div>

                            <div class="form-group" id="form_bisnis">
                                <label class="mr-sm-4" for="inlineFormCustomSelect">Nama Bisnis</label><br>
                                <select class="form-control" id="business_relation" name="business_relation">
                                    <option value="">Pilih</option>
                                    @foreach ($business_option as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group{{ $errors->has('percent') ? ' has-error' : ''}}">
                                {!! Form::label('percent', 'Persen: ', ['class' => 'control-label']) !!}
                                <div class="with-icon">
                                    <span class="la-user lar color-gray"></span>
                                {!! Form::number('percent', null, ['class' => 'form-control mb-1 ih-medium ip-gray radius-xs b-light', 'id'=>'percent','required' => 'required','placeholder'=>'0.00']) !!}
                                </div>
                                {!! $errors->first('percent', '<p class="help-block">:message</p>') !!}
                                <small class="font-14 text-muted">Catatan : Gunakan tanda titik "." untuk mengisi bilangan desimal. </small>
                            </div>
                            
                            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                                {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
                                {!! Form::select('status', (["1" => "Aktif", "2" => "Tidak Aktif"]), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control','placeholder'=>'Pilih Status']) !!}
                                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary text-uppercase" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-dark text-uppercase"  id="create" onclick="store1()">
                    Simpan
                </button>
                <button type="button" class="btn btn-dark text-uppercase"  id="update" onclick="updated()">
                     Edit
                </button>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="item-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 500px">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h4 class="mb-1" id="modal-title"></h4>
                </div>
                <div class="p-4 pb-0">
                    <form id="form" action="#">
                        <input type="hidden" class="form-control" id="id" disabled>
                        <div class="mb-3">
                            <label class="col-form-label" for="item">Item:</label>
                            <select class="form-control" id="item" name="item">
                                <option value="">Pilih</option>
                                @foreach ($master_item as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="ket_bahan">Keterangan Bahan:</label>
                            <select class="form-control" id="ket_bahan" name="ket_bahan">
                                <option value="">Pilih</option>
                                @foreach ($keterangan_bahan as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="type_bahan">Type Bahan:</label>
                            <select class="form-control" id="type_bahan" name="type_bahan">
                                <option value="">Pilih</option>
                                @foreach ($type_bahan as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="diameter">Diameter:</label>
                            <input type="number" class="form-control" id="diameter">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="tinggi">Tinggi:</label>
                            <input type="number" class="form-control" id="tinggi">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="lebar">Lebar:</label>
                            <input type="number" class="form-control" id="lebar">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="panjang">Panjang:</label>
                            <input type="number" class="form-control" id="panjang">
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="warna">Warna:</label>
                            <select class="form-control" id="warna" name="warna">
                                <option value="">Pilih</option>
                                @foreach ($warna as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="group_kualitas">Group Kualitas:</label>
                            <select class="form-control" id="group_kualitas" name="group_kualitas">
                                <option value="">Pilih</option>
                                @foreach ($group_kualitas as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="kualitas">Kualitas:</label>
                            <select class="form-control" id="kualitas" name="kualitas">
                                <option value="">Pilih</option>
                                @foreach ($kualitas as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="jenis_kayu">Jenis Kayu:</label>
                            <select class="form-control" id="jenis_kayu" name="jenis_kayu">
                                <option value="">Pilih</option>
                                @foreach ($jenis_kayu as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="keterangan_proses">Keterangan Proses:</label>
                            <select class="form-control" id="keterangan_proses" name="keterangan_proses">
                                <option value="">Pilih</option>
                                @foreach ($keterangan_proses as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="status">Status:</label>
                            <select class="form-control" id="status" name="status">
                                <option value="">Pilih</option>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                                
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="button" id="create" onclick="store()">Simpan</button>
                <button class="btn btn-primary" type="button" id="update" onclick="updated()">Update</button>
            </div>
        </div>
    </div>
</div>

<script>
    function edit(data){
        console.log(data.id);

        $('#form').trigger("reset");
        $('#modal-title').text('Edit Data Item');
        $('#id').val(data.id);
        $('#item').val(data.master_items_id);
        $('#ket_bahan').val(data.master_keterangan_bahans_id);
        $('#type_bahan').val(data.master_type_bahans_id);
        $('#diameter').val(data.diameter);
        $('#tinggi').val(data.tinggi);
        $('#lebar').val(data.lebar);
        $('#panjang').val(data.panjang);
        $('#warna').val(data.master_warnas_id);
        $('#group_kualitas').val(data.master_group_kualitas_id);
        $('#kualitas').val(data.master_kualitas_id);
        $('#jenis_kayu').val(data.master_jenis_kayus_id);
        $('#keterangan_proses').val(data.master_keterangan_proses_id);
        $('#status').val(data.status);
        $("#create").hide();
        $("#update").show();
        $('#modal-basic').modal('show');
    }

    function create() {
        $('#modal-title').text('Add Data Item');
        $('#form').trigger("reset");
        $("#update").hide();
        $("#create").show();
        $("#modal-basic").modal('show');
    }

    function store() {
        var item = $('#item').val();
        var keterangan_bahan = $('#ket_bahan').val();
        var type_bahan = $('#type_bahan').val();
        var diameter = $('#diameter').val();
        var tinggi = $('#tinggi').val();
        var lebar = $('#lebar').val();
        var panjang = $('#panjang').val();
        var warna = $('#warna').val();
        var group_kualitas = $('#group_kualitas').val();
        var kualitas = $('#kualitas').val();
        var jenis_kayu = $('#jenis_kayu').val();
        var keterangan_proses = $('#keterangan_proses').val();
        var status = $('#status').val();

        var form_data = new FormData();
        form_data.append('_token', '{{ csrf_token() }}');
        form_data.append('item', item);
        form_data.append('keterangan_bahan', keterangan_bahan);
        form_data.append('type_bahan', type_bahan);
        form_data.append('diameter', diameter);
        form_data.append('tinggi', tinggi);
        form_data.append('lebar', lebar);
        form_data.append('panjang', panjang);
        form_data.append('warna', warna);
        form_data.append('group_kualitas', group_kualitas);
        form_data.append('kualitas', kualitas);
        form_data.append('jenis_kayu', jenis_kayu);
        form_data.append('keterangan_proses', keterangan_proses);
        form_data.append('status', status);
        // console.log(business_relation);
        createOverlay("process...");

        $.ajax({
            type : "POST",
            url: "{!! route('item.store') !!}",
            contentType : false,
            processData : false,
            data: form_data,
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{route('item.index')}}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }

    

    function updated(){
        var id = $('#id').val();
        var item = $('#item').val();
        var keterangan_bahan = $('#ket_bahan').val();
        var type_bahan = $('#type_bahan').val();
        var diameter = $('#diameter').val();
        var tinggi = $('#tinggi').val();
        var lebar = $('#lebar').val();
        var panjang = $('#panjang').val();
        var warna = $('#warna').val();
        var group_kualitas = $('#group_kualitas').val();
        var kualitas = $('#kualitas').val();
        var jenis_kayu = $('#jenis_kayu').val();
        var keterangan_proses = $('#keterangan_proses').val();
        var status = $('#status').val();
        createOverlay("Proses...");
        $.ajax({
            type : "PUT",
            url: "{!! route('item.update',["id"]) !!}",
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'id' : id,
                'item' :item,
                'keterangan_bahan' :keterangan_bahan,
                'type_bahan' :type_bahan,
                'diameter' :diameter,
                'tinggi' :tinggi,
                'lebar' :lebar,
                'panjang' :panjang,
                'warna' :warna,
                'group_kualitas' :group_kualitas,
                'kualitas' :kualitas,
                'jenis_kayu' :jenis_kayu,
                'keterangan_proses' :keterangan_proses,
                'status' :status,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                            window.location = "{{route('item.index')}}";
                    }, 500);              
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }

</script>
