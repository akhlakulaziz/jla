<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{-- <x-app-layout>
   <nav class="breadcrumb bg-white push">
      <a class="breadcrumb-item" href="{{route('dashboard')}}">Dashboard</a>
      <a class="breadcrumb-item" href="{{route('business.index')}}">Kelola Usaha</a>
      <span class="breadcrumb-item active">Kepemilikan</span>
   </nav>
   <a href="{{route('business.index')}}" title="Back"><button class="btn btn-dark btn-md mb-2">
      <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</button>
   </a>
      <div class="row">
         <div class="col-md-5 col-xl-3">
            <!-- END side menu -->
            @include('layouts.partials._side-menu-bussiness')
            <!-- END side menu -->

         </div>
         <div class="col-md-7 col-xl-9">
            <!-- Message List -->
            <div class="block">
                  <div class="block-header block-header-default bg-gray">
                     <div class="block-title">
                        <strong>Kepemilikan</strong> 
                     </div>
                  </div>
                  <div class="block-content">
                  @can ('owners-create')
                     <button type="button" class="btn btn-dark btn-sm" data-toggle="modal" onClick="create1()" data-target="#modal-owners"><i class="si si-plus"></i> Add New</button>
                  @endcan
                        <div class="table-responsive" style="min-height: ">
                              <table class="table table-striped table-vcenter">
                                 <thead>
                                    <tr>
                                       <th style="width: 10%;">#</th>
                                       <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                                       <th style="width: 30%;">Nama</th>
                                       <th style="width: 15%;">Persen</th>
                                       <th style="width: 15%;">Status</th>
                                          <th class="text-center" style="width: 100px;">Aksi</th>
                                    </tr> 
                                 </thead>
                                 <tbody>
                                    @foreach ($owners as $owner)
                                       @if (isset($owner->users))
                                          <tr>
                                             <td>{{ $loop->iteration }}</td>
                                             <td class="text-center">
                                                @if($owner->users != null)
                                                   <img class="img-avatar img-avatar48" src="{{$owner->users->profile_photo_url}}" alt="">
                                                @elseif ($owner->owner_business->image != null)
                                                   <img class="img-avatar img-avatar48" src="{{ asset('profil_usaha/'. $owner->owner_business->image) }}" alt="" srcset="">
                                                @else
                                                   <img class="img-avatar img-avatar48" src="{{ asset('profil_usaha/dummy-image.png') }}" alt="" >
                                                @endif
                                             </td>
                                             <td>
                                                @if($owner->users != null)
                                                   @if($owner->users->JRU == 2)
                                                      {{$owner->users->name}} (Non Personil)
                                                   @else
                                                      {{$owner->users->name}}
                                                   @endif
                                                @elseif($owner->business_relation != null)
                                                   {{$owner->owner_business->name}} (Bisnis)
                                                @endif
                                             </td>
                                             <td>{{$owner->percent}}</td>
                                             <td>@if($owner->status==2)
                                                Tidak Aktif
                                                @elseif($owner->status==1)
                                                   Aktif
                                                @endif
                                             </td>
                                             <td>
                                                @canany(['owners-edit', 'owners-delete',])
                                                @can ('owners-edit')
                                                   <button type="button" onClick="edit1({{ json_encode($owner) }})"  title="Edit" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-owners">
                                                      <i class="fa fa-pencil" aria-hidden="true"></i> 
                                                   </button>
                                                @endcan
                                                @can ('owners-delete')
                                                   <button type="button" onClick="remove({{ $owner->id}})" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-info-confirmed" title="Delete Pemilik">
                                                      <i class="fa fa-trash" aria-hidden="true"></i>
                                                   </button>
                                                @endcan
                                                @endcanany
                                             </td>
                                          </tr>
                                       @endif
                                    @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      @include('pages.business.owners.modal')
      @include('pages.business.owners.delete')
      @include('pages.business.owners.user')
</x-app-layout> --}}


<x-app-layout>
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header border-bottom">
            <div class="row flex-between-end">
               <div class="col-auto align-self-center">
                  <h5 class="mb-0" data-anchor="data-anchor">Item</h5>
               </div>
               
               <div class="col-auto ms-auto">
                  <div class="nav nav-pills nav-pills-falcon flex-grow-1 mt-2" role="tablist">
                     <form action="{{ route('item.index') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Pencarian" aria-label="Search">
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-auto align-self-center">
               {{-- @can ('master-bidang-usaha-create') --}}
                  {{-- <button type="button" data-toggle="modal" onClick="create()" data-target="#modal-users"><i class="si si-plus"></i> Tambah Pengguna</button> --}}

                  {{-- <a href="#" class="btn btn-success btn-sm" title="Add New MasterItem" data-toggle="modal" onClick="create()" data-target="#error-modal"> --}}
                     <a href="#" class="btn btn-success btn-sm" title="Add New MasterItem" data-bs-toggle="modal" onClick="create()" data-bs-target="#item-modal">
                     <span class="fas fa-plus" data-fa-transform="shrink-3 down-2"></span>
                     <span class="ms-1">New</span>
                  </a>
               {{-- @endcan --}}
               
            </div>
         </div>
         <div class="card-body pt-0">
            <div class="tab-content">
               <div class="tab-pane preview-tab-pane active" role="tabpanel" aria-labelledby="tab-dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502" id="dom-6fa4c848-cf7f-4ed7-bab0-9326a3ce9502">
                  <div class="table-responsive scrollbar">
                     <table class="table">
                        <thead>
                           <tr>
                              <th style="width: 10%;">No</th>
                              <th scope="col">No Part Item</th>
                              <th scope="col">Item</th>
                              <th scope="col">Ket Bahan</th>
                              <th scope="col">Type Bahan</th>
                              <th scope="col">Diameter</th>
                              <th scope="col">Tinggi</th>
                              <th scope="col">Lebar</th>
                              <th scope="col">Panjang</th>
                              <th scope="col">Warna</th>
                              <th scope="col">Group Kualitas</th>
                              <th scope="col">Kualitas</th>
                              <th scope="col">Jenis Kayu</th>
                              <th scope="col">Ket Proses</th>
                              <th scope="col">Status</th>
                              <th class="text-end" scope="col">Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($items as $item)
                              <tr>
                                 <td>{{ $loop->iteration }}</td>
                                 <td>{{ $item->no_part_item }}</td>
                                 <td>{{ $item->item->name }}</td>
                                 <td>{{ $item->ket_bahan->name }}</td>
                                 <td>{{ $item->type_bahan->name  }}</td>
                                 <td>{{ $item->diameter }}</td>
                                 <td>{{ $item->tinggi }}</td>
                                 <td>{{ $item->lebar }}</td>
                                 <td>{{ $item->panjang }}</td>
                                 <td>{{ $item->warna->name  }}</td>
                                 <td>{{ $item->group_kualitas->name }}</td>
                                 <td>{{ $item->kualitas->name }}</td>
                                 <td>{{ $item->jenis_kayu->name }}</td>
                                 <td>{{ $item->ket_proses->name }}</td>
                                 <td>
                                    @if ($item->status == 1)
                                       Active
                                    @else
                                       Inactive
                                    @endif
                                 </td>
                                 <td class="text-end">
                                    <div>
                                       <button class="btn p-0" type="button" onClick="edit({{ json_encode($item) }})" title="Edit" data-bs-toggle="modal" data-bs-target="#item-modal"><span class="text-500 fas fa-edit"></span></button>
                                       <button class="btn p-0 ms-2" type="button" onClick="remove({{ json_encode($item->id) }})" title="Delete" data-bs-toggle="modal" data-bs-target="#modal-info-confirmed"><span class="text-500 fas fa-trash-alt"></span></button>
                                    </div>
                                 </td>
                              </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @include('pages.harian-proses.items.modal')
   @include('pages.harian-proses.items.delete')
</x-app-layout>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
