<x-jet-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{ __('Informasi Profile') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Perbarui informasi profil dan alamat email akun Anda.') }}
    </x-slot>

    <x-slot name="form">

        <x-jet-action-message on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>

        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div class="mb-2" x-data="{photoName: null, photoPreview: null}">
                <!-- Profile Photo File Input -->
                <input type="file" hidden
                       wire:model="photo"
                       x-ref="photo"
                       class="{{ $errors->has('name') ? 'is-invalid' : '' }}"
                       x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('Foto') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" class="rounded-circle" height="80px" width="80px">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <img x-bind:src="photoPreview" class="rounded-circle" width="80px" height="80px">
                </div>

                <x-jet-secondary-button class="mt-2 me-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Pilih Foto Baru') }}
				</x-jet-secondary-button>
				
				@if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        <div wire:loading wire:target="deleteProfilePhoto" class="spinner-border spinner-border-sm" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>

                        {{ __('Hapus Foto') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
            {{-- <small class="font-14 text-muted">Catatan : Ukuran Maksimal 5 Mb dengan Dimensi Gambar Maksimal 1000x1000 px.</small> --}}
        @endif

        <div class="w-md-75">
            <!-- Name -->
            <div class="mb-3 mt-3">
                <x-jet-label for="name" value="{{ __('Nama') }}" />
                <x-jet-input id="name" type="text" class="{{ $errors->has('name') ? 'is-invalid' : '' }}" wire:model.defer="state.name" autocomplete="name" />
                <x-jet-input-error for="name" />
            </div>

            <!-- Email -->
            <div class="mb-3">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" type="email" class="{{ $errors->has('email') ? 'is-invalid' : '' }}" wire:model.defer="state.email" readonly/>
                <x-jet-input-error for="email" />
            </div>
            <!-- NIK -->
            {{-- <div class="mb-3">
                <x-jet-label for="phone" value="{{ __('NIK') }}" />
                <x-jet-input id="nik" type="number" class="{{ $errors->has('nik') ? 'is-invalid' : '' }}" wire:model.defer="state.nik" placeholder="contoh: 3374xxxxxxx"/>
                <x-jet-input-error for="nik" />
            </div> --}}
            <!-- Phone -->
            <div class="mb-3">
                <x-jet-label for="phone" value="{{ __('Nomor HP') }}" />
                <x-jet-input id="phone" type="number" class="{{ $errors->has('phone') ? 'is-invalid' : '' }}" wire:model.defer="state.phone" placeholder="contoh: 08xxxxxx"/>
                <x-jet-input-error for="phone" />
            </div>
            <!-- Address -->
            <div class="mb-3">
                <x-jet-label for="address" value="{{ __('Alamat') }}" />
                <x-jet-input id="address" type="tedt" class="{{ $errors->has('address') ? 'is-invalid' : '' }}" wire:model.defer="state.address" />
                <x-jet-input-error for="address" />
            </div>
            <!-- Place of Birth -->
            <div class="mb-3">
                <x-jet-label for="gender" value="{{ __('Jenis Kelamin') }}" />
                    <select id="gender"  class="form-control {{ $errors->has('gender') ? 'is-invalid' : '' }}"  wire:model.defer="state.gender" >
                        <option value=""> Pilih Jenis Kelamin</option>
                        <option value="1" {{ $this->user->gender== '1' ? 'selected' : '' }} >
                            Laki - Laki
                        </option>
                        <option value="2" {{ $this->user->gender== '2' ? 'selected' : '' }}>
                            Perempuan
                        </option>
                    </select>

                <x-jet-input-error for="gender" />    
            </div>
            <!-- Place of Birth -->
            <div class="mb-3">
                <x-jet-label for="place_of_birth" value="{{ __('Tempat Lahir') }}" />
                <x-jet-input id="place_of_birth" type="text" class="{{ $errors->has('place_of_birth') ? 'is-invalid' : '' }}" wire:model.defer="state.place_of_birth" />
                <x-jet-input-error for="place_of_birth" />
            </div>
            <!-- Date of Birth -->
            <div class="mb-3">
                <x-jet-label for="date_of_birth" value="{{ __('Tanggal Lahir') }}" />
                <x-jet-input id="date_of_birth" type="date" class="{{ $errors->has('date_of_birth') ? 'is-invalid' : '' }}" wire:model.defer="state.date_of_birth" />
                <x-jet-input-error for="date_of_birth" />
            </div>
        </div>
    </x-slot>

    <x-slot name="actions">
		<div class="d-flex align-items-baseline">
			<x-jet-button>
                <div wire:loading class="spinner-border spinner-border-sm" role="status" wire:target="checkout">
                    <span class="visually-hidden">Loading...</span>
                </div>

				{{ __('Simpan') }}
			</x-jet-button>
		</div>
    </x-slot>
</x-jet-form-section>