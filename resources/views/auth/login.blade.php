{{-- =============================================Start New====================================================== --}}
<x-guest-layout>
    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main" id="top">

            @if (session('status'))
                <div class="alert alert-success mb-3 rounded-0" role="alert">
                    {{ session('status') }}
                </div>
            @endif
        <div class="container" data-layout="container">
            <script>
                var isFluid = JSON.parse(localStorage.getItem('isFluid'));
                if (isFluid) {
                    var container = document.querySelector('[data-layout]');
                    container.classList.remove('container');
                    container.classList.add('container-fluid');
                }
            </script>
            <div class="row flex-center min-vh-100 py-6">
                <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 col-xxl-4">
                    <a class="d-flex flex-center mb-4" href="#"><span class="font-sans-serif fw-bolder fs-5 d-inline-block">{{ config('app.name', 'Laravel') }}</span></a>
                    {{-- <x-jet-authentication-card-logo /> --}}
                    <div class="card">
                        <div class="card-body p-4 p-sm-5">
                            <div class="row flex-between-center mb-2">
                                <div class="col-auto">
                                    <h5>Log in</h5>
                                </div>
                            </div>

                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="mb-3">
                                    <x-jet-input class="{{ $errors->has('email') ? 'is-invalid' : '' }}" type="email"
                                        name="email" :value="old('email')" placeholder="Email address" required />
                                    <x-jet-input-error for="email"></x-jet-input-error>
                                </div>
                                <div class="mb-3">
                                    <x-jet-input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password"
                                        name="password" required autocomplete="current-password" placeholder="Password"/>
                                    <x-jet-input-error for="password"></x-jet-input-error>
                                </div>
                                <div class="row flex-between-center">
                                    <div class="col-auto">
                                        <div class="form-check mb-0">
                                            <x-jet-checkbox id="remember_me" name="remember" />
                                            <label class="form-check-label mb-0" for="basic-checkbox">Remember me</label>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        @if (Route::has('password.request'))
                                            <a class="fs--1" href="{{ route('password.request') }}">
                                                {{ __('Forgot your password?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Log in</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->

    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src={{asset('assets2/vendors/popper/popper.min.js')}}></script>
    <script src={{asset('assets2/vendors/bootstrap/bootstrap.min.js')}}></script>
    <script src={{asset('assets2/vendors/anchorjs/anchor.min.js')}}></script>
    <script src={{asset('assets2/vendors/is/is.min.js')}}></script>
    <script src={{asset('assets2/vendors/fontawesome/all.min.js')}}></script>
    <script src={{asset('assets2/vendors/lodash/lodash.min.js')}}></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
    <script src={{asset('assets2/vendors/list.js/list.min.js')}}></script>
    <script src={{asset('assets2/js/theme.js')}}></script>
</x-guest-layout>
{{-- =============================================End New====================================================== --}}
